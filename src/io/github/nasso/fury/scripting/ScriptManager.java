package io.github.nasso.fury.scripting;

import io.github.nasso.fury.level.Level;

import java.io.File;
import java.io.FileNotFoundException;

public abstract class ScriptManager {
	private Level lvl = null;
	
	public ScriptManager(Level lvl) {
		this.lvl = lvl;
	}
	
	public Level getLevel() {
		return this.lvl;
	}
	
	public abstract boolean supportsLanguage(ScriptingLanguage lang);
	
	public abstract Object runScript(String src);
	
	public abstract Object runScript(File src) throws FileNotFoundException;
}
