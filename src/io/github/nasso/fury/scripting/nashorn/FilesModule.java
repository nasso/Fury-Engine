package io.github.nasso.fury.scripting.nashorn;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FilesModule {
	public static boolean exists(String path) {
		return Files.exists(Paths.get(path));
	}
}
