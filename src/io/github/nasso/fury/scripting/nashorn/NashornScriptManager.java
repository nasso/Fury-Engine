package io.github.nasso.fury.scripting.nashorn;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.Game;
import io.github.nasso.fury.level.Level;
import io.github.nasso.fury.scripting.ScriptManager;
import io.github.nasso.fury.scripting.ScriptingLanguage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class NashornScriptManager extends ScriptManager {
	private NashornScriptEngine engine = (NashornScriptEngine) new ScriptEngineManager().getEngineByName("nashorn");
	
	public NashornScriptManager(Level lvl) {
		super(lvl);
		
		StringBuilder furyConstsBldr = new StringBuilder();
		furyConstsBldr.append("var Fury = {");
		
		try {
			Class<Fury> furyClass = Fury.class;
			Field[] fields = furyClass.getFields();
			for(int i = 0, l = fields.length; i < l; i++) {
				Field f = fields[i];
				int mods = f.getModifiers();
				if(Modifier.isStatic(mods) && Modifier.isPublic(mods)) {
					furyConstsBldr.append(f.getName() + ":");
					
					if(f.getType().equals(String.class)) furyConstsBldr.append("'" + f.get(null) + "'");
					else furyConstsBldr.append(f.get(null));
					
					furyConstsBldr.append(",");
				}
			}
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		}
		
		furyConstsBldr.append("}");
		
		this.runScript(furyConstsBldr.toString());
		this.runScript(FuryUtils.readFile("res/scripts/nashorn/index.min.js", true));
		
		ScriptObjectMirror gameConstructor = (ScriptObjectMirror) this.engine.get("Game");
		this.engine.put("game", gameConstructor.newObject(Game.getLaunchedGame()));
		
		ScriptObjectMirror lvlConstructor = (ScriptObjectMirror) this.engine.get("Level");
		this.engine.put("level", lvlConstructor.newObject(lvl));
	}
	
	public Object runScript(String src) {
		try {
			return this.engine.compile(src).eval();
		} catch(ScriptException e) {
			System.err.println(e.getMessage() + "\n\tin " + e.getFileName() + "@" + e.getLineNumber() + ":" + e.getColumnNumber());
		}
		
		return null;
	}
	
	public Object runScript(File src) throws FileNotFoundException {
		try {
			return this.engine.compile(new FileReader(src)).eval();
		} catch(ScriptException e) {
			System.err.println(e.getMessage() + "\n\tin " + e.getFileName() + "@" + e.getLineNumber() + ":" + e.getColumnNumber());
		}
		
		return null;
	}
	
	public boolean supportsLanguage(ScriptingLanguage lang) {
		return lang == ScriptingLanguage.JAVASCRIPT;
	}
}
