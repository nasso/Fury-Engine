package io.github.nasso.fury.nanovg;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.nanovg.NanoVGGL3.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.joml.Matrix3f;
import org.joml.Vector2f;
import org.lwjgl.nanovg.NVGColor;
import org.lwjgl.nanovg.NVGGlyphPosition;
import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.nanovg.NVGTextRow;
import org.lwjgl.nanovg.NanoVGGL3;

public class NVGContext {
	private static final boolean DEBUG = true;
	private static NVGContext singlectx;
	
	private long ctx = 0;
	
	// Arrays used in some methods
	private int[] itemp_a = new int[1];
	private int[] itemp_b = new int[1];
	private float[] ftemp_a = new float[1];
	private float[] ftemp_b = new float[1];
	private float[] ftemp_c = new float[1];
	
	private NVGContext(int flags) {
		this.ctx = NanoVGGL3.nvgCreate(flags);
	}
	
	// Begin drawing a new frame
	// Calls to nanovg drawing API should be wrapped in nvgBeginFrame() &
	// nvgEndFrame()
	// nvgBeginFrame() defines the size of the window to render to in relation
	// currently
	// set viewport (i.e. glViewport on GL backends). Device pixel ration allows
	// to
	// control the rendering on Hi-DPI devices.
	// For example, GLFW returns two dimension for an opened window: window size
	// and
	// frame buffer size. In that case you would set windowWidth/Height to the
	// window size
	// devicePixelRatio to: frameBufferWidth / windowWidth.
	
	public void beginFrame(int windowWidth, int windowHeight, float devicePixelRatio) {
		nvgBeginFrame(this.ctx, windowWidth, windowHeight, devicePixelRatio);
	}
	
	public void cancelFrame() {
		nvgCancelFrame(this.ctx);
	}
	
	public void endFrame() {
		nvgEndFrame(this.ctx);
	}
	
	//
	// State Handling
	//
	// NanoVG contains state which represents how paths will be rendered.
	// The state contains transform, fill and stroke styles, text and font
	// styles,
	// and scissor clipping.
	
	public void save() {
		nvgSave(this.ctx);
	}
	
	public void restore() {
		nvgRestore(this.ctx);
	}
	
	public void reset() {
		nvgReset(this.ctx);
	}
	
	//
	// Render styles
	//
	// Fill and stroke render style can be either a solid color or a paint which
	// is a gradient or a pattern.
	// Solid color is simply defined as a color value, different kinds of paints
	// can be created
	// using nvgLinearGradient(), nvgBoxGradient(), nvgRadialGradient() and
	// nvgImagePattern().
	//
	// Current render style can be saved and restored using nvgSave() and
	// nvgRestore().
	
	public void strokeColor(NVGColor color) {
		nvgStrokeColor(this.ctx, color);
	}
	
	public void strokePaint(NVGPaint paint) {
		nvgStrokePaint(this.ctx, paint);
	}
	
	public void fillColor(NVGColor color) {
		nvgFillColor(this.ctx, color);
	}
	
	public void fillPaint(NVGPaint paint) {
		nvgFillPaint(this.ctx, paint);
	}
	
	public void miterLimit(float limit) {
		nvgMiterLimit(this.ctx, limit);
	}
	
	public void strokeWidth(float size) {
		nvgStrokeWidth(this.ctx, size);
	}
	
	public void lineCap(int cap) {
		nvgLineCap(this.ctx, cap);
	}
	
	public void lineJoin(int join) {
		nvgLineJoin(this.ctx, join);
	}
	
	public void globalAlpha(float alpha) {
		nvgGlobalAlpha(this.ctx, alpha);
	}
	
	//
	// Transforms
	//
	// The paths, gradients, patterns and scissor region are transformed by an
	// transformation
	// matrix at the time when they are passed to the API.
	// The current transformation matrix is a affine matrix:
	// [sx kx tx]
	// [ky sy ty]
	// [ 0 0 1]
	// Where: sx,sy define scaling, kx,ky skewing, and tx,ty translation.
	// The last row is assumed to be 0,0,1 and is not stored.
	//
	// Apart from nvgResetTransform(), each transformation function first
	// creates
	// specific transformation matrix and pre-multiplies the current
	// transformation by it.
	//
	// Current coordinate system (transformation) can be saved and restored
	// using nvgSave() and nvgRestore().
	
	public void resetTransform() {
		nvgResetTransform(this.ctx);
	}
	
	public void transform(float a, float b, float c, float d, float e, float f) {
		nvgTransform(this.ctx, a, b, c, d, e, f);
	}
	
	public void translate(float x, float y) {
		nvgTranslate(this.ctx, x, y);
	}
	
	public void rotate(float angle) {
		nvgRotate(this.ctx, angle);
	}
	
	public void skewX(float angle) {
		nvgSkewX(this.ctx, angle);
	}
	
	public void skewY(float angle) {
		nvgSkewY(this.ctx, angle);
	}
	
	public void scale(float x, float y) {
		nvgScale(this.ctx, x, y);
	}
	
	public void currentTransform(float[] xform) {
		nvgCurrentTransform(this.ctx, xform);
	}
	
	public void currentTransform(FloatBuffer xform) {
		nvgCurrentTransform(this.ctx, xform);
	}
	
	public void currentTransform(Matrix3f xform) {
		xform.identity();
		float[] data = new float[9];
		nvgCurrentTransform(this.ctx, data);
		xform.set(data);
	}
	
	//
	// Images
	//
	// NanoVG allows you to load jpg, png, psd, tga, pic and gif files to be
	// used for rendering.
	// In addition you can upload your own image. The image loading is provided
	// by stb_image.
	// The parameter imageFlags is combination of flags defined in
	// NVGimageFlags.
	
	public int createImage(String filename, int imageFlags) {
		return nvgCreateImage(this.ctx, filename, imageFlags);
	}
	
	public int createImageMem(int imageFlags, ByteBuffer data) {
		return nvgCreateImageMem(this.ctx, imageFlags, data);
	}
	
	public int createImageRGBA(int w, int h, int imageFlags, ByteBuffer data) {
		return nvgCreateImageRGBA(this.ctx, w, h, imageFlags, data);
	}
	
	public void updateImage(int image, ByteBuffer data) {
		nvgUpdateImage(this.ctx, image, data);
	}
	
	public Vector2f imageSize(int image) {
		return this.imageSize(image, null);
	}
	
	public Vector2f imageSize(int image, Vector2f target) {
		if(target == null) target = new Vector2f();
		
		nvgImageSize(this.ctx, image, this.itemp_a, this.itemp_b);
		
		target.x = this.itemp_a[0];
		target.y = this.itemp_b[0];
		
		return target;
	}
	
	public void deleteImage(int image) {
		nvgDeleteImage(this.ctx, image);
	}
	
	//
	// Scissoring
	//
	// Scissoring allows you to clip the rendering into a rectangle. This is
	// useful for various
	// user interface cases like rendering a text edit or a timeline.
	
	public void scissor(float x, float y, float w, float h) {
		nvgScissor(this.ctx, x, y, w, h);
	}
	
	public void intersectScissor(float x, float y, float w, float h) {
		nvgIntersectScissor(this.ctx, x, y, w, h);
	}
	
	public void resetScissor() {
		nvgResetScissor(this.ctx);
	}
	
	//
	// Paths
	//
	// Drawing a new shape starts with nvgBeginPath(), it clears all the
	// currently defined paths.
	// Then you define one or more paths and sub-paths which describe the shape.
	// The are functions
	// to draw common shapes like rectangles and circles, and lower level
	// step-by-step functions,
	// which allow to define a path curve by curve.
	//
	// NanoVG uses even-odd fill rule to draw the shapes. Solid shapes should
	// have counter clockwise
	// winding and holes should have counter clockwise order. To specify winding
	// of a path you can
	// call nvgPathWinding(). This is useful especially for the common shapes,
	// which are drawn CCW.
	//
	// Finally you can fill the path using current fill style by calling
	// nvgFill(), and stroke it
	// with current stroke style by calling nvgStroke().
	//
	// The curve segments and sub-paths are transformed by the current
	// transform.
	
	public void beginPath() {
		nvgBeginPath(this.ctx);
	}
	
	public void moveTo(float x, float y) {
		nvgMoveTo(this.ctx, x, y);
	}
	
	public void lineTo(float x, float y) {
		nvgLineTo(this.ctx, x, y);
	}
	
	public void bezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y) {
		nvgBezierTo(this.ctx, c1x, c1y, c2x, c2y, x, y);
	}
	
	public void quadTo(float cx, float cy, float x, float y) {
		nvgQuadTo(this.ctx, cx, cy, x, y);
	}
	
	public void arcTo(float x1, float y1, float x2, float y2, float radius) {
		nvgArcTo(this.ctx, x1, y1, x2, y2, radius);
	}
	
	public void closePath() {
		nvgClosePath(this.ctx);
	}
	
	public void pathWinding(int dir) {
		nvgPathWinding(this.ctx, dir);
	}
	
	public void arc(float cx, float cy, float r, float a0, float a1, int dir) {
		nvgArc(this.ctx, cx, cy, r, a0, a1, dir);
	}
	
	public void rect(float x, float y, float w, float h) {
		nvgRect(this.ctx, x, y, w, h);
	}
	
	public void roundedRect(float x, float y, float w, float h, float r) {
		nvgRoundedRect(this.ctx, x, y, w, h, r);
	}
	
	public void ellipse(float cx, float cy, float rx, float ry) {
		nvgEllipse(this.ctx, cx, cy, rx, ry);
	}
	
	public void circle(float cx, float cy, float r) {
		nvgCircle(this.ctx, cx, cy, r);
	}
	
	public void fill() {
		nvgFill(this.ctx);
	}
	
	public void stroke() {
		nvgStroke(this.ctx);
	}
	
	//
	// Paints
	//
	// NanoVG supports four types of paints: linear gradient, box gradient,
	// radial gradient and image pattern.
	// These can be used as paints for strokes and fills.
	
	public NVGPaint linearGradient(float sx, float sy, float ex, float ey, NVGColor icol, NVGColor ocol) {
		return nvgLinearGradient(this.ctx, sx, sy, ex, ey, icol, ocol, NVGPaint.create());
	}
	
	public NVGPaint linearGradient(float sx, float sy, float ex, float ey, NVGColor icol, NVGColor ocol, NVGPaint target) {
		return nvgLinearGradient(this.ctx, sx, sy, ex, ey, icol, ocol, target);
	}
	
	public NVGPaint boxGradient(float x, float y, float w, float h, float r, float f, NVGColor icol, NVGColor ocol) {
		return nvgBoxGradient(this.ctx, x, y, w, h, r, f, icol, ocol, NVGPaint.create());
	}
	
	public NVGPaint boxGradient(float x, float y, float w, float h, float r, float f, NVGColor icol, NVGColor ocol, NVGPaint target) {
		return nvgBoxGradient(this.ctx, x, y, w, h, r, f, icol, ocol, target);
	}
	
	public NVGPaint radialGradient(float cx, float cy, float inr, float outr, NVGColor icol, NVGColor ocol) {
		return nvgRadialGradient(this.ctx, cx, cy, inr, outr, icol, ocol, NVGPaint.create());
	}
	
	public NVGPaint radialGradient(float cx, float cy, float inr, float outr, NVGColor icol, NVGColor ocol, NVGPaint target) {
		return nvgRadialGradient(this.ctx, cx, cy, inr, outr, icol, ocol, target);
	}
	
	public NVGPaint imagePattern(float ox, float oy, float ex, float ey, float angle, int image, float alpha) {
		return nvgImagePattern(this.ctx, ox, oy, ex, ey, angle, image, alpha, NVGPaint.create());
	}
	
	public NVGPaint imagePattern(float ox, float oy, float ex, float ey, float angle, int image, float alpha, NVGPaint target) {
		return nvgImagePattern(this.ctx, ox, oy, ex, ey, angle, image, alpha, target);
	}
	
	//
	// Text
	//
	// NanoVG allows you to load .ttf files and use the font to render text.
	//
	// The appearance of the text can be defined by setting the current text
	// style
	// and by specifying the fill color. Common text and font settings such as
	// font size, letter spacing and text align are supported. Font blur allows
	// you
	// to create simple text effects such as drop shadows.
	//
	// At render time the font face can be set based on the font handles or
	// name.
	//
	// Font measure functions return values in local space, the calculations are
	// carried in the same resolution as the final rendering. This is done
	// because
	// the text glyph positions are snapped to the nearest pixels sharp
	// rendering.
	//
	// The local space means that values are not rotated or scale as per the
	// current
	// transformation. For example if you set font size to 12, which would mean
	// that
	// line height is 16, then regardless of the current scaling and rotation,
	// the
	// returned line height is always 16. Some measures may vary because of the
	// scaling
	// since aforementioned pixel snapping.
	//
	// While this may sound a little odd, the setup allows you to always render
	// the
	// same way regardless of scaling. I.e. following works regardless of
	// scaling:
	//
	// const char* txt = "Text me up.";
	// nvgTextBounds(vg, x,y, txt, NULL, bounds);
	// nvgBeginPath(vg);
	// nvgRoundedRect(vg, bounds[0],bounds[1], bounds[2]-bounds[0],
	// bounds[3]-bounds[1]);
	// nvgFill(vg);
	//
	// Note: currently only solid color fill is supported for text.
	
	public int createFont(String name, String filename) {
		return nvgCreateFont(this.ctx, name, filename);
	}
	
	public int createFontMem(String name, ByteBuffer data, int freeData) {
		return nvgCreateFontMem(this.ctx, name, data, freeData);
	}
	
	public int findFont(String name) {
		return nvgFindFont(this.ctx, name);
	}
	
	public void fontSize(float size) {
		nvgFontSize(this.ctx, size);
	}
	
	public void fontBlur(float blur) {
		nvgFontBlur(this.ctx, blur);
	}
	
	public void textLetterSpacing(float spacing) {
		nvgTextLetterSpacing(this.ctx, spacing);
	}
	
	public void textLineHeight(float lineHeight) {
		nvgTextLineHeight(this.ctx, lineHeight);
	}
	
	public void textAlign(int align) {
		nvgTextAlign(this.ctx, align);
	}
	
	public void fontFaceId(int font) {
		nvgFontFaceId(this.ctx, font);
	}
	
	public void fontFace(String font) {
		nvgFontFace(this.ctx, font);
	}
	
	public float text(float x, float y, String string, long end) {
		return nvgText(this.ctx, x, y, string, end);
	}
	
	public float text(float x, float y, String string) {
		return nvgText(this.ctx, x, y, string, NULL);
	}
	
	public void textBox(float x, float y, float breakRowWidth, String string, long end) {
		nvgTextBox(this.ctx, x, y, breakRowWidth, string, end);
	}
	
	public void textBox(float x, float y, float breakRowWidth, String string) {
		nvgTextBox(this.ctx, x, y, breakRowWidth, string, NULL);
	}
	
	public float textBounds(float x, float y, String string, long end, float[] bounds) {
		return nvgTextBounds(this.ctx, x, y, string, end, bounds);
	}
	
	public float textBounds(float x, float y, String string, float[] bounds) {
		return nvgTextBounds(this.ctx, x, y, string, NULL, bounds);
	}
	
	public void textBoxBounds(float x, float y, float breakRowWidth, String string, long end, float[] bounds) {
		nvgTextBoxBounds(this.ctx, x, y, breakRowWidth, string, end, bounds);
	}
	
	public void textBoxBounds(float x, float y, float breakRowWidth, String string, float[] bounds) {
		nvgTextBoxBounds(this.ctx, x, y, breakRowWidth, string, NULL, bounds);
	}
	
	public float[] textBoxBounds(float x, float y, float breakRowWidth, String string, long end) {
		float[] bounds = new float[4];
		nvgTextBoxBounds(this.ctx, x, y, breakRowWidth, string, end, bounds);
		return bounds;
	}
	
	public float[] textBoxBounds(float x, float y, float breakRowWidth, String string) {
		return this.textBoxBounds(x, y, breakRowWidth, string, NULL);
	}
	
	public int textGlyphPositions(float x, float y, String string, long end, NVGGlyphPosition.Buffer positions) {
		return nvgTextGlyphPositions(this.ctx, x, y, string, end, positions);
	}
	
	public int textGlyphPosition(float x, float y, String string, NVGGlyphPosition.Buffer positions) {
		return nvgTextGlyphPositions(this.ctx, x, y, string, NULL, positions);
	}
	
	public void textMetrics(float[] ascender, float[] descender, float[] lineh) {
		nvgTextMetrics(this.ctx, ascender, descender, lineh);
	}
	
	public NVGTextMetrics textMetrics() {
		nvgTextMetrics(this.ctx, this.ftemp_a, this.ftemp_b, this.ftemp_c);
		return new NVGTextMetrics(this.ftemp_a[0], this.ftemp_b[0], this.ftemp_c[0]);
	}
	
	public int textBreakLines(String string, long end, float breakRowWidth, NVGTextRow.Buffer rows, int maxRows) {
		return nvgTextBreakLines(this.ctx, string, end, breakRowWidth, rows);
	}
	
	public int textBreakLines(String string, float breakRowWidth, NVGTextRow.Buffer rows, int maxRows) {
		return nvgTextBreakLines(this.ctx, string, NULL, breakRowWidth, rows);
	}
	
	// Global customs
	public void fillRect(float x, float y, float w, float h) {
		this.save();
		this.beginPath();
		this.rect(x, y, w, h);
		this.fill();
		this.restore();
	}
	
	public void strokeRect(float x, float y, float w, float h) {
		this.save();
		this.beginPath();
		this.rect(x, y, w, h);
		this.stroke();
		this.restore();
	}
	
	// Dispose
	public void dispose() {
		NanoVGGL3.nvgDelete(this.ctx);
	}
	
	/**
	 * Gets the singleton instance
	 * 
	 * @return The singleton context
	 */
	public static NVGContext get() {
		if(singlectx == null) create(true);
		
		return singlectx;
	}
	
	/**
	 * Creates the NanoVG context singleton
	 * 
	 * @param antialias
	 *            If antialiasing should be used
	 * @return The created context
	 */
	public static NVGContext create(boolean antialias) {
		int flags = 0;
		
		if(DEBUG) flags |= NVG_DEBUG;
		
		if(antialias) flags |= NVG_ANTIALIAS;
		
		return singlectx = new NVGContext(flags);
	}
	
	/**
	 * Deletes the singleton context.
	 */
	public static void delete() {
		if(singlectx != null) {
			singlectx.dispose();
			singlectx = null;
		}
	}
	
	//
	// Color utils
	//
	// Colors in NanoVG are stored as unsigned ints in ABGR format.
	
	public static NVGColor rgb(byte r, byte g, byte b) {
		return nvgRGB(r, g, b, NVGColor.create());
	}
	
	public static NVGColor rgbf(float r, float g, float b) {
		return nvgRGBf(r, g, b, NVGColor.create());
	}
	
	public static NVGColor rgba(byte r, byte g, byte b, byte a) {
		return nvgRGBA(r, g, b, a, NVGColor.create());
	}
	
	public static NVGColor rgbaf(float r, float g, float b, float a) {
		return nvgRGBAf(r, g, b, a, NVGColor.create());
	}
	
	public static NVGColor lerpRGBA(NVGColor c0, NVGColor c1, float u) {
		return nvgLerpRGBA(c0, c1, u, NVGColor.create());
	}
	
	public static NVGColor transRGBA(NVGColor c0, byte a) {
		return nvgTransRGBA(c0, a, NVGColor.create());
	}
	
	public static NVGColor transRGBAf(NVGColor c0, float a) {
		return nvgTransRGBAf(c0, a, NVGColor.create());
	}
	
	public static NVGColor hsl(float h, float s, float l) {
		return nvgHSL(h, s, l, NVGColor.create());
	}
	
	public static NVGColor hsla(float h, float s, float l, byte a) {
		return nvgHSLA(h, s, l, a, NVGColor.create());
	}
}
