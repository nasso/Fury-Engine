package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLDeferredRenderingProgram extends OGLProgram {
	public static final int MAX_CASCADED_SHADOWS = 6;
	
	public OGLDeferredRenderingProgram() {
		super("Deferred rendering", FuryUtils.readFile("res/shaders/fullQuad.vs", true), FuryUtils.readFile("res/shaders/deferredRendering.fs", true));
		
		this.setPositionAttrib("position_quad");
		this.link();
		
		this.loadUniforms("viewInv", "projInv", "gbuffer_diffuse", "gbuffer_position", "gbuffer_normal", "gbuffer_depth", "light.shadowMap.cascadeDistanceFade", "light.shadowMap.interCascadeFade", "light.shadowMap.cascadeMaxDistance", "light.shadowMap.cascadeDistribExp", "light.shadowMap.cascadeCount", "light.color", "light.groundColor", "light.position", "light.direction", "light.type", "light.energy", "light.dist", "light.coneSize", "light.coneBlend", "light.blinnPhong", "light.specular", "light.diffuse", "light.shadows", "lightingEnabled");
		
		// Load all cascades uniforms
		for(int i = 0, l = MAX_CASCADED_SHADOWS; i < l; i++) {
			this.loadUniforms("depthMap[" + i + "]", "light.shadowMap.cascades[" + i + "].shadowMat", "light.shadowMap.cascades[" + i + "].poissonSpread", "light.shadowMap.cascades[" + i + "].bias", "light.shadowMap.cascades[" + i + "].near", "light.shadowMap.cascades[" + i + "].far", "light.shadowMap.cascades[" + i + "].poissonSamples", "light.shadowMap.cascades[" + i + "].bufferType", "light.shadowMap.cascades[" + i + "].normalCull");
		}
	}
}
