package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLPostFXProgram extends OGLProgram {
	public OGLPostFXProgram(String name, String fragmentSource, boolean color, boolean depth) {
		super(name, FuryUtils.readFile("res/shaders/fullQuad.vs", true), fragmentSource);
		
		this.setPositionAttrib("position_quad");
		this.link();
		
		if(color) this.loadUniform("color");
		
		if(depth) this.loadUniform("depth");
	}
}
