package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL15.*;

import java.nio.IntBuffer;

public class OGLElementArrayBuffer {
	private static int bufferCount = 0;
	
	private int id = 0;
	
	private int version = -1;
	
	public OGLElementArrayBuffer() {
		this.id = glGenBuffers();
		bufferCount++;
	}
	
	public OGLElementArrayBuffer loadData(IntBuffer data) {
		return this.loadData(data, GL_STATIC_DRAW);
	}
	
	public OGLElementArrayBuffer loadData(IntBuffer data, int usage) {
		this.bind();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, data, usage);
		
		return this;
	}
	
	public OGLElementArrayBuffer loadData(int[] data) {
		return this.loadData(data, GL_STATIC_DRAW);
	}
	
	public OGLElementArrayBuffer loadData(int[] data, int usage) {
		this.bind();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, data, usage);
		
		return this;
	}
	
	public OGLElementArrayBuffer bind() {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.id);
		return this;
	}
	
	public OGLElementArrayBuffer unbind() {
		unbindAll();
		return this;
	}
	
	public void dispose() {
		if(glIsBuffer(this.id)) {
			glDeleteBuffers(this.id);
			bufferCount--;
			this.id = 0;
		}
	}
	
	public static void unbindAll() {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	
	public static int getElementArrayBufferCount() {
		return bufferCount;
	}
	
	public int getVersion() {
		return this.version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
}
