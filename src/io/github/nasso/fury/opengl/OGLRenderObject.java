package io.github.nasso.fury.opengl;

import io.github.nasso.fury.level.Mesh;

class OGLRenderObject {
	private static long last_id = 0;
	private long id = last_id++;
	
	private Mesh originMesh = null;
	private OGLVAOGeometry vao;
	private float z = 0;
	
	public long getID() {
		return this.id;
	}
	
	public Mesh getMesh() {
		return this.originMesh;
	}
	
	public void setMesh(Mesh originMesh) {
		this.originMesh = originMesh;
	}
	
	public float getZ() {
		return this.z;
	}
	
	public void setZ(float z) {
		this.z = z;
	}
	
	public OGLVAOGeometry getVAO() {
		return this.vao;
	}
	
	public void setVAO(OGLVAOGeometry vao) {
		this.vao = vao;
	}
}
