package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import io.github.nasso.fury.graphics.fx.PostEffect;
import io.github.nasso.fury.graphics.fx.ToneMapping;
import io.github.nasso.fury.graphics.fx.ToneMapping.ToneMappingType;

public class OGLToneMappingFilter extends OGLPostEffect {
	public static final int TYPE_REINHARD = 0;
	public static final int TYPE_EXPOSURE = 1;
	public static final int TYPE_AUTO_EXPOSURE = 2;
	
	private OGLToneMappingPostFXProgram program;
	
	private ToneMappingType type = ToneMappingType.REINHARD;
	private float exposure = 1.0f;
	private float adjustSpeed = 0.1f;
	
	public OGLToneMappingFilter() {
		this.program = new OGLToneMappingPostFXProgram();
	}
	
	public void apply(OGLTexture2D sourceColor, OGLTexture2D sourceDepth, OGLFramebuffer2D dest) {
		int type = TYPE_REINHARD;
		
		if(this.type == ToneMappingType.EXPOSURE) type = TYPE_EXPOSURE;
		else if(this.type == ToneMappingType.AUTO_EXPOSURE) {
			// TODO: type = TYPE_AUTO_EXPOSURE;
		}
		
		dest.bind();
		
		this.program.use();
		this.program.loadToUniform("color", sourceColor, 0);
		this.program.loadToUniform("type", type);
		this.program.loadToUniform("exposure", this.exposure);
		this.program.loadToUniform("adjustSpeed", this.adjustSpeed);
		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		this.program.unuse();
		
		dest.unbind();
	}
	
	public void dispose() {
		this.program.dispose();
	}
	
	public ToneMappingType getType() {
		return this.type;
	}
	
	public void setType(ToneMappingType type) {
		this.type = type;
	}
	
	public float getExposure() {
		return this.exposure;
	}
	
	public void setExposure(float exposure) {
		this.exposure = exposure;
	}
	
	public float getAdjustSpeed() {
		return this.adjustSpeed;
	}
	
	public void setAdjustSpeed(float adjustSpeed) {
		this.adjustSpeed = adjustSpeed;
	}
	
	public void update(PostEffect e) {
		if(e instanceof ToneMapping) {
			ToneMapping effect = (ToneMapping) e;
			
			this.setType(effect.getType());
			this.setExposure(effect.getExposure());
			this.setAdjustSpeed(effect.getAdjustSpeed());
		}
	}
}
