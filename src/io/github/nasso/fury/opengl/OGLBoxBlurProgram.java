package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLBoxBlurProgram extends OGLPostFXProgram {
	public OGLBoxBlurProgram() {
		super("Box blur program", FuryUtils.readFile("res/shaders/postfx/boxBlur.fs", true), true, false);
		
		this.loadUniforms("size", "textureDim");
	}
}
