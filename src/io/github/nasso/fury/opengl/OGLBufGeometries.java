package io.github.nasso.fury.opengl;

import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.graphics.DirectGeometry;
import io.github.nasso.fury.graphics.Geometry;

import java.util.HashMap;
import java.util.Map;

public class OGLBufGeometries {
	private Map<AbstractGeometry, BufferGeometry> geometries = new HashMap<AbstractGeometry, BufferGeometry>();
	
	public BufferGeometry get(AbstractGeometry absgeom) {
		if(this.geometries.get(absgeom) != null) return this.geometries.get(absgeom);
		
		BufferGeometry buffergeom = null;
		
		if(absgeom instanceof BufferGeometry) buffergeom = (BufferGeometry) absgeom;
		else if(absgeom instanceof DirectGeometry) {
			DirectGeometry geom = (DirectGeometry) absgeom;
			
			buffergeom = geom.getBufferGeometryVersion();
		} else if(absgeom instanceof Geometry) {
			Geometry geom = (Geometry) absgeom;
			
			buffergeom = geom.getBufferGeometryVersion();
		}
		
		this.geometries.put(absgeom, buffergeom);
		
		return buffergeom;
	}
}
