package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLGBufferProgram extends OGLProgram {
	public OGLGBufferProgram() {
		super("G-Buffer", FuryUtils.readFile("res/shaders/gbuffer.vs", true), FuryUtils.readFile("res/shaders/gbuffer.fs", true));
		
		this.setPositionAttrib("position_model");
		this.setNormalAttrib("normal_model");
		this.setUVAttrib("uv");
		this.setUV2Attrib("uv2");
		
		this.link();
		
		this.loadUniform("projViewModel", "proj*(view*model)");
		this.loadUniform("viewModel", "view*model");
		this.loadUniform("normalView", "normal(view*model)");
		
		this.loadUniforms("receiveShadows",
		
		"material.diffuseTexture", "material.normalTexture", "material.specularTexture",
		
		"material.diffuseColor",
		
		"material.uvScaling", "material.normalMapIntensity", "material.shininess",
		// "material.reflectivity",
		"material.specularIntensity", "material.alphaThreshold",
		
		"material.hasDiffuseTexture", "material.hasNormalTexture", "material.hasSpecularTexture", "material.faceSideNormalCorrection", "material.opaque");
	}
}
