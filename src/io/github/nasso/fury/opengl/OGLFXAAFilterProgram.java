package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLFXAAFilterProgram extends OGLPostFXProgram {
	public OGLFXAAFilterProgram() {
		super("FXAA filter program", FuryUtils.readFile("res/shaders/postfx/fxaa.fs", true), true, false);
		
		this.loadUniforms("reduceMin", "reduceMul", "spanMax", "resolution");
	}
}
