package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLFullTextureProgram extends OGLProgram {
	public OGLFullTextureProgram() {
		super("Fullscreen texture quad", FuryUtils.readFile("res/shaders/fullQuad.vs", true), FuryUtils.readFile("res/shaders/fullTextureQuad.fs", true));
		
		this.setPositionAttrib("position_quad");
		this.link();
		
		this.loadUniforms("color", "depth", "copyDepth");
	}
}
