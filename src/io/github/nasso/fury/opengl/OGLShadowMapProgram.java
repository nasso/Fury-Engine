package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLShadowMapProgram extends OGLProgram {
	public OGLShadowMapProgram() {
		super("Shadow map", FuryUtils.readFile("res/shaders/shadowMap.vs", true), FuryUtils.readFile("res/shaders/shadowMap.fs", true));
		
		this.setPositionAttrib("position_model");
		this.setUVAttrib("uv");
		this.link();
		
		this.loadUniform("projViewModel", "proj*(view*model)");
		
		this.loadUniforms("material.diffuseTexture",
		
		"material.uvScaling", "material.alphaThreshold",
		
		"material.hasDiffuseTexture", "material.opaque");
	}
}
