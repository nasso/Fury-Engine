package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLGammaPostFXProgram extends OGLPostFXProgram {
	public OGLGammaPostFXProgram() {
		super("Gamma filter program", FuryUtils.readFile("res/shaders/postfx/gamma.fs", true), true, false);
		
		this.loadUniforms("gamma");
	}
}
