package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.graphics.ShadowProperties;
import io.github.nasso.fury.graphics.ShadowsProperties;
import io.github.nasso.fury.level.Camera;
import io.github.nasso.fury.level.Light;
import io.github.nasso.fury.maths.Box3D;
import io.github.nasso.fury.maths.Frustum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class OGLCascadedShadowMaps {
	public static final int MAX_CASCADE_COUNT = OGLDeferredRenderingProgram.MAX_CASCADED_SHADOWS;
	
	private Map<Light, List<OGLShadowMap>> sunShadowMaps = new HashMap<Light, List<OGLShadowMap>>();
	private Map<Light, OGLShadowMap> pointShadowMaps = new HashMap<Light, OGLShadowMap>();
	private Map<Light, OGLShadowMap> spotShadowMaps = new HashMap<Light, OGLShadowMap>();
	
	private Material defaultMaterial = new Material();
	
	private OGLShadowMapProgram program;
	
	public OGLCascadedShadowMaps() {
		this.program = new OGLShadowMapProgram();
	}
	
	private Material _lastMaterial = null;
	
	private void uploadMaterialUniforms(OGLTextures textures, Material mat) {
		if(this._lastMaterial == mat) return;
		
		if(!mat.isOpaque()) {
			OGLTexture2D oglTex = textures.update(mat.getDiffuseTexture());
			
			this.program.loadToUniform("material.diffuseTexture", oglTex, 0);
			this.program.loadToUniform("material.hasDiffuseTexture", oglTex != null);
		} else this.program.loadToUniform("material.hasDiffuseTexture", false);
		
		this.program.loadToUniform("material.uvScaling", mat.getUVScaling());
		this.program.loadToUniform("material.alphaThreshold", mat.getAlphaThreshold());
		this.program.loadToUniform("material.opaque", mat.isOpaque());
		
		glPolygonMode(GL_FRONT_AND_BACK, Fury.getOpenGLConst(mat.getPolygonMode()));
		glPointSize(mat.getPointSize());
		glLineWidth(mat.getLineWidth());
		
		this._lastMaterial = mat;
	}
	
	private Matrix4f _worldInv = new Matrix4f();
	private Matrix4f _pvm = new Matrix4f();
	private Frustum _frus = new Frustum();
	
	private List<OGLShadowMap> updateCascades(List<OGLRenderObject> sce, Light light, Camera cam, Material materialUniform, OGLTextures textures) {
		List<OGLShadowMap> cascadeMaps = this.sunShadowMaps.get(light);
		
		if(!light.isCastShadow()) {
			if(cascadeMaps != null) {
				// Dispose the shadow map if the light don't cast shadow anymore
				for(int i = 0, l = cascadeMaps.size(); i < l; i++)
					if(cascadeMaps.get(i) != null) cascadeMaps.get(i).dispose();
				
				cascadeMaps.clear();
				this.sunShadowMaps.remove(light);
			}
			
			return null;
		}
		
		ShadowsProperties allProps = light.getShadowsProps();
		int count = Math.min(MAX_CASCADE_COUNT, allProps.getCascadeCount());
		
		if(cascadeMaps == null) {
			// Create the shadow map for the sun
			cascadeMaps = new ArrayList<OGLShadowMap>();
			
			this.sunShadowMaps.put(light, cascadeMaps);
		}
		
		if(cascadeMaps.size() != count) {
			if(cascadeMaps.size() > count) while(cascadeMaps.size() > count)
				cascadeMaps.remove(0);
			else while(cascadeMaps.size() < count)
				cascadeMaps.add(new OGLShadowMap(allProps.getShadowProperties(cascadeMaps.size()).getShadowMapSize()));
			
			// Refresh the Z positions
			float distributionExp = allProps.getCascadeDistributionExp();
			float maxDistN = allProps.getCascadeMaxDistance();
			float interFade = allProps.getInterCascadeFade();
			
			for(int i = 0, l = cascadeMaps.size(); i < l; i++) {
				OGLShadowMap map = cascadeMaps.get(i);
				
				float cascNear = (float) Math.pow((float) i / (float) count, distributionExp) * maxDistN;
				float cascFar = (float) Math.pow((float) (i + 1) / (float) count, distributionExp) * maxDistN;
				
				if(i != 0) cascNear -= interFade;
				
				map.setCascadeZNear(cascNear);
				map.setCascadeZFar(cascFar);
			}
		}
		
		for(int i = 0, l = cascadeMaps.size(); i < l; i++) {
			OGLShadowMap osm = cascadeMaps.get(i);
			ShadowProperties p = allProps.getShadowProperties(i);
			
			if(osm.getSize() != p.getShadowMapSize()) {
				osm.setSize(p.getShadowMapSize());
				osm.update();
			}
		}
		
		light.getMatrixWorld().invert(this._worldInv);
		
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(1.0f, 1.0f);
		
		this.program.use();
		for(int m = 0, l = cascadeMaps.size(); m < l; m++) {
			OGLShadowMap map = cascadeMaps.get(m);
			glViewport(0, 0, map.getSize(), map.getSize());
			
			Matrix4f lightProjView = this.calcLightFrustum(light, map, this._worldInv, cam);
			map.getLightProjView().set(lightProjView);
			
			this._frus.setFromMatrix(lightProjView);
			
			map.getFramebuffer().bind();
			
			glClear(GL_DEPTH_BUFFER_BIT);
			for(int i = 0, length = sce.size(); i < length; i++) {
				OGLRenderObject obj = sce.get(i);
				
				if(!this._frus.intersectsMesh(obj.getMesh())) continue;
				
				Material mat = materialUniform;
				
				if(mat == null) mat = obj.getMesh().getMaterial();
				
				if(mat == null) mat = this.defaultMaterial;
				
				AbstractGeometry ageom = obj.getMesh().getGeometry();
				if(ageom == null) continue;
				
				this.uploadMaterialUniforms(textures, mat);
				
				this.program.loadToUniform("proj*(view*model)", lightProjView.mul(obj.getMesh().getMatrixWorld(), this._pvm));
				
				OGLVAOGeometry vaoGeom = obj.getVAO();
				vaoGeom.bind();
				
				if(ageom.getCullFace() != Fury.NONE) {
					glEnable(GL_CULL_FACE);
					
					glCullFace(Fury.getOpenGLConst(ageom.getCullFace()));
				}
				
				if(vaoGeom.hasIndices()) glDrawElements(ageom.getRenderMode(), vaoGeom.getVertexCount(), GL_UNSIGNED_INT, 0);
				else glDrawArrays(ageom.getRenderMode(), 0, vaoGeom.getVertexCount());
				
				glDisable(GL_CULL_FACE);
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
			map.getFramebuffer().unbind();
		}
		
		this.program.unuse();
		
		glDisable(GL_POLYGON_OFFSET_FILL);
		glDisable(GL_DEPTH_TEST);
		
		this._lastMaterial = null;
		
		return cascadeMaps;
	}
	
	private OGLShadowMap updateSpot(List<OGLRenderObject> sce, Light light, Material materialUniform, OGLTextures textures) {
		OGLShadowMap shadowMap = this.spotShadowMaps.get(light);
		
		if(!light.isCastShadow() && shadowMap != null) {
			shadowMap.dispose();
			
			return null;
		}
		
		int shadowMapSize = light.getShadowsProps().getShadowProperties(0).getShadowMapSize();
		
		if(shadowMap == null) {
			shadowMap = new OGLShadowMap(shadowMapSize);
			
			this.spotShadowMaps.put(light, shadowMap);
		}
		
		light.getMatrixWorld().invert(this._worldInv);
		
		Matrix4f projView = shadowMap.getLightProjView();
		projView.setPerspective((float) Math.toRadians(light.getConeSize() * 2), 1, light.getShadowZNear(), light.getShadowZFar());
		projView.mul(this._worldInv);
		
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(1.0f, 1.0f);
		glViewport(0, 0, shadowMapSize, shadowMapSize);
		
		this.program.use();
		
		this._frus.setFromMatrix(projView);
		
		shadowMap.getFramebuffer().bind();
		
		glClear(GL_DEPTH_BUFFER_BIT);
		for(int i = 0, length = sce.size(); i < length; i++) {
			OGLRenderObject obj = sce.get(i);
			
			if(!this._frus.intersectsMesh(obj.getMesh())) continue;
			
			Material mat = materialUniform;
			
			if(mat == null) mat = obj.getMesh().getMaterial();
			
			if(mat == null) mat = this.defaultMaterial;
			
			AbstractGeometry ageom = obj.getMesh().getGeometry();
			if(ageom == null) continue;
			
			this.uploadMaterialUniforms(textures, mat);
			
			this.program.loadToUniform("proj*(view*model)", projView.mul(obj.getMesh().getMatrixWorld(), this._pvm));
			
			if(ageom.getCullFace() != Fury.NONE) {
				glEnable(GL_CULL_FACE);
				
				glCullFace(Fury.getOpenGLConst(ageom.getCullFace()));
			}
			
			OGLVAOGeometry vaoGeom = obj.getVAO();
			vaoGeom.bind();
			
			if(vaoGeom.hasIndices()) glDrawElements(ageom.getRenderMode(), vaoGeom.getVertexCount(), GL_UNSIGNED_INT, 0);
			else glDrawArrays(ageom.getRenderMode(), 0, vaoGeom.getVertexCount());
			
			glDisable(GL_CULL_FACE);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
		shadowMap.getFramebuffer().unbind();
		
		this.program.unuse();
		
		glDisable(GL_POLYGON_OFFSET_FILL);
		glDisable(GL_DEPTH_TEST);
		
		return shadowMap;
	}
	
	List<OGLShadowMap> _list = new ArrayList<OGLShadowMap>();
	
	public List<OGLShadowMap> update(List<OGLRenderObject> sce, Light light, Camera cam, Material materialUniform, OGLTextures textures) {
		int lightType = light.getType();
		
		if(lightType == Fury.LIGHT_TYPE_SUN) {
			this.disposeAll(light, this.pointShadowMaps);
			this.disposeAll(light, this.spotShadowMaps);
			
			return this.updateCascades(sce, light, cam, materialUniform, textures);
		} else if(lightType == Fury.LIGHT_TYPE_POINT) {
			this.disposeAllCascades(light, this.sunShadowMaps);
			this.disposeAll(light, this.spotShadowMaps);
			
			// TODO: Point shadows
		} else if(lightType == Fury.LIGHT_TYPE_SPOT) {
			this.disposeAll(light, this.pointShadowMaps);
			this.disposeAllCascades(light, this.sunShadowMaps);
			
			this._list.clear();
			this._list.add(this.updateSpot(sce, light, materialUniform, textures));
			return this._list;
		}
		
		return null;
	}
	
	Box3D _viewBounds = new Box3D();
	Matrix4f _frustum = new Matrix4f();
	Matrix4f _projViewInv = new Matrix4f();
	Vector4f _vec4 = new Vector4f();
	Vector3f[] _bounds = { new Vector3f(), new Vector3f(), new Vector3f(), new Vector3f(),
	
	new Vector3f(), new Vector3f(), new Vector3f(), new Vector3f(), };
	
	private Matrix4f calcLightFrustum(Light l, OGLShadowMap map, Matrix4f lightView, Camera cam) {
		// ProjViewInv = Proj (clip) view -> World
		// Then
		// LightView = World -> Light view
		
		cam.getProjViewMatrix().invert(this._projViewInv);
		
		// Trick to get the clip space depth
		this._vec4.set(0, 0, -map.getCascadeZNear(), 1);
		this._vec4.mul(cam.getProjectionMatrix());
		float near = this._vec4.w != 0 ? this._vec4.z / this._vec4.w : this._vec4.z;
		
		this._vec4.set(0, 0, -map.getCascadeZFar(), 1);
		this._vec4.mul(cam.getProjectionMatrix());
		float far = this._vec4.w != 0 ? this._vec4.z / this._vec4.w : this._vec4.z;
		
		// Near and far
		this._bounds[0].set(-1.0f, 1.0f, near);
		this._bounds[1].set(1.0f, 1.0f, near);
		this._bounds[2].set(1.0f, -1.0f, near);
		this._bounds[3].set(-1.0f, -1.0f, near);
		this._bounds[4].set(-1.0f, 1.0f, far);
		this._bounds[5].set(1.0f, 1.0f, far);
		this._bounds[6].set(1.0f, -1.0f, far);
		this._bounds[7].set(-1.0f, -1.0f, far);
		
		for(int i = 0; i < 8; i++)
			this._bounds[i].mulProject(this._projViewInv).mulPosition(lightView);
		
		this._viewBounds.setContaining(this._bounds);
		
		this._frustum.setOrtho(this._viewBounds.getMin().x, this._viewBounds.getMax().x, this._viewBounds.getMin().y, this._viewBounds.getMax().y, l.getShadowZNearSun(), l.getShadowZFarSun());
		this._frustum.mul(lightView);
		
		return this._frustum;
	}
	
	private void disposeAllCascades(Light light, Map<Light, List<OGLShadowMap>> list) {
		if(list.get(light) != null) {
			List<OGLShadowMap> map = list.get(light);
			
			for(int i = 0, l = map.size(); i < l; i++)
				if(map.get(i) != null) map.get(i).dispose();
			
			map.clear();
			list.remove(light);
		}
	}
	
	private void disposeAll(Light light, Map<Light, OGLShadowMap> list) {
		if(list.get(light) != null) {
			OGLShadowMap map = list.get(light);
			
			if(map != null) map.dispose();
			
			list.remove(light);
		}
	}
	
	public void disposeAll() {
		this.program.dispose();
		
		Set<Light> keySet = this.sunShadowMaps.keySet();
		for(Light l : keySet)
			this.disposeAllCascades(l, this.sunShadowMaps);
		
		keySet = this.pointShadowMaps.keySet();
		for(Light l : keySet)
			this.disposeAll(l, this.pointShadowMaps);
		
		keySet = this.spotShadowMaps.keySet();
		for(Light l : keySet)
			this.disposeAll(l, this.spotShadowMaps);
	}
}
