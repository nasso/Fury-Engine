package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLSkyboxProgram extends OGLProgram {
	public static final int MAX_SUN_COUNT = 8;
	
	public OGLSkyboxProgram() {
		super("Skybox", FuryUtils.readFile("res/shaders/skybox.vs", true), FuryUtils.readFile("res/shaders/skybox.fs", true));
		
		this.setPositionAttrib("position_model");
		this.link();
		
		this.loadUniform("projView", "proj*[view, notranslate]");
		
		this.loadUniforms("sky.type", "sky.cubeTexture",
		
		"sky.sunCount",
		
		"sky.up", "sky.bottomColor", "sky.topColor",
		
		"sky.cosPower",
		
		"scale");
		
		for(int i = 0; i < MAX_SUN_COUNT; i++)
			this.loadUniforms("sky.sun[" + i + "].color", "sky.sun[" + i + "].direction", "sky.sun[" + i + "].energy", "sky.sun[" + i + "].innerEdge", "sky.sun[" + i + "].outerEdge", "sky.sun[" + i + "].brightness");
	}
}
