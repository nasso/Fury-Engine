package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL30.*;

import org.joml.Matrix4f;

public class OGLShadowMap {
	private OGLFramebuffer2D framebuffer;
	private OGLTexture2D depthTexture;
	
	private Matrix4f lightViewProj = new Matrix4f();
	
	private float cascadeZNear = 0;
	private float cascadeZFar = 0;
	
	private int size;
	private int cascadeIndex = 0;
	
	public OGLShadowMap(int size) {
		this.framebuffer = new OGLFramebuffer2D();
		this.depthTexture = new OGLTexture2D.Builder().internalFormat(GL_DEPTH_COMPONENT16).format(GL_DEPTH_COMPONENT).type(GL_FLOAT).minFilter(GL_LINEAR).magFilter(GL_LINEAR).wrapS(GL_CLAMP_TO_EDGE).wrapT(GL_CLAMP_TO_EDGE).width(size).height(size).build();
		
		this.depthTexture.bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		this.depthTexture.unbind();
		
		this.framebuffer.bind();
		this.framebuffer.bindTexture(GL_DEPTH_ATTACHMENT, this.depthTexture);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		this.framebuffer.unbind();
		
		this.size = size;
	}
	
	public void update() {
		this.depthTexture.setWidth(this.size);
		this.depthTexture.setHeight(this.size);
		
		this.depthTexture.update();
	}
	
	public void dispose() {
		this.framebuffer.dispose();
		this.depthTexture.dispose();
	}
	
	public int getSize() {
		return this.size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public OGLTexture2D getDepthTexture() {
		return this.depthTexture;
	}
	
	public OGLFramebuffer2D getFramebuffer() {
		return this.framebuffer;
	}
	
	public Matrix4f getLightProjView() {
		return this.lightViewProj;
	}
	
	public void setLightProjView(Matrix4f lightPV) {
		this.lightViewProj.set(lightPV);
	}
	
	public int getCascadeIndex() {
		return this.cascadeIndex;
	}
	
	public void setCascadeIndex(int cascadeIndex) {
		this.cascadeIndex = cascadeIndex;
	}
	
	public float getCascadeZNear() {
		return this.cascadeZNear;
	}
	
	public void setCascadeZNear(float cascadeZNear) {
		this.cascadeZNear = cascadeZNear;
	}
	
	public float getCascadeZFar() {
		return this.cascadeZFar;
	}
	
	public void setCascadeZFar(float cascadeZFar) {
		this.cascadeZFar = cascadeZFar;
	}
}
