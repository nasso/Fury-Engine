package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.EXTTextureFilterAnisotropic.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL21.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.ByteBuffer;

public class OGLCubeTexture {
	public static class Builder {
		private int width = 512;
		private int height = 512;
		private int magFilter = GL_LINEAR;
		private int minFilter = GL_LINEAR_MIPMAP_LINEAR;
		private int wrapR = GL_CLAMP_TO_EDGE;
		private int wrapS = GL_CLAMP_TO_EDGE;
		private int wrapT = GL_CLAMP_TO_EDGE;
		private int internalFormat = GL_SRGB_ALPHA;
		private int format = GL_RGBA;
		private int type = GL_UNSIGNED_BYTE;
		private int anisotropy = 1;
		
		private ByteBuffer posXData = null;
		private ByteBuffer negXData = null;
		private ByteBuffer posYData = null;
		private ByteBuffer negYData = null;
		private ByteBuffer posZData = null;
		private ByteBuffer negZData = null;
		
		public Builder width(int value) {
			this.width = value;
			return this;
		}
		
		public Builder height(int value) {
			this.height = value;
			return this;
		}
		
		public Builder magFilter(int value) {
			this.magFilter = value;
			return this;
		}
		
		public Builder minFilter(int value) {
			this.minFilter = value;
			return this;
		}
		
		public Builder wrapR(int value) {
			this.wrapR = value;
			return this;
		}
		
		public Builder wrapS(int value) {
			this.wrapS = value;
			return this;
		}
		
		public Builder wrapT(int value) {
			this.wrapT = value;
			return this;
		}
		
		public Builder internalFormat(int value) {
			this.internalFormat = value;
			return this;
		}
		
		public Builder format(int value) {
			this.format = value;
			return this;
		}
		
		public Builder type(int value) {
			this.type = value;
			return this;
		}
		
		public Builder anisotropy(int value) {
			this.anisotropy = value;
			return this;
		}
		
		public Builder negZData(ByteBuffer value) {
			this.negZData = value;
			return this;
		}
		
		public Builder negXData(ByteBuffer value) {
			this.negXData = value;
			return this;
		}
		
		public Builder posYData(ByteBuffer value) {
			this.posYData = value;
			return this;
		}
		
		public Builder negYData(ByteBuffer value) {
			this.negYData = value;
			return this;
		}
		
		public Builder posZData(ByteBuffer value) {
			this.posZData = value;
			return this;
		}
		
		public Builder posXData(ByteBuffer value) {
			this.posXData = value;
			return this;
		}
		
		public OGLCubeTexture build() {
			return new OGLCubeTexture(this.width, this.height, this.magFilter, this.minFilter, this.wrapR, this.wrapS, this.wrapT, this.internalFormat, this.format, this.type, this.anisotropy, this.posXData, this.negXData, this.posYData, this.negYData, this.posZData, this.negZData);
		}
	}
	
	private static int textureCount = 0;
	
	private static int last_id = 0;
	private int id = last_id++;
	
	private int width = 512;
	private int height = 512;
	private int magFilter = GL_LINEAR;
	private int minFilter = GL_LINEAR_MIPMAP_LINEAR;
	private int wrapR = GL_CLAMP_TO_EDGE;
	private int wrapS = GL_CLAMP_TO_EDGE;
	private int wrapT = GL_CLAMP_TO_EDGE;
	private int internalFormat = GL_SRGB_ALPHA;
	private int format = GL_RGBA;
	private int type = GL_UNSIGNED_BYTE;
	private int anisotropy = 1;
	
	private ByteBuffer posXData = null;
	private ByteBuffer negXData = null;
	private ByteBuffer posYData = null;
	private ByteBuffer negYData = null;
	private ByteBuffer posZData = null;
	private ByteBuffer negZData = null;
	
	private int version = 0;
	
	public OGLCubeTexture(int width, int height, int magFilter, int minFilter, int wrapR, int wrapS, int wrapT, int internalFormat, int format, int type, int anisotropy, ByteBuffer posXData, ByteBuffer negXData, ByteBuffer posYData, ByteBuffer negYData, ByteBuffer posZData, ByteBuffer negZData) {
		this.width = width;
		this.height = height;
		this.magFilter = magFilter;
		this.minFilter = minFilter;
		this.wrapR = wrapR;
		this.wrapS = wrapS;
		this.wrapT = wrapT;
		this.internalFormat = internalFormat;
		this.format = format;
		this.type = type;
		this.anisotropy = anisotropy;
		
		this.posXData = posXData;
		this.negXData = negXData;
		this.posYData = posYData;
		this.negYData = negYData;
		this.posZData = posZData;
		this.negZData = negZData;
		
		this.update();
	}
	
	public OGLCubeTexture update() {
		if(!glIsTexture(this.id)) {
			this.id = glGenTextures();
			textureCount++;
		}
		
		this.bind();
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, this.internalFormat, this.width, this.height, 0, this.format, this.type, this.posXData);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, this.internalFormat, this.width, this.height, 0, this.format, this.type, this.negXData);
		
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, this.internalFormat, this.width, this.height, 0, this.format, this.type, this.posYData);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, this.internalFormat, this.width, this.height, 0, this.format, this.type, this.negYData);
		
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, this.internalFormat, this.width, this.height, 0, this.format, this.type, this.posZData);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, this.internalFormat, this.width, this.height, 0, this.format, this.type, this.negZData);
		
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, this.magFilter);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, this.minFilter);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, this.wrapR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, this.wrapS);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, this.wrapT);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, this.anisotropy);
		
		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		
		return this;
	}
	
	public ByteBuffer getPosXData() {
		return this.posXData;
	}
	
	public void setPosXData(ByteBuffer posXData) {
		this.posXData = posXData;
	}
	
	public ByteBuffer getNegXData() {
		return this.negXData;
	}
	
	public void setNegXData(ByteBuffer negXData) {
		this.negXData = negXData;
	}
	
	public ByteBuffer getPosYData() {
		return this.posYData;
	}
	
	public void setPosYData(ByteBuffer posYData) {
		this.posYData = posYData;
	}
	
	public ByteBuffer getNegYData() {
		return this.negYData;
	}
	
	public void setNegYData(ByteBuffer negYData) {
		this.negYData = negYData;
	}
	
	public ByteBuffer getPosZData() {
		return this.posZData;
	}
	
	public void setPosZData(ByteBuffer posZData) {
		this.posZData = posZData;
	}
	
	public ByteBuffer getNegZData() {
		return this.negZData;
	}
	
	public void setNegZData(ByteBuffer negZData) {
		this.negZData = negZData;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	public int getVersion() {
		return this.version;
	}
	
	public int getID() {
		return this.id;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getMagFilter() {
		return this.magFilter;
	}
	
	public int getMinFilter() {
		return this.minFilter;
	}
	
	public int getWrapR() {
		return this.wrapR;
	}
	
	public int getWrapS() {
		return this.wrapS;
	}
	
	public int getWrapT() {
		return this.wrapT;
	}
	
	public int getInternalFormat() {
		return this.internalFormat;
	}
	
	public int getFormat() {
		return this.format;
	}
	
	public int getType() {
		return this.type;
	}
	
	public int getAnisotropy() {
		return this.anisotropy;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setMagFilter(int magFilter) {
		this.magFilter = magFilter;
	}
	
	public void setMinFilter(int minFilter) {
		this.minFilter = minFilter;
	}
	
	public void setWrapR(int wrapR) {
		this.wrapR = wrapR;
	}
	
	public void setWrapS(int wrapS) {
		this.wrapS = wrapS;
	}
	
	public void setWrapT(int wrapT) {
		this.wrapT = wrapT;
	}
	
	public void setInternalFormat(int internalFormat) {
		this.internalFormat = internalFormat;
	}
	
	public void setFormat(int format) {
		this.format = format;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public void setAnisotropy(int anisotropy) {
		this.anisotropy = Math.min(OGLManager.get().maxAnisotropy, anisotropy);
	}
	
	public void dispose() {
		if(glIsTexture(this.id)) {
			glDeleteTextures(this.id);
			textureCount--;
		}
		
		this.id = 0;
	}
	
	public void bind() {
		glBindTexture(GL_TEXTURE_CUBE_MAP, this.id);
	}
	
	public void unbind() {
		unbindAll();
	}
	
	public boolean isNull() {
		return !glIsTexture(this.id);
	}
	
	public static void unbindAll() {
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}
	
	public static int getCubeTextureCount() {
		return textureCount;
	}
}
