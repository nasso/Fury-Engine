package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLToneMappingPostFXProgram extends OGLPostFXProgram {
	public OGLToneMappingPostFXProgram() {
		super("Tone mapping", FuryUtils.readFile("res/shaders/postfx/toneMapping.fs", true), true, false);
		
		this.loadUniforms("type", "exposure", "adjustSpeed");
	}
}
