package io.github.nasso.fury.opengl;

import io.github.nasso.fury.event.Event;
import io.github.nasso.fury.graphics.BufferAttribute;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.function.Consumer;

public class OGLVAOGeometry {
	private OGLArrayBuffer positions = null;
	private OGLArrayBuffer normals = null;
	private OGLArrayBuffer uvs = null;
	private OGLArrayBuffer uvs2 = null;
	private OGLArrayBuffer interleavedVBO = null;
	private OGLElementArrayBuffer indices = null;
	private OGLVertexArray vao;
	
	private BufferAttribute posAttrib;
	private BufferAttribute normAttrib;
	private BufferAttribute uvAttrib;
	private BufferAttribute uv2Attrib;
	private BufferAttribute indicesAttrib;
	
	private Consumer<Event> disposeAction;
	
	private int interleavedPosVersion = -1;
	private int interleavedNormVersion = -1;
	private int interleavedUVVersion = -1;
	private int interleavedUV2Version = -1;
	private int interleavedSize = 0;
	
	private int vertexCount = 0;
	private boolean hasIndices = false;
	
	public OGLVAOGeometry() {
		this.vao = new OGLVertexArray();
		
		this.disposeAction = (o) -> {
			OGLVAOGeometry.this.dispose();
		};
	}
	
	public Consumer<Event> disposeAction() {
		return this.disposeAction;
	}
	
	public void dispose() {
		if(this.positions != null) this.positions.dispose();
		if(this.normals != null) this.normals.dispose();
		if(this.uvs != null) this.uvs.dispose();
		if(this.uvs2 != null) this.uvs2.dispose();
		if(this.interleavedVBO != null) this.interleavedVBO.dispose();
		if(this.indices != null) this.indices.dispose();
		if(this.vao != null) this.vao.dispose();
		
		this.positions = null;
		this.normals = null;
		this.uvs = null;
		this.uvs2 = null;
		this.indices = null;
		this.vao = null;
	}
	
	public void bind() {
		this.vao.bind();
	}
	
	public void unbind() {
		this.vao.unbind();
	}
	
	private void constraintVertexCount(int count) {
		if(this.vertexCount < 0) this.vertexCount = count;
		else this.vertexCount = Math.min(this.vertexCount, count);
	}
	
	private boolean interleavedNeedUpdate() {
		if((this.posAttrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.POSITION_ATTRIB_LOCATION)) && (this.normAttrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.NORMAL_ATTRIB_LOCATION)) && (this.uvAttrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.UV_ATTRIB_LOCATION)) && (this.uv2Attrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.UV2_ATTRIB_LOCATION)) && (this.interleavedVBO != null) && (this.indicesAttrib == null) == (this.indices == null)) {
			if(this.posAttrib != null && this.interleavedPosVersion != this.posAttrib.getVersion()) return true;
			if(this.normAttrib != null && this.interleavedNormVersion != this.normAttrib.getVersion()) return true;
			if(this.uvAttrib != null && this.interleavedUVVersion != this.uvAttrib.getVersion()) return true;
			if(this.uv2Attrib != null && this.interleavedUV2Version != this.uv2Attrib.getVersion()) return true;
			if(this.indices != null && this.indices.getVersion() != this.indicesAttrib.getVersion()) return true;
			
			return false;
		}
		
		return true;
	}
	
	private void updateInterleaved() {
		if(this.positions != null) {
			this.positions.dispose();
			this.positions = null;
		}
		
		if(this.normals != null) {
			this.normals.dispose();
			this.normals = null;
		}
		
		if(this.uvs != null) {
			this.uvs.dispose();
			this.uvs = null;
		}
		
		if(this.uvs2 != null) {
			this.uvs2.dispose();
			this.uvs2 = null;
		}
		
		if(!this.interleavedNeedUpdate()) return;
		
		if(this.interleavedVBO == null) this.interleavedVBO = new OGLArrayBuffer();
		
		this.vao.bind();
		this.interleavedVBO.bind();
		
		this.vertexCount = -1;
		this.hasIndices = this.indicesAttrib != null;
		
		if(this.hasIndices) {
			if(this.indices == null) this.indices = new OGLElementArrayBuffer();
			
			if(this.indices.getVersion() != this.indicesAttrib.getVersion()) {
				Buffer data = this.indicesAttrib.getData();
				
				// Must explicitly send an IntBuffer
				if(data instanceof IntBuffer) {
					this.indices.loadData((IntBuffer) data);
					this.indices.setVersion(this.indicesAttrib.getVersion());
				}
			}
			
			this.vertexCount = this.indicesAttrib.getCount();
			
			this.indices.bind();
		} else {
			if(this.posAttrib != null) this.constraintVertexCount(this.posAttrib.getCount() / 3);
			if(this.normAttrib != null) this.constraintVertexCount(this.normAttrib.getCount() / 3);
			if(this.uvAttrib != null) this.constraintVertexCount(this.uvAttrib.getCount() / 2);
			if(this.uv2Attrib != null) this.constraintVertexCount(this.uv2Attrib.getCount() / 2);
			
			OGLElementArrayBuffer.unbindAll();
		}
		
		int bufferSize = 0;
		int posCount = 0;
		int normCount = 0;
		int uvCount = 0;
		int uv2Count = 0;
		if(this.posAttrib != null) {
			posCount = (this.hasIndices ? this.posAttrib.getCount() : this.vertexCount * 3);
			bufferSize += posCount;
		}
		
		if(this.normAttrib != null) {
			normCount = (this.hasIndices ? this.normAttrib.getCount() : this.vertexCount * 3);
			bufferSize += normCount;
		}
		
		if(this.uvAttrib != null) {
			uvCount = (this.hasIndices ? this.uvAttrib.getCount() : this.vertexCount * 2);
			bufferSize += uvCount;
		}
		
		if(this.uv2Attrib != null) {
			uv2Count = (this.hasIndices ? this.uv2Attrib.getCount() : this.vertexCount * 2);
			bufferSize += uv2Count;
		}
		if(this.interleavedSize != bufferSize) {
			this.interleavedVBO.allocate(bufferSize * 4);
			this.interleavedSize = bufferSize;
		}
		
		FloatBuffer posBuffer, normBuffer, uvBuffer, uv2Buffer;
		posBuffer = normBuffer = uvBuffer = uv2Buffer = null;
		
		if(this.posAttrib != null) posBuffer = ((FloatBuffer) this.posAttrib.getData());
		
		if(this.normAttrib != null) normBuffer = ((FloatBuffer) this.normAttrib.getData());
		
		if(this.uvAttrib != null) uvBuffer = ((FloatBuffer) this.uvAttrib.getData());
		
		if(this.uv2Attrib != null) uv2Buffer = ((FloatBuffer) this.uv2Attrib.getData());
		
		float[] total = new float[bufferSize];
		int offset = 0;
		int posOffset = 0;
		int normOffset = 0;
		int uvOffset = 0;
		int uv2Offset = 0;
		for(int i = 0; i < this.vertexCount; i++) {
			if(posBuffer != null && posBuffer.limit() > posOffset) {
				total[offset++] = posBuffer.get(posOffset++);
				total[offset++] = posBuffer.get(posOffset++);
				total[offset++] = posBuffer.get(posOffset++);
			}
			
			if(normBuffer != null && normBuffer.limit() > normOffset) {
				total[offset++] = normBuffer.get(normOffset++);
				total[offset++] = normBuffer.get(normOffset++);
				total[offset++] = normBuffer.get(normOffset++);
			}
			
			if(uvBuffer != null && uvBuffer.limit() > uvOffset) {
				total[offset++] = uvBuffer.get(uvOffset++);
				total[offset++] = uvBuffer.get(uvOffset++);
			}
			
			if(uv2Buffer != null && uv2Buffer.limit() > uv2Offset) {
				total[offset++] = uv2Buffer.get(uv2Offset++);
				total[offset++] = uv2Buffer.get(uv2Offset++);
			}
		}
		
		this.interleavedVBO.loadSubData(total);
		
		this.vao.setAttribLocationEnabled(OGLProgram.POSITION_ATTRIB_LOCATION, this.posAttrib != null);
		this.vao.setAttribLocationEnabled(OGLProgram.NORMAL_ATTRIB_LOCATION, this.normAttrib != null);
		this.vao.setAttribLocationEnabled(OGLProgram.UV_ATTRIB_LOCATION, this.uvAttrib != null);
		this.vao.setAttribLocationEnabled(OGLProgram.UV2_ATTRIB_LOCATION, this.uv2Attrib != null);
		
		int attribDataCount = 0;
		if(this.posAttrib != null) attribDataCount += 12; // 3 * 4
		if(this.normAttrib != null) attribDataCount += 12; // 3 * 4
		if(this.uvAttrib != null) attribDataCount += 8; // 2 * 4
		if(this.uv2Attrib != null) attribDataCount += 8; // 2 * 4
		
		if(this.posAttrib != null) {
			this.vao.loadVBOToAttrib(OGLProgram.POSITION_ATTRIB_LOCATION, 3, this.interleavedVBO, attribDataCount, 0);
			
			this.interleavedPosVersion = this.posAttrib.getVersion();
		}
		if(this.normAttrib != null) {
			this.vao.loadVBOToAttrib(OGLProgram.NORMAL_ATTRIB_LOCATION, 3, this.interleavedVBO, attribDataCount, 12);
			
			this.interleavedNormVersion = this.normAttrib.getVersion();
		}
		if(this.uvAttrib != null) {
			this.vao.loadVBOToAttrib(OGLProgram.UV_ATTRIB_LOCATION, 2, this.interleavedVBO, attribDataCount, (this.normAttrib != null ? 24 : 12));
			
			this.interleavedUVVersion = this.uvAttrib.getVersion();
		}
		if(this.uv2Attrib != null) {
			this.vao.loadVBOToAttrib(OGLProgram.UV2_ATTRIB_LOCATION, 2, this.interleavedVBO, attribDataCount, (this.normAttrib != null ? (this.uvAttrib != null ? 32 : 24) : 12));
			
			this.interleavedUV2Version = this.uv2Attrib.getVersion();
		}
		
		this.interleavedVBO.unbind();
		this.vao.unbind();
	}
	
	private boolean classicNeedUpdate() {
		if((this.posAttrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.POSITION_ATTRIB_LOCATION)) && (this.normAttrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.NORMAL_ATTRIB_LOCATION)) && (this.uvAttrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.UV_ATTRIB_LOCATION)) && (this.uv2Attrib != null) == (this.vao.isAttribLocationEnabled(OGLProgram.UV2_ATTRIB_LOCATION)) && (this.posAttrib == null) == (this.positions == null) && (this.normAttrib == null) == (this.normals == null) && (this.uvAttrib == null) == (this.uvs == null) && (this.uv2Attrib == null) == (this.uvs2 == null) && (this.indicesAttrib == null) == (this.indices == null)) {
			if(this.positions != null && this.positions.getVersion() != this.posAttrib.getVersion()) return true;
			if(this.normals != null && this.normals.getVersion() != this.normAttrib.getVersion()) return true;
			if(this.uvs != null && this.uvs.getVersion() != this.uvAttrib.getVersion()) return true;
			if(this.uvs2 != null && this.uvs2.getVersion() != this.uv2Attrib.getVersion()) return true;
			if(this.indices != null && this.indices.getVersion() != this.indicesAttrib.getVersion()) return true;
			
			return false;
		}
		
		return true;
	}
	
	private void updateClassic() {
		if(this.interleavedVBO != null) {
			this.interleavedVBO.dispose();
			this.interleavedVBO = null;
		}
		
		if(!this.classicNeedUpdate()) return;
		
		this.vao.bind();
		
		this.vertexCount = -1;
		this.vao.setAttribLocationEnabled(OGLProgram.POSITION_ATTRIB_LOCATION, this.posAttrib != null);
		if(this.posAttrib != null) {
			if(this.positions == null) this.positions = new OGLArrayBuffer();
			
			if(this.positions.getVersion() != this.posAttrib.getVersion()) {
				this.positions.loadData(this.posAttrib.getData());
				this.positions.setVersion(this.posAttrib.getVersion());
				
				this.vao.loadVBOToAttrib(OGLProgram.POSITION_ATTRIB_LOCATION, 3, this.positions);
			}
			
			this.constraintVertexCount(this.posAttrib.getCount() / 3);
		}
		
		this.vao.setAttribLocationEnabled(OGLProgram.NORMAL_ATTRIB_LOCATION, this.normAttrib != null);
		if(this.normAttrib != null) {
			if(this.normals == null) this.normals = new OGLArrayBuffer();
			
			if(this.normals.getVersion() != this.normAttrib.getVersion()) {
				this.normals.loadData(this.normAttrib.getData());
				this.normals.setVersion(this.normAttrib.getVersion());
				
				this.vao.loadVBOToAttrib(OGLProgram.NORMAL_ATTRIB_LOCATION, 3, this.normals);
			}
			
			this.constraintVertexCount(this.normAttrib.getCount() / 3);
		}
		
		this.vao.setAttribLocationEnabled(OGLProgram.UV_ATTRIB_LOCATION, this.uvAttrib != null);
		if(this.uvAttrib != null) {
			if(this.uvs == null) this.uvs = new OGLArrayBuffer();
			
			if(this.uvs.getVersion() != this.uvAttrib.getVersion()) {
				this.uvs.loadData(this.uvAttrib.getData());
				this.uvs.setVersion(this.uvAttrib.getVersion());
				
				this.vao.loadVBOToAttrib(OGLProgram.UV_ATTRIB_LOCATION, 2, this.uvs);
			}
			
			this.constraintVertexCount(this.uvAttrib.getCount() / 2);
		}
		
		this.vao.setAttribLocationEnabled(OGLProgram.UV2_ATTRIB_LOCATION, this.uv2Attrib != null);
		if(this.uv2Attrib != null) {
			if(this.uvs2 == null) this.uvs2 = new OGLArrayBuffer();
			
			if(this.uvs2.getVersion() != this.uv2Attrib.getVersion()) {
				this.uvs2.loadData(this.uv2Attrib.getData());
				this.uvs2.setVersion(this.uv2Attrib.getVersion());
				
				this.vao.loadVBOToAttrib(OGLProgram.UV2_ATTRIB_LOCATION, 2, this.uvs2);
			}
			
			this.constraintVertexCount(this.uv2Attrib.getCount() / 2);
		}
		
		this.hasIndices = this.indicesAttrib != null;
		if(this.hasIndices) {
			if(this.indices == null) this.indices = new OGLElementArrayBuffer();
			
			if(this.indices.getVersion() != this.indicesAttrib.getVersion()) {
				Buffer data = this.indicesAttrib.getData();
				
				// Must explicitly send an IntBuffer
				if(data instanceof IntBuffer) {
					this.indices.loadData((IntBuffer) data);
					this.indices.setVersion(this.indicesAttrib.getVersion());
				}
			}
			
			this.vertexCount = this.indicesAttrib.getCount();
			
			this.indices.bind();
		} else OGLElementArrayBuffer.unbindAll();
		
		OGLArrayBuffer.unbindAll();
		this.vao.unbind();
	}
	
	public void update() {
		if(OGLManager.get().useInterleavedVBOs) this.updateInterleaved();
		else this.updateClassic();
	}
	
	public BufferAttribute getPosAttrib() {
		return this.posAttrib;
	}
	
	public void setPosAttrib(BufferAttribute posAttrib) {
		this.posAttrib = posAttrib;
	}
	
	public BufferAttribute getNormAttrib() {
		return this.normAttrib;
	}
	
	public void setNormAttrib(BufferAttribute normAttrib) {
		this.normAttrib = normAttrib;
	}
	
	public BufferAttribute getUVAttrib() {
		return this.uvAttrib;
	}
	
	public void setUVAttrib(BufferAttribute uvAttrib) {
		this.uvAttrib = uvAttrib;
	}
	
	public BufferAttribute getUV2Attrib() {
		return this.uv2Attrib;
	}
	
	public void setUV2Attrib(BufferAttribute uv2Attrib) {
		this.uv2Attrib = uv2Attrib;
	}
	
	public OGLVertexArray getVAO() {
		return this.vao;
	}
	
	public BufferAttribute getIndicesAttrib() {
		return this.indicesAttrib;
	}
	
	public void setIndicesAttrib(BufferAttribute indicesAttrib) {
		this.indicesAttrib = indicesAttrib;
	}
	
	public boolean hasIndices() {
		return this.hasIndices;
	}
	
	public int getVertexCount() {
		return this.vertexCount;
	}
}
