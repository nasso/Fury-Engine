package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLDeferredStencilCullProgram extends OGLProgram {
	public OGLDeferredStencilCullProgram() {
		super("Deferred stencil culling", FuryUtils.readFile("res/shaders/deferredStencilCull.vs", true), FuryUtils.readFile("res/shaders/deferredStencilCull.fs", true));
		
		this.setPositionAttrib("position_model");
		this.link();
		
		this.loadUniform("projViewModel", "proj*(view*model)");
	}
}
