package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.HashMap;
import java.util.Map;

public class OGLVertexArray {
	private Map<Integer, Boolean> locEnableState = new HashMap<Integer, Boolean>();
	private static OGLVertexArray boundVAO = null;
	private static int vaoCount = 0;
	
	private int id = 0;
	
	public OGLVertexArray() {
		this.id = glGenVertexArrays();
		vaoCount++;
	}
	
	public OGLVertexArray bind() {
		if(boundVAO != this) {
			glBindVertexArray(this.id);
			boundVAO = this;
		}
		
		return this;
	}
	
	public OGLVertexArray unbind() {
		unbindAll();
		return this;
	}
	
	public void setAttribLocationEnabled(int loc, boolean enabled) {
		if(enabled) glEnableVertexAttribArray(loc);
		else glDisableVertexAttribArray(loc);
		this.locEnableState.put(loc, enabled);
	}
	
	public boolean isAttribLocationEnabled(int loc) {
		return this.locEnableState.getOrDefault(loc, false);
	}
	
	/**
	 * DON'T FORGET TO ENABLE IT, THIS METHOD DOESN'T DO IT
	 * 
	 * @param loc
	 * @param size
	 * @param vbo
	 */
	public void loadVBOToAttrib(int loc, int size, OGLArrayBuffer vbo, int stride, int offset) {
		vbo.bind();
		glVertexAttribPointer(loc, size, vbo.getType(), false, stride, offset);
	}
	
	public void loadVBOToAttrib(int loc, int size, OGLArrayBuffer vbo, int offset) {
		this.loadVBOToAttrib(loc, size, vbo, 0, offset);
	}
	
	public void loadVBOToAttrib(int loc, int size, OGLArrayBuffer vbo) {
		this.loadVBOToAttrib(loc, size, vbo, 0);
	}
	
	public void loadIndices(OGLElementArrayBuffer indices) {
		indices.bind();
	}
	
	public static void unbindAll() {
		if(boundVAO != null) {
			glBindVertexArray(0);
			boundVAO = null;
		}
	}
	
	public static int getVAOCount() {
		return vaoCount;
	}
	
	public void dispose() {
		if(glIsVertexArray(this.id)) {
			glDeleteVertexArrays(this.id);
			vaoCount--;
			this.id = 0;
		}
	}
	
}
