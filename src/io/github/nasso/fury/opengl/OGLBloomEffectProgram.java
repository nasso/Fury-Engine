package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLBloomEffectProgram extends OGLPostFXProgram {
	public OGLBloomEffectProgram() {
		super("Bloom program", FuryUtils.readFile("res/shaders/postfx/bloom.fs", true), true, false);
		
		this.loadUniforms("size", "textureLength", "threshold", "smoothness");
	}
}
