package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import io.github.nasso.fury.data.TextureData;
import io.github.nasso.fury.graphics.Renderer;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;

public abstract class OGLRenderer extends Renderer {
	public OGLRenderer(int width, int height) {
		super(width, height);
	}
	
	public TextureData takeScreenshot() {
		int width = this.getWidth();
		int height = this.getHeight();
		
		OGLFramebuffer2D.unbindAll();
		
		ByteBuffer data = BufferUtils.createByteBuffer(width * height * 3);
		glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
		
		TextureData tex = new TextureData();
		tex.setData(data);
		tex.setWidth(width);
		tex.setHeight(height);
		tex.setBytesPerPixel(3);
		
		return tex;
	}
}
