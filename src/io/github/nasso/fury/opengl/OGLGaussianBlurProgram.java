package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLGaussianBlurProgram extends OGLPostFXProgram {
	public OGLGaussianBlurProgram() {
		super("Gaussian blur program", FuryUtils.readFile("res/shaders/postfx/gaussianBlur.fs", true), true, false);
		
		this.loadUniforms("size", "horizontal", "textureLength");
	}
}
