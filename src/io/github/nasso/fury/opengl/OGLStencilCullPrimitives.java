package io.github.nasso.fury.opengl;

import java.util.ArrayList;
import java.util.List;

class OGLStencilCullPrimitives {
	private int sphereIndicesSize = 0;
	private OGLArrayBuffer spherePositions;
	private OGLElementArrayBuffer sphereIndices;
	private OGLVertexArray sphereVAO;
	
	private int cubeIndicesSize = 0;
	private OGLArrayBuffer cubePositions;
	private OGLElementArrayBuffer cubeIndices;
	private OGLVertexArray cubeVAO;
	
	public OGLStencilCullPrimitives(int sphereWidthSegments, int sphereHeightSegments, int coneSegments) {
		this.generateSphere(1, sphereWidthSegments, sphereHeightSegments);
		this.generateCube(1, 1, 1);
	}
	
	private void generateSphere(float radius, int widthSegments, int heightSegments) {
		radius = Math.abs(radius);
		
		widthSegments = Math.max(3, widthSegments);
		heightSegments = Math.max(2, heightSegments);
		
		float phiStart = 0;
		float phiLength = (float) (Math.PI * 2);
		
		float thetaStart = 0;
		float thetaLength = (float) Math.PI;
		
		float thetaEnd = thetaStart + thetaLength;
		
		int vertexCount = (widthSegments + 1) * (heightSegments + 1);
		
		float[] positions = new float[vertexCount * 3];
		
		int index = 0;
		int[][] vertices = new int[heightSegments + 1][widthSegments + 1];
		
		for(int y = 0; y <= heightSegments; y++) {
			float v = (float) y / (float) heightSegments;
			
			for(int x = 0; x <= widthSegments; x++) {
				float u = (float) x / (float) widthSegments;
				
				float px = (float) (-radius * Math.cos(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength));
				float py = (float) (radius * Math.cos(thetaStart + v * thetaLength));
				float pz = (float) (radius * Math.sin(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength));
				
				// Multiply the index by the item size (x, y, z = 3)
				// So the index is the vector index
				positions[index * 3] = px;
				positions[index * 3 + 1] = py;
				positions[index * 3 + 2] = pz;
				
				vertices[y][x] = index++;
			}
		}
		
		List<Integer> indices = new ArrayList<Integer>();
		
		for(int y = 0; y < heightSegments; y++)
			for(int x = 0; x < widthSegments; x++) {
				int v1 = vertices[y][x + 1];
				int v2 = vertices[y][x];
				int v3 = vertices[y + 1][x];
				int v4 = vertices[y + 1][x + 1];
				
				if(y != 0 || thetaStart > 0) {
					indices.add(v1);
					indices.add(v2);
					indices.add(v4);
				}
				
				if(y != heightSegments - 1 || thetaEnd < Math.PI) {
					indices.add(v2);
					indices.add(v3);
					indices.add(v4);
				}
			}
		
		int[] indicesArray = new int[indices.size()];
		for(int i = 0; i < indicesArray.length; i++)
			indicesArray[i] = indices.get(i);
		
		this.sphereIndicesSize = indicesArray.length;
		
		// Now, OpenGL part
		this.spherePositions = new OGLArrayBuffer();
		this.spherePositions.loadData(positions);
		
		this.sphereIndices = new OGLElementArrayBuffer();
		this.sphereIndices.loadData(indicesArray);
		
		this.sphereVAO = new OGLVertexArray();
		
		this.sphereVAO.bind();
		this.sphereVAO.setAttribLocationEnabled(0, true);
		this.sphereVAO.loadVBOToAttrib(0, 3, this.spherePositions);
		this.sphereVAO.loadIndices(this.sphereIndices);
		this.sphereVAO.unbind();
		
		this.spherePositions.unbind();
		this.sphereIndices.unbind();
	}
	
	private void generateCube(float sizeX, float sizeY, float sizeZ) {
		float sizeXHalf = sizeX / 2.0f;
		float sizeYHalf = sizeY / 2.0f;
		float sizeZHalf = sizeZ / 2.0f;
		
		float[] positions = new float[24]; // 24 = 8 * 3 vertices in a cube
		int[] indices = new int[36]; // 36 = 6 * 2 * 3 = 6 faces * 2 triangles *
										// 3 vertices
		
		int i = 0;
		positions[i++] = sizeXHalf; // x
		positions[i++] = -sizeYHalf; // y
		positions[i++] = sizeZHalf; // z
		
		positions[i++] = -sizeXHalf; // x
		positions[i++] = -sizeYHalf; // y
		positions[i++] = sizeZHalf; // z
		
		positions[i++] = sizeXHalf; // x
		positions[i++] = sizeYHalf; // y
		positions[i++] = sizeZHalf; // z
		
		positions[i++] = -sizeXHalf; // x
		positions[i++] = sizeYHalf; // y
		positions[i++] = sizeZHalf; // z
		
		positions[i++] = sizeXHalf; // x
		positions[i++] = -sizeYHalf; // y
		positions[i++] = -sizeZHalf; // z
		
		positions[i++] = -sizeXHalf; // x
		positions[i++] = -sizeYHalf; // y
		positions[i++] = -sizeZHalf; // z
		
		positions[i++] = sizeXHalf; // x
		positions[i++] = sizeYHalf; // y
		positions[i++] = -sizeZHalf; // z
		
		positions[i++] = -sizeXHalf; // x
		positions[i++] = sizeYHalf; // y
		positions[i] = -sizeZHalf; // z
		
		i = 0;
		// Top face
		indices[i++] = 0;
		indices[i++] = 2;
		indices[i++] = 1;
		indices[i++] = 1;
		indices[i++] = 2;
		indices[i++] = 3;
		
		// Sides
		indices[i++] = 2;
		indices[i++] = 6;
		indices[i++] = 7;
		indices[i++] = 3;
		indices[i++] = 2;
		indices[i++] = 7;
		
		indices[i++] = 3;
		indices[i++] = 7;
		indices[i++] = 5;
		indices[i++] = 3;
		indices[i++] = 5;
		indices[i++] = 1;
		
		indices[i++] = 0;
		indices[i++] = 1;
		indices[i++] = 4;
		indices[i++] = 4;
		indices[i++] = 1;
		indices[i++] = 5;
		
		indices[i++] = 0;
		indices[i++] = 4;
		indices[i++] = 6;
		indices[i++] = 2;
		indices[i++] = 0;
		indices[i++] = 6;
		
		// Bottom face
		indices[i++] = 4;
		indices[i++] = 7;
		indices[i++] = 6;
		indices[i++] = 4;
		indices[i++] = 5;
		indices[i++] = 7;
		
		this.cubeIndicesSize = i;
		
		// OpenGL part
		this.cubePositions = new OGLArrayBuffer();
		this.cubePositions.loadData(positions);
		
		this.cubeIndices = new OGLElementArrayBuffer();
		this.cubeIndices.loadData(indices);
		
		this.cubeVAO = new OGLVertexArray();
		
		this.cubeVAO.bind();
		this.cubeVAO.setAttribLocationEnabled(0, true);
		this.cubeVAO.loadVBOToAttrib(0, 3, this.cubePositions);
		this.cubeVAO.loadIndices(this.cubeIndices);
		this.cubeVAO.unbind();
		
		this.cubePositions.unbind();
		this.cubeIndices.unbind();
	}
	
	public OGLVertexArray getSphere() {
		return this.sphereVAO;
	}
	
	public OGLVertexArray getCube() {
		return this.cubeVAO;
	}
	
	public int getSphereIndicesSize() {
		return this.sphereIndicesSize;
	}
	
	public int getCubeIndicesSize() {
		return this.cubeIndicesSize;
	}
	
	public void dispose() {
		this.sphereVAO.dispose();
		this.spherePositions.dispose();
		this.sphereIndices.dispose();
		
		this.cubeVAO.dispose();
		this.cubePositions.dispose();
		this.cubeIndices.dispose();
	}
}
