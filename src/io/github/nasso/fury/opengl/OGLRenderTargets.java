package io.github.nasso.fury.opengl;

import io.github.nasso.fury.event.Event;
import io.github.nasso.fury.graphics.RenderTarget2D;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class OGLRenderTargets {
	private Map<RenderTarget2D, OGLRenderTarget2D> targets2d = new HashMap<RenderTarget2D, OGLRenderTarget2D>();
	
	private Consumer<Event> targetDisposer;
	
	private OGLTextures textures;
	
	public OGLRenderTargets(OGLTextures textureManager) {
		this.textures = textureManager;
		
		this.targetDisposer = (s) -> {
			if(this.targets2d.containsKey(s.getSource())) {
				this.targets2d.get(s.getSource()).dispose(false);
				this.targets2d.remove(s.getSource());
			}
		};
	}
	
	public OGLRenderTarget2D update(RenderTarget2D rt) {
		OGLRenderTarget2D target = this.targets2d.get(rt);
		
		if(target == null) {
			target = new OGLRenderTarget2D(this.textures.update(rt.getTexture()));
			
			rt.addEventListener("dispose", this.targetDisposer);
			this.targets2d.put(rt, target);
		} else target.setColorTexture(this.textures.update(rt.getTexture()));
		
		return target;
	}
	
	public OGLRenderTarget2D get(RenderTarget2D rt) {
		return this.targets2d.get(rt);
	}
	
	public Map<RenderTarget2D, OGLRenderTarget2D> getTargets2D() {
		return this.targets2d;
	}
	
	public void disposeAll(boolean deleteTextures) {
		Set<RenderTarget2D> keys = this.targets2d.keySet();
		for(RenderTarget2D rt : keys)
			this.targets2d.get(rt).dispose(deleteTextures);
		
		this.targets2d.clear();
	}
}
