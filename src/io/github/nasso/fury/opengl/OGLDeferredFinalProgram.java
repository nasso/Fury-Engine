package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLDeferredFinalProgram extends OGLProgram {
	public OGLDeferredFinalProgram() {
		super("Final deferred program", FuryUtils.readFile("res/shaders/fullQuad.vs", true), FuryUtils.readFile("res/shaders/deferredFinal.fs", true));
		
		this.setPositionAttrib("position_quad");
		this.link();
		
		this.loadUniforms("projViewInv", "camPos",
		
		"color", "depth",
		
		"fog.enabled",
		
		"fog.type",
		
		"fog.near", "fog.far", "fog.density",
		
		"fog.color");
	}
}
