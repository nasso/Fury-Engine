package io.github.nasso.fury.opengl;

import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.level.Camera;
import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.maths.Projector;

import java.util.HashMap;
import java.util.Map;

import org.joml.Vector3f;

/**
 * Stores objects VAO by mesh ID Also store a BufferGeometry version of each
 * mesh geometry
 * 
 * @author Nasso
 */
public class OGLObjects {
	// Buffer geometries
	private OGLBufGeometries geometries = new OGLBufGeometries();
	
	// Render items stored by mesh
	private Map<Mesh, OGLRenderObject> objects = new HashMap<Mesh, OGLRenderObject>();
	
	// VAOs, stored by BufferGeometry
	private Map<BufferGeometry, OGLVAOGeometry> vaos = new HashMap<BufferGeometry, OGLVAOGeometry>();
	
	private OGLVAOGeometry update(BufferGeometry geom) {
		if(geom == null) return null;
		
		OGLVAOGeometry vao = this.vaos.get(geom);
		
		if(vao == null) {
			vao = new OGLVAOGeometry();
			geom.addEventListener("dispose", vao.disposeAction());
			
			this.vaos.put(geom, vao);
		}
		
		vao.setPosAttrib(geom.getPositions());
		vao.setNormAttrib(geom.getNormals());
		vao.setUVAttrib(geom.getUVs());
		vao.setUV2Attrib(geom.getUVs2());
		vao.setIndicesAttrib(geom.getIndices());
		vao.update();
		
		return vao;
	}
	
	public OGLRenderObject update(Mesh mesh, Camera cam) {
		AbstractGeometry mgeom = mesh.getGeometry();
		
		if(mesh == null || mgeom == null) return null;
		
		OGLRenderObject robj = this.objects.get(mesh);
		
		if(robj == null) {
			robj = new OGLRenderObject();
			robj.setMesh(mesh);
			robj.setZ(Projector.project(new Vector3f(mesh.getPosition()), cam).z);
			
			this.objects.put(mesh, robj);
		}
		
		BufferGeometry geom = this.geometries.get(mgeom);
		geom.updateFromObject(mesh);
		
		OGLVAOGeometry vao = this.update(geom);
		if(vao == null) return null;
		
		robj.setVAO(vao);
		
		return robj;
	}
	
	public void disposeAll() {
		for(BufferGeometry g : this.vaos.keySet()) {
			OGLVAOGeometry vao = this.vaos.get(g);
			
			if(vao != null) vao.dispose();
		}
		
		this.vaos.clear();
		this.objects.clear();
	}
}
