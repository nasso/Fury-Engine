package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import io.github.nasso.fury.core.LaunchSettings;

import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL;

public class OGLManager {
	public static final int VERSION_MAJOR = 3;
	public static final int VERSION_MINOR = 3;
	
	private static OGLManager singleton;
	
	// Capabilities and their properties
	public final boolean anisotropySupported;
	public final int maxAnisotropy;
	public final int maxCombinedTextureImageUnits;
	public final int maxFragmentTextureImageUnits;
	
	public final boolean useInterleavedVBOs;
	
	public OGLManager(LaunchSettings settings) {
		this.useInterleavedVBOs = true;
		
		GL.createCapabilities();
		
		this.anisotropySupported = GL.getCapabilities().GL_EXT_texture_filter_anisotropic;
		
		if(this.anisotropySupported) {
			this.maxAnisotropy = glGetInteger(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT);
			System.out.println("[OGLManager] + EXTTextureFilterAnisotropic supported");
		} else {
			this.maxAnisotropy = 1;
			System.out.println("[OGLManager] - EXTTextureFilterAnisotropic not supported");
		}
		
		this.maxCombinedTextureImageUnits = glGetInteger(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
		System.out.println("[OGLManager] " + this.maxCombinedTextureImageUnits + " max texture units (combined)");
		
		this.maxFragmentTextureImageUnits = glGetInteger(GL_MAX_TEXTURE_IMAGE_UNITS);
		System.out.println("[OGLManager] " + this.maxFragmentTextureImageUnits + " max texture units (fragment)");
	}
	
	public static OGLManager get() {
		return singleton;
	}
	
	public static OGLManager init(LaunchSettings settings) {
		if(singleton == null) singleton = new OGLManager(settings);
		
		return singleton;
	}
	
	public static void dispose() {
		singleton = null;
		
		GL.destroy();
	}
	
	// Utils
	public static void fastCheckError(int err) {
		String name = getError(err);
		
		if(name != null) System.err.println("OpenGL error: " + name);
		else System.out.println("No OpenGL error.");
	}
	
	public static void fastCheckError() {
		fastCheckError(glGetError());
	}
	
	public static String getError(int err) {
		String error = null;
		
		switch(err) {
			case GL_INVALID_ENUM:
				error = "GL_INVALID_ENUM";
				break;
			case GL_INVALID_VALUE:
				error = "GL_INVALID_VALUE";
				break;
			case GL_INVALID_OPERATION:
				error = "GL_INVALID_OPERATION";
				break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				error = "GL_INVALID_FRAMEBUFFER_OPERATION";
				break;
			case GL_OUT_OF_MEMORY:
				error = "GL_OUT_OF_MEMORY";
				break;
			case GL_STACK_UNDERFLOW:
				error = "GL_STACK_UNDERFLOW";
				break;
			case GL_STACK_OVERFLOW:
				error = "GL_STACK_OVERFLOW";
				break;
		}
		
		return error;
	}
	
	public static String getError() {
		return getError(glGetError());
	}
}
