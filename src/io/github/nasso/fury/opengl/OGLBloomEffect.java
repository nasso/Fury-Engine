package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL30.*;
import io.github.nasso.fury.graphics.fx.Bloom;
import io.github.nasso.fury.graphics.fx.PostEffect;

public class OGLBloomEffect extends OGLPostEffect {
	public static final int BLURRED_TEXTURE_COUNT = 3;
	private OGLGaussianBlurProgram gaussianProgram;
	private OGLBloomEffectProgram program;
	private OGLBloomEffectCombProgram combProgram;
	
	private OGLFramebuffer2D[] fbos1 = new OGLFramebuffer2D[BLURRED_TEXTURE_COUNT];
	private OGLTexture2D[] fbos1Target = new OGLTexture2D[BLURRED_TEXTURE_COUNT];
	
	private OGLFramebuffer2D[] fbos2 = new OGLFramebuffer2D[BLURRED_TEXTURE_COUNT];
	private OGLTexture2D[] fbos2Target = new OGLTexture2D[BLURRED_TEXTURE_COUNT];
	
	private float size = 1.0f;
	private float threshold = 0.9f;
	private float smoothness = 0.1f;
	private float strength = 0.5f;
	private int downsample = 4;
	
	public OGLBloomEffect(int w, int h) {
		this.gaussianProgram = new OGLGaussianBlurProgram();
		this.program = new OGLBloomEffectProgram();
		this.combProgram = new OGLBloomEffectCombProgram();
		
		for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++) {
			this.fbos1[i] = new OGLFramebuffer2D();
			this.fbos2[i] = new OGLFramebuffer2D();
			
			OGLTexture2D.Builder targetBuilder = new OGLTexture2D.Builder().width(Math.max(1, w / (this.downsample + i))).height(Math.max(1, h / (this.downsample + i))).internalFormat(GL_RGBA16).type(GL_FLOAT).magFilter(GL_LINEAR).minFilter(GL_NEAREST).wrapS(GL_MIRRORED_REPEAT).wrapT(GL_MIRRORED_REPEAT);
			this.fbos1Target[i] = targetBuilder.build();
			this.fbos2Target[i] = targetBuilder.build();
			
			this.fbos1[i].bind();
			this.fbos1[i].bindTexture(GL_COLOR_ATTACHMENT0, this.fbos1Target[i]);
			this.fbos2[i].bind();
			this.fbos2[i].bindTexture(GL_COLOR_ATTACHMENT0, this.fbos2Target[i]);
			this.fbos2[i].unbind();
		}
		
		this.setWidth(w);
		this.setHeight(h);
	}
	
	public void updateSize(int w, int h) {
		this.setWidth(w);
		this.setHeight(h);
		
		this.refreshFBOs();
	}
	
	private void refreshFBOs() {
		for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++) {
			int newWidth = Math.max(1, this.getWidth() / (this.downsample + i));
			int newHeight = Math.max(1, this.getHeight() / (this.downsample + i));
			
			this.fbos1Target[i].setWidth(newWidth);
			this.fbos1Target[i].setHeight(newHeight);
			
			this.fbos2Target[i].setWidth(newWidth);
			this.fbos2Target[i].setHeight(newHeight);
			
			this.fbos1Target[i].update();
			this.fbos2Target[i].update();
		}
	}
	
	public void applyVerticalBlur(OGLTexture2D sourceColor, int downsample, OGLFramebuffer2D dest) {
		int downWidth = Math.max(1, this.getWidth() / downsample);
		int downHeight = Math.max(1, this.getHeight() / downsample);
		
		glViewport(0, 0, downWidth, downHeight);
		
		this.gaussianProgram.loadToUniform("color", sourceColor, 0);
		this.gaussianProgram.loadToUniform("size", this.size);
		this.gaussianProgram.loadToUniform("textureLength", downHeight);
		this.gaussianProgram.loadToUniform("horizontal", false);
		
		dest.bind();
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}
	
	public void dispose() {
		this.gaussianProgram.dispose();
		this.program.dispose();
		this.combProgram.dispose();
		
		for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++) {
			this.fbos1[i].dispose();
			this.fbos1Target[i].dispose();
			
			this.fbos2[i].dispose();
			this.fbos2Target[i].dispose();
		}
	}
	
	public float getSize() {
		return this.size;
	}
	
	public void setSize(float size) {
		this.size = size;
	}
	
	public int getDownsample() {
		return this.downsample;
	}
	
	public void setDownsample(int downsample) {
		if(this.downsample != downsample) {
			this.downsample = downsample;
			
			this.refreshFBOs();
		}
	}
	
	public void apply(OGLTexture2D sourceColor, OGLTexture2D sourceDepth, OGLFramebuffer2D dest) {
		// Horizontal blur and downscale and bloom pass (high color with
		// threshold only) -> fbos1
		this.program.use();
		this.program.loadToUniform("color", sourceColor, 0);
		this.program.loadToUniform("size", this.size);
		this.program.loadToUniform("threshold", this.threshold);
		this.program.loadToUniform("smoothness", this.smoothness);
		for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++) {
			int downWidth = Math.max(1, this.getWidth() / (this.downsample + i));
			int downHeight = Math.max(1, this.getHeight() / (this.downsample + i));
			
			glViewport(0, 0, downWidth, downHeight);
			this.program.loadToUniform("textureLength", downWidth);
			
			this.fbos1[i].bind();
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
		
		// Vertical blur -> fbos2
		this.gaussianProgram.use();
		for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++)
			this.applyVerticalBlur(this.fbos1Target[i], this.downsample + i, this.fbos2[i]);
		
		// Comb fbos2 -> dest
		glViewport(0, 0, this.getWidth(), this.getHeight());
		dest.bind();
		this.combProgram.use();
		this.combProgram.loadToUniform("strength", this.strength);
		this.combProgram.loadToUniform("color", sourceColor, 0);
		for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++)
			this.combProgram.loadToUniform("blurredTexture[" + i + "]", this.fbos2Target[i], i + 1);
		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		this.combProgram.unuse();
		dest.unbind();
	}
	
	public float getThreshold() {
		return this.threshold;
	}
	
	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}
	
	public float getStrength() {
		return this.strength;
	}
	
	public void setStrength(float strength) {
		this.strength = strength;
	}
	
	public float getSmoothness() {
		return this.smoothness;
	}
	
	public void setSmoothness(float smoothness) {
		this.smoothness = smoothness;
	}
	
	public void update(PostEffect e) {
		if(e instanceof Bloom) {
			Bloom bloom = (Bloom) e;
			
			this.setSize(bloom.getSize());
			this.setDownsample(bloom.getDownsample());
			this.setThreshold(bloom.getThreshold());
			this.setStrength(bloom.getStrength());
			this.setSmoothness(bloom.getSmoothness());
		}
	}
}
