package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import io.github.nasso.fury.maths.Color;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.joml.Vector3i;
import org.joml.Vector4f;
import org.joml.Vector4i;
import org.lwjgl.BufferUtils;

public class OGLProgram {
	public static final int POSITION_ATTRIB_LOCATION = 0;
	public static final int NORMAL_ATTRIB_LOCATION = 1;
	public static final int UV_ATTRIB_LOCATION = 2;
	public static final int UV2_ATTRIB_LOCATION = 3;
	
	private Map<String, Integer> uniforms = new HashMap<String, Integer>();
	
	private int id;
	private int vertShader;
	private int fragShader;
	
	private String name;
	
	public OGLProgram(String programName, String vertSource, String fragSource) {
		this.name = programName;
		
		this.vertShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(this.vertShader, vertSource);
		
		this.fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(this.fragShader, fragSource);
		
		glCompileShader(this.vertShader);
		glCompileShader(this.fragShader);
		
		int vCompileStatus = glGetShaderi(this.vertShader, GL_COMPILE_STATUS);
		int fCompileStatus = glGetShaderi(this.fragShader, GL_COMPILE_STATUS);
		
		if(vCompileStatus == GL_FALSE) {
			System.err.println("Vertex shader compile error:");
			System.err.println(glGetShaderInfoLog(this.vertShader));
		}
		
		if(fCompileStatus == GL_FALSE) {
			System.err.println("Fragment shader compile error:");
			System.err.println(glGetShaderInfoLog(this.fragShader));
		}
		
		this.id = glCreateProgram();
		glAttachShader(this.id, this.vertShader);
		glAttachShader(this.id, this.fragShader);
	}
	
	public void setPositionAttrib(String name) {
		glBindAttribLocation(this.id, POSITION_ATTRIB_LOCATION, name);
	}
	
	public void setNormalAttrib(String name) {
		glBindAttribLocation(this.id, NORMAL_ATTRIB_LOCATION, name);
	}
	
	public void setUVAttrib(String name) {
		glBindAttribLocation(this.id, UV_ATTRIB_LOCATION, name);
	}
	
	public void setUV2Attrib(String name) {
		glBindAttribLocation(this.id, UV2_ATTRIB_LOCATION, name);
	}
	
	public void link() {
		glLinkProgram(this.id);
		
		int linkStatus = glGetProgrami(this.id, GL_LINK_STATUS);
		
		if(linkStatus == GL_FALSE) {
			System.err.println("Program link error:");
			System.err.println(glGetProgramInfoLog(this.id));
		}
		
		glDetachShader(this.id, this.vertShader);
		glDetachShader(this.id, this.fragShader);
		
		glDeleteShader(this.vertShader);
		glDeleteShader(this.fragShader);
	}
	
	public void loadUniforms(String... uniforms) {
		for(int i = 0, l = uniforms.length; i < l; i++)
			this.loadUniform(uniforms[i]);
	}
	
	public void loadUniform(String name) {
		this.loadUniform(name, name);
	}
	
	public void loadUniform(String shaderName, String name) {
		int loc = glGetUniformLocation(this.id, shaderName);
		
		if(loc == -1) System.out.println("Warning: Unfound or unused uniform '" + shaderName + "' in program '" + this.name + "'");
		
		this.uniforms.put(name, loc);
	}
	
	public int getUniformLoc(String name) {
		return this.uniforms.get(name);
	}
	
	public void loadToUniform(String name, boolean value) {
		this.loadToUniform(name, value ? 1 : 0);
	}
	
	public void loadToUniform(String name, int value) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform1i(uniform, value);
	}
	
	public void loadToUniform(String name, int a, int b) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform2i(uniform, a, b);
	}
	
	public void loadToUniform(String name, int a, int b, int c) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform3i(uniform, a, b, c);
	}
	
	public void loadToUniform(String name, int a, int b, int c, int d) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform4i(uniform, a, b, c, d);
	}
	
	public void loadToUniform(String name, float value) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform1f(uniform, value);
	}
	
	public void loadToUniform(String name, float a, float b) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform2f(uniform, a, b);
	}
	
	public void loadToUniform(String name, float a, float b, float c) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform3f(uniform, a, b, c);
	}
	
	public void loadToUniform(String name, float a, float b, float c, float d) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) glUniform4f(uniform, a, b, c, d);
	}
	
	public void loadToUniform(String name, Vector2i v) {
		this.loadToUniform(name, v.x, v.y);
	}
	
	public void loadToUniform(String name, Vector3i v) {
		this.loadToUniform(name, v.x, v.y, v.z);
	}
	
	public void loadToUniform(String name, Vector4i v) {
		this.loadToUniform(name, v.x, v.y, v.z, v.w);
	}
	
	public void loadToUniform(String name, Vector2f v) {
		this.loadToUniform(name, v.x, v.y);
	}
	
	public void loadToUniform(String name, Vector3f v) {
		this.loadToUniform(name, v.x, v.y, v.z);
	}
	
	public void loadToUniformRGB(String name, Color color) {
		this.loadToUniform(name, color.r, color.g, color.b);
	}
	
	public void loadToUniformRGBA(String name, Color color) {
		this.loadToUniform(name, color.r, color.g, color.b, color.a);
	}
	
	public void loadToUniform(String name, Vector4f v) {
		this.loadToUniform(name, v.x, v.y, v.z, v.w);
	}
	
	public void loadToUniform(String name, Matrix3f mat3) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) {
			FloatBuffer buffer = mat3.get(BufferUtils.createFloatBuffer(9));
			
			glUniformMatrix3fv(uniform, false, buffer);
		}
	}
	
	public void loadToUniform(String name, Matrix4f mat4) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) {
			FloatBuffer buffer = mat4.get(BufferUtils.createFloatBuffer(16));
			
			glUniformMatrix4fv(uniform, false, buffer);
		}
	}
	
	public void loadToUniform(String name, OGLTexture2D tex, int textureUnit) {
		if(tex == null || tex.isNull() || textureUnit < 0 || textureUnit >= OGLManager.get().maxFragmentTextureImageUnits) return;
		
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) {
			glUniform1i(uniform, textureUnit);
			
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			tex.bind();
		}
	}
	
	public void loadToUniform(String name, OGLCubeTexture tex, int textureUnit) {
		if(tex == null || tex.isNull() || textureUnit < 0 || textureUnit < OGLManager.get().maxFragmentTextureImageUnits) return;
		
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		if(uniform != -1) {
			glUniform1i(uniform, textureUnit);
			
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			tex.bind();
		}
		
	}
	
	public boolean hasUniform(String name) {
		int uniform = this.uniforms.getOrDefault(name, -1);
		
		return uniform != -1;
	}
	
	public void dispose() {
		if(glIsProgram(this.id)) {
			glDeleteProgram(this.id);
			this.id = 0;
		}
	}
	
	public void use() {
		glUseProgram(this.id);
	}
	
	public void unuse() {
		unuseAll();
	}
	
	public static void unuseAll() {
		glUseProgram(0);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
