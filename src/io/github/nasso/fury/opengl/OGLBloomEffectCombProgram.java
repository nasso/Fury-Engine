package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLBloomEffectCombProgram extends OGLPostFXProgram {
	public OGLBloomEffectCombProgram() {
		super("Bloom comb program", FuryUtils.readFile("res/shaders/postfx/bloomComb.fs", true), true, false);
		
		this.loadUniform("strength");
		for(int i = 0; i < OGLBloomEffect.BLURRED_TEXTURE_COUNT; i++)
			this.loadUniform("blurredTexture[" + i + "]");
	}
}
