package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.event.Event;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.graphics.CubeTexture;
import io.github.nasso.fury.graphics.Texture2D;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class OGLTextures {
	private Map<Texture2D, OGLTexture2D> textures2D = new HashMap<Texture2D, OGLTexture2D>();
	private Map<CubeTexture, OGLCubeTexture> cubeTextures = new HashMap<CubeTexture, OGLCubeTexture>();
	
	private Consumer<Event> texDisposer;
	
	public OGLTextures() {
		this.texDisposer = (e) -> {
			Observable tex = e.getSource();
			
			if(this.textures2D.containsKey(tex)) {
				this.textures2D.get(tex).dispose();
				this.textures2D.remove(tex);
			}
			
			if(this.cubeTextures.containsKey(tex)) {
				this.textures2D.get(tex).dispose();
				this.textures2D.remove(tex);
			}
			
			tex.removeEventListener("dispose", this.texDisposer);
		};
	}
	
	public void disposeAll() {
		// 2D clear
		for(Texture2D t : this.textures2D.keySet()) {
			OGLTexture2D tex = this.textures2D.get(t);
			
			if(tex != null) tex.dispose();
		}
		
		this.textures2D.clear();
		
		// Cube clear
		for(CubeTexture t : this.cubeTextures.keySet()) {
			OGLCubeTexture tex = this.cubeTextures.get(t);
			
			if(tex != null) tex.dispose();
		}
		
		this.cubeTextures.clear();
	}
	
	public OGLTexture2D update(Texture2D tex) {
		if(tex == null) return null;
		
		OGLTexture2D oglTex = this.textures2D.get(tex);
		
		if(oglTex == null) {
			oglTex = new OGLTexture2D(tex.getWidth(), tex.getHeight(), Fury.getOpenGLConst(tex.getMagFilter()), Fury.getOpenGLConst(tex.getMinFilter()), Fury.getOpenGLConst(tex.getWrapS()), Fury.getOpenGLConst(tex.getWrapT()), Fury.getOpenGLConst(tex.getInternalFormat()), Fury.getOpenGLConst(tex.getFormat()), Fury.getOpenGLConst(tex.getType()), tex.getAnisotropy(), tex.getData());
			oglTex.setVersion(tex.getVersion());
			
			tex.addEventListener("dispose", this.texDisposer);
			this.textures2D.put(tex, oglTex);
		} else if(oglTex.getVersion() != tex.getVersion()) {
			oglTex.setWidth(tex.getWidth());
			oglTex.setHeight(tex.getHeight());
			oglTex.setMagFilter(Fury.getOpenGLConst(tex.getMagFilter()));
			oglTex.setMinFilter(Fury.getOpenGLConst(tex.getMinFilter()));
			oglTex.setWrapS(Fury.getOpenGLConst(tex.getWrapS()));
			oglTex.setWrapT(Fury.getOpenGLConst(tex.getWrapT()));
			oglTex.setInternalFormat(Fury.getOpenGLConst(tex.getInternalFormat()));
			oglTex.setFormat(Fury.getOpenGLConst(tex.getFormat()));
			oglTex.setType(Fury.getOpenGLConst(tex.getType()));
			oglTex.setAnisotropy(tex.getAnisotropy());
			oglTex.setData(tex.getData());
			oglTex.update();
			
			oglTex.setVersion(tex.getVersion());
		}
		
		return oglTex;
	}
	
	public OGLCubeTexture update(CubeTexture tex) {
		if(tex == null) return null;
		
		OGLCubeTexture oglTex = this.cubeTextures.get(tex);
		
		if(oglTex == null) {
			oglTex = new OGLCubeTexture(tex.getWidth(), tex.getHeight(), Fury.getOpenGLConst(tex.getMagFilter()), Fury.getOpenGLConst(tex.getMinFilter()), Fury.getOpenGLConst(tex.getWrapR()), Fury.getOpenGLConst(tex.getWrapS()), Fury.getOpenGLConst(tex.getWrapT()), Fury.getOpenGLConst(tex.getInternalFormat()), Fury.getOpenGLConst(tex.getFormat()), Fury.getOpenGLConst(tex.getType()), tex.getAnisotropy(), tex.getPosXData(), tex.getNegXData(), tex.getPosYData(), tex.getNegYData(), tex.getPosZData(), tex.getNegZData());
			oglTex.setVersion(tex.getVersion());
			
			tex.addEventListener("dispose", this.texDisposer);
			this.cubeTextures.put(tex, oglTex);
		} else if(oglTex.getVersion() != tex.getVersion()) {
			oglTex.setWidth(tex.getWidth());
			oglTex.setHeight(tex.getHeight());
			oglTex.setMagFilter(Fury.getOpenGLConst(tex.getMagFilter()));
			oglTex.setMinFilter(Fury.getOpenGLConst(tex.getMinFilter()));
			oglTex.setWrapR(Fury.getOpenGLConst(tex.getWrapR()));
			oglTex.setWrapS(Fury.getOpenGLConst(tex.getWrapS()));
			oglTex.setWrapT(Fury.getOpenGLConst(tex.getWrapT()));
			oglTex.setInternalFormat(Fury.getOpenGLConst(tex.getInternalFormat()));
			oglTex.setFormat(Fury.getOpenGLConst(tex.getFormat()));
			oglTex.setType(Fury.getOpenGLConst(tex.getType()));
			oglTex.setAnisotropy(tex.getAnisotropy());
			oglTex.setPosXData(tex.getPosXData());
			oglTex.setNegXData(tex.getNegXData());
			oglTex.setPosYData(tex.getPosYData());
			oglTex.setNegYData(tex.getNegYData());
			oglTex.setPosZData(tex.getPosZData());
			oglTex.setNegZData(tex.getNegZData());
			oglTex.update();
			
			oglTex.setVersion(tex.getVersion());
		}
		
		return oglTex;
	}
}
