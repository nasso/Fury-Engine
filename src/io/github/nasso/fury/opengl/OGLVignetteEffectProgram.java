package io.github.nasso.fury.opengl;

import io.github.nasso.fury.core.FuryUtils;

public class OGLVignetteEffectProgram extends OGLPostFXProgram {
	public OGLVignetteEffectProgram() {
		super("Vignette effect program", FuryUtils.readFile("res/shaders/postfx/vignette.fs", true), true, false);
		
		this.loadUniforms("vignetteColor", "parameters");
	}
}
