package io.github.nasso.fury.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.data.TextureData;
import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.graphics.RenderPass;
import io.github.nasso.fury.graphics.RenderTarget;
import io.github.nasso.fury.graphics.RenderTarget2D;
import io.github.nasso.fury.graphics.ShadowProperties;
import io.github.nasso.fury.graphics.ShadowsProperties;
import io.github.nasso.fury.graphics.Texture2D;
import io.github.nasso.fury.graphics.fx.Bloom;
import io.github.nasso.fury.graphics.fx.BoxBlur;
import io.github.nasso.fury.graphics.fx.FXAA;
import io.github.nasso.fury.graphics.fx.Gamma;
import io.github.nasso.fury.graphics.fx.GaussianBlur;
import io.github.nasso.fury.graphics.fx.PostEffect;
import io.github.nasso.fury.graphics.fx.ToneMapping;
import io.github.nasso.fury.graphics.fx.Vignette;
import io.github.nasso.fury.level.Camera;
import io.github.nasso.fury.level.Fog;
import io.github.nasso.fury.level.Light;
import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Object3D;
import io.github.nasso.fury.level.Scene;
import io.github.nasso.fury.level.Sky;
import io.github.nasso.fury.maths.Frustum;
import io.github.nasso.fury.maths.Sphere;

public class OGLDeferredRenderer extends OGLRenderer {
	public static final int CLIP_SPHERE_W_SEGMENTS = 24;
	public static final int CLIP_SPHERE_H_SEGMENTS = 12;
	public static final int CLIP_CONE_SEGMENTS = 12;
	public static final int GBUFFER_DIFFUSE_LOC = 0;
	public static final int GBUFFER_POSITION_LOC = 1;
	public static final int GBUFFER_NORMAL_LOC = 2;
	
	private int[] gbufferDrawBuffers = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	
	// Managers
	private OGLTextures textures = new OGLTextures();
	private OGLObjects objects = new OGLObjects();
	private OGLRenderTargets renderTargets = new OGLRenderTargets(this.textures);
	private OGLCascadedShadowMaps cascadedShadowMaps;
	
	private List<OGLRenderObject> opaqueObjects = new ArrayList<OGLRenderObject>();
	private List<OGLRenderObject> transparentObjects = new ArrayList<OGLRenderObject>();
	private List<OGLRenderObject> shadowCasterObjects = new ArrayList<OGLRenderObject>();
	private List<Light> lights = new ArrayList<Light>();
	
	private Comparator<OGLRenderObject> painterSortStable;
	private Comparator<OGLRenderObject> reversePainterSortStable;
	
	private Material defaultMaterial = new Material();
	
	private Camera _camera;
	private Scene _scene;
	private Frustum _frustum = new Frustum();
	
	private OGLGBufferProgram gbufferProgram;
	private OGLDeferredRenderingProgram deferredProgram;
	private OGLDeferredFinalProgram finalProgram;
	private OGLFullTextureProgram fullProgram;
	private OGLDeferredStencilCullProgram stencilCullProgram;
	private OGLSkyboxProgram skyboxProgram;
	
	private OGLStencilCullPrimitives stencilCullPrimitives;
	
	private DeferredFramebuffers defBuffers;
	private Map<Integer, DeferredFramebuffers> sizedDefBuffers = new HashMap<Integer, DeferredFramebuffers>();
	
	private OGLVertexArray quadVAO;
	private OGLArrayBuffer quadPositionVBO;
	
	private Map<PostEffect, OGLPostEffect> postFXs = new HashMap<PostEffect, OGLPostEffect>();
	private Map<Integer, Map<PostEffect, OGLPostEffect>> sizedPostFXs = new HashMap<Integer, Map<PostEffect, OGLPostEffect>>();
	
	private boolean sortObjects = true;
	
	public OGLDeferredRenderer(int width, int height) {
		super(width, height);
		
		this.painterSortStable = (a, b) -> {
			// Sort by Material to optimize the uniform uploads
			Material aMat = a.getMesh().getMaterial();
			Material bMat = b.getMesh().getMaterial();
			
			if(aMat != bMat) if(aMat != null && bMat != null) return Integer.compare(aMat.id, bMat.id);
			
			if(a.getZ() != b.getZ()) return Float.compare(a.getZ(), b.getZ());
			
			return Long.compare(a.getID(), b.getID());
		};
		
		this.reversePainterSortStable = (a, b) -> {
			if(a.getZ() != b.getZ()) return Float.compare(a.getZ(), b.getZ());
			
			return Long.compare(a.getID(), b.getID());
		};
		
		this.cascadedShadowMaps = new OGLCascadedShadowMaps();
		
		this.gbufferProgram = new OGLGBufferProgram();
		this.deferredProgram = new OGLDeferredRenderingProgram();
		this.finalProgram = new OGLDeferredFinalProgram();
		this.fullProgram = new OGLFullTextureProgram();
		this.stencilCullProgram = new OGLDeferredStencilCullProgram();
		this.skyboxProgram = new OGLSkyboxProgram();
		
		this.defBuffers = new DeferredFramebuffers(width, height);
		
		this.quadVAO = new OGLVertexArray();
		this.quadPositionVBO = new OGLArrayBuffer();
		
		// Bind the VAO
		this.quadVAO.bind();
		
		// Setup the VBO
		this.quadPositionVBO.loadData(new float[] { -1, -1, 1, -1, -1, 1, 1, 1 });
		
		// Bind the VBO to the VAO at POSITION_ATTRIB_LOCATION
		this.quadVAO.setAttribLocationEnabled(OGLProgram.POSITION_ATTRIB_LOCATION, true);
		this.quadVAO.loadVBOToAttrib(OGLProgram.POSITION_ATTRIB_LOCATION, 2, this.quadPositionVBO);
		
		// Unbind everything
		this.quadPositionVBO.unbind();
		this.quadVAO.unbind();
		
		this.stencilCullPrimitives = new OGLStencilCullPrimitives(CLIP_SPHERE_W_SEGMENTS, CLIP_SPHERE_H_SEGMENTS, CLIP_CONE_SEGMENTS);
	}
	
	private static int getSizeHashCode(int x, int y) {
		// Assume x and y to be < Short.MAX_VALUE * 2
		x -= Short.MAX_VALUE;
		y -= Short.MAX_VALUE;
		
		return ((x << 16) & 0xFFFFFFFF) | (y & 0xFFFFFFFF);
	}
	
	private Map<RenderTarget, Integer> usesCount = new HashMap<RenderTarget, Integer>();
	
	public void render(List<RenderPass> passes) {
		// Delete unused deferred buffers
		Set<Integer> sizeHashSet = this.sizedDefBuffers.keySet();
		sizes: for(int sizeHash : sizeHashSet) {
			for(int i = 0, l = passes.size(); i < l; i++) {
				RenderPass pass = passes.get(i);
				
				if(pass.getTarget() != null) if(pass.getTarget() instanceof RenderTarget2D) {
					Texture2D passTargetTex = ((RenderTarget2D) pass.getTarget()).getTexture();
					
					if(getSizeHashCode(passTargetTex.getWidth(), passTargetTex.getHeight()) == sizeHash) continue sizes;
				}
			}
			
			// If da loop hasn't restart here, it means that we found no render
			// pass using the size !
			// Disabled passes aren't deleted. To delete a pass, it must be
			// removed from the render pass stack
			
			// Now let's delete everything using this OLD size !
			this.sizedDefBuffers.get(sizeHash).dispose();
			this.sizedDefBuffers.remove(sizeHash);
		}
		
		sizeHashSet = this.sizedPostFXs.keySet();
		sizes: for(int sizeHash : sizeHashSet) {
			for(int i = 0, l = passes.size(); i < l; i++) {
				RenderPass pass = passes.get(i);
				
				if(pass.getTarget() != null) if(pass.getTarget() instanceof RenderTarget2D) {
					Texture2D passTargetTex = ((RenderTarget2D) pass.getTarget()).getTexture();
					
					if(getSizeHashCode(passTargetTex.getWidth(), passTargetTex.getHeight()) == sizeHash) continue sizes;
				}
			}
			
			Map<PostEffect, OGLPostEffect> sizePostFXs = this.sizedPostFXs.get(sizeHash);
			if(sizePostFXs != null) {
				Set<PostEffect> keySet = sizePostFXs.keySet();
				for(PostEffect e : keySet)
					sizePostFXs.get(e).dispose();
				
				sizePostFXs.clear();
			}
			
			this.sizedPostFXs.remove(sizeHash);
		}
		
		for(int i = 0, l = passes.size(); i < l; i++) {
			RenderPass pass = passes.get(i);
			
			if(pass.getTarget() != null) this.usesCount.put(pass.getTarget(), 0);
		}
		
		int uses = 0;
		for(int i = 0, defaultUses = 0, l = passes.size(); i < l; i++) {
			RenderPass pass = passes.get(i);
			
			if(!pass.isEnabled()) continue;
			
			if(pass.getTarget() == null) {
				uses = defaultUses;
				defaultUses++;
			} else {
				int targetUses = this.usesCount.get(pass.getTarget());
				uses = targetUses;
				
				targetUses++;
				this.usesCount.put(pass.getTarget(), targetUses);
			}
			
			this.render(pass, uses == 0);
		}
	}
	
	private List<OGLShadowMap> _maps = new ArrayList<OGLShadowMap>();
	private Vector3f _lightPos = new Vector3f();
	private Vector3f _lightPosWorld = new Vector3f();
	private Vector3f _lightDir = new Vector3f();
	private Matrix4f _pvm = new Matrix4f();
	private Matrix4f _model = new Matrix4f();
	private Matrix4f _viewModel = new Matrix4f();
	private Matrix4f _viewInv = new Matrix4f();
	private Matrix4f _projInv = new Matrix4f();
	private Matrix4f _projViewInv = new Matrix4f();
	private Matrix4f _projViewSky = new Matrix4f();
	
	public void render(RenderPass pass, boolean clear) {
		Scene sce = pass.getScene();
		Camera cam = pass.getCamera();
		RenderTarget aTarget = pass.getTarget();
		List<PostEffect> postFXPipeline = pass.getPostEffects();
		
		RenderTarget2D target = null;
		if(aTarget instanceof RenderTarget2D) target = (RenderTarget2D) aTarget;
		
		// Auto add camera
		if(!sce.hasChild(cam)) {
			System.out.println("[OGLDeferredRenderer] The camera \"" + cam.getName() + "\" has been automatically added to the scene \"" + sce.getName() + "\"");
			sce.addChildren(cam);
		}
		
		int width = this.getWidth();
		int height = this.getHeight();
		int sizeHash = getSizeHashCode(width, height);
		
		if(sce == null || cam == null) return;
		
		cam.updateMatrixWorld();
		
		this._scene = sce;
		this._camera = cam;
		if(cam.isFrustumCulled()) this._frustum.setFromMatrix(this._camera.getProjViewMatrix());
		
		this.opaqueObjects.clear();
		this.transparentObjects.clear();
		this.shadowCasterObjects.clear();
		this.lights.clear();
		
		// Projecting the scene will add all objects to the right list
		this.projectObject(sce);
		this.projectObjectShadow(sce);
		
		if(this.sortObjects) {
			this.opaqueObjects.sort(this.painterSortStable);
			this.shadowCasterObjects.sort(this.painterSortStable);
			this.transparentObjects.sort(this.reversePainterSortStable);
		}
		
		DeferredFramebuffers usedBuffers = this.defBuffers;
		
		if(target != null) {
			width = target.getTexture().getWidth();
			height = target.getTexture().getHeight();
			
			if(width != this.getWidth() || height != this.getHeight()) {
				sizeHash = getSizeHashCode(width, height);
				
				usedBuffers = this.sizedDefBuffers.get(sizeHash);
				
				if(usedBuffers == null) {
					usedBuffers = new DeferredFramebuffers(width, height);
					
					this.sizedDefBuffers.put(sizeHash, usedBuffers);
				}
			}
		}
		
		GBuffer gbuffer = usedBuffers.getGBuffer();
		PBuffer pbuffer = usedBuffers.getPBuffer();
		PostFXPaddle[] paddles = usedBuffers.getPostFXPaddles();
		
		glViewport(0, 0, width, height);
		// region GEOM_PASS
		//
		// [ GEOMETRY PASS ]
		//
		
		// Bind the gbuffer
		gbuffer.getFBO().bind();
		
		// Setup draw buffers GL_COLOR_ATTACHMENT[0, 1, 2]
		glDrawBuffers(this.gbufferDrawBuffers);
		
		// Clear all the draw buffers of the gbuffer with 0 alpha
		glClearColor(0f, 0f, 0f, 0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		// Render all the scene using the gbuffer shader
		this.renderPass(this.opaqueObjects, this.gbufferProgram);
		// TODO: transparent objects
		
		// After that, we don't draw to the depth buffer anymore
		glDrawBuffers(GL_COLOR_ATTACHMENT0);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		// endregion GEOM_PASS
		
		// --- Full screen part ---
		this.quadVAO.bind();
		
		// region LIGHT_PASS
		
		//
		// [ LIGHT PASS ]
		//
		
		// Unbind the gbuffer and bind the pbuffer
		pbuffer.getFBO().bind();
		glDrawBuffers(GL_COLOR_ATTACHMENT0);
		
		// Clear.
		glClear(GL_COLOR_BUFFER_BIT);
		
		// Enable additive blending
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_ONE, GL_ONE);
		
		glEnable(GL_STENCIL_TEST);
		
		cam.getMatrixWorld().get(this._viewInv);
		cam.getProjectionMatrix().invert(this._projInv);
		
		this.deferredProgram.use();
		this.deferredProgram.loadToUniform("viewInv", this._viewInv);
		this.deferredProgram.loadToUniform("projInv", this._projInv);
		
		if(this.lights.isEmpty() || !sce.isLighting()) {
			this.deferredProgram.loadToUniform("gbuffer_position", gbuffer.getPositionTarget(), 0);
			
			this.deferredProgram.loadToUniform("light.type", -1);
			this.deferredProgram.loadToUniform("lightingEnabled", sce.isLighting());
			if(!sce.isLighting()) this.deferredProgram.loadToUniform("gbuffer_diffuse", gbuffer.getDiffuseTarget(), 1);
			
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		} else for(int i = 0, l = this.lights.size(); i < l; i++) {
			Light light = this.lights.get(i);
			int lightType = light.getType();
			
			if(light.getEnergy() == 0.0f || (!light.isDiffuse() && !light.isSpecular()) || (light.getColor().x == 0.0f && light.getColor().y == 0.0f && light.getColor().z == 0.0f)) continue;
			
			if(lightType == Fury.LIGHT_TYPE_POINT || lightType == Fury.LIGHT_TYPE_SPOT) {
				if(light.getDistance() == 0.0f) continue;
			} else if(lightType == Fury.LIGHT_TYPE_SPOT) if(light.getConeSize() == 0.0f) continue;
			
			cam.getViewMatrix().mul(light.getMatrixWorld(), this._viewModel);
			this._lightPosWorld.set(0f).mulPosition(light.getMatrixWorld()).mulPosition(cam.getViewMatrix(), this._lightPos);
			this._lightDir.set(0, 0, -1).mulDirection(this._viewModel).normalize();
			
			if(lightType == Fury.LIGHT_TYPE_POINT || lightType == Fury.LIGHT_TYPE_SPOT) { // Spot uses sphere
				// Clear the stencil buffer
				glEnable(GL_STENCIL_TEST);
				glStencilFunc(GL_EQUAL, 0, 0xFF);
				glStencilMask(0xFF); // write 1
				glDepthMask(false);
				glClear(GL_STENCIL_BUFFER_BIT);
				
				// Clip to the light bounding sphere if it's a point light
				this.stencilCullProgram.use();
				
				this._pvm.identity();
				this._model.identity().translate(this._lightPosWorld).scale(light.getDistance() + 0.1f); // +
																											// offset
				cam.getViewMatrix().mul(this._model, this._pvm);
				cam.getProjectionMatrix().mul(this._pvm, this._pvm);
				
				this.stencilCullProgram.loadToUniform("proj*(view*model)", this._pvm);
				
				// The gbuffer has a depth_stencil buffer.
				// - The depth buffer contains the depth of the scene. We need
				// it to clip, but don't write to it !
				// - The stencil buffer is cleared here, because we'll write the
				// light clipping information (sphere).
				
				// And: The pbuffer has the same depth_stencil buffer !!! No
				// need to copy it !
				// But, we must be sure to write only on the stencil buffer, and
				// don't touch the depth buffer
				
				// USING HERE: pbuffer
				
				// No color and no depth write
				glColorMask(false, false, false, false);
				glStencilMask(0xFF); // write 1
				
				// Setup to write light volume: STENCIL_TEST and DEPTH_TEST
				glEnable(GL_DEPTH_TEST);
				
				glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
				glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);
				
				// Here we go, draw the light bounding sphere
				
				this.stencilCullPrimitives.getSphere().bind();
				glDrawElements(GL_TRIANGLES, this.stencilCullPrimitives.getSphereIndicesSize(), GL_UNSIGNED_INT, 0);
				
				// Now, let the stencil clip everything out of the light volume
				glStencilFunc(GL_NOTEQUAL, 0, 0xFF); // 0xFF = 1111 1111 = all
														// bits = no mask
				
				glDisable(GL_DEPTH_TEST);
				
				// Set back the write states
				glColorMask(true, true, true, true);
				glStencilMask(0x00); // disable writes
				
				// Set back the things like they were
				this.quadVAO.bind(); // VAO
				this.deferredProgram.use(); // PROGRAM
				glDepthMask(true);
			} else glDisable(GL_STENCIL_TEST);
			
			// Others type of light (AMBIENT, SUN) can't be clipped.
			
			// region updateshadows
			ShadowsProperties props = light.getShadowsProps();
			this._maps.clear();
			
			List<OGLShadowMap> updatedCascades = this.cascadedShadowMaps.update(this.shadowCasterObjects, light, cam, sce.getOverrideMaterial(), this.textures);
			
			if(updatedCascades != null) this._maps.addAll(updatedCascades);
			
			glViewport(0, 0, width, height);
			
			this.deferredProgram.use(); // use back the program because updating
										// the shadow maps change it
										// Must rebind the gbuffer target cause the shadow maps updater can
										// use them
			this.deferredProgram.loadToUniform("gbuffer_diffuse", gbuffer.getDiffuseTarget(), 0);
			this.deferredProgram.loadToUniform("gbuffer_position", gbuffer.getPositionTarget(), 1);
			this.deferredProgram.loadToUniform("gbuffer_normal", gbuffer.getNormalTarget(), 2);
			this.deferredProgram.loadToUniform("gbuffer_depth", gbuffer.getDepthTarget(), 3);
			pbuffer.getFBO().bind();
			this.quadVAO.bind();
			
			// Upload to uniforms
			if(this._maps.isEmpty()) this.deferredProgram.loadToUniform("light.shadows", false);
			else {
				int cascadeCount = this._maps.size();
				
				for(int s = 0; s < cascadeCount; s++) {
					OGLShadowMap shadowMap = this._maps.get(s);
					if(shadowMap == null) continue;
					
					ShadowProperties cascadeProps = props.getShadowProperties(s);
					
					int bufferType = cascadeProps.getType();
					
					// active texture = 4 because there is gbuffer
					this.deferredProgram.loadToUniform("depthMap[" + s + "]", shadowMap.getDepthTexture(), 4 + s);
					
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].shadowMat", shadowMap.getLightProjView());
					
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].poissonSpread", cascadeProps.getPoissonSpread());
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].bias", cascadeProps.getBias());
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].near", shadowMap.getCascadeZNear());
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].far", shadowMap.getCascadeZFar());
					
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].poissonSamples", cascadeProps.getPoissonSamples());
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].bufferType", bufferType);
					
					this.deferredProgram.loadToUniform("light.shadowMap.cascades[" + s + "].normalCull", cascadeProps.isNormalCulling());
				}
				
				this.deferredProgram.loadToUniform("light.shadowMap.cascadeDistanceFade", props.getCascadeDistanceFade());
				this.deferredProgram.loadToUniform("light.shadowMap.interCascadeFade", props.getInterCascadeFade());
				this.deferredProgram.loadToUniform("light.shadowMap.cascadeMaxDistance", props.getCascadeMaxDistance());
				this.deferredProgram.loadToUniform("light.shadowMap.cascadeDistribExp", props.getCascadeDistributionExp());
				
				this.deferredProgram.loadToUniform("light.shadowMap.cascadeCount", cascadeCount);
				
				this.deferredProgram.loadToUniform("light.shadows", true);
			}
			// endregion shadows
			
			this.deferredProgram.loadToUniform("light.color", light.getColor());
			this.deferredProgram.loadToUniform("light.groundColor", light.getGroundColor());
			this.deferredProgram.loadToUniform("light.position", this._lightPos);
			this.deferredProgram.loadToUniform("light.direction", this._lightDir);
			
			this.deferredProgram.loadToUniform("light.type", light.getType());
			
			this.deferredProgram.loadToUniform("light.energy", light.getEnergy());
			this.deferredProgram.loadToUniform("light.dist", light.getDistance());
			this.deferredProgram.loadToUniform("light.coneSize", light.getConeSize());
			this.deferredProgram.loadToUniform("light.coneBlend", light.getConeBlend());
			
			this.deferredProgram.loadToUniform("light.blinnPhong", light.isBlinnPhong());
			this.deferredProgram.loadToUniform("light.specular", light.isSpecular());
			this.deferredProgram.loadToUniform("light.diffuse", light.isDiffuse());
			
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
		
		glDisable(GL_STENCIL_TEST);
		glDisable(GL_DEPTH_TEST);
		// endregion LIGHT_PASS
		
		// region SKYBOX_PASS
		// Render to the PostFX fbo 1
		paddles[0].getFBO().bind();
		
		if(clear) {
			if(sce.getSky() != null) { // Skybox stuff
				glDisable(GL_BLEND);
				
				Sky sky = sce.getSky();
				OGLCubeTexture cubeTexture = this.textures.update(sce.getSky().getCubeTexture());
				
				Matrix4f proj = cam.getProjectionMatrix();
				cam.getViewMatrix().get(this._projViewSky).setTranslation(0, 0, 0);
				
				Matrix4f projView = proj.mul(this._projViewSky, this._projViewSky);
				
				this.skyboxProgram.use();
				this.skyboxProgram.loadToUniform("sky.type", sky.getType());
				this.skyboxProgram.loadToUniform("sky.cubeTexture", cubeTexture, 0);
				
				this.skyboxProgram.loadToUniform("sky.up", sky.getUp());
				this.skyboxProgram.loadToUniformRGB("sky.bottomColor", sky.getBottomColor());
				this.skyboxProgram.loadToUniformRGB("sky.topColor", sky.getTopColor());
				
				this.skyboxProgram.loadToUniform("sky.cosPower", sky.getCosPower());
				
				List<Light> suns = sky.getSuns();
				int sunCount = 0;
				for(int i = 0, l = suns.size(); i < l; i++) {
					Light sun = suns.get(i);
					
					if(sun.getType() != Fury.LIGHT_TYPE_SUN) continue;
					
					this.skyboxProgram.loadToUniform("sky.sun[" + i + "].color", sun.getColor());
					this.skyboxProgram.loadToUniform("sky.sun[" + i + "].direction", this._lightDir.set(0, 0, -1).mulDirection(sun.getMatrixWorld()));
					this.skyboxProgram.loadToUniform("sky.sun[" + i + "].energy", sun.getEnergy());
					this.skyboxProgram.loadToUniform("sky.sun[" + i + "].innerEdge", sun.getSunInnerEdge());
					this.skyboxProgram.loadToUniform("sky.sun[" + i + "].outerEdge", sun.getSunOuterEdge());
					this.skyboxProgram.loadToUniform("sky.sun[" + i + "].brightness", sun.getSunBrightness());
					sunCount++;
				}
				this.skyboxProgram.loadToUniform("sky.sunCount", sunCount);
				this.skyboxProgram.loadToUniform("proj*[view, notranslate]", projView);
				this.skyboxProgram.loadToUniform("scale", sce.getSkyboxDistance());
				
				this.stencilCullPrimitives.getCube().bind();
				glDrawElements(GL_TRIANGLES, this.stencilCullPrimitives.getCubeIndicesSize(), GL_UNSIGNED_INT, 0);
				
				this.skyboxProgram.unuse();
				
				// Set back the thing like they were
				glEnable(GL_BLEND);
				this.quadVAO.bind(); // Rebind the quad vao !
			} else {
				glClearColor(0f, 0f, 0f, 1f);
				glClear(GL_COLOR_BUFFER_BIT);
			}
		} else {
			glClearColor(0f, 0f, 0f, 0f);
			glClear(GL_COLOR_BUFFER_BIT);
		}
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		// endregion SKYBOX_PASS
		
		// region FOG_PASS
		cam.getProjViewMatrix().invert(this._projViewInv);
		
		this.finalProgram.use();
		this.finalProgram.loadToUniform("projViewInv", this._projViewInv);
		this.finalProgram.loadToUniform("camPos", cam.getPosition());
		
		this.finalProgram.loadToUniform("color", pbuffer.getTarget(), 0);
		this.finalProgram.loadToUniform("depth", gbuffer.getDepthTarget(), 1);
		
		if(sce.getFog() != null) {
			Fog fog = sce.getFog();
			
			this.finalProgram.loadToUniform("fog.enabled", true);
			this.finalProgram.loadToUniform("fog.type", fog.getType());
			this.finalProgram.loadToUniformRGB("fog.color", fog.getColor());
			this.finalProgram.loadToUniform("fog.near", fog.getNear());
			this.finalProgram.loadToUniform("fog.far", fog.getFar());
			this.finalProgram.loadToUniform("fog.density", fog.getDensity());
		} else this.finalProgram.loadToUniform("fog.enabled", false);
		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		this.finalProgram.unuse();
		glDisable(GL_BLEND);
		// endregion FOG_PASS
		
		// region POST_FX_PASS
		// PostFX !! ping ponging
		OGLTexture2D finalTexture = paddles[0].getTarget();
		for(int i = 0, l = postFXPipeline.size(); i < l; i++) {
			PostEffect absEffect = postFXPipeline.get(i);
			
			if(!absEffect.isEnabled()) continue;
			
			// Ping-Pong with fbos
			OGLTexture2D sourceColor = paddles[0].getTarget();
			OGLFramebuffer2D dest = paddles[1].getFBO();
			
			if(finalTexture == paddles[0].getTarget()) // If the previous pass
				// was on the target 1
				// Just use the target 2 now (switch)
				finalTexture = paddles[1].getTarget();
			else if(finalTexture == paddles[1].getTarget()) {
				// Same but reversed
				finalTexture = paddles[0].getTarget();
				
				sourceColor = paddles[1].getTarget();
				dest = paddles[0].getFBO();
				
				// source = target 2
				// dest = target 1
			} else {
				// wtf
				System.out.println("We lost the ball during the Ping-Pong match with FBOs.");
				continue;
			}
			
			boolean useDefault = absEffect instanceof Gamma || absEffect instanceof ToneMapping || absEffect instanceof Vignette || usedBuffers == this.defBuffers;
			
			OGLPostEffect oglEffect;
			if(useDefault) oglEffect = this.postFXs.get(absEffect);
			else {
				Map<PostEffect, OGLPostEffect> sizePostFXs = this.sizedPostFXs.get(sizeHash);
				
				if(sizePostFXs == null) {
					sizePostFXs = new HashMap<PostEffect, OGLPostEffect>();
					this.sizedPostFXs.put(sizeHash, sizePostFXs);
				}
				
				oglEffect = sizePostFXs.get(absEffect);
			}
			
			if(oglEffect == null) {
				if(absEffect instanceof Gamma) oglEffect = new OGLGammaFilter();
				else if(absEffect instanceof ToneMapping) oglEffect = new OGLToneMappingFilter();
				else if(absEffect instanceof Vignette) oglEffect = new OGLVignetteEffect();
				else if(absEffect instanceof GaussianBlur) oglEffect = new OGLGaussianBlur(width, height);
				else if(absEffect instanceof BoxBlur) oglEffect = new OGLBoxBlur(width, height);
				else if(absEffect instanceof Bloom) oglEffect = new OGLBloomEffect(width, height);
				else if(absEffect instanceof FXAA) oglEffect = new OGLFXAAFilter(width, height);
				
				if(useDefault) this.postFXs.put(absEffect, oglEffect);
				else {
					Map<PostEffect, OGLPostEffect> sizePostFXs = this.sizedPostFXs.get(sizeHash);
					
					sizePostFXs.put(absEffect, oglEffect);
				}
			}
			
			oglEffect.update(absEffect);
			oglEffect.apply(sourceColor, gbuffer.getDepthTarget(), dest);
		}
		// endregion POST_FX_PASS
		
		// region FINAL_PASS
		if(target != null) {
			OGLRenderTarget2D oglTarget = this.renderTargets.update(target);
			
			oglTarget.getFBO().bind();
		} else OGLFramebuffer2D.unbindAll();
		
		glEnable(GL_BLEND);
		
		this.fullProgram.use();
		this.fullProgram.loadToUniform("color", finalTexture, 0);
		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		this.fullProgram.unuse();
		
		this.quadVAO.unbind();
		
		glDisable(GL_BLEND);
		
		if(target != null) OGLFramebuffer2D.unbindAll();
	}
	
	public void setWidth(int w) {
		if(this.getWidth() == w) return;
		
		super.setWidth(w);
		
		this.updateSize();
	}
	
	public void setHeight(int h) {
		if(this.getHeight() == h) return;
		
		super.setHeight(h);
		
		this.updateSize();
	}
	
	public void setSize(int w, int h) {
		// Ok, so this branch save ~3 ms. That's a lot. Trust me.
		if(this.getWidth() == w && this.getHeight() == h) return;
		
		super.setSize(w, h);
		
		this.updateSize();
	}
	
	private void updateSize() {
		int width = this.getWidth();
		int height = this.getHeight();
		
		this.defBuffers.updateSize(width, height);
		
		Set<PostEffect> keySet = this.postFXs.keySet();
		for(PostEffect e : keySet)
			this.postFXs.get(e).updateSize(width, height);
	}
	
	private Map<OGLProgram, Material> _lastMaterials = new HashMap<OGLProgram, Material>();
	
	private void uploadMaterialUniforms(OGLProgram program, Material mat) {
		Material lastMaterial = this._lastMaterials.get(program);
		if(lastMaterial == mat) return;
		
		OGLTexture2D oglDifTex = this.textures.update(mat.getDiffuseTexture());
		OGLTexture2D oglNormTex = this.textures.update(mat.getNormalTexture());
		OGLTexture2D oglSpecTex = this.textures.update(mat.getSpecularTexture());
		
		program.loadToUniform("material.diffuseTexture", oglDifTex, 0);
		program.loadToUniform("material.normalTexture", oglNormTex, 1);
		program.loadToUniform("material.specularTexture", oglSpecTex, 2);
		
		program.loadToUniform("material.hasDiffuseTexture", oglDifTex != null);
		program.loadToUniform("material.hasNormalTexture", oglNormTex != null);
		program.loadToUniform("material.hasSpecularTexture", oglSpecTex != null);
		
		program.loadToUniform("material.diffuseColor", mat.getDiffuseColor());
		
		program.loadToUniform("material.uvScaling", mat.getUVScaling());
		program.loadToUniform("material.normalMapIntensity", mat.getNormalMapIntensity());
		program.loadToUniform("material.opacity", mat.getOpacity());
		program.loadToUniform("material.shininess", mat.getShininess());
		program.loadToUniform("material.reflectivity", mat.getReflectivity());
		program.loadToUniform("material.specularIntensity", mat.getSpecularIntensity());
		program.loadToUniform("material.alphaThreshold", mat.getAlphaThreshold());
		
		program.loadToUniform("material.transparent", mat.isTransparent());
		program.loadToUniform("material.opaque", mat.isOpaque());
		program.loadToUniform("material.depthTest", mat.isDepthTest());
		program.loadToUniform("material.depthWrite", mat.isDepthWrite());
		program.loadToUniform("material.faceSideNormalCorrection", mat.isFaceSideNormalCorrection());
		
		glPolygonMode(GL_FRONT_AND_BACK, Fury.getOpenGLConst(mat.getPolygonMode()));
		glPointSize(mat.getPointSize());
		glLineWidth(mat.getLineWidth());
		
		if(mat.isDepthTest()) glEnable(GL_DEPTH_TEST);
		else glDisable(GL_DEPTH_TEST);
		
		if(mat.isDepthWrite()) glDepthMask(true);
		else glDepthMask(false);
		
		this._lastMaterials.put(program, mat);
	}
	
	private void renderPass(List<OGLRenderObject> objects, OGLProgram program) {
		program.use();
		
		// Try to load the uniform anyway because there's no expensive
		// computation needed (just a getter)
		// If the uniform doesn't exists in the shaders, it'll automatically do
		// nothing. (see OGLProgram::loadToUniform)
		program.loadToUniform("proj", this._camera.getProjectionMatrix());
		program.loadToUniform("view", this._camera.getViewMatrix());
		
		if(program.hasUniform("proj*view")) program.loadToUniform("proj*view", this._camera.getProjViewMatrix());
		
		this.renderObjects(objects, this._scene.getOverrideMaterial(), program);
	}
	
	private void renderObjects(List<OGLRenderObject> objects, Material materialUniform, OGLProgram program) {
		for(int i = 0, l = objects.size(); i < l; i++) {
			OGLRenderObject obj = objects.get(i);
			
			this.renderObject(obj, materialUniform, program);
		}
		
		this._lastMaterials.clear();
	}
	
	private Matrix3f _normalMat = new Matrix3f();
	
	private void renderObject(OGLRenderObject obj, Material materialUniform, OGLProgram program) {
		Material mat = materialUniform;
		
		if(mat == null) mat = obj.getMesh().getMaterial();
		
		if(mat == null) mat = this.defaultMaterial;
		
		AbstractGeometry ageom = obj.getMesh().getGeometry();
		if(ageom == null) return;
		
		this.uploadMaterialUniforms(program, mat);
		
		program.loadToUniform("model", obj.getMesh().getMatrixWorld());
		program.loadToUniform("receiveShadows", obj.getMesh().isReceiveShadow());
		
		this._camera.getProjectionMatrix().mul(this._camera.getViewMatrix().mul(obj.getMesh().getMatrixWorld(), this._viewModel), this._pvm);
		program.loadToUniform("view*model", this._viewModel);
		program.loadToUniform("proj*(view*model)", this._pvm);
		program.loadToUniform("normal(view*model)", this._viewModel.normal(this._normalMat));
		
		OGLVAOGeometry vaoGeom = obj.getVAO();
		vaoGeom.bind();
		
		if(ageom.getCullFace() != Fury.NONE) {
			glEnable(GL_CULL_FACE);
			glCullFace(Fury.getOpenGLConst(ageom.getCullFace()));
		}
		
		if(vaoGeom.hasIndices()) glDrawElements(Fury.getOpenGLConst(ageom.getRenderMode()), vaoGeom.getVertexCount(), GL_UNSIGNED_INT, 0);
		else glDrawArrays(Fury.getOpenGLConst(ageom.getRenderMode()), 0, vaoGeom.getVertexCount());
		
		glDisable(GL_CULL_FACE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	
	private void projectObject(Object3D o) {
		if(!o.isVisible()) return;
		
		if(o.isMatrixAutoUpdate()) o.updateMatrixWorld(); // Update if needed
		
		this.prepareRenderItem(o);
		
		for(int i = 0, l = o.getChildren().size(); i < l; i++)
			this.projectObject(o.getChildren().get(i));
	}
	
	private void projectObjectShadow(Object3D obj) {
		if(!obj.isVisible() || !obj.isCastShadow()) return;
		
		if(obj instanceof Mesh) {
			Mesh mesh = (Mesh) obj;
			
			if(mesh == null || mesh.getGeometry() == null) return;
			
			OGLRenderObject m = this.objects.update(mesh, this._camera);
			
			this.shadowCasterObjects.add(m);
		}
		
		for(int i = 0, l = obj.getChildren().size(); i < l; i++)
			this.projectObjectShadow(obj.getChildren().get(i));
	}
	
	public List<Light> getLights() {
		return this.lights;
	}
	
	private Sphere _sphere = new Sphere();
	
	private void prepareRenderItem(Object3D obj) {
		if(obj instanceof Mesh) {
			List<OGLRenderObject> list;
			Mesh mesh = (Mesh) obj;
			
			if(mesh == null || mesh.getGeometry() == null) return;
			
			// Frustum cull
			if(this._camera.isFrustumCulled() && mesh.isFrustumCulled() && !this._frustum.intersectsMesh(mesh)) return;
			
			if(mesh.getMaterial() != null && mesh.getMaterial().isTransparent()) list = this.transparentObjects;
			else list = this.opaqueObjects;
			
			OGLRenderObject m = this.objects.update(mesh, this._camera);
			
			if(m != null) list.add(m);
		} else if(obj instanceof Light) {
			Light light = (Light) obj;
			
			if(!light.isEnabled()) return;
			
			if(light.isFrustumCulled() && (light.getType() == Fury.LIGHT_TYPE_POINT || light.getType() == Fury.LIGHT_TYPE_SPOT)) if(!this._frustum.intersectsSphere(this._sphere.setPosition(light.getPosition()).setRadius(light.getDistance()))) return;
			
			this.lights.add(light);
		}
	}
	
	public boolean isSortObjects() {
		return this.sortObjects;
	}
	
	public void setSortObjects(boolean sortObjects) {
		this.sortObjects = sortObjects;
	}
	
	public void dispose() {
		// Ensure everything is clear
		this.opaqueObjects.clear();
		this.transparentObjects.clear();
		this.shadowCasterObjects.clear();
		this.lights.clear();
		
		this.renderTargets.disposeAll(true);
		this.objects.disposeAll();
		this.textures.disposeAll();
		this.cascadedShadowMaps.disposeAll();
		
		this.gbufferProgram.dispose();
		this.deferredProgram.dispose();
		this.finalProgram.dispose();
		this.fullProgram.dispose();
		this.stencilCullProgram.dispose();
		this.skyboxProgram.dispose();
		
		this.stencilCullPrimitives.dispose();
		
		this.defBuffers.dispose();
		
		this.quadVAO.dispose();
		this.quadPositionVBO.dispose();
		
		Set<PostEffect> keySet = this.postFXs.keySet();
		for(PostEffect e : keySet)
			this.postFXs.get(e).dispose();
		
		this.postFXs.clear();
		
		// Just delete every sized post fx
		Set<Integer> hashSizeSet = this.sizedPostFXs.keySet();
		for(int sizeHash : hashSizeSet) {
			Map<PostEffect, OGLPostEffect> sizePostFXs = this.sizedPostFXs.get(sizeHash);
			
			keySet = sizePostFXs.keySet();
			for(PostEffect e : keySet)
				sizePostFXs.get(e).dispose();
			
			sizePostFXs.clear();
		}
		
		hashSizeSet.clear();
	}
	
	public TextureData takeScreenshot() {
		int width = this.getWidth();
		int height = this.getHeight();
		
		ByteBuffer data = BufferUtils.createByteBuffer(width * height * 3);
		glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
		
		TextureData tex = new TextureData();
		tex.setData(data);
		tex.setWidth(width);
		tex.setHeight(height);
		tex.setBytesPerPixel(3);
		
		return tex;
	}
	
	private static class DeferredFramebuffers {
		private GBuffer gbuffer;
		private PBuffer pbuffer;
		
		private PostFXPaddle[] postFXPaddles = new PostFXPaddle[2];
		
		public DeferredFramebuffers(int width, int height) {
			this.gbuffer = new GBuffer(width, height);
			this.pbuffer = new PBuffer(width, height, this.gbuffer);
			
			for(int i = 0; i < this.postFXPaddles.length; i++)
				this.postFXPaddles[i] = new PostFXPaddle(width, height);
		}
		
		public void updateSize(int width, int height) {
			this.gbuffer.updateSize(width, height);
			this.pbuffer.updateSize(width, height);
			
			for(int i = 0; i < this.postFXPaddles.length; i++)
				this.postFXPaddles[i].updateSize(width, height);
		}
		
		public GBuffer getGBuffer() {
			return this.gbuffer;
		}
		
		public PBuffer getPBuffer() {
			return this.pbuffer;
		}
		
		public PostFXPaddle[] getPostFXPaddles() {
			return this.postFXPaddles;
		}
		
		public void dispose() {
			this.gbuffer.dispose();
			this.pbuffer.dispose();
			
			for(int i = 0; i < this.postFXPaddles.length; i++)
				this.postFXPaddles[i].dispose();
		}
	}
	
	private static class PostFXPaddle {
		private OGLFramebuffer2D postFXRenderFBO;
		private OGLTexture2D postFXRenderFBOTarget;
		
		public PostFXPaddle(int width, int height) {
			OGLTexture2D.Builder postFXRenderTargetBuilder = new OGLTexture2D.Builder().width(width).height(height).internalFormat(GL_RGBA).type(GL_FLOAT).minFilter(GL_NEAREST).magFilter(GL_NEAREST).wrapS(GL_CLAMP_TO_EDGE).wrapT(GL_CLAMP_TO_EDGE);
			this.postFXRenderFBO = new OGLFramebuffer2D();
			this.postFXRenderFBOTarget = postFXRenderTargetBuilder.build();
			
			this.postFXRenderFBO.bind();
			this.postFXRenderFBO.bindTexture(GL_COLOR_ATTACHMENT0, this.postFXRenderFBOTarget);
			this.postFXRenderFBO.unbind();
		}
		
		public OGLFramebuffer2D getFBO() {
			return this.postFXRenderFBO;
		}
		
		public OGLTexture2D getTarget() {
			return this.postFXRenderFBOTarget;
		}
		
		public void updateSize(int width, int height) {
			this.postFXRenderFBOTarget.setWidth(width);
			this.postFXRenderFBOTarget.setHeight(height);
			
			this.postFXRenderFBOTarget.update();
		}
		
		public void dispose() {
			this.postFXRenderFBO.dispose();
			this.postFXRenderFBOTarget.dispose();
		}
	}
	
	private static class PBuffer {
		private OGLFramebuffer2D pbuffer;
		private OGLTexture2D pbufferTarget;
		
		public PBuffer(int width, int height, GBuffer gbuffer) {
			this.pbuffer = new OGLFramebuffer2D();
			this.pbufferTarget = new OGLTexture2D.Builder().width(width).height(height).internalFormat(GL_RGBA16F).type(GL_FLOAT).minFilter(GL_NEAREST).magFilter(GL_NEAREST).build();
			
			this.pbuffer.bind();
			this.pbuffer.bindTexture(GL_COLOR_ATTACHMENT0, this.pbufferTarget);
			this.pbuffer.bindTexture(GL_DEPTH_STENCIL_ATTACHMENT, gbuffer.getDepthTarget());
			this.pbuffer.unbind();
		}
		
		public OGLFramebuffer2D getFBO() {
			return this.pbuffer;
		}
		
		public OGLTexture2D getTarget() {
			return this.pbufferTarget;
		}
		
		public void updateSize(int width, int height) {
			this.pbufferTarget.setWidth(width);
			this.pbufferTarget.setHeight(height);
			
			this.pbufferTarget.update();
		}
		
		public void dispose() {
			this.pbuffer.dispose();
			this.pbufferTarget.dispose();
		}
	}
	
	private static class GBuffer {
		private OGLFramebuffer2D gbuffer;
		private OGLTexture2D gbufferDiffuseTarget;
		private OGLTexture2D gbufferPositionTarget;
		private OGLTexture2D gbufferNormalTarget;
		private OGLTexture2D gbufferDepthTarget;
		
		public GBuffer(int width, int height) {
			OGLTexture2D.Builder gbufferTargetBuilder = new OGLTexture2D.Builder().width(width).height(height).internalFormat(GL_RGBA16F).type(GL_FLOAT).minFilter(GL_NEAREST).magFilter(GL_NEAREST);
			
			this.gbuffer = new OGLFramebuffer2D();
			this.gbufferDiffuseTarget = gbufferTargetBuilder.build();
			this.gbufferPositionTarget = gbufferTargetBuilder.build();
			this.gbufferNormalTarget = gbufferTargetBuilder.build();
			this.gbufferDepthTarget = gbufferTargetBuilder.internalFormat(GL_DEPTH24_STENCIL8).format(GL_DEPTH_COMPONENT).build();
			
			this.gbuffer.bind();
			this.gbuffer.bindTexture(GL_COLOR_ATTACHMENT0, this.gbufferDiffuseTarget);
			this.gbuffer.bindTexture(GL_COLOR_ATTACHMENT1, this.gbufferPositionTarget);
			this.gbuffer.bindTexture(GL_COLOR_ATTACHMENT2, this.gbufferNormalTarget);
			this.gbuffer.bindTexture(GL_DEPTH_STENCIL_ATTACHMENT, this.gbufferDepthTarget);
			this.gbuffer.unbind();
		}
		
		public OGLFramebuffer2D getFBO() {
			return this.gbuffer;
		}
		
		public OGLTexture2D getDiffuseTarget() {
			return this.gbufferDiffuseTarget;
		}
		
		public OGLTexture2D getDepthTarget() {
			return this.gbufferDepthTarget;
		}
		
		public OGLTexture2D getPositionTarget() {
			return this.gbufferPositionTarget;
		}
		
		public OGLTexture2D getNormalTarget() {
			return this.gbufferNormalTarget;
		}
		
		public void updateSize(int width, int height) {
			this.gbufferDiffuseTarget.setWidth(width);
			this.gbufferDiffuseTarget.setHeight(height);
			this.gbufferPositionTarget.setWidth(width);
			this.gbufferPositionTarget.setHeight(height);
			this.gbufferNormalTarget.setWidth(width);
			this.gbufferNormalTarget.setHeight(height);
			this.gbufferDepthTarget.setWidth(width);
			this.gbufferDepthTarget.setHeight(height);
			
			this.gbufferDiffuseTarget.update();
			this.gbufferPositionTarget.update();
			this.gbufferNormalTarget.update();
			this.gbufferDepthTarget.update();
		}
		
		public void dispose() {
			this.gbuffer.dispose();
			this.gbufferDiffuseTarget.dispose();
			this.gbufferPositionTarget.dispose();
			this.gbufferNormalTarget.dispose();
			this.gbufferDepthTarget.dispose();
		}
	}
}
