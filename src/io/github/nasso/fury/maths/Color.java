package io.github.nasso.fury.maths;

import io.github.nasso.fury.nanovg.NVGContext;

import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.nanovg.NVGColor;

public class Color {
	/**
	 * The color transparent with an alpha of 0%.
	 */
	public static final Color TRANSPARENT = new Color(0f, 0f, 0f, 0f);
	/**
	 * The color black with an RGB value of #000000 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#000000;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color BLACK = new Color(0f);
	/**
	 * The color dark gray with an RGB value of #404040 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#404040;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color DARK_GRAY = new Color(0.25f);
	/**
	 * The color gray with an RGB value of #808080 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#808080;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color GRAY = new Color(0.5f);
	/**
	 * The color light gray with an RGB value of #BFBFBF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#BFBFBF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color LIGHT_GRAY = new Color(0.75f);
	/**
	 * The color white with an RGB value of #FFFFFF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#FFFFFF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color WHITE = new Color(1f);
	
	// ------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * The color red with an RGB value of #FF0000 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#FF0000;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color RED = new Color(1f, 0f, 0f);
	/**
	 * The color orange with an RGB value of #FF8000 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#FF8000;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color ORANGE = new Color(1f, 0.5f, 0f);
	/**
	 * The color yellow with an RGB value of #FFFF00 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#FFFF00;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color YELLOW = new Color(1f, 1f, 0f);
	/**
	 * The color yellow-green with an RGB value of #80FF00 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#80FF00;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color YELLOW_GREEN = new Color(0.5f, 1f, 0f);
	/**
	 * The color lime with an RGB value of #00FF00 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#00FF00;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color LIME = new Color(0f, 1f, 0f);
	/**
	 * The color blue-green with an RGB value of #00FF80 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#00FF80;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color BLUE_GREEN = new Color(0f, 1f, 0.5f);
	/**
	 * The color cyan with an RGB value of #00FFFF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#00FFFF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color CYAN = new Color(0f, 1f, 1f);
	/**
	 * The color royal-blue with an RGB value of #0080FF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#0080FF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color ROYAL_BLUE = new Color(0f, 0.5f, 1f);
	/**
	 * The color blue with an RGB value of #0000FF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#0000FF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color BLUE = new Color(0f, 0f, 1f);
	/**
	 * The color ??? with an RGB value of #8000FF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#8000FF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color PURPLE = new Color(0.5f, 0f, 1f);
	/**
	 * The color fuchsia with an RGB value of #FF00FF <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#FF00FF;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color FUCHSIA = new Color(1f, 0f, 1f);
	/**
	 * The color candy-pink with an RGB value of #FF0080 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#FF0080;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color CANDY_PINK = new Color(1f, 0f, 0.5f);
	
	// ------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * The color maroon with an RGB value of #800000 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#800000;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color MAROON = new Color(0.5f, 0f, 0f);
	/**
	 * The color olive with an RGB value of #808000 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#808000;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color OLIVE = new Color(0.5f, 0.5f, 0f);
	/**
	 * The color green with an RGB value of #008000 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#008000;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color GREEN = new Color(0f, 0.5f, 0f);
	/**
	 * The color teal with an RGB value of #008080 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#008080;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color TEAL = new Color(0f, 0.5f, 0.5f);
	/**
	 * The color navy with an RGB value of #000080 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#000080;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color NAVY = new Color(0f, 0f, 0.5f);
	/**
	 * The color purple with an RGB value of #800080 <div style=
	 * "border:1px solid black;width:40px;height:20px;background-color:#800080;float:right;margin: 0 10px 0 0"
	 * ></div><br/>
	 * <br/>
	 */
	public static final Color DARK_PURPLE = new Color(0.5f, 0f, 0.5f);
	
	// ----
	public float r = 0, g = 0, b = 0, a = 1;
	
	public Color(String colorStr) {
		this.set(colorStr);
	}
	
	public Color(float r, float g, float b) {
		this(r, g, b, 1f);
	}
	
	public Color(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(float gray) {
		this(gray, gray, gray);
	}
	
	public Color() {
		this(0);
	}
	
	public Color(Color color) {
		this(color.r, color.g, color.b, color.a);
	}
	
	public Color set(Color other) {
		if(other == null) return this;
		
		this.r = other.r;
		this.g = other.g;
		this.b = other.b;
		this.a = other.a;
		
		return this;
	}
	
	public Color setVec3(Vector3f other) {
		if(other == null) return this;
		
		this.r = other.x;
		this.g = other.y;
		this.b = other.z;
		this.a = 1.0f;
		
		return this;
	}
	
	public Color setVec4(Vector4f other) {
		if(other == null) return this;
		
		this.r = other.x;
		this.g = other.y;
		this.b = other.z;
		this.a = other.w;
		
		return this;
	}
	
	public Color setInts(int gray) {
		this.setInts(gray, gray, gray);
		
		return this;
	}
	
	public Color setInts(int r, int g, int b) {
		this.setInts(r, g, b, 255);
		
		return this;
	}
	
	public Color setInts(int r, int g, int b, int a) {
		this.r = r / 255.0f;
		this.g = g / 255.0f;
		this.b = b / 255.0f;
		this.a = a / 255.0f;
		
		return this;
	}
	
	public String toString() {
		return "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.a + ")";
	}
	
	public NVGColor toNVG() {
		return NVGContext.rgbaf(this.r, this.g, this.b, this.a);
	}
	
	public Vector3f toVec3() {
		return new Vector3f(this.r, this.g, this.b);
	}
	
	public Vector4f toVec4() {
		return new Vector4f(this.r, this.g, this.b, this.a);
	}
	
	public Color set(String colorStr) {
		if(colorStr.matches("^#[0-9A-Fa-f]{6}$")) {
			int rgb = Integer.parseInt(colorStr.substring(1), 16);
			
			this.r = ((rgb >> 16) & 0xFF) / 255.0f;
			this.g = ((rgb >> 8) & 0xFF) / 255.0f;
			this.b = (rgb & 0xFF) / 255.0f;
			this.a = 1.0f;
		} else if(colorStr.matches("^#[0-9A-Fa-f]{3}$")) {
			int rgb = Integer.parseInt(colorStr.substring(1), 16);
			
			this.r = ((rgb >> 8) & 0xF) / 15.0f;
			this.g = ((rgb >> 4) & 0xF) / 15.0f;
			this.b = (rgb & 0xF) / 15.0f;
			this.a = 1.0f;
		}
		
		return this;
	}
	
	public static Color fromInt(int gray) {
		return new Color().setInts(gray);
	}
	
	public static Color fromInt(int r, int g, int b) {
		return new Color().setInts(r, g, b);
	}
	
	public static Color fromInt(int r, int g, int b, int a) {
		return new Color().setInts(r, g, b, a);
	}
}
