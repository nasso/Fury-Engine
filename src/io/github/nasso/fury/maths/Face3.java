package io.github.nasso.fury.maths;

import org.joml.Vector3f;

public class Face3 {
	public int a, b, c;
	public boolean has3Norms = true;
	public Vector3f norm = new Vector3f();
	public Vector3f aNorm = new Vector3f(0, 0, 1),
			bNorm = new Vector3f(0, 0, 1), cNorm = new Vector3f(0, 0, 1);
	
	public Face3(int a, int b, int c, Vector3f aNorm, Vector3f bNorm, Vector3f cNorm) {
		this.a = a;
		this.b = b;
		this.c = c;
		
		this.aNorm.set(aNorm);
		this.bNorm.set(bNorm);
		this.cNorm.set(cNorm);
	}
	
	public Face3(int a, int b, int c, Vector3f normal) {
		this.a = a;
		this.b = b;
		this.c = c;
		
		this.norm.set(normal);
		
		this.has3Norms = false;
	}
	
	public Face3(int a, int b, int c) {
		this(a, b, c, new Vector3f(0, 0, 1));
	}
}
