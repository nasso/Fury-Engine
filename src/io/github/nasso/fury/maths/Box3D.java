package io.github.nasso.fury.maths;

import java.util.List;

import org.joml.Vector3f;

public class Box3D {
	private Vector3f min = new Vector3f();
	private Vector3f max = new Vector3f();
	
	public Box3D(float posx, float posy, float posz, float maxx, float maxy, float maxz) {
		this.min.set(posx, posy, posz);
		this.max.set(maxx, maxy, maxz);
	}
	
	public Box3D(Vector3f pos, Vector3f max) {
		this(pos.x, pos.y, pos.z, max.x, max.y, max.z);
	}
	
	public Box3D() {
		this(0, 0, 0, 0, 0, 0);
	}
	
	public Box3D setMin(float minx, float miny, float minz) {
		this.min.set(minx, miny, minz);
		
		return this;
	}
	
	public Vector3f getMin() {
		return this.min;
	}
	
	public Box3D setMax(float maxx, float maxy, float maxz) {
		this.max.set(maxx, maxy, maxz);
		
		return this;
	}
	
	public Vector3f getMax() {
		return this.max;
	}
	
	public Vector3f getCenter(Vector3f target) {
		if(target == null) target = new Vector3f();
		
		return this.min.add(this.max, target).div(2);
	}
	
	public Vector3f getCenter() {
		return this.getCenter(null);
	}
	
	public Vector3f getSize(Vector3f target) {
		if(target == null) target = new Vector3f();
		
		return this.max.sub(this.min, target);
	}
	
	public Vector3f getSize() {
		return this.getSize(null);
	}
	
	public Box3D zero() {
		this.min.set(0);
		this.max.set(0);
		
		return this;
	}
	
	public Box3D setContaining(List<Vector3f> points) {
		this.zero();
		
		if(points.isEmpty()) return this;
		
		this.min.set(points.get(0));
		this.max.set(points.get(0));
		
		for(int i = 0, l = points.size(); i < l; i++)
			this.expandTo(points.get(i));
		
		return this;
	}
	
	public Box3D setContaining(Vector3f[] points) {
		this.zero();
		
		if(points.length == 0) return this;
		
		this.min.set(points[0]);
		this.max.set(points[0]);
		
		for(int i = 1, l = points.length; i < l; i++)
			this.expandTo(points[i]);
		
		return this;
	}
	
	public Box3D expandTo(Vector3f point) {
		this.min.min(point);
		this.max.max(point);
		
		return this;
	}
	
	public boolean contains(Vector3f p) {
		if(p == null) return false;
		
		return (p.x >= this.min.x && p.y >= this.min.y && p.z >= this.min.z) && // Min
																				// clip
		(p.x <= this.max.x && p.y <= this.max.y && p.z <= this.max.z); // Max
																		// clip
	}
	
	public String toString() {
		return "box3d( min: " + this.min.x + " " + this.min.y + " " + this.min.z + " max: " + this.max.x + " " + this.max.y + " " + this.max.z + ")";
	}
	
	public Box3D copy(Box3D other) {
		this.min.set(other.min);
		this.max.set(other.max);
		
		return this;
	}
	
	public Box3D clone() {
		return new Box3D().copy(this);
	}
}
