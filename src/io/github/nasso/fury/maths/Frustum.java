package io.github.nasso.fury.maths;

import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.level.Mesh;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Frustum {
	private Plane[] planes = new Plane[6];
	
	public Frustum() {
		for(int i = 0; i < this.planes.length; i++)
			this.planes[i] = new Plane();
	}
	
	public Frustum setFromMatrix(Matrix4f mat) {
		float[] m = new float[16];
		mat.get(m);
		
		this.planes[0].setComponents(m[3] - m[0], m[7] - m[4], m[11] - m[8], m[15] - m[12]).normalize();
		this.planes[1].setComponents(m[3] + m[0], m[7] + m[4], m[11] + m[8], m[15] + m[12]).normalize();
		this.planes[2].setComponents(m[3] + m[1], m[7] + m[5], m[11] + m[9], m[15] + m[13]).normalize();
		this.planes[3].setComponents(m[3] - m[1], m[7] - m[5], m[11] - m[9], m[15] - m[13]).normalize();
		this.planes[4].setComponents(m[3] - m[2], m[7] - m[6], m[11] - m[10], m[15] - m[14]).normalize();
		this.planes[5].setComponents(m[3] + m[2], m[7] + m[6], m[11] + m[10], m[15] + m[14]).normalize();
		
		return this;
	}
	
	private Sphere _sphere = new Sphere();
	
	public boolean intersectsMesh(Mesh obj) {
		AbstractGeometry geom = obj.getGeometry();
		
		if(geom == null) return false;
		
		if(geom.getBoundingSphere() == null) geom.computeBoundingSphere();
		
		this._sphere.copy(geom.getBoundingSphere()).applyMatrix(obj.getMatrixWorld());
		
		return this.intersectsSphere(this._sphere);
	}
	
	public boolean intersectsSphere(Sphere sphere) {
		Vector3f center = sphere.getPosition();
		float negRad = -sphere.getRadius();
		
		for(int i = 0; i < this.planes.length; i++) {
			float distance = this.planes[i].distanceToPoint(center);
			
			if(distance < negRad) return false;
		}
		
		return true;
	}
}
