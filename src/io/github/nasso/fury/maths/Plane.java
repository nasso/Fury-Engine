package io.github.nasso.fury.maths;

import org.joml.Vector3f;

public class Plane {
	private Vector3f normal = new Vector3f(1, 0, 0);
	private float constant = 0f;
	
	public Plane(float normX, float normY, float normZ, float constant) {
		this.set(normX, normY, normZ, constant);
	}
	
	public Plane(float normX, float normY, float normZ) {
		this(normX, normY, normZ, 0f);
	}
	
	public Plane(Vector3f normal) {
		this(normal.x, normal.y, normal.z);
	}
	
	public Plane() {
		this(1, 0, 0);
	}
	
	public Vector3f getNormal() {
		return this.normal;
	}
	
	public void setNormal(Vector3f normal) {
		this.normal.set(normal);
	}
	
	public void setNormal(float x, float y, float z) {
		this.normal.set(x, y, z);
	}
	
	public float getConstant() {
		return this.constant;
	}
	
	public void setConstant(float constant) {
		this.constant = constant;
	}
	
	public Plane setComponents(float x, float y, float z, float w) {
		this.setNormal(x, y, z);
		this.constant = w;
		
		return this;
	}
	
	public Plane normalize() {
		float normalLengthInverse = 1f / this.normal.length();
		this.normal.mul(normalLengthInverse);
		this.constant *= normalLengthInverse;
		
		return this;
	}
	
	public float distanceToPoint(Vector3f point) {
		return this.normal.dot(point) + this.constant;
	}
	
	public Plane set(Vector3f normal) {
		return this.set(normal.x, normal.y, normal.z);
	}
	
	public Plane set(float x, float y, float z) {
		return this.set(x, y, z, 0f);
	}
	
	public Plane set(float x, float y, float z, float w) {
		this.setNormal(x, y, z);
		this.setConstant(w);
		
		return this;
	}
	
	public Plane set(Vector3f normal, int w) {
		return this.set(normal.x, normal.y, normal.z, w);
	}
}
