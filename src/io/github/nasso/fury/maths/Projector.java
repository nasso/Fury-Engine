package io.github.nasso.fury.maths;

import io.github.nasso.fury.level.Camera;

import org.joml.Vector3f;

public class Projector {
	private Projector() {
	}
	
	public static Vector3f project(Vector3f v, Camera cam) {
		return new Vector3f(v).mulProject(cam.getProjViewMatrix());
	}
}
