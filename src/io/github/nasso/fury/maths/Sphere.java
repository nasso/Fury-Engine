package io.github.nasso.fury.maths;

import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Sphere {
	protected Vector3f pos = new Vector3f();
	private float radius = 0f;
	
	public Sphere(float radius) {
		this.setRadius(radius);
	}
	
	public Sphere() {
		this(1);
	}
	
	private Vector3f _scale = new Vector3f();
	
	public Sphere applyMatrix(Matrix4f mat) {
		this.pos.mulPosition(mat);
		
		mat.getScale(this._scale);
		this.radius = this.radius * Math.max(this._scale.x, Math.max(this._scale.y, this._scale.z));
		
		return this;
	}
	
	public float getRadius() {
		return this.radius;
	}
	
	public Sphere setRadius(float radius) {
		this.radius = radius;
		
		return this;
	}
	
	public boolean contains(Vector3f p) {
		return this.pos.distance(p) <= this.radius;
	}
	
	public Vector3f getPosition() {
		return this.pos;
	}
	
	public Sphere setPosition(Vector3f pos) {
		this.pos = pos;
		
		return this;
	}
	
	public Sphere zero() {
		this.pos.set(0);
		this.radius = 0;
		
		return this;
	}
	
	public Sphere setContaining(List<Vector3f> points) {
		this.zero();
		
		for(int i = 0, l = points.size(); i < l; i++)
			this.expandTo(points.get(i));
		
		return this;
	}
	
	public Sphere expandTo(Vector3f p) {
		this.radius = Math.max(this.radius, this.pos.distance(p));
		
		return this;
	}
	
	public Sphere copy(Sphere other) {
		this.radius = other.radius;
		this.pos.set(other.pos);
		
		return this;
	}
	
	public Sphere clone() {
		return new Sphere().copy(this);
	}
}
