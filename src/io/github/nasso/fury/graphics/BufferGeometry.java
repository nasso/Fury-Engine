package io.github.nasso.fury.graphics;

import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Object3D;
import io.github.nasso.fury.maths.Box3D;
import io.github.nasso.fury.maths.Sphere;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

public class BufferGeometry extends AbstractGeometry {
	private BufferAttribute positions = null;
	private BufferAttribute normals = null;
	private BufferAttribute uvs = null;
	private BufferAttribute uvs2 = null;
	private BufferAttribute indices = null;
	
	public BufferAttribute getPositions() {
		return this.positions;
	}
	
	public void setPositions(BufferAttribute positions) {
		this.positions = positions;
	}
	
	public BufferAttribute getNormals() {
		return this.normals;
	}
	
	public void setNormals(BufferAttribute normals) {
		this.normals = normals;
	}
	
	public BufferAttribute getUVs() {
		return this.uvs;
	}
	
	public void setUVs(BufferAttribute uvs) {
		this.uvs = uvs;
	}
	
	public BufferAttribute getUVs2() {
		return this.uvs2;
	}
	
	public void setUVs2(BufferAttribute uvs2) {
		this.uvs2 = uvs2;
	}
	
	public BufferAttribute getIndices() {
		return this.indices;
	}
	
	public void setIndices(BufferAttribute indices) {
		this.indices = indices;
	}
	
	public BufferGeometry fromGeometry(AbstractGeometry geom) {
		if(geom instanceof Geometry) return this.fromGeometry((Geometry) geom);
		else if(geom instanceof DirectGeometry) return this.fromGeometry((DirectGeometry) geom);
		else if(geom instanceof BufferGeometry) return this;
		
		return this;
	}
	
	public BufferGeometry fromGeometry(Geometry geom) {
		DirectGeometry direct = new DirectGeometry().fromGeometry(geom);
		
		return this.fromGeometry(direct);
	}
	
	public BufferGeometry fromGeometry(DirectGeometry geom) {
		BufferAttribute positions = BufferAttribute.fromVector3fList(geom.getVertices());
		this.setPositions(positions);
		
		if(geom.getNormals() != null && !geom.getNormals().isEmpty()) this.setNormals(BufferAttribute.fromVector3fList(geom.getNormals()));
		
		if(geom.getUVs() != null && !geom.getUVs().isEmpty()) this.setUVs(BufferAttribute.fromVector2fList(geom.getUVs()));
		
		if(geom.getUVs2() != null && !geom.getUVs2().isEmpty()) this.setUVs2(BufferAttribute.fromVector2fList(geom.getUVs2()));
		
		if(geom.getIndices() != null && !geom.getIndices().isEmpty()) this.setIndices(BufferAttribute.fromIntegerList(geom.getIndices()));
		
		if(geom.getBoundingSphere() != null) this.setBoundingSphere(geom.getBoundingSphere().clone());
		
		if(geom.getBoundingBox() != null) this.setBoundingBox(geom.getBoundingBox().clone());
		
		return this;
	}
	
	public Geometry getGeometryVersion() {
		return null;
	}
	
	public DirectGeometry getDirectGeometryVersion() {
		return null;
	}
	
	public BufferGeometry getBufferGeometryVersion() {
		return this;
	}
	
	public BufferGeometry updateFromObject(Mesh obj) {
		AbstractGeometry geom = obj.getGeometry();
		
		if(geom == null) return this;
		
		DirectGeometry direct = geom.getDirectGeometryVersion();
		
		if(direct == null) return this.fromGeometry(geom);
		
		if(geom instanceof Geometry) {
			Geometry gg = (Geometry) geom;
			
			direct.setVerticesNeedUpdate(gg.doesVerticesNeedUpdate());
			direct.setNormalsNeedUpdate(gg.doesNormalsNeedUpdate());
			direct.setUVsNeedUpdate(gg.doesUVsNeedUpdate());
			
			gg.setVerticesNeedUpdate(false);
			gg.setNormalsNeedUpdate(false);
			gg.setUVsNeedUpdate(false);
		} else if(geom instanceof DirectGeometry) {
			DirectGeometry gg = (DirectGeometry) geom;
			
			direct.setVerticesNeedUpdate(gg.doesVerticesNeedUpdate());
			direct.setNormalsNeedUpdate(gg.doesNormalsNeedUpdate());
			direct.setUVsNeedUpdate(gg.doesUVsNeedUpdate());
			
			gg.setVerticesNeedUpdate(false);
			gg.setNormalsNeedUpdate(false);
			gg.setUVsNeedUpdate(false);
		}
		
		direct = (DirectGeometry) geom;
		if(direct.doesVerticesNeedUpdate()) {
			BufferAttribute attrib = this.positions;
			
			if(attrib != null) {
				attrib.copyVector3fList(direct.getVertices());
				attrib.needsUpdate(true);
			}
			
			direct.setVerticesNeedUpdate(false);
		}
		
		if(direct.doesNormalsNeedUpdate()) {
			BufferAttribute attrib = this.normals;
			
			if(attrib != null) {
				attrib.copyVector3fList(direct.getNormals());
				attrib.needsUpdate(true);
			}
			
			direct.setNormalsNeedUpdate(false);
		}
		
		if(direct.doesUVsNeedUpdate()) {
			BufferAttribute attrib = this.uvs;
			BufferAttribute attrib2 = this.uvs2;
			
			if(attrib != null) {
				attrib.copyVector2fList(direct.getUVs());
				attrib.needsUpdate(true);
			}
			
			if(attrib2 != null) {
				attrib2.copyVector2fList(direct.getUVs2());
				attrib2.needsUpdate(true);
			}
			
			direct.setUVsNeedUpdate(false);
		}
		
		return this;
	}
	
	public BufferGeometry setFromObject(Object3D obj) {
		if(obj instanceof Mesh) {
			Mesh m = (Mesh) obj;
			
			if(m.getGeometry() instanceof Geometry) this.fromGeometry((Geometry) m.getGeometry());
		}
		
		return this;
	}
	
	public Sphere computeBoundingSphere() {
		if(this.getBoundingSphere() == null) this.setBoundingSphere(new Sphere());
		
		if(this.positions == null || this.positions.getData() == null || !(this.positions.getData() instanceof FloatBuffer)) return this.getBoundingSphere();
		
		FloatBuffer pos = (FloatBuffer) this.positions.getData();
		List<Vector3f> positions = new ArrayList<Vector3f>();
		
		for(int i = 0, l = this.positions.getCount(); i < l; i += 3) {
			Vector3f vec3 = new Vector3f();
			
			vec3.x = pos.get(i);
			vec3.y = pos.get(i + 1);
			vec3.z = pos.get(i + 2);
			
			positions.add(vec3);
		}
		
		return this.getBoundingSphere().setContaining(positions);
	}
	
	public Box3D computeBoundingBox() {
		if(this.getBoundingBox() == null) this.setBoundingBox(new Box3D());
		
		if(this.positions == null || this.positions.getData() == null || !(this.positions.getData() instanceof FloatBuffer)) return this.getBoundingBox();
		
		FloatBuffer pos = (FloatBuffer) this.positions.getData();
		List<Vector3f> positions = new ArrayList<Vector3f>();
		
		for(int i = 0, l = this.positions.getCount(); i < l; i += 3) {
			Vector3f vec3 = new Vector3f();
			
			vec3.x = pos.get(i);
			vec3.y = pos.get(i + 1);
			vec3.z = pos.get(i + 2);
			
			positions.add(vec3);
		}
		
		return this.getBoundingBox().setContaining(positions);
	}
}
