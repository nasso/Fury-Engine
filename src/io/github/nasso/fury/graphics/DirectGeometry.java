package io.github.nasso.fury.graphics;

import io.github.nasso.fury.maths.Box3D;
import io.github.nasso.fury.maths.Face3;
import io.github.nasso.fury.maths.Sphere;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class DirectGeometry extends AbstractGeometry {
	private List<Vector3f> vertices = new ArrayList<Vector3f>();
	private List<Vector3f> normals = new ArrayList<Vector3f>();
	private List<Vector2f> uvs = new ArrayList<Vector2f>();
	private List<Vector2f> uvs2 = new ArrayList<Vector2f>();
	private List<Integer> indices = new ArrayList<Integer>();
	
	private boolean verticesNeedUpdate = false;
	private boolean normalsNeedUpdate = false;
	private boolean uvsNeedUpdate = false;
	
	private BufferGeometry bufferGeometryVersion = null;
	
	public DirectGeometry() {
		
	}
	
	public DirectGeometry fromGeometry(Geometry geom) {
		List<Face3> faces = geom.getFaces();
		List<Vector3f> vertices = geom.getVertices();
		List<Vector2f[]> uvs1 = geom.getFaceVertexUVs();
		List<Vector2f[]> uvs2 = geom.getFaceVertexUVs2();
		
		boolean hasUVs1 = uvs1 != null && !uvs1.isEmpty();
		boolean hasUVs2 = uvs2 != null && !uvs2.isEmpty();
		
		Face3 face;
		for(int i = 0, l = faces.size(); i < l; i++) {
			face = faces.get(i);
			
			this.vertices.add(vertices.get(face.a));
			this.vertices.add(vertices.get(face.b));
			this.vertices.add(vertices.get(face.c));
			
			if(face.has3Norms) {
				this.normals.add(face.aNorm);
				this.normals.add(face.bNorm);
				this.normals.add(face.cNorm);
			} else {
				this.normals.add(face.norm);
				this.normals.add(face.norm);
				this.normals.add(face.norm);
			}
			
			if(hasUVs1) {
				Vector2f[] uv = uvs1.get(i);
				
				if(uv != null && uv.length == 3) {
					this.uvs.add(uv[0]);
					this.uvs.add(uv[1]);
					this.uvs.add(uv[2]);
				} else {
					this.uvs.add(new Vector2f());
					this.uvs.add(new Vector2f());
					this.uvs.add(new Vector2f());
				}
			}
			
			if(hasUVs2) {
				Vector2f[] uv = uvs2.get(i);
				
				if(uv != null && uv.length == 3) {
					this.uvs2.add(uv[0]);
					this.uvs2.add(uv[1]);
					this.uvs2.add(uv[2]);
				} else {
					this.uvs2.add(new Vector2f());
					this.uvs2.add(new Vector2f());
					this.uvs2.add(new Vector2f());
				}
			}
		}
		
		this.setVerticesNeedUpdate(geom.doesVerticesNeedUpdate());
		this.setNormalsNeedUpdate(geom.doesNormalsNeedUpdate());
		this.setUVsNeedUpdate(geom.doesUVsNeedUpdate());
		
		return this;
	}
	
	public Box3D computeBoundingBox() {
		if(this.getBoundingBox() == null) this.setBoundingBox(new Box3D());
		
		return this.getBoundingBox().setContaining(this.vertices);
	}
	
	public Sphere computeBoundingSphere() {
		if(this.getBoundingSphere() == null) this.setBoundingSphere(new Sphere());
		
		return this.getBoundingSphere().setContaining(this.vertices);
	}
	
	public List<Vector3f> getVertices() {
		return this.vertices;
	}
	
	public List<Vector3f> getNormals() {
		return this.normals;
	}
	
	public List<Vector2f> getUVs() {
		return this.uvs;
	}
	
	public List<Vector2f> getUVs2() {
		return this.uvs2;
	}
	
	public List<Integer> getIndices() {
		return this.indices;
	}
	
	public Geometry getGeometryVersion() {
		return null;
	}
	
	public DirectGeometry getDirectGeometryVersion() {
		return this;
	}
	
	public BufferGeometry getBufferGeometryVersion() {
		if(this.bufferGeometryVersion == null) this.bufferGeometryVersion = new BufferGeometry().fromGeometry(this);
		
		return this.bufferGeometryVersion;
	}
	
	public boolean doesVerticesNeedUpdate() {
		return this.verticesNeedUpdate;
	}
	
	public void setVerticesNeedUpdate(boolean verticesNeedUpdate) {
		this.verticesNeedUpdate = verticesNeedUpdate;
	}
	
	public boolean doesNormalsNeedUpdate() {
		return this.normalsNeedUpdate;
	}
	
	public void setNormalsNeedUpdate(boolean normalsNeedUpdate) {
		this.normalsNeedUpdate = normalsNeedUpdate;
	}
	
	public boolean doesUVsNeedUpdate() {
		return this.uvsNeedUpdate;
	}
	
	public void setUVsNeedUpdate(boolean uvsNeedUpdate) {
		this.uvsNeedUpdate = uvsNeedUpdate;
	}
}
