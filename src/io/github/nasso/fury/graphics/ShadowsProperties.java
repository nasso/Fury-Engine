package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;

import java.util.ArrayList;
import java.util.List;

public class ShadowsProperties {
	public static final int MAX_CASCADE_COUNT = 6;
	
	private List<ShadowProperties> cascadeProperties = new ArrayList<ShadowProperties>();
	
	private float cascadeMaxDistance = 400;
	private float interCascadeFade = 2;
	private float cascadeDistanceFade = 30;
	private float cascadeDistributionExp = 2.8f;
	
	public ShadowsProperties() {
		ShadowProperties[] props = new ShadowProperties[4];
		
		props[0] = new ShadowProperties();
		props[1] = new ShadowProperties();
		props[2] = new ShadowProperties();
		props[3] = new ShadowProperties();
		
		props[0].setBias(0.0016f);
		props[1].setBias(0.0032f);
		props[2].setBias(0.0048f);
		props[3].setBias(0.0064f);
		
		props[0].setPoissonSpread(650);
		props[1].setPoissonSpread(900);
		
		props[0].setType(Fury.SHADOW_BUFFER_TYPE_ROTATED_POISSON_SAMPLED);
		props[1].setType(Fury.SHADOW_BUFFER_TYPE_POISSON_SAMPLED);
		props[2].setType(Fury.SHADOW_BUFFER_TYPE_SIMPLE);
		props[3].setType(Fury.SHADOW_BUFFER_TYPE_SIMPLE);
		
		for(int i = 0; i < props.length; i++)
			this.cascadeProperties.add(props[i]);
	}
	
	public ShadowsProperties(PrimitiveProperties props) {
		this.set(props);
	}
	
	public String toString() {
		return "ShadowsProperties: [cascades=" + this.cascadeProperties.size() + "]";
	}
	
	public List<ShadowProperties> getCascadeProperties() {
		return this.cascadeProperties;
	}
	
	public void set(PrimitiveProperties props) {
		this.cascadeMaxDistance = props.getFloat("cascadeMaxDistance", this.cascadeMaxDistance);
		this.interCascadeFade = props.getFloat("interCascadeFade", this.interCascadeFade);
		this.cascadeDistanceFade = props.getFloat("cascadeDistanceFade", this.cascadeDistanceFade);
		this.cascadeDistributionExp = props.getFloat("cascadeDistributionExp", this.cascadeDistributionExp);
	}
	
	public ShadowProperties getShadowProperties(int cascade) {
		return this.cascadeProperties.get(cascade);
	}
	
	public int getCascadeCount() {
		return this.cascadeProperties.size();
	}
	
	public void setCascadeCount(int cascadeCount) {
		if(this.cascadeProperties.size() > cascadeCount) while(this.cascadeProperties.size() > cascadeCount)
			this.cascadeProperties.remove(0);
		else while(this.cascadeProperties.size() < cascadeCount)
			this.cascadeProperties.add(new ShadowProperties());
	}
	
	public float getCascadeMaxDistance() {
		return this.cascadeMaxDistance;
	}
	
	public void setCascadeMaxDistance(float cascadeMaxDistance) {
		this.cascadeMaxDistance = cascadeMaxDistance;
	}
	
	public float getInterCascadeFade() {
		return this.interCascadeFade;
	}
	
	public void setInterCascadeFade(float interCascadeFade) {
		this.interCascadeFade = interCascadeFade;
	}
	
	public float getCascadeDistanceFade() {
		return this.cascadeDistanceFade;
	}
	
	public void setCascadeDistanceFade(float cascadeDistanceFade) {
		this.cascadeDistanceFade = cascadeDistanceFade;
	}
	
	public float getCascadeDistributionExp() {
		return this.cascadeDistributionExp;
	}
	
	public void setCascadeDistributionExp(float cascadeDistributionExp) {
		this.cascadeDistributionExp = cascadeDistributionExp;
	}
}
