package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Fury;

public class RenderTarget2D extends RenderTarget {
	private Texture2D texture;
	
	public RenderTarget2D(Texture2D texture) {
		this.texture = texture;
	}
	
	public RenderTarget2D(int width, int height) {
		this(new Texture2D.Builder().width(width).height(height).minFilter(Fury.NEARSET).magFilter(Fury.LINEAR).internalFormat(Fury.RGBA).wrapS(Fury.REPEAT).wrapT(Fury.REPEAT).type(Fury.UNSIGNED_BYTE).build());
	}
	
	public Texture2D getTexture() {
		return this.texture;
	}
	
	public void setTexture(Texture2D texture) {
		this.texture = texture;
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
	}
}
