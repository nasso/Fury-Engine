package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.level.ShadowBufferType;

public class ShadowProperties {
	private int type = Fury.SHADOW_BUFFER_TYPE_ROTATED_POISSON_SAMPLED;
	
	private int shadowMapSize = 2048;
	private int poissonSamples = 16;
	
	private float poissonSpread = 1200.0f;
	private float bias = 0.005f;
	
	private boolean normalCulling = true;
	
	// cascaded shadow mapping
	
	public ShadowProperties() {
	}
	
	public void set(PrimitiveProperties props) {
		this.type = ShadowBufferType.valueOf(props.getString("type", "ROTATED_POISSON_SAMPLED"));
		
		this.shadowMapSize = props.getInt("shadowMapSize", this.shadowMapSize);
		this.poissonSamples = props.getInt("poissonSamples", this.poissonSamples);
		
		this.poissonSpread = props.getFloat("poissonSpread", this.poissonSpread);
		this.bias = props.getFloat("bias", this.bias);
		
		this.normalCulling = props.getBool("normalCulling", this.normalCulling);
	}
	
	public int getShadowMapSize() {
		return this.shadowMapSize;
	}
	
	public void setShadowMapSize(int shadowMapSize) {
		this.shadowMapSize = shadowMapSize;
	}
	
	public float getBias() {
		return this.bias;
	}
	
	public void setBias(float bias) {
		this.bias = bias;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		if(FuryUtils.check4int(type, Fury.SHADOW_BUFFER_TYPE_SIMPLE, Fury.SHADOW_BUFFER_TYPE_POISSON_SAMPLED, Fury.SHADOW_BUFFER_TYPE_STRATIFIED_POISSON_SAMPLED, Fury.SHADOW_BUFFER_TYPE_ROTATED_POISSON_SAMPLED)) this.type = type;
	}
	
	public int getPoissonSamples() {
		return this.poissonSamples;
	}
	
	public void setPoissonSamples(int poissonSamples) {
		this.poissonSamples = poissonSamples;
	}
	
	public float getPoissonSpread() {
		return this.poissonSpread;
	}
	
	public void setPoissonSpread(float poissonSpread) {
		this.poissonSpread = poissonSpread;
	}
	
	public boolean isNormalCulling() {
		return this.normalCulling;
	}
	
	public void setNormalCulling(boolean normalCulling) {
		this.normalCulling = normalCulling;
	}
}
