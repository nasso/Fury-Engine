package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.level.LevelObject;

import java.nio.ByteBuffer;

public class Texture2D extends Observable implements Disposable, LevelObject {
	public static class Builder {
		private int width = 512;
		private int height = 512;
		private int magFilter = Fury.LINEAR;
		private int minFilter = Fury.LINEAR_MIPMAP_LINEAR;
		private int wrapS = Fury.REPEAT;
		private int wrapT = Fury.REPEAT;
		private int internalFormat = Fury.SRGB_ALPHA;
		private int format = Fury.RGBA;
		private int type = Fury.UNSIGNED_BYTE;
		private int anisotropy = 1;
		
		private ByteBuffer data = null;
		
		public Builder width(int value) {
			this.width = value;
			return this;
		}
		
		public Builder height(int value) {
			this.height = value;
			return this;
		}
		
		public Builder magFilter(int value) {
			this.magFilter = value;
			return this;
		}
		
		public Builder minFilter(int value) {
			this.minFilter = value;
			return this;
		}
		
		public Builder wrapS(int value) {
			this.wrapS = value;
			return this;
		}
		
		public Builder wrapT(int value) {
			this.wrapT = value;
			return this;
		}
		
		public Builder internalFormat(int value) {
			this.internalFormat = value;
			return this;
		}
		
		public Builder format(int value) {
			this.format = value;
			return this;
		}
		
		public Builder type(int value) {
			this.type = value;
			return this;
		}
		
		public Builder anisotropy(int value) {
			this.anisotropy = value;
			return this;
		}
		
		public Builder data(ByteBuffer value) {
			this.data = value;
			return this;
		}
		
		public Texture2D build() {
			return new Texture2D(this.width, this.height, this.magFilter, this.minFilter, this.wrapS, this.wrapT, this.internalFormat, this.format, this.type, this.anisotropy, this.data);
		}
	}
	
	private int width = 512;
	private int height = 512;
	private int magFilter = Fury.LINEAR;
	private int minFilter = Fury.LINEAR_MIPMAP_LINEAR;
	private int wrapS = Fury.REPEAT;
	private int wrapT = Fury.REPEAT;
	private int internalFormat = Fury.SRGB_ALPHA;
	private int format = Fury.RGBA;
	private int type = Fury.UNSIGNED_BYTE;
	private int anisotropy = 1;
	
	private ByteBuffer data = null;
	
	private int version = 0;
	
	private String id = "";
	
	public void setID(String id) {
		this.id = id;
	}
	
	public String getID() {
		return this.id;
	}
	
	private String name = "";
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Texture2D(int width, int height, int magFilter, int minFilter, int wrapS, int wrapT, int internalFormat, int format, int type, int anisotropy, ByteBuffer data) {
		this.width = width;
		this.height = height;
		this.magFilter = magFilter;
		this.minFilter = minFilter;
		this.wrapS = wrapS;
		this.wrapT = wrapT;
		this.internalFormat = internalFormat;
		this.format = format;
		this.type = type;
		this.anisotropy = anisotropy;
		this.data = data;
	}
	
	public Texture2D(PrimitiveProperties props, ByteBuffer data) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		this(props.getInt("width", 512), props.getInt("height", 512), (int) Fury.class.getField(props.getString("magFilter", "LINEAR")).get(null), (int) Fury.class.getField(props.getString("minFilter", "LINEAR_MIPMAP_LINEAR")).get(null), (int) Fury.class.getField(props.getString("wrapS", "REPEAT")).get(null), (int) Fury.class.getField(props.getString("wrapT", "REPEAT")).get(null), (int) Fury.class.getField(props.getString("internalFormat", "SRGB_ALPHA")).get(null), (int) Fury.class.getField(props.getString("format", "RGBA")).get(null), (int) Fury.class.getField(props.getString("type", "UNSIGNED_BYTE")).get(null), props.getInt("anisotropy", 0), data);
	}
	
	public void needUpdate() {
		this.version++;
	}
	
	public int getVersion() {
		return this.version;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getMagFilter() {
		return this.magFilter;
	}
	
	public int getMinFilter() {
		return this.minFilter;
	}
	
	public int getWrapS() {
		return this.wrapS;
	}
	
	public int getWrapT() {
		return this.wrapT;
	}
	
	public int getInternalFormat() {
		return this.internalFormat;
	}
	
	public int getFormat() {
		return this.format;
	}
	
	public int getType() {
		return this.type;
	}
	
	public int getAnisotropy() {
		return this.anisotropy;
	}
	
	public ByteBuffer getData() {
		return this.data;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setMagFilter(int magFilter) {
		this.magFilter = magFilter;
	}
	
	public void setMinFilter(int minFilter) {
		this.minFilter = minFilter;
	}
	
	public void setWrapS(int wrapS) {
		this.wrapS = wrapS;
	}
	
	public void setWrapT(int wrapT) {
		this.wrapT = wrapT;
	}
	
	public void setInternalFormat(int internalFormat) {
		this.internalFormat = internalFormat;
	}
	
	public void setFormat(int format) {
		this.format = format;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public void setAnisotropy(int anisotropy) {
		this.anisotropy = anisotropy;
	}
	
	public void setData(ByteBuffer data) {
		this.data = data;
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
	}
}
