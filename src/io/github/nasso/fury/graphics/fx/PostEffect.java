package io.github.nasso.fury.graphics.fx;

import io.github.nasso.fury.core.PrimitiveProperties;

public abstract class PostEffect {
	private boolean enabled = true;
	
	public abstract void set(PrimitiveProperties props);
	
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
