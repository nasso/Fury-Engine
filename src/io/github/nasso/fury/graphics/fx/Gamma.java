package io.github.nasso.fury.graphics.fx;

import io.github.nasso.fury.core.PrimitiveProperties;

public class Gamma extends PostEffect {
	private float gamma = 2.2f;
	
	public Gamma(float gamma) {
		this.gamma = gamma;
	}
	
	public Gamma() {
		this(2.2f);
	}
	
	public float getGamma() {
		return this.gamma;
	}
	
	public void setGamma(float gamma) {
		this.gamma = gamma;
	}
	
	public void set(PrimitiveProperties props) {
		this.gamma = props.getFloat("gamma", this.gamma);
	}
}
