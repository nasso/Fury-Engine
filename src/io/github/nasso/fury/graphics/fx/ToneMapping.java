package io.github.nasso.fury.graphics.fx;

import io.github.nasso.fury.core.PrimitiveProperties;

public class ToneMapping extends PostEffect {
	public static enum ToneMappingType {
		REINHARD, EXPOSURE, AUTO_EXPOSURE
	}
	
	private ToneMappingType type = ToneMappingType.EXPOSURE;
	private float exposure = 1.0f;
	private float adjustSpeed = 0.1f;
	
	public ToneMapping() {
		this(1.0f);
	}
	
	public ToneMapping(float exposure) {
		this.exposure = exposure;
	}
	
	public ToneMappingType getType() {
		return this.type;
	}
	
	public void setType(ToneMappingType type) {
		this.type = type;
	}
	
	public float getExposure() {
		return this.exposure;
	}
	
	public void setExposure(float exposure) {
		this.exposure = exposure;
	}
	
	public float getAdjustSpeed() {
		return this.adjustSpeed;
	}
	
	public void setAdjustSpeed(float adjustSpeed) {
		this.adjustSpeed = adjustSpeed;
	}
	
	public void set(PrimitiveProperties props) {
		this.type = ToneMappingType.valueOf(props.getString("type", "EXPOSURE"));
		this.exposure = props.getFloat("exposure", this.exposure);
		this.adjustSpeed = props.getFloat("adjustSpeed", this.adjustSpeed);
	}
}
