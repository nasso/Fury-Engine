package io.github.nasso.fury.graphics.fx;

import io.github.nasso.fury.core.PrimitiveProperties;

public class Bloom extends PostEffect {
	private float threshold = 1.0f;
	private float smoothness = 0.4f;
	private float strength = 0.75f;
	private float size = 1.0f;
	private int downsample = 4;
	
	public Bloom(float threshold, float smoothness, float strength, float size, int downsample) {
		this.threshold = threshold;
		this.smoothness = smoothness;
		this.strength = strength;
		this.size = size;
		this.downsample = downsample;
	}
	
	public Bloom(float threshold, float smoothness, float strength, float size) {
		this.threshold = threshold;
		this.smoothness = smoothness;
		this.strength = strength;
		this.size = size;
	}
	
	public Bloom(float threshold, float smoothness, float strength) {
		this.threshold = threshold;
		this.smoothness = smoothness;
		this.strength = strength;
	}
	
	public Bloom(float threshold, float smoothness) {
		this.threshold = threshold;
		this.smoothness = smoothness;
	}
	
	public Bloom(float threshold) {
		this.threshold = threshold;
	}
	
	public Bloom() {
		super();
	}
	
	public float getThreshold() {
		return this.threshold;
	}
	
	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}
	
	public float getStrength() {
		return this.strength;
	}
	
	public void setStrength(float strength) {
		this.strength = strength;
	}
	
	public int getDownsample() {
		return this.downsample;
	}
	
	public void setDownsample(int downsample) {
		this.downsample = downsample;
	}
	
	public float getSize() {
		return this.size;
	}
	
	public void setSize(float size) {
		this.size = size;
	}
	
	public float getSmoothness() {
		return this.smoothness;
	}
	
	public void setSmoothness(float smoothness) {
		this.smoothness = smoothness;
	}
	
	public void set(PrimitiveProperties props) {
		this.threshold = props.getFloat("threshold", this.threshold);
		this.smoothness = props.getFloat("smoothness", this.smoothness);
		this.strength = props.getFloat("strength", this.strength);
		this.size = props.getFloat("size", this.size);
		this.downsample = props.getInt("downsample", this.downsample);
	}
}
