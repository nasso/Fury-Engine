package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.level.LevelObject;

public abstract class RenderTarget extends Observable implements Disposable, LevelObject {
	private String lvl_id = "";
	
	public void setID(String id) {
		this.lvl_id = id;
	}
	
	public String getID() {
		return this.lvl_id;
	}
	
	private String name = "";
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
