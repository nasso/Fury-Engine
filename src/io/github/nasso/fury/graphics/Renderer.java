package io.github.nasso.fury.graphics;

import io.github.nasso.fury.data.TextureData;

import java.util.List;

public abstract class Renderer {
	private RenderMode renderMode = RenderMode.DEFERRED_RENDERING;
	private int width, height;
	
	public Renderer(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public abstract void render(List<RenderPass> pass);
	
	public abstract TextureData takeScreenshot();
	
	public abstract void dispose();
	
	public RenderMode getRenderMode() {
		return this.renderMode;
	}
	
	public void setRenderMode(RenderMode renderMode) {
		this.renderMode = renderMode;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
}
