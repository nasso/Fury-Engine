package io.github.nasso.fury.graphics;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;

public class BufferAttribute {
	private static long last_id = 0;
	
	private long id = last_id++;
	
	private int version = 0;
	private int count = 0;
	private boolean dynamic = false;
	private boolean normalized = false;
	
	private Buffer data;
	
	public BufferAttribute(Buffer data) {
		this.data = data;
		
		this.count = data.capacity();
		this.version++;
	}
	
	public BufferAttribute() {
		
	}
	
	public static BufferAttribute fromIntegerList(int... data) {
		return new BufferAttribute().copyIntegerList(data);
	}
	
	public static BufferAttribute fromFloatList(float... data) {
		return new BufferAttribute().copyFloatList(data);
	}
	
	public static BufferAttribute fromFloatList(List<Float> data) {
		return new BufferAttribute().copyFloatList(data);
	}
	
	public static BufferAttribute fromVector2fList(List<Vector2f> data) {
		return new BufferAttribute().copyVector2fList(data);
	}
	
	public static BufferAttribute fromVector3fList(List<Vector3f> data) {
		return new BufferAttribute().copyVector3fList(data);
	}
	
	public static BufferAttribute fromVector4fList(List<Vector4f> data) {
		return new BufferAttribute().copyVector4fList(data);
	}
	
	public static BufferAttribute fromIntegerList(List<Integer> data) {
		return new BufferAttribute().copyIntegerList(data);
	}
	
	public BufferAttribute copyIntegerList(int... data) {
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		
		buffer.flip();
		
		this.count = data.length;
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public BufferAttribute copyFloatList(float... data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		
		buffer.flip();
		
		this.count = data.length;
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public BufferAttribute copyFloatList(List<Float> data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.size());
		
		for(int i = 0, l = data.size(); i < l; i++)
			buffer.put(data.get(i));
		
		buffer.flip();
		
		this.count = data.size();
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public BufferAttribute copyVector2fList(List<Vector2f> data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(2 * data.size());
		
		Vector2f vec2;
		for(int i = 0, l = data.size(); i < l; i++) {
			vec2 = data.get(i);
			
			buffer.put(vec2.x);
			buffer.put(vec2.y);
		}
		
		buffer.flip();
		
		this.count = data.size() * 2;
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public BufferAttribute copyVector3fList(List<Vector3f> data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(3 * data.size());
		
		Vector3f vec3;
		for(int i = 0, l = data.size(); i < l; i++) {
			vec3 = data.get(i);
			
			buffer.put(vec3.x);
			buffer.put(vec3.y);
			buffer.put(vec3.z);
		}
		
		buffer.flip();
		
		this.count = data.size() * 3;
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public BufferAttribute copyVector4fList(List<Vector4f> data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(4 * data.size());
		
		Vector4f vec4;
		for(int i = 0, l = data.size(); i < l; i++) {
			vec4 = data.get(i);
			
			buffer.put(vec4.x);
			buffer.put(vec4.y);
			buffer.put(vec4.z);
			buffer.put(vec4.w);
		}
		
		buffer.flip();
		
		this.count = data.size() * 4;
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public BufferAttribute copyIntegerList(List<Integer> data) {
		IntBuffer buffer = BufferUtils.createIntBuffer(data.size());
		
		for(int i = 0, l = data.size(); i < l; i++)
			buffer.put(data.get(i));
		
		buffer.flip();
		
		this.count = data.size();
		this.data = buffer;
		this.version++;
		
		return this;
	}
	
	public int getVersion() {
		return this.version;
	}
	
	public BufferAttribute needsUpdate(boolean value) {
		if(value) this.version++;
		
		return this;
	}
	
	public Buffer getData() {
		return this.data;
	}
	
	/**
	 * Don't forget to manually set the count if you call this by yourself !
	 * 
	 * @param data
	 * @return
	 */
	public BufferAttribute setData(Buffer data) {
		this.data = data;
		
		return this;
	}
	
	public boolean isDynamic() {
		return this.dynamic;
	}
	
	public BufferAttribute setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
		return this;
	}
	
	public boolean isNormalized() {
		return this.normalized;
	}
	
	public BufferAttribute setNormalized(boolean normalized) {
		this.normalized = normalized;
		return this;
	}
	
	public long getID() {
		return this.id;
	}
	
	public BufferAttribute setCount(int count) {
		this.count = count;
		
		return this;
	}
	
	public int getCount() {
		return this.count;
	}
}
