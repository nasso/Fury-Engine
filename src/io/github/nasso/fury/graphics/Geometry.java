package io.github.nasso.fury.graphics;

import io.github.nasso.fury.maths.Box3D;
import io.github.nasso.fury.maths.Face3;
import io.github.nasso.fury.maths.Sphere;

import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class Geometry extends AbstractGeometry {
	private String name = "";
	
	private List<Vector3f> vertices = new ArrayList<Vector3f>();
	private List<Face3> faces = new ArrayList<Face3>();
	private List<Vector2f[]> faceVertexUVs = new ArrayList<Vector2f[]>();
	private List<Vector2f[]> faceVertexUVs2 = new ArrayList<Vector2f[]>();
	
	private boolean verticesNeedUpdate = false;
	private boolean elementsNeedUpdate = false;
	private boolean uvsNeedUpdate = false;
	private boolean normalsNeedUpdate = false;
	
	private BufferGeometry bufferGeometryVersion = null;
	private DirectGeometry directGeometryVersion = null;
	
	/**
	 * Bakes matrix transform directly into vertex coordinates.
	 * 
	 * @param mat
	 *            The transform
	 */
	public Geometry applyMatrix(Matrix4f mat) {
		// Positions
		for(int i = 0, l = this.vertices.size(); i < l; i++)
			mat.transformPosition(this.vertices.get(i));
		
		// Normals
		Matrix3f normalMatrix = new Matrix3f().set(mat).invert().transpose();
		for(int i = 0, l = this.faces.size(); i < l; i++) {
			Face3 face = this.faces.get(i);
			
			normalMatrix.transform(face.norm);
			face.norm.normalize();
			
			normalMatrix.transform(face.aNorm);
			face.aNorm.normalize();
			normalMatrix.transform(face.bNorm);
			face.bNorm.normalize();
			normalMatrix.transform(face.cNorm);
			face.cNorm.normalize();
		}
		
		if(this.getBoundingBox() != null) this.computeBoundingBox();
		
		if(this.getBoundingSphere() != null) this.computeBoundingSphere();
		
		this.verticesNeedUpdate = true;
		this.normalsNeedUpdate = true;
		
		return this;
	}
	
	public void computeFaceNormals() {
		Vector3f cb = new Vector3f();
		Vector3f ab = new Vector3f();
		
		Vector3f vA, vB, vC;
		
		for(int f = 0, l = this.faces.size(); f < l; f++) {
			Face3 face = this.faces.get(f);
			
			vA = this.vertices.get(face.a);
			vB = this.vertices.get(face.b);
			vC = this.vertices.get(face.c);
			
			vC.sub(vB, cb);
			vA.sub(vB, ab);
			
			cb.cross(ab).normalize();
			
			face.norm.set(cb);
		}
	}
	
	public void computeVertexNormals(boolean areaWeighted) {
		int v, f;
		int vl, fl;
		Face3 face;
		Vector3f[] normals = new Vector3f[this.vertices.size()];
		
		for(v = 0, vl = normals.length; v < vl; v++)
			normals[v] = new Vector3f();
		
		if(areaWeighted) {
			// vertex normals weighted by triangle areas
			// http://www.iquilezles.org/www/articles/normals/normals.htm
			
			Vector3f cb = new Vector3f(), ab = new Vector3f();
			Vector3f vA, vB, vC;
			
			for(f = 0, fl = this.faces.size(); f < fl; f++) {
				face = this.faces.get(f);
				
				vA = this.vertices.get(face.a);
				vB = this.vertices.get(face.b);
				vC = this.vertices.get(face.c);
				
				vC.sub(vB, cb);
				vA.sub(vB, ab);
				cb.cross(ab);
				
				normals[face.a].add(cb);
				normals[face.b].add(cb);
				normals[face.c].add(cb);
			}
		} else for(f = 0, fl = this.faces.size(); f < fl; f++) {
			face = this.faces.get(f);
			
			normals[face.a].add(face.norm);
			normals[face.b].add(face.norm);
			normals[face.c].add(face.norm);
		}
		
		for(v = 0, vl = normals.length; v < vl; v++)
			normals[v].normalize();
		
		for(f = 0, fl = this.faces.size(); f < fl; f++) {
			face = this.faces.get(f);
			
			face.aNorm.set(normals[face.a]);
			face.bNorm.set(normals[face.b]);
			face.cNorm.set(normals[face.c]);
		}
		
		if(!this.faces.isEmpty()) this.normalsNeedUpdate = true;
	}
	
	public void computeVertexNormals() {
		this.computeVertexNormals(true);
	}
	
	public Box3D computeBoundingBox() {
		if(this.getBoundingBox() == null) this.setBoundingBox(new Box3D());
		
		return this.getBoundingBox().setContaining(this.vertices);
	}
	
	public Sphere computeBoundingSphere() {
		if(this.getBoundingSphere() == null) this.setBoundingSphere(new Sphere());
		
		return this.getBoundingSphere().setContaining(this.vertices);
	}
	
	public Geometry getGeometryVersion() {
		return this;
	}
	
	public BufferGeometry getBufferGeometryVersion() {
		if(this.bufferGeometryVersion == null) this.bufferGeometryVersion = new BufferGeometry().fromGeometry(this.getDirectGeometryVersion());
		
		return this.bufferGeometryVersion;
	}
	
	public DirectGeometry getDirectGeometryVersion() {
		if(this.directGeometryVersion == null) this.directGeometryVersion = new DirectGeometry().fromGeometry(this);
		
		return this.directGeometryVersion;
	}
	
	// Getters, setters
	public String getName() {
		return this.name;
	}
	
	public List<Vector3f> getVertices() {
		return this.vertices;
	}
	
	public List<Face3> getFaces() {
		return this.faces;
	}
	
	public List<Vector2f[]> getFaceVertexUVs() {
		return this.faceVertexUVs;
	}
	
	public List<Vector2f[]> getFaceVertexUVs2() {
		return this.faceVertexUVs2;
	}
	
	public boolean doesVerticesNeedUpdate() {
		return this.verticesNeedUpdate;
	}
	
	public boolean doesElementsNeedUpdate() {
		return this.elementsNeedUpdate;
	}
	
	public boolean doesUVsNeedUpdate() {
		return this.uvsNeedUpdate;
	}
	
	public boolean doesNormalsNeedUpdate() {
		return this.normalsNeedUpdate;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setVerticesNeedUpdate(boolean verticesNeedUpdate) {
		this.verticesNeedUpdate = verticesNeedUpdate;
	}
	
	public void setElementsNeedUpdate(boolean elementsNeedUpdate) {
		this.elementsNeedUpdate = elementsNeedUpdate;
	}
	
	public void setUVsNeedUpdate(boolean uvsNeedUpdate) {
		this.uvsNeedUpdate = uvsNeedUpdate;
	}
	
	public void setNormalsNeedUpdate(boolean normalsNeedUpdate) {
		this.normalsNeedUpdate = normalsNeedUpdate;
	}
}
