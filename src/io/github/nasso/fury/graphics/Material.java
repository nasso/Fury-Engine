package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.level.LevelObject;
import io.github.nasso.fury.maths.Color;

import org.joml.Vector3f;

public class Material extends Observable implements Disposable, LevelObject {
	private static int last_id = 0;
	public final int id = last_id++;
	
	private String name = "";
	
	private Texture2D diffuseTexture;
	private Texture2D normalTexture;
	private Texture2D specularTexture;
	
	// TODO: env cube map for environment reflection
	
	private Vector3f diffuseColor = new Vector3f(1);
	
	private float uvScaling = 1;
	private float normalMapIntensity = 0.8f;
	private float opacity = 1.0f;
	private float alphaThreshold = 0.5f;
	private float shininess = 32.0f;
	private float reflectivity = 0.0f;
	private float specularIntensity = 0.25f;
	private float pointSize = 1f;
	private float lineWidth = 1f;
	
	private int polygonMode = Fury.FILL;
	
	private boolean transparent = false;
	private boolean opaque = true;
	private boolean depthTest = true;
	private boolean depthWrite = true;
	private boolean faceSideNormalCorrection = true;
	private boolean needsUpdate = true;
	
	private String lvl_id = "";
	
	public void setID(String id) {
		this.lvl_id = id;
	}
	
	public String getID() {
		return this.lvl_id;
	}
	
	public Material() {
		
	}
	
	public void set(PrimitiveProperties props) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		this.diffuseColor.set(props.getVector3f("diffuseColor", this.diffuseColor));
		
		this.uvScaling = props.getFloat("uvScaling", 1.0f);
		this.normalMapIntensity = props.getFloat("normalMapIntensity", 0.8f);
		this.opacity = props.getFloat("opacity", 1.0f);
		this.alphaThreshold = props.getFloat("alphaThreshold", 0.5f);
		this.shininess = props.getFloat("shininess", 32.0f);
		this.reflectivity = props.getFloat("reflectivity", 0.0f);
		this.specularIntensity = props.getFloat("specularIntensity", 0.25f);
		this.pointSize = props.getFloat("pointSize", 1.0f);
		this.lineWidth = props.getFloat("lineWidth", 1.0f);
		
		this.polygonMode = (int) Fury.class.getField(props.getString("polygonMode", "FILL")).get(null);
		
		this.transparent = props.getBool("transparent", false);
		this.opaque = props.getBool("opaque", true);
		this.depthTest = props.getBool("depthTest", true);
		this.depthWrite = props.getBool("depthWrite", true);
		this.faceSideNormalCorrection = props.getBool("faceSideNormalCorrection", true);
		this.needsUpdate = props.getBool("needsUpdate", true);
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
	}
	
	public Material copy(Material src) {
		this.diffuseTexture = src.diffuseTexture;
		this.normalTexture = src.normalTexture;
		this.specularTexture = src.specularTexture;
		
		this.diffuseColor.set(src.diffuseColor);
		
		this.uvScaling = src.uvScaling;
		this.normalMapIntensity = src.normalMapIntensity;
		this.opacity = src.opacity;
		this.alphaThreshold = src.alphaThreshold;
		this.shininess = src.shininess;
		this.reflectivity = src.reflectivity;
		this.specularIntensity = src.specularIntensity;
		this.pointSize = src.pointSize;
		this.lineWidth = src.lineWidth;
		
		this.polygonMode = src.polygonMode;
		
		this.transparent = src.transparent;
		this.opaque = src.opaque;
		this.depthTest = src.depthTest;
		this.depthWrite = src.depthWrite;
		this.faceSideNormalCorrection = src.faceSideNormalCorrection;
		this.needsUpdate = src.needsUpdate;
		
		return this;
	}
	
	public Material clone() {
		return new Material().copy(this);
	}
	
	public Texture2D getDiffuseTexture() {
		return this.diffuseTexture;
	}
	
	public void setDiffuseTexture(Texture2D diffuseTexture) {
		this.diffuseTexture = diffuseTexture;
	}
	
	public Texture2D getNormalTexture() {
		return this.normalTexture;
	}
	
	public void setNormalTexture(Texture2D normalTexture) {
		this.normalTexture = normalTexture;
	}
	
	public Texture2D getSpecularTexture() {
		return this.specularTexture;
	}
	
	public void setSpecularTexture(Texture2D specularTexture) {
		this.specularTexture = specularTexture;
	}
	
	public float getShininess() {
		return this.shininess;
	}
	
	public void setShininess(float shininess) {
		this.shininess = shininess;
	}
	
	public float getReflectivity() {
		return this.reflectivity;
	}
	
	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
	
	public boolean isTransparent() {
		return this.transparent;
	}
	
	public void setTransparent(boolean transparent) {
		this.transparent = transparent;
	}
	
	public float getOpacity() {
		return this.opacity;
	}
	
	public void setOpacity(float opacity) {
		this.opacity = opacity;
	}
	
	public boolean isDepthTest() {
		return this.depthTest;
	}
	
	public void setDepthTest(boolean depthTest) {
		this.depthTest = depthTest;
	}
	
	public boolean isDepthWrite() {
		return this.depthWrite;
	}
	
	public void setDepthWrite(boolean depthWrite) {
		this.depthWrite = depthWrite;
	}
	
	public boolean isNeedsUpdate() {
		return this.needsUpdate;
	}
	
	public void setNeedsUpdate(boolean needsUpdate) {
		this.needsUpdate = needsUpdate;
	}
	
	public Vector3f getDiffuseColor() {
		return this.diffuseColor;
	}
	
	public void setDiffuseColor(Vector3f diffuseColor) {
		this.diffuseColor.set(diffuseColor);
	}
	
	public void setDiffuseColor(Color color) {
		this.setDiffuseColor(color.r, color.g, color.b);
	}
	
	public void setDiffuseColor(float r, float g, float b) {
		this.diffuseColor.set(r, g, b);
	}
	
	public void setDiffuseColor(int color) {
		this.diffuseColor.set(((color >> 16) & 0xFF) / 255.0f, ((color >> 8) & 0xFF) / 255.0f, ((color >> 0) & 0xFF) / 255.0f);
	}
	
	public float getUVScaling() {
		return this.uvScaling;
	}
	
	public void setUVScaling(float uvScaling) {
		this.uvScaling = uvScaling;
	}
	
	public float getSpecularIntensity() {
		return this.specularIntensity;
	}
	
	public void setSpecularIntensity(float specularIntensity) {
		this.specularIntensity = specularIntensity;
	}
	
	public boolean isFaceSideNormalCorrection() {
		return this.faceSideNormalCorrection;
	}
	
	public void setFaceSideNormalCorrection(boolean faceSideNormalCorrection) {
		this.faceSideNormalCorrection = faceSideNormalCorrection;
	}
	
	public int getPolygonMode() {
		return this.polygonMode;
	}
	
	public void setPolygonMode(int polygonMode) {
		this.polygonMode = polygonMode;
	}
	
	public float getPointSize() {
		return this.pointSize;
	}
	
	public void setPointSize(float pointSize) {
		this.pointSize = pointSize;
	}
	
	public float getLineWidth() {
		return this.lineWidth;
	}
	
	public void setLineWidth(float lineWidth) {
		this.lineWidth = lineWidth;
	}
	
	public boolean isOpaque() {
		return this.opaque;
	}
	
	public void setOpaque(boolean opaque) {
		this.opaque = opaque;
	}
	
	public float getAlphaThreshold() {
		return this.alphaThreshold;
	}
	
	public void setAlphaThreshold(float alphaThreshold) {
		this.alphaThreshold = alphaThreshold;
	}
	
	public float getNormalMapIntensity() {
		return this.normalMapIntensity;
	}
	
	public void setNormalMapIntensity(float normalMapIntensity) {
		this.normalMapIntensity = normalMapIntensity;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "[material: \"" + this.name + "\"]";
	}
}
