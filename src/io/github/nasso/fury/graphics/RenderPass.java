package io.github.nasso.fury.graphics;

import io.github.nasso.fury.graphics.fx.PostEffect;
import io.github.nasso.fury.level.Camera;
import io.github.nasso.fury.level.LevelObject;
import io.github.nasso.fury.level.Scene;

import java.util.ArrayList;
import java.util.List;

public class RenderPass implements LevelObject {
	private Scene sce;
	private Camera cam;
	private RenderTarget target;
	private boolean enabled = true;
	private List<PostEffect> postEffects = new ArrayList<PostEffect>();
	
	private String lvl_id = "";
	
	public void setID(String id) {
		this.lvl_id = id;
	}
	
	public String getID() {
		return this.lvl_id;
	}
	
	private String name = "";
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public RenderPass(Scene sce, Camera cam, RenderTarget target) {
		this.setScene(sce);
		this.setCamera(cam);
		this.setTarget(target);
	}
	
	public RenderPass(Scene sce, Camera cam) {
		this(sce, cam, null);
	}
	
	public Scene getScene() {
		return this.sce;
	}
	
	public void setScene(Scene sce) {
		this.sce = sce;
	}
	
	public Camera getCamera() {
		return this.cam;
	}
	
	public void setCamera(Camera cam) {
		this.cam = cam;
	}
	
	public RenderTarget getTarget() {
		return this.target;
	}
	
	public void setTarget(RenderTarget target) {
		this.target = target;
	}
	
	public List<PostEffect> getPostEffects() {
		return this.postEffects;
	}
	
	public RenderPass addPostEffects(PostEffect... effect) {
		for(int i = 0, l = effect.length; i < l; i++)
			this.postEffects.add(effect[i]);
		
		return this;
	}
	
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
