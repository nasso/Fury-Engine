package io.github.nasso.fury.graphics;

public enum RenderMode {
	FORWARD_RENDERING, DEFERRED_RENDERING, DEFERRED_LIGHTING
}
