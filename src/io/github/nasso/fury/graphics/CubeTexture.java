package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.level.LevelObject;
import io.github.nasso.fury.opengl.OGLManager;

import java.nio.ByteBuffer;

public class CubeTexture extends Observable implements Disposable, LevelObject {
	public static class Builder {
		private int width = 512;
		private int height = 512;
		private int magFilter = Fury.LINEAR;
		private int minFilter = Fury.LINEAR_MIPMAP_LINEAR;
		private int wrapR = Fury.CLAMP_TO_EDGE;
		private int wrapS = Fury.CLAMP_TO_EDGE;
		private int wrapT = Fury.CLAMP_TO_EDGE;
		private int internalFormat = Fury.SRGB_ALPHA;
		private int format = Fury.RGBA;
		private int type = Fury.UNSIGNED_BYTE;
		private int anisotropy = 1;
		
		private ByteBuffer posXData = null;
		private ByteBuffer negXData = null;
		private ByteBuffer posYData = null;
		private ByteBuffer negYData = null;
		private ByteBuffer posZData = null;
		private ByteBuffer negZData = null;
		
		public Builder() {
			
		}
		
		public Builder(PrimitiveProperties props) {
			this.width = props.getInt("width", props.getInt("size", 16));
			this.height = props.getInt("height", props.getInt("size", 16));
			this.magFilter = Fury.valueOf(props.getString("magFilter", "LINEAR"));
			this.minFilter = Fury.valueOf(props.getString("minFilter", "LINEAR_MIPMAP_LINEAR"));
			this.wrapR = Fury.valueOf(props.getString("wrapR", "CLAMP_TO_EDGE"));
			this.wrapS = Fury.valueOf(props.getString("wrapS", "CLAMP_TO_EDGE"));
			this.wrapT = Fury.valueOf(props.getString("wrapT", "CLAMP_TO_EDGE"));
			this.internalFormat = Fury.valueOf(props.getString("internalFormat", "SRGB_ALPHA"));
			this.format = Fury.valueOf(props.getString("format", "RGBA"));
			this.type = Fury.valueOf(props.getString("type", "UNSIGNED_BYTE"));
			this.anisotropy = props.getInt("anisotropy", 0);
		}
		
		public Builder width(int value) {
			this.width = value;
			return this;
		}
		
		public Builder height(int value) {
			this.height = value;
			return this;
		}
		
		public Builder magFilter(int value) {
			this.magFilter = value;
			return this;
		}
		
		public Builder minFilter(int value) {
			this.minFilter = value;
			return this;
		}
		
		public Builder wrapR(int value) {
			this.wrapR = value;
			return this;
		}
		
		public Builder wrapS(int value) {
			this.wrapS = value;
			return this;
		}
		
		public Builder wrapT(int value) {
			this.wrapT = value;
			return this;
		}
		
		public Builder internalFormat(int value) {
			this.internalFormat = value;
			return this;
		}
		
		public Builder format(int value) {
			this.format = value;
			return this;
		}
		
		public Builder type(int value) {
			this.type = value;
			return this;
		}
		
		public Builder anisotropy(int value) {
			this.anisotropy = value;
			return this;
		}
		
		public Builder negZData(ByteBuffer value) {
			this.negZData = value;
			return this;
		}
		
		public Builder negXData(ByteBuffer value) {
			this.negXData = value;
			return this;
		}
		
		public Builder posYData(ByteBuffer value) {
			this.posYData = value;
			return this;
		}
		
		public Builder negYData(ByteBuffer value) {
			this.negYData = value;
			return this;
		}
		
		public Builder posZData(ByteBuffer value) {
			this.posZData = value;
			return this;
		}
		
		public Builder posXData(ByteBuffer value) {
			this.posXData = value;
			return this;
		}
		
		public CubeTexture build() {
			return new CubeTexture(this.width, this.height, this.magFilter, this.minFilter, this.wrapR, this.wrapS, this.wrapT, this.internalFormat, this.format, this.type, this.anisotropy, this.posXData, this.negXData, this.posYData, this.negYData, this.posZData, this.negZData);
		}
	}
	
	private int width = 512;
	private int height = 512;
	private int magFilter = Fury.LINEAR;
	private int minFilter = Fury.LINEAR_MIPMAP_LINEAR;
	private int wrapR = Fury.CLAMP_TO_EDGE;
	private int wrapS = Fury.CLAMP_TO_EDGE;
	private int wrapT = Fury.CLAMP_TO_EDGE;
	private int internalFormat = Fury.SRGB_ALPHA;
	private int format = Fury.RGBA;
	private int type = Fury.UNSIGNED_BYTE;
	private int anisotropy = 1;
	
	private ByteBuffer posXData = null;
	private ByteBuffer negXData = null;
	private ByteBuffer posYData = null;
	private ByteBuffer negYData = null;
	private ByteBuffer posZData = null;
	private ByteBuffer negZData = null;
	
	private int version = 0;
	
	private String id = "";
	
	public void setID(String id) {
		this.id = id;
	}
	
	public String getID() {
		return this.id;
	}
	
	private String name = "";
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * I think it's the biggest constructor ever.
	 * 
	 * @param width
	 * @param height
	 * @param magFilter
	 * @param minFilter
	 * @param wrapS
	 * @param wrapT
	 * @param internalFormat
	 * @param format
	 * @param type
	 * @param anisotropy
	 * @param data
	 * @param xdivs
	 *            Grid x divisions (column count)
	 * @param ydivs
	 *            Grid y divisions (row count)
	 * @param lCaseX
	 *            Grid X position of the left texture
	 * @param lCaseY
	 *            Grid Y position of the left texture
	 * @param fCaseX
	 *            Grid X position of the front texture
	 * @param fCaseY
	 *            Grid Y position of the front texture
	 * @param rCaseX
	 *            Grid X position of the right texture
	 * @param rCaseY
	 *            Grid Y position of the right texture
	 * @param bCaseX
	 *            Grid X position of the back texture
	 * @param bCaseY
	 *            Grid Y position of the back texture
	 * @param uCaseX
	 *            Grid X position of the up texture
	 * @param uCaseY
	 *            Grid Y position of the up texture
	 * @param dCaseX
	 *            Grid X position of the down texture
	 * @param dCaseY
	 *            Grid Y position of the down texture
	 */
	public CubeTexture(int width, int height, int magFilter, int minFilter, int wrapR, int wrapS, int wrapT, int internalFormat, int format, int type, int anisotropy, ByteBuffer posXData, ByteBuffer negXData, ByteBuffer posYData, ByteBuffer negYData, ByteBuffer posZData, ByteBuffer negZData) {
		this.width = width;
		this.height = height;
		this.magFilter = magFilter;
		this.minFilter = minFilter;
		this.wrapS = wrapS;
		this.wrapT = wrapT;
		this.internalFormat = internalFormat;
		this.format = format;
		this.type = type;
		this.anisotropy = anisotropy;
		
		this.posXData = posXData;
		this.negXData = negXData;
		this.posYData = posYData;
		this.negYData = negYData;
		this.posZData = posZData;
		this.negZData = negZData;
	}
	
	public CubeTexture(PrimitiveProperties props, ByteBuffer posXData, ByteBuffer negXData, ByteBuffer posYData, ByteBuffer negYData, ByteBuffer posZData, ByteBuffer negZData) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		this(props.getInt("width", props.getInt("size", 16)), props.getInt("height", props.getInt("size", 16)), (int) Fury.class.getField(props.getString("magFilter", "LINEAR")).get(null), (int) Fury.class.getField(props.getString("minFilter", "LINEAR_MIPMAP_LINEAR")).get(null), (int) Fury.class.getField(props.getString("wrapR", "CLAMP_TO_EDGE")).get(null), (int) Fury.class.getField(props.getString("wrapS", "CLAMP_TO_EDGE")).get(null), (int) Fury.class.getField(props.getString("wrapT", "CLAMP_TO_EDGE")).get(null), (int) Fury.class.getField(props.getString("internalFormat", "SRGB_ALPHA")).get(null), (int) Fury.class.getField(props.getString("format", "RGBA")).get(null), (int) Fury.class.getField(props.getString("type", "UNSIGNED_BYTE")).get(null), props.getInt("anisotropy", 0), posXData, negXData, posYData, negYData, posZData, negZData);
	}
	
	public ByteBuffer getPosXData() {
		return this.posXData;
	}
	
	public void setPosXData(ByteBuffer posXData) {
		this.posXData = posXData;
	}
	
	public ByteBuffer getNegXData() {
		return this.negXData;
	}
	
	public void setNegXData(ByteBuffer negXData) {
		this.negXData = negXData;
	}
	
	public ByteBuffer getPosYData() {
		return this.posYData;
	}
	
	public void setPosYData(ByteBuffer posYData) {
		this.posYData = posYData;
	}
	
	public ByteBuffer getNegYData() {
		return this.negYData;
	}
	
	public void setNegYData(ByteBuffer negYData) {
		this.negYData = negYData;
	}
	
	public ByteBuffer getPosZData() {
		return this.posZData;
	}
	
	public void setPosZData(ByteBuffer posZData) {
		this.posZData = posZData;
	}
	
	public ByteBuffer getNegZData() {
		return this.negZData;
	}
	
	public void setNegZData(ByteBuffer negZData) {
		this.negZData = negZData;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
	
	public void needUpdate() {
		this.version++;
	}
	
	public int getVersion() {
		return this.version;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getMagFilter() {
		return this.magFilter;
	}
	
	public int getMinFilter() {
		return this.minFilter;
	}
	
	public int getWrapS() {
		return this.wrapS;
	}
	
	public int getWrapT() {
		return this.wrapT;
	}
	
	public int getInternalFormat() {
		return this.internalFormat;
	}
	
	public int getFormat() {
		return this.format;
	}
	
	public int getType() {
		return this.type;
	}
	
	public int getAnisotropy() {
		return this.anisotropy;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setMagFilter(int magFilter) {
		this.magFilter = magFilter;
	}
	
	public void setMinFilter(int minFilter) {
		this.minFilter = minFilter;
	}
	
	public int getWrapR() {
		return this.wrapR;
	}
	
	public void setWrapR(int wrapR) {
		this.wrapR = wrapR;
	}
	
	public void setWrapS(int wrapS) {
		this.wrapS = wrapS;
	}
	
	public void setWrapT(int wrapT) {
		this.wrapT = wrapT;
	}
	
	public void setInternalFormat(int internalFormat) {
		this.internalFormat = internalFormat;
	}
	
	public void setFormat(int format) {
		this.format = format;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public void setAnisotropy(int anisotropy) {
		this.anisotropy = Math.min(OGLManager.get().maxAnisotropy, anisotropy);
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
	}
}
