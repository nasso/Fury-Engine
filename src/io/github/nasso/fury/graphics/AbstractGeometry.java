package io.github.nasso.fury.graphics;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.level.LevelObject;
import io.github.nasso.fury.maths.Box3D;
import io.github.nasso.fury.maths.Sphere;

public abstract class AbstractGeometry extends Observable implements Disposable, LevelObject {
	private int renderMode = Fury.TRIANGLES;
	private int cullFace = Fury.BACK;
	
	private Sphere boundingSphere = null;
	private Box3D boundingBox = null;
	
	private String id = "";
	
	public void setID(String id) {
		this.id = id;
	}
	
	public String getID() {
		return this.id;
	}
	
	private String name = "";
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
	}
	
	public Sphere getBoundingSphere() {
		return this.boundingSphere;
	}
	
	public Box3D getBoundingBox() {
		return this.boundingBox;
	}
	
	public void setBoundingSphere(Sphere sphere) {
		this.boundingSphere = sphere;
	}
	
	public void setBoundingBox(Box3D box) {
		this.boundingBox = box;
	}
	
	public abstract Sphere computeBoundingSphere();
	
	public abstract Box3D computeBoundingBox();
	
	public abstract Geometry getGeometryVersion();
	
	public abstract DirectGeometry getDirectGeometryVersion();
	
	public abstract BufferGeometry getBufferGeometryVersion();
	
	public int getRenderMode() {
		return this.renderMode;
	}
	
	public void setRenderMode(int renderMode) {
		this.renderMode = renderMode;
	}
	
	public int getCullFace() {
		return this.cullFace;
	}
	
	public void setCullFace(int cullFace) {
		this.cullFace = cullFace;
	}
}