package io.github.nasso.fury.extra;

import io.github.nasso.fury.level.NFClippedCamera;
import io.github.nasso.fury.level.Object3D;

public class OrthographicCamera extends NFClippedCamera {
	private float left, right, top, bottom, zoom;
	
	public OrthographicCamera(float left, float right, float top, float bottom, float near, float far) {
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
		this.near = near;
		this.far = far;
		
		this.zoom = 1;
	}
	
	public OrthographicCamera(float left, float right, float top, float bottom) {
		this(left, right, top, bottom, 0.1f, 2000f);
	}
	
	private OrthographicCamera() {
		// So this is private just for the clone method
	}
	
	public float getLeft() {
		return this.left;
	}
	
	public void setLeft(float left) {
		this.left = left;
	}
	
	public float getRight() {
		return this.right;
	}
	
	public void setRight(float right) {
		this.right = right;
	}
	
	public float getTop() {
		return this.top;
	}
	
	public void setTop(float top) {
		this.top = top;
	}
	
	public float getBottom() {
		return this.bottom;
	}
	
	public void setBottom(float bottom) {
		this.bottom = bottom;
	}
	
	public float getZoom() {
		return this.zoom;
	}
	
	public void setZoom(float zoom) {
		this.zoom = zoom;
	}
	
	public void updateProjectionMatrix() {
		float dx = (this.right - this.left) / (2 * this.zoom);
		float dy = (this.top - this.bottom) / (2 * this.zoom);
		float cx = (this.right + this.left) / 2;
		float cy = (this.top + this.bottom) / 2;
		
		this.projectionMatrix.setOrtho(cx - dx, cx + dx, cy + dy, cy - dy, this.near, this.far);
	}
	
	@Override
	public OrthographicCamera copy(Object3D obj) {
		if(!(obj instanceof OrthographicCamera)) throw new IllegalArgumentException(obj + " isn't an instance of OrthographicCamera");
		
		OrthographicCamera cam = (OrthographicCamera) obj;
		
		super.copy(cam);
		this.left = cam.left;
		this.right = cam.right;
		this.top = cam.top;
		this.bottom = cam.bottom;
		this.near = cam.near;
		this.far = cam.far;
		this.zoom = cam.zoom;
		
		return this;
	}
	
	@Override
	public OrthographicCamera clone() {
		return new OrthographicCamera().copy(this);
	}
}
