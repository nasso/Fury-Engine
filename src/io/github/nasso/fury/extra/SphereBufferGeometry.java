package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.BufferAttribute;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.maths.Sphere;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

public class SphereBufferGeometry extends BufferGeometry {
	private float radius = 1;
	private float phiStart = 0, phiLength = (float) (Math.PI * 2.0);
	private float thetaStart = 0, thetaLength = (float) Math.PI;
	
	private int widthSegments = 8, heightSegments = 8;
	
	private boolean wireframe = false;
	
	public SphereBufferGeometry() {
		this(1);
	}
	
	public SphereBufferGeometry(float radius) {
		this(radius, 8, 8);
	}
	
	public SphereBufferGeometry(float radius, int ws, int hs) {
		this.setRadius(radius);
		this.setWidthSegments(ws);
		this.setHeightSegments(hs);
		
		this.updateGeometry();
	}
	
	public SphereBufferGeometry(PrimitiveProperties props) {
		this.setRadius(props.getFloat("radius", this.radius));
		this.setPhiStart(props.getFloat("phiStart", this.phiStart));
		this.setPhiLength(props.getFloat("phiLength", this.phiLength));
		this.setThetaStart(props.getFloat("thetaStart", this.thetaStart));
		this.setThetaLength(props.getFloat("thetaLength", this.thetaLength));
		
		this.setWidthSegments(props.getInt("widthSegments", props.getInt("segments", this.widthSegments)));
		this.setHeightSegments(props.getInt("heightSegments", props.getInt("segments", this.heightSegments)));
		
		this.setWireframe(props.getBool("wireframe", false));
		
		this.updateGeometry();
	}
	
	private float[] _positions;
	private float[] _normals;
	private float[] _uvs;
	private Vector3f _normal = new Vector3f();
	
	public void updateGeometry() {
		float thetaEnd = this.thetaStart + this.thetaLength;
		int vertexCount = ((this.widthSegments + 1) * (this.heightSegments + 1));
		
		this._positions = new float[vertexCount * 3];
		this._normals = new float[vertexCount * 3];
		this._uvs = new float[vertexCount * 2];
		
		int index = 0;
		
		int[][] vertices = new int[this.heightSegments + 1][this.widthSegments + 1];
		
		for(int y = 0; y <= this.heightSegments; y++) {
			float v = (float) y / (float) this.heightSegments;
			
			for(int x = 0; x <= this.widthSegments; x++) {
				float u = (float) x / (float) this.widthSegments;
				
				float px = (float) (-this.radius * Math.cos(this.phiStart + u * this.phiLength) * Math.sin(this.thetaStart + v * this.thetaLength));
				float py = (float) (this.radius * Math.sin(this.phiStart + u * this.phiLength) * Math.sin(this.thetaStart + v * this.thetaLength));
				float pz = (float) (this.radius * Math.cos(this.thetaStart + v * this.thetaLength));
				
				this._normal.set(px, py, pz).normalize();
				
				this._positions[index * 3] = px;
				this._positions[index * 3 + 1] = py;
				this._positions[index * 3 + 2] = pz;
				
				this._normals[index * 3] = this._normal.x;
				this._normals[index * 3 + 1] = this._normal.y;
				this._normals[index * 3 + 2] = this._normal.z;
				
				this._uvs[index * 2] = u;
				this._uvs[index * 2 + 1] = 1.0f - v;
				
				vertices[y][x] = index;
				
				index++;
			}
		}
		
		List<Integer> indicesList = new ArrayList<Integer>();
		
		if(this.wireframe) for(int y = 0; y < this.heightSegments; y++)
			for(int x = 0; x < this.widthSegments; x++) {
				int v1 = vertices[y][x + 1];
				int v2 = vertices[y][x];
				int v3 = vertices[y + 1][x];
				int v4 = vertices[y + 1][x + 1];
				
				if(y != 0 || this.thetaStart > 0) {
					indicesList.add(v1);
					indicesList.add(v2);
					indicesList.add(v1);
					indicesList.add(v4);
				}
				
				if(y != this.heightSegments - 1 || thetaEnd < Math.PI) {
					indicesList.add(v2);
					indicesList.add(v3);
					indicesList.add(v3);
					indicesList.add(v4);
				}
			}
		else for(int y = 0; y < this.heightSegments; y++)
			for(int x = 0; x < this.widthSegments; x++) {
				int v1 = vertices[y][x + 1];
				int v2 = vertices[y][x];
				int v3 = vertices[y + 1][x];
				int v4 = vertices[y + 1][x + 1];
				
				if(y != 0 || this.thetaStart > 0) {
					indicesList.add(v1);
					indicesList.add(v4);
					indicesList.add(v2);
				}
				
				if(y != this.heightSegments - 1 || thetaEnd < Math.PI) {
					indicesList.add(v2);
					indicesList.add(v4);
					indicesList.add(v3);
				}
			}
		
		this.setRenderMode(this.wireframe ? Fury.LINES : Fury.TRIANGLES);
		this.setIndices(BufferAttribute.fromIntegerList(indicesList));
		this.setPositions(BufferAttribute.fromFloatList(this._positions));
		this.setNormals(BufferAttribute.fromFloatList(this._normals));
		this.setUVs(BufferAttribute.fromFloatList(this._uvs));
		
		this.setBoundingSphere(new Sphere(this.radius));
	}
	
	public float getRadius() {
		return this.radius;
	}
	
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public float getPhiStart() {
		return this.phiStart;
	}
	
	public void setPhiStart(float phiStart) {
		this.phiStart = phiStart;
	}
	
	public float getPhiLength() {
		return this.phiLength;
	}
	
	public void setPhiLength(float phiLength) {
		this.phiLength = phiLength;
	}
	
	public float getThetaStart() {
		return this.thetaStart;
	}
	
	public void setThetaStart(float thetaStart) {
		this.thetaStart = thetaStart;
	}
	
	public float getThetaLength() {
		return this.thetaLength;
	}
	
	public void setThetaLength(float thetaLength) {
		this.thetaLength = thetaLength;
	}
	
	public int getWidthSegments() {
		return this.widthSegments;
	}
	
	public void setWidthSegments(int widthSegments) {
		this.widthSegments = Math.max(widthSegments, 3);
	}
	
	public int getHeightSegments() {
		return this.heightSegments;
	}
	
	public void setHeightSegments(int heightSegments) {
		this.heightSegments = Math.max(heightSegments, 2);
	}
	
	public boolean isWireframe() {
		return this.wireframe;
	}
	
	public void setWireframe(boolean wireframe) {
		this.wireframe = wireframe;
	}
}
