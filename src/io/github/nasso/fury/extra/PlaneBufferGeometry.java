package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.BufferAttribute;
import io.github.nasso.fury.graphics.BufferGeometry;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Vector2f;
import org.lwjgl.BufferUtils;

public class PlaneBufferGeometry extends BufferGeometry {
	private float width = 1;
	private float height = 1;
	private int widthdivs = 1;
	private int heightdivs = 1;
	
	public PlaneBufferGeometry(float width, float height, int widthdivs, int heightdivs) {
		this.setWidth(width);
		this.setHeight(height);
		this.setWidthDivs(widthdivs);
		this.setHeightDivs(heightdivs);
		
		this.updateGeometry();
	}
	
	public PlaneBufferGeometry(float sizex, float sizey) {
		this(sizex, sizey, 1, 1);
	}
	
	public PlaneBufferGeometry(PrimitiveProperties props) {
		Vector2f sizevec = props.getVector2f("size");
		Vector2f divsvec = props.getVector2f("divs");
		
		this.setWidth(props.getFloat("sizex", (sizevec == null ? props.getFloat("size", 0) : sizevec.x)));
		this.setHeight(props.getFloat("sizey", (sizevec == null ? props.getFloat("size", 0) : sizevec.y)));
		this.setWidthDivs(props.getInt("xdivs", (divsvec == null ? props.getInt("divs", 1) : (int) divsvec.x)));
		this.setHeightDivs(props.getInt("ydivs", (divsvec == null ? props.getInt("divs", 1) : (int) divsvec.y)));
		
		this.updateGeometry();
	}
	
	public PlaneBufferGeometry() {
		this(1, 1);
	}
	
	public void updateGeometry() {
		float width_half = this.width / 2;
		float height_half = this.height / 2;
		
		int gridX1 = this.widthdivs + 1;
		int gridY1 = this.heightdivs + 1;
		
		float segment_width = this.width / this.widthdivs;
		float segment_height = this.height / this.heightdivs;
		
		FloatBuffer positions = BufferUtils.createFloatBuffer(gridX1 * gridY1 * 3);
		FloatBuffer normals = BufferUtils.createFloatBuffer(gridX1 * gridY1 * 3);
		FloatBuffer uvs = BufferUtils.createFloatBuffer(gridX1 * gridY1 * 2);
		
		int offset = 0;
		int offset2 = 0;
		
		for(int iy = 0; iy < gridY1; iy++) {
			float y = iy * segment_height - height_half;
			
			for(int ix = 0; ix < gridX1; ix++) {
				float x = ix * segment_width - width_half;
				
				positions.put(offset, x);
				positions.put(offset + 1, -y);
				positions.put(offset + 2, 0);
				
				normals.put(offset, 0);
				normals.put(offset + 1, 0);
				normals.put(offset + 2, 1);
				
				uvs.put(offset2, (float) ix / this.widthdivs);
				uvs.put(offset2 + 1, 1f - (float) iy / this.heightdivs);
				
				offset += 3;
				offset2 += 2;
			}
		}
		
		offset = 0;
		
		IntBuffer indices = BufferUtils.createIntBuffer(this.widthdivs * this.heightdivs * 6);
		
		for(int iy = 0; iy < this.heightdivs; iy++)
			for(int ix = 0; ix < this.widthdivs; ix++) {
				int a = ix + gridX1 * iy;
				int b = ix + gridX1 * (iy + 1);
				int c = (ix + 1) + gridX1 * (iy + 1);
				int d = (ix + 1) + gridX1 * iy;
				
				indices.put(offset, a);
				indices.put(offset + 1, b);
				indices.put(offset + 2, d);
				
				indices.put(offset + 3, b);
				indices.put(offset + 4, c);
				indices.put(offset + 5, d);
				
				offset += 6;
			}
		
		this.setIndices(new BufferAttribute().setData(indices).setCount(this.widthdivs * this.heightdivs * 6));
		this.setPositions(new BufferAttribute().setData(positions).setCount(gridX1 * gridY1 * 3));
		this.setNormals(new BufferAttribute().setData(normals).setCount(gridX1 * gridY1 * 3));
		this.setUVs(new BufferAttribute().setData(uvs).setCount(gridX1 * gridY1 * 2));
	}
	
	public float getWidth() {
		return this.width;
	}
	
	public void setWidth(float width) {
		this.width = width;
	}
	
	public float getHeight() {
		return this.height;
	}
	
	public void setHeight(float height) {
		this.height = height;
	}
	
	public int getWidthDivs() {
		return this.widthdivs;
	}
	
	public void setWidthDivs(int divs) {
		this.widthdivs = divs;
	}
	
	public int getHeightDivs() {
		return this.heightdivs;
	}
	
	public void setHeightDivs(int divs) {
		this.heightdivs = divs;
	}
}
