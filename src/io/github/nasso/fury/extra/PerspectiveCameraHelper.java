package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.graphics.BufferAttribute;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Object3D;

public class PerspectiveCameraHelper extends Object3D {
	private Mesh upTri = new Mesh();
	private Mesh cameraBody = new Mesh();
	
	public PerspectiveCameraHelper() {
		Material upaxisMat = new Material();
		upaxisMat.setDiffuseColor(0x000000);
		
		this.upTri.setMaterial(upaxisMat);
		this.cameraBody.setMaterial(upaxisMat.clone());
		
		this.addChildren(this.upTri, this.cameraBody);
		
		this.generateGeometry(0.8f, 0.4f);
	}
	
	private void generateGeometry(float upTriBaseSize, float upTriHeight) {
		BufferGeometry upTriGeom = new BufferGeometry();
		upTriGeom.setPositions(BufferAttribute.fromFloatList(upTriBaseSize / 2.0f, 0, -1, -upTriBaseSize / 2.0f, 0, -1, 0, upTriHeight, -1));
		upTriGeom.setNormals(BufferAttribute.fromFloatList(0, 0, -1, 0, 0, -1, 0, 0, -1));
		upTriGeom.setCullFace(Fury.NONE);
		
		this.upTri.setGeometry(upTriGeom);
		this.upTri.setPosition(0, 1.1f, 0);
		
		BufferGeometry camGeom = new BufferGeometry();
		camGeom.setPositions(BufferAttribute.fromFloatList(0, 0, 0, 1, 1, -1, -1, 1, -1, 1, -1, -1, -1, -1, -1));
		camGeom.setNormals(BufferAttribute.fromFloatList(0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1));
		camGeom.setIndices(BufferAttribute.fromIntegerList(0, 1, 0, 2, 0, 3, 0, 4, 1, 2, 2, 4, 4, 3, 3, 1));
		camGeom.setRenderMode(Fury.LINES);
		camGeom.setCullFace(Fury.NONE);
		
		this.cameraBody.setGeometry(camGeom);
	}
	
	public void update(PerspectiveCamera cam) {
		this.cameraBody.setScale(1f, 1f / cam.getRatio(), 1);
		this.upTri.setPosition(0, 1.1f / cam.getRatio(), 0);
	}
	
	public Mesh getUpTriangle() {
		return this.upTri;
	}
	
	public Mesh getCameraBody() {
		return this.cameraBody;
	}
}
