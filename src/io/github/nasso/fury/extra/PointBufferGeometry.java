package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.BufferAttribute;
import io.github.nasso.fury.graphics.BufferGeometry;

public class PointBufferGeometry extends BufferGeometry {
	public PointBufferGeometry() {
		this.setPositions(BufferAttribute.fromFloatList(0, 0, 0));
		this.setNormals(BufferAttribute.fromFloatList(0, 0, 1));
	}
	
	public PointBufferGeometry(PrimitiveProperties props) {
		this.setPositions(BufferAttribute.fromFloatList(0, 0, 0));
		this.setNormals(BufferAttribute.fromFloatList(0, 0, 1));
	}
}
