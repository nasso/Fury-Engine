package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.Game;
import io.github.nasso.fury.core.GameWindow;
import io.github.nasso.fury.core.PrimitiveProperties;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class FPSCamera extends PerspectiveCamera {
	private Vector3f orientation = new Vector3f(0, 0, -1);
	private Vector3f center = new Vector3f();
	
	private boolean freeLookFly = false;
	private boolean allowVerticalFly = true;
	
	private float phi = 0, theta = 0;
	private float mouseSensitivity = 0.1f;
	private float speed = 2;
	private float runSpeedMultiplier = 3;
	private float vibrationAmount = 0f;
	private float bodyHeight = 1.8f;
	
	private int actionForward = Fury.KEY_W;
	private int actionStrafeLeft = Fury.KEY_A;
	private int actionBackward = Fury.KEY_S;
	private int actionStrafeRight = Fury.KEY_D;
	private int actionRun = Fury.KEY_LEFT_CONTROL;
	private int actionUp = Fury.KEY_SPACE;
	private int actionDown = Fury.KEY_LEFT_SHIFT;
	
	// really private
	private boolean isForwarding = false;
	private boolean isBackwarding = false;
	private boolean isRightStrafing = false;
	private boolean isLeftStrafing = false;
	private boolean isRunning = false;
	
	public FPSCamera(float fov, float ratio, float near, float far) {
		super(fov, ratio, near, far);
		
		this.setPosition(0, 0, this.bodyHeight);
		
		// Calc orientation
		float phiRadians = (float) Math.toRadians(this.phi);
		float thetaRadians = (float) Math.toRadians(this.theta);
		
		this.orientation.set((float) (Math.cos(phiRadians) * Math.cos(thetaRadians)), (float) (Math.cos(phiRadians) * Math.sin(thetaRadians)), (float) Math.sin(phiRadians));
		
		this.position.add(this.orientation, this.center);
		this.lookAt(this.center);
		
		this.addEventListener("tick", (e) -> {
			FPSCamera.this.tick((float) e.getData());
		});
	}
	
	public FPSCamera() {
		this(70, 1, 0.1f, 1000f);
	}
	
	public void set(PrimitiveProperties props) {
		super.set(props);
		
		this.phi = props.getFloat("phi", this.phi);
		this.theta = props.getFloat("theta", this.theta);
		this.mouseSensitivity = props.getFloat("mouseSensitivity", this.mouseSensitivity);
		this.speed = props.getFloat("speed", this.speed);
		this.runSpeedMultiplier = props.getFloat("runSpeedMultiplier", this.runSpeedMultiplier);
		this.vibrationAmount = props.getFloat("vibrationAmount", this.vibrationAmount);
		this.bodyHeight = props.getFloat("bodyHeight", this.bodyHeight);
		
		try {
			this.actionForward = Fury.class.getField(props.getString("actionForward", "KEY_W")).getInt(null);
			this.actionStrafeLeft = Fury.class.getField(props.getString("actionStrafeLeft", "KEY_A")).getInt(null);
			this.actionBackward = Fury.class.getField(props.getString("actionBackward", "KEY_S")).getInt(null);
			this.actionStrafeRight = Fury.class.getField(props.getString("actionStrafeRight", "KEY_D")).getInt(null);
			this.actionRun = Fury.class.getField(props.getString("actionRun", "KEY_LEFT_CONTROL")).getInt(null);
			this.actionUp = Fury.class.getField(props.getString("actionUp", "KEY_SPACE")).getInt(null);
			this.actionDown = Fury.class.getField(props.getString("actionDown", "KEY_LEFT_SHIFT")).getInt(null);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch(NoSuchFieldException e) {
			e.printStackTrace();
		} catch(SecurityException e) {
			e.printStackTrace();
		}
		
		this.orientation.set(props.getVector3f("orientation", this.orientation));
		this.center.set(props.getVector3f("center", this.center));
		
		this.freeLookFly = props.getBool("freeLookFly", this.freeLookFly);
		this.allowVerticalFly = props.getBool("allowVerticalFly", this.allowVerticalFly);
	}
	
	// Objects for update()
	private Vector2f _relMouse = new Vector2f();
	private Vector3f _orientation2D = new Vector3f();
	private Vector3f _orthoVector = new Vector3f();
	private Vector3f _deltayedOrientation = new Vector3f();
	private Vector3f _deltayedOrtho = new Vector3f();
	private Vector3f _deltayedUp = new Vector3f();
	
	public boolean tick(float delta) {
		Game g = Game.getLaunchedGame();
		GameWindow w = g.getWindow();
		
		this._relMouse.set(w.getMouseRelX(), w.getMouseRelY());
		this.phi += -this._relMouse.y * this.mouseSensitivity;
		this.theta += -this._relMouse.x * this.mouseSensitivity;
		
		if(this.vibrationAmount != 0f) {
			// vibrationAmound is multiplied by 0.01
			this.phi += (Math.random() * 2 - 1) * this.vibrationAmount * 0.01f;
			this.theta += (Math.random() * 2 - 1) * this.vibrationAmount * 0.01f;
		}
		
		if(this.phi > 89) this.phi = 89;
		else if(this.phi < -89) this.phi = -89;
		
		this.isForwarding = w.isDown(this.actionForward);
		this.isBackwarding = w.isDown(this.actionBackward);
		this.isRightStrafing = w.isDown(this.actionStrafeRight);
		this.isLeftStrafing = w.isDown(this.actionStrafeLeft);
		this.isRunning = w.isDown(this.actionRun);
		
		float usedSpeed = (this.isRunning ? this.speed * this.runSpeedMultiplier : this.speed);
		
		// Calc orientation
		float phiRadians = (float) Math.toRadians(this.phi);
		float thetaRadians = (float) Math.toRadians(this.theta);
		
		this.orientation.set((float) (Math.cos(phiRadians) * Math.cos(thetaRadians)), (float) (Math.cos(phiRadians) * Math.sin(thetaRadians)), (float) Math.sin(phiRadians));
		
		// Calc the 2D orientation
		this._orientation2D.set(this.orientation);
		
		this._orientation2D.z = 0;
		this._orientation2D.normalize();
		
		this.orientation.cross(this.up, this._orthoVector);
		this._orthoVector.normalize();
		
		if(this.freeLookFly) this.orientation.mul(usedSpeed * delta, this._deltayedOrientation);
		else this._orientation2D.mul(usedSpeed * delta, this._deltayedOrientation);
		
		this._orthoVector.mul(usedSpeed * delta, this._deltayedOrtho);
		this.up.mul(usedSpeed * delta, this._deltayedUp);
		
		if(this.isForwarding) this.position.add(this._deltayedOrientation);
		
		if(this.isLeftStrafing) this.position.sub(this._deltayedOrtho);
		
		if(this.isBackwarding) this.position.sub(this._deltayedOrientation);
		
		if(this.isRightStrafing) this.position.add(this._deltayedOrtho);
		
		if(this.allowVerticalFly) {
			if(w.isDown(this.actionUp)) this.position.add(this._deltayedUp);
			
			if(w.isDown(this.actionDown)) this.position.sub(this._deltayedUp);
		}
		
		this.position.add(this.orientation, this.center);
		this.lookAt(this.center);
		
		return true;
	}
	
	public float getPhi() {
		return this.phi;
	}
	
	public float getTheta() {
		return this.theta;
	}
	
	public void setPhi(float v) {
		this.phi = v;
	}
	
	public void setTheta(float v) {
		this.theta = v;
	}
	
	public Vector3f getOrientation() {
		return this.orientation;
	}
	
	public void setMouseSensitivity(float sens) {
		this.mouseSensitivity = sens;
	}
	
	public float getMouseSensitivity() {
		return this.mouseSensitivity;
	}
	
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	public float getSpeed() {
		return this.speed;
	}
	
	public float getRunSpeedMultiplier() {
		return this.runSpeedMultiplier;
	}
	
	public void setRunSpeedMultiplier(float runSpeed) {
		this.runSpeedMultiplier = runSpeed;
	}
	
	public void setAllowVerticalFly(boolean v) {
		this.allowVerticalFly = v;
	}
	
	public boolean isAllowVerticalFly() {
		return this.allowVerticalFly;
	}
	
	public void setFreeLookfly(boolean v) {
		this.freeLookFly = v;
	}
	
	public boolean isFreeLookfly() {
		return this.freeLookFly;
	}
	
	public float getBodyHeight() {
		return this.bodyHeight;
	}
	
	public void setBodyHeight(float bodyHeight) {
		this.bodyHeight = bodyHeight;
	}
	
	public boolean isRunning() {
		return this.isRunning;
	}
	
	public boolean isFreeLookFly() {
		return this.freeLookFly;
	}
	
	public void setFreeLookFly(boolean freeLookFly) {
		this.freeLookFly = freeLookFly;
	}
	
	public float getVibrationAmount() {
		return this.vibrationAmount;
	}
	
	public void setVibrationAmount(float vibrationAmount) {
		this.vibrationAmount = vibrationAmount;
	}
	
	public int getActionForward() {
		return this.actionForward;
	}
	
	public void setActionForward(int actionForward) {
		this.actionForward = actionForward;
	}
	
	public int getActionStrafeLeft() {
		return this.actionStrafeLeft;
	}
	
	public void setActionStrafeLeft(int actionStrafeLeft) {
		this.actionStrafeLeft = actionStrafeLeft;
	}
	
	public int getActionBackward() {
		return this.actionBackward;
	}
	
	public void setActionBackward(int actionBackward) {
		this.actionBackward = actionBackward;
	}
	
	public int getActionStrafeRight() {
		return this.actionStrafeRight;
	}
	
	public void setActionStrafeRight(int actionStrafeRight) {
		this.actionStrafeRight = actionStrafeRight;
	}
	
	public int getActionRun() {
		return this.actionRun;
	}
	
	public void setActionRun(int actionRun) {
		this.actionRun = actionRun;
	}
	
	public int getActionUp() {
		return this.actionUp;
	}
	
	public void setActionUp(int actionUp) {
		this.actionUp = actionUp;
	}
	
	public int getActionDown() {
		return this.actionDown;
	}
	
	public void setActionDown(int actionDown) {
		this.actionDown = actionDown;
	}
	
	public boolean isForwarding() {
		return this.isForwarding;
	}
	
	public boolean isBackwarding() {
		return this.isBackwarding;
	}
	
	public boolean isRightStrafing() {
		return this.isRightStrafing;
	}
	
	public boolean isLeftStrafing() {
		return this.isLeftStrafing;
	}
}
