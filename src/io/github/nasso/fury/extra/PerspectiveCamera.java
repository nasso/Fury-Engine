package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.level.NFClippedCamera;
import io.github.nasso.fury.level.Object3D;

public class PerspectiveCamera extends NFClippedCamera {
	private float fov = 70;
	private float ratio = 16f / 9f;
	
	public PerspectiveCamera(float fov, float ratio, float near, float far) {
		this.fov = fov;
		this.ratio = ratio;
		this.near = near;
		this.far = far;
		
		this.updateProjectionMatrix();
	}
	
	private PerspectiveCamera() {
		// Private just for the clone method
	}
	
	public void set(PrimitiveProperties props) {
		super.set(props);
		
		this.fov = props.getFloat("fov", this.fov);
		this.ratio = props.getFloat("ratio", this.ratio);
		
		this.updateProjectionMatrix();
	}
	
	public void updateProjectionMatrix() {
		this.projectionMatrix.setPerspective((float) Math.toRadians(this.fov), this.ratio, this.near, this.far);
	}
	
	public float getFOV() {
		return this.fov;
	}
	
	public void setFOV(float fov) {
		this.fov = fov;
		
		this.updateProjectionMatrix();
	}
	
	public float getRatio() {
		return this.ratio;
	}
	
	public void setRatio(float ratio) {
		this.ratio = ratio;
		
		this.updateProjectionMatrix();
	}
	
	public void setRatio(float width, float height) {
		this.setRatio(width / height);
	}
	
	public void setNear(float near) {
		this.near = near;
		
		this.updateProjectionMatrix();
	}
	
	public void setFar(float far) {
		this.far = far;
		
		this.updateProjectionMatrix();
	}
	
	public PerspectiveCamera copy(Object3D obj) {
		if(!(obj instanceof PerspectiveCamera)) throw new IllegalArgumentException(obj + " isn't an instance of PerspectiveCamera");
		
		PerspectiveCamera cam = (PerspectiveCamera) obj;
		
		super.copy(cam);
		this.fov = cam.fov;
		this.ratio = cam.ratio;
		this.near = cam.near;
		this.far = cam.far;
		
		return this;
	}
	
	public PerspectiveCamera clone() {
		return new PerspectiveCamera().copy(this);
	}
}
