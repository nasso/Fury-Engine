package io.github.nasso.fury.extra;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.BufferAttribute;
import io.github.nasso.fury.graphics.BufferGeometry;

import org.joml.Vector3f;

/**
 * A lot of methods of this class comes from the amazing javascript library
 * THREE.js
 * 
 * @author Nasso
 */
public class BoxBufferGeometry extends BufferGeometry {
	private float sizex, sizey, sizez;
	private int xdivs, ydivs, zdivs;
	private boolean wireframe;
	
	public BoxBufferGeometry() {
		this(1);
	}
	
	public BoxBufferGeometry(PrimitiveProperties props) {
		Vector3f sizevec = props.getVector3f("size");
		Vector3f divsvec = props.getVector3f("divs");
		
		this.setSizeX(props.getFloat("sizex", (sizevec == null ? props.getFloat("size", 0) : sizevec.x)));
		this.setSizeY(props.getFloat("sizey", (sizevec == null ? props.getFloat("size", 0) : sizevec.y)));
		this.setSizeZ(props.getFloat("sizez", (sizevec == null ? props.getFloat("size", 0) : sizevec.z)));
		this.setXdivs(props.getInt("xdivs", (divsvec == null ? props.getInt("divs", 1) : (int) divsvec.x)));
		this.setYdivs(props.getInt("ydivs", (divsvec == null ? props.getInt("divs", 1) : (int) divsvec.y)));
		this.setZdivs(props.getInt("zdivs", (divsvec == null ? props.getInt("divs", 1) : (int) divsvec.z)));
		this.setWireframe(props.getBool("wireframe", false));
		
		this.updateGeometry();
	}
	
	public BoxBufferGeometry(float size) {
		this(size, size, size);
	}
	
	public BoxBufferGeometry(float sizex, float sizey, float sizez) {
		this(sizex, sizey, sizez, false);
	}
	
	public BoxBufferGeometry(float sizex, float sizey, float sizez, boolean wireframe) {
		this(sizex, sizey, sizez, 1, 1, 1, wireframe);
	}
	
	public BoxBufferGeometry(float sizex, float sizey, float sizez, int xdivs, int ydivs, int zdivs) {
		this(sizex, sizey, sizez, xdivs, ydivs, zdivs, false);
	}
	
	public BoxBufferGeometry(float sizex, float sizey, float sizez, int xdivs, int ydivs, int zdivs, boolean wireframe) {
		this.setSizeX(sizex);
		this.setSizeY(sizey);
		this.setSizeZ(sizez);
		this.setXdivs(xdivs);
		this.setYdivs(ydivs);
		this.setZdivs(zdivs);
		this.setWireframe(wireframe);
		
		this.updateGeometry();
	}
	
	public String toString() {
		return "BoxBufferGeometry[size=" + this.sizex + " " + this.sizey + " " + this.sizez + " divs=" + this.xdivs + " " + this.ydivs + " " + this.zdivs + (this.wireframe ? " wireframe]" : "]");
	}
	
	public float getSizeX() {
		return this.sizex;
	}
	
	public void setSizeX(float sizex) {
		this.sizex = sizex;
	}
	
	public float getSizeY() {
		return this.sizey;
	}
	
	public void setSizeY(float sizey) {
		this.sizey = sizey;
	}
	
	public float getSizeZ() {
		return this.sizez;
	}
	
	public void setSizeZ(float sizez) {
		this.sizez = sizez;
	}
	
	public int getXdivs() {
		return this.xdivs;
	}
	
	public void setXdivs(int xdivs) {
		this.xdivs = Math.max(xdivs, 1);
	}
	
	public int getYdivs() {
		return this.ydivs;
	}
	
	public void setYdivs(int ydivs) {
		this.ydivs = Math.max(ydivs, 1);
	}
	
	public int getZdivs() {
		return this.zdivs;
	}
	
	public void setZdivs(int zdivs) {
		this.zdivs = Math.max(zdivs, 1);
	}
	
	public boolean isWireframe() {
		return this.wireframe;
	}
	
	public void setWireframe(boolean wireframe) {
		this.wireframe = wireframe;
	}
	
	private int _vertexBufferOffset = 0;
	private int _uvBufferOffset = 0;
	private int _indexBufferOffset = 0;
	private int _numberOfVertices = 0;
	private int[] _indices;
	private float[] _vertices;
	private float[] _normals;
	private float[] _uvs;
	
	public void updateGeometry() {
		int widthSegments = this.xdivs;
		int heightSegments = this.ydivs;
		int depthSegments = this.zdivs;
		
		int vertexCount = this.calculateVertexCount(widthSegments, heightSegments, depthSegments);
		int indexCount = this.calculateIndexCount(widthSegments, heightSegments, depthSegments);
		
		this._indices = new int[indexCount];
		this._vertices = new float[vertexCount * 3];
		this._normals = new float[vertexCount * 3];
		this._uvs = new float[vertexCount * 2];
		
		this._vertexBufferOffset = 0;
		this._uvBufferOffset = 0;
		this._indexBufferOffset = 0;
		this._numberOfVertices = 0;
		
		this.buildPlane(2, 1, 0, -1, -1, this.sizez, this.sizey, this.sizex, depthSegments, heightSegments); // pos
																												// x
		this.buildPlane(2, 1, 0, 1, -1, this.sizez, this.sizey, -this.sizex, depthSegments, heightSegments); // neg
																												// x
		this.buildPlane(0, 2, 1, 1, 1, this.sizex, this.sizez, this.sizey, widthSegments, depthSegments); // pos
																											// y
		this.buildPlane(0, 2, 1, 1, -1, this.sizex, this.sizez, -this.sizey, widthSegments, depthSegments); // neg
																											// y
		this.buildPlane(0, 1, 2, 1, -1, this.sizex, this.sizey, this.sizez, widthSegments, heightSegments); // pos
																											// z
		this.buildPlane(0, 1, 2, -1, -1, this.sizex, this.sizey, -this.sizez, widthSegments, heightSegments); // neg
																												// z
		
		this.setRenderMode(this.wireframe ? Fury.LINES : Fury.TRIANGLES);
		this.setIndices(BufferAttribute.fromIntegerList(this._indices));
		this.setPositions(BufferAttribute.fromFloatList(this._vertices));
		this.setNormals(BufferAttribute.fromFloatList(this._normals));
		this.setUVs(BufferAttribute.fromFloatList(this._uvs));
	}
	
	private void buildPlane(int u, int v, int w, int udir, int vdir, float width, float height, float depth, int gridX, int gridY) {
		float segmentWidth = width / gridX;
		float segmentHeight = height / gridY;
		
		float widthHalf = width / 2f;
		float heightHalf = height / 2f;
		float depthHalf = depth / 2f;
		
		int gridX1 = gridX + 1;
		int gridY1 = gridY + 1;
		
		int vertexCounter = 0;
		
		Vector3f vector = new Vector3f();
		for(int iy = 0; iy < gridY1; iy++) {
			float y = iy * segmentHeight - heightHalf;
			
			for(int ix = 0; ix < gridX1; ix++) {
				float x = ix * segmentWidth - widthHalf;
				
				vector.set(u, x * udir);
				vector.set(v, y * vdir);
				vector.set(w, depthHalf);
				
				this._vertices[this._vertexBufferOffset] = vector.x;
				this._vertices[this._vertexBufferOffset + 1] = vector.y;
				this._vertices[this._vertexBufferOffset + 2] = vector.z;
				
				vector.set(u, 0);
				vector.set(v, 0);
				vector.set(w, depth > 0 ? 1 : -1);
				
				this._normals[this._vertexBufferOffset] = vector.x;
				this._normals[this._vertexBufferOffset + 1] = vector.y;
				this._normals[this._vertexBufferOffset + 2] = vector.z;
				
				this._uvs[this._uvBufferOffset] = (float) ix / (float) gridX;
				this._uvs[this._uvBufferOffset + 1] = 1f - ((float) iy / (float) gridY);
				
				this._vertexBufferOffset += 3;
				this._uvBufferOffset += 2;
				vertexCounter += 1;
			}
		}
		
		for(int iy = 0; iy < gridY; iy++)
			for(int ix = 0; ix < gridX; ix++) {
				int a = this._numberOfVertices + ix + gridX1 * iy;
				int b = this._numberOfVertices + ix + gridX1 * (iy + 1);
				int c = this._numberOfVertices + (ix + 1) + gridX1 * (iy + 1);
				int d = this._numberOfVertices + (ix + 1) + gridX1 * iy;
				
				if(this.wireframe) {
					this._indices[this._indexBufferOffset] = a;
					this._indices[this._indexBufferOffset + 1] = b;
					
					this._indices[this._indexBufferOffset + 2] = b;
					this._indices[this._indexBufferOffset + 3] = c;
					
					this._indices[this._indexBufferOffset + 4] = c;
					this._indices[this._indexBufferOffset + 5] = d;
					
					this._indices[this._indexBufferOffset + 6] = d;
					this._indices[this._indexBufferOffset + 7] = a;
					
					this._indexBufferOffset += 8;
				} else {
					this._indices[this._indexBufferOffset] = a;
					this._indices[this._indexBufferOffset + 1] = b;
					this._indices[this._indexBufferOffset + 2] = d;
					
					this._indices[this._indexBufferOffset + 3] = b;
					this._indices[this._indexBufferOffset + 4] = c;
					this._indices[this._indexBufferOffset + 5] = d;
					
					this._indexBufferOffset += 6;
				}
			}
		
		this._numberOfVertices += vertexCounter;
	}
	
	private int calculateVertexCount(int w, int h, int d) {
		int vertices = 0;
		
		vertices += (w + 1) * (h + 1) * 2;
		vertices += (w + 1) * (d + 1) * 2;
		vertices += (d + 1) * (h + 1) * 2;
		
		return vertices;
	}
	
	private int calculateIndexCount(int w, int h, int d) {
		int index = 0;
		
		index += w * h * 2;
		index += w * d * 2;
		index += d * h * 2;
		
		if(this.wireframe) return index * 8;
		
		return index * 6;
	}
}
