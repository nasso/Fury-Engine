package io.github.nasso.fury.data;

import io.github.nasso.fury.level.Level;

import java.io.File;

public abstract class FuryLevelLoader {
	public abstract Level loadLevel(File f);
}
