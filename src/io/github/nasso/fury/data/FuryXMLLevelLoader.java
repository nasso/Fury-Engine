package io.github.nasso.fury.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.CubeTexture;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.graphics.RenderPass;
import io.github.nasso.fury.graphics.RenderTarget;
import io.github.nasso.fury.graphics.ShadowProperties;
import io.github.nasso.fury.graphics.ShadowsProperties;
import io.github.nasso.fury.graphics.Texture2D;
import io.github.nasso.fury.graphics.fx.PostEffect;
import io.github.nasso.fury.level.Camera;
import io.github.nasso.fury.level.Fog;
import io.github.nasso.fury.level.Level;
import io.github.nasso.fury.level.LevelObject;
import io.github.nasso.fury.level.Light;
import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Object3D;
import io.github.nasso.fury.level.Scene;
import io.github.nasso.fury.level.Sky;
import io.github.nasso.fury.maths.Color;
import io.github.nasso.fury.scripting.ScriptingLanguage;

public class FuryXMLLevelLoader extends FuryLevelLoader {
	private DocumentBuilderFactory dbf;
	private DocumentBuilder builder;
	
	public FuryXMLLevelLoader() {
		this.dbf = DocumentBuilderFactory.newInstance();
		
		try {
			this.builder = this.dbf.newDocumentBuilder();
		} catch(ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	private Element getElementByTagName(Element e, String name) {
		Node l = e.getElementsByTagName(name).item(0);
		if(l == null) return null;
		
		return (Element) l;
	}
	
	private PrimitiveProperties getProperties(Element parent) {
		PrimitiveProperties props = new PrimitiveProperties();
		
		NodeList bools = parent.getElementsByTagName("bool");
		NodeList ints = parent.getElementsByTagName("int");
		NodeList floats = parent.getElementsByTagName("float");
		NodeList strings = parent.getElementsByTagName("string");
		NodeList vecs = parent.getElementsByTagName("vector");
		NodeList mats = parent.getElementsByTagName("matrix");
		
		int i, l;
		for(i = 0, l = bools.getLength(); i < l; i++) {
			Element e = (Element) bools.item(i);
			if(e.getParentNode() != parent) continue;
			
			props.setBool(e.getAttribute("name"), Boolean.valueOf(e.getTextContent()));
		}
		
		for(i = 0, l = ints.getLength(); i < l; i++) {
			Element e = (Element) ints.item(i);
			if(e.getParentNode() != parent) continue;
			
			props.setInt(e.getAttribute("name"), Integer.valueOf(e.getTextContent()));
		}
		
		for(i = 0, l = floats.getLength(); i < l; i++) {
			Element e = (Element) floats.item(i);
			if(e.getParentNode() != parent) continue;
			
			props.setFloat(e.getAttribute("name"), Float.valueOf(e.getTextContent()));
		}
		
		for(i = 0, l = strings.getLength(); i < l; i++) {
			Element e = (Element) strings.item(i);
			if(e.getParentNode() != parent) continue;
			
			props.setString(e.getAttribute("name"), e.getTextContent().trim());
		}
		
		for(i = 0, l = vecs.getLength(); i < l; i++) {
			Element e = (Element) vecs.item(i);
			if(e.getParentNode() != parent) continue;
			
			String name = e.getAttribute("name");
			String[] coords = e.getTextContent().trim().split(" ");
			
			if(coords.length == 2) props.setVector2f(name, new Vector2f(Float.valueOf(coords[0]), Float.valueOf(coords[1])));
			else if(coords.length == 3) props.setVector3f(name, new Vector3f(Float.valueOf(coords[0]), Float.valueOf(coords[1]), Float.valueOf(coords[2])));
			else if(coords.length == 4) props.setVector4f(name, new Vector4f(Float.valueOf(coords[0]), Float.valueOf(coords[1]), Float.valueOf(coords[2]), Float.valueOf(coords[3])));
		}
		
		for(i = 0, l = mats.getLength(); i < l; i++) {
			Element e = (Element) mats.item(i);
			if(e.getParentNode() != parent) continue;
			
			String name = e.getAttribute("name");
			String[] coords = e.getTextContent().trim().split(" ");
			
			if(coords.length == 9) props.setMatrix3f(name, new Matrix3f(Float.valueOf(coords[0]), Float.valueOf(coords[1]), Float.valueOf(coords[2]), Float.valueOf(coords[3]), Float.valueOf(coords[4]), Float.valueOf(coords[5]), Float.valueOf(coords[6]), Float.valueOf(coords[7]), Float.valueOf(coords[8])));
			else if(coords.length == 16) props.setMatrix4f(name, new Matrix4f(Float.valueOf(coords[0]), Float.valueOf(coords[1]), Float.valueOf(coords[2]), Float.valueOf(coords[3]), Float.valueOf(coords[4]), Float.valueOf(coords[5]), Float.valueOf(coords[6]), Float.valueOf(coords[7]), Float.valueOf(coords[8]), Float.valueOf(coords[9]), Float.valueOf(coords[10]), Float.valueOf(coords[11]), Float.valueOf(coords[12]), Float.valueOf(coords[13]), Float.valueOf(coords[14]), Float.valueOf(coords[15])));
		}
		
		return props;
	}
	
	private void loadGeometries(Level lvl, NodeList geometrynodes) {
		for(int i = 0, l = geometrynodes.getLength(); i < l; i++) {
			Element e = (Element) geometrynodes.item(i);
			String type = e.getAttribute("type");
			String id = e.getAttribute("id");
			String name = e.getAttribute("name");
			
			PrimitiveProperties props = this.getProperties(e);
			
			String[] typeParts = type.split("/");
			
			AbstractGeometry geom = null;
			
			if(typeParts[0].equals("extra")) try {
				Class<?> geomclass = Class.forName("io.github.nasso.fury.extra." + typeParts[1]);
				
				if(geomclass != null) {
					Constructor<?> constr = geomclass.getConstructor(PrimitiveProperties.class);
					
					if(constr != null) geom = (AbstractGeometry) constr.newInstance(props);
				}
			} catch(ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch(NoSuchMethodException e1) {
				e1.printStackTrace();
			} catch(SecurityException e1) {
				e1.printStackTrace();
			} catch(InstantiationException e1) {
				e1.printStackTrace();
			} catch(IllegalAccessException e1) {
				e1.printStackTrace();
			} catch(IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch(InvocationTargetException e1) {
				e1.printStackTrace();
			}
			else if(typeParts[0].equals("data")) {
				String src = e.getAttribute("src");
				File srcfile = new File(src);
				int targetIndex = Integer.valueOf(e.getAttribute("targetIndex"));
				
				try {
					geom = ModelLoader.loadMesh(srcfile.getAbsolutePath(), targetIndex).getGeometry();
				} catch(NumberFormatException e1) {
					e1.printStackTrace();
				} catch(IOException e1) {
					e1.printStackTrace();
				}
			}
			
			if(geom != null) {
				geom.setID(id);
				geom.setName(name);
				lvl.geometries().add(geom);
			}
		}
	}
	
	private void loadTextures(Level lvl, NodeList texturenodes) {
		for(int i = 0, l = texturenodes.getLength(); i < l; i++) {
			Element e = (Element) texturenodes.item(i);
			
			String type = e.getAttribute("type");
			String src = e.getAttribute("src");
			String id = e.getAttribute("id");
			String name = e.getAttribute("name");
			
			String[] typeParts = type.split("/");
			
			PrimitiveProperties props = this.getProperties(e);
			
			if(typeParts[0].equals("data")) if(typeParts[1].equals("2d")) try {
				TextureData data = TextureIO.loadTextureData(src);
				
				props.setInt("width", props.getInt("width", props.getInt("size", data.getWidth())));
				props.setInt("height", props.getInt("height", props.getInt("size", data.getHeight())));
				
				Texture2D tex = new Texture2D(props, data.getData());
				tex.setID(id);
				tex.setName(name);
				lvl.textures2d().add(tex);
			} catch(IOException e1) {
				e1.printStackTrace();
			} catch(IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch(IllegalAccessException e1) {
				e1.printStackTrace();
			} catch(NoSuchFieldException e1) {
				e1.printStackTrace();
			} catch(SecurityException e1) {
				e1.printStackTrace();
			}
			else if(typeParts[1].equals("cube")) try {
				CubeTexture tex = TextureIO.loadCubeTexture(src, new CubeTextureLayout(props), props);
				tex.setID(id);
				tex.setName(name);
				lvl.cubeTextures().add(tex);
			} catch(IOException e1) {
				e1.printStackTrace();
			} catch(IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch(SecurityException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void loadMaterials(Level lvl, NodeList materialnodes) {
		for(int i = 0, l = materialnodes.getLength(); i < l; i++) {
			Element e = (Element) materialnodes.item(i);
			
			String id = e.getAttribute("id");
			String name = e.getAttribute("name");
			
			PrimitiveProperties props = this.getProperties(e);
			
			try {
				Material mat = new Material();
				mat.set(props);
				
				mat.setName(name);
				mat.setID(id);
				
				Element difelem = this.getElementByTagName(e, "diffuse");
				Element norelem = this.getElementByTagName(e, "normal");
				Element specelem = this.getElementByTagName(e, "specular");
				
				if(difelem != null) {
					if(difelem.hasAttribute("texture")) {
						LevelObject obj = lvl.getObjectById(difelem.getAttribute("texture"));
						
						if(obj instanceof Texture2D) mat.setDiffuseTexture((Texture2D) obj);
					}
					
					if(difelem.hasAttribute("color")) mat.setDiffuseColor(new Color(difelem.getAttribute("color")));
				}
				
				if(norelem != null) {
					if(norelem.hasAttribute("texture")) {
						LevelObject obj = lvl.getObjectById(norelem.getAttribute("texture"));
						
						if(obj instanceof Texture2D) mat.setNormalTexture((Texture2D) obj);
					}
					
					if(norelem.hasAttribute("intensity")) mat.setNormalMapIntensity(Float.valueOf(norelem.getAttribute("intensity")));
					
					if(norelem.hasAttribute("faceSideCorrection")) mat.setNormalMapIntensity(Float.valueOf(norelem.getAttribute("faceSideCorrection")));
				}
				
				if(specelem != null) {
					if(specelem.hasAttribute("texture")) {
						LevelObject obj = lvl.getObjectById(specelem.getAttribute("texture"));
						
						if(obj instanceof Texture2D) mat.setSpecularTexture((Texture2D) obj);
					}
					
					if(specelem.hasAttribute("intensity")) mat.setSpecularIntensity(Float.valueOf(specelem.getAttribute("intensity")));
					
					if(specelem.hasAttribute("shininess")) mat.setShininess(Float.valueOf(specelem.getAttribute("shininess")));
				}
				
				lvl.materials().add(mat);
			} catch(IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch(IllegalAccessException e1) {
				e1.printStackTrace();
			} catch(NoSuchFieldException e1) {
				e1.printStackTrace();
			} catch(SecurityException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void loadChildren(Level lvl, Element e, Object3D o) {
		Element childrenel = this.getElementByTagName(e, "children");
		if(childrenel == null) return;
		
		NodeList objs = childrenel.getElementsByTagName("object");
		
		for(int i = 0, l = objs.getLength(); i < l; i++) {
			Element oe = (Element) objs.item(i);
			
			if(oe.getParentNode() != childrenel) continue;
			
			PrimitiveProperties props = this.getProperties(oe);
			
			String[] type = oe.getAttribute("type").split("/");
			String name = oe.getAttribute("name");
			Object3D obj = null;
			
			if(type[0].equals("object")) {
				if(type[1].equals("import")) {
					String src = oe.getAttribute("src");
					
					try {
						obj = ModelLoader.load(src);
					} catch(IOException e1) {
						e1.printStackTrace();
					}
				} else obj = new Object3D();
			} else if(type[0].equals("mesh")) {
				Element geomEl = this.getElementByTagName(oe, "geometry");
				Element matEl = this.getElementByTagName(oe, "material");
				
				AbstractGeometry g = null;
				Material m = null;
				
				if(geomEl != null) {
					String target = geomEl.getAttribute("target");
					
					LevelObject lvlobj = lvl.getObjectById(target);
					
					if(lvlobj instanceof AbstractGeometry) g = (AbstractGeometry) lvlobj;
				}
				
				if(matEl != null) {
					String target = matEl.getAttribute("target");
					
					LevelObject lvlobj = lvl.getObjectById(target);
					
					if(lvlobj instanceof Material) m = (Material) lvlobj;
				}
				
				obj = new Mesh(g, m);
			} else if(type[0].equals("light")) {
				props.setString("lightType", type[1].toUpperCase());
				
				Element shadEl = this.getElementByTagName(oe, "shadows");
				
				if(shadEl != null) {
					PrimitiveProperties shadProps = this.getProperties(shadEl);
					
					ShadowsProperties shadows = new ShadowsProperties(shadProps);
					
					NodeList cascadesEl = shadEl.getElementsByTagName("cascade");
					for(int j = 0, h = cascadesEl.getLength(); j < h; j++) {
						Element ce = (Element) cascadesEl.item(j);
						
						PrimitiveProperties cascadePrimProps = this.getProperties(ce);
						cascadePrimProps.setString("type", ce.getAttribute("type"));
						
						ShadowProperties cascade = new ShadowProperties();
						cascade.set(cascadePrimProps);
						
						shadows.getCascadeProperties().add(cascade);
					}
					
					obj = new Light(shadows);
				} else obj = new Light();
			} else if(type[0].equals("extra")) try {
				Class<?> objcls = Class.forName("io.github.nasso.fury.extra." + type[1]);
				
				if(Object.class.isAssignableFrom(objcls)) obj = (Object3D) objcls.newInstance();
			} catch(ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch(InstantiationException e1) {
				e1.printStackTrace();
			} catch(IllegalAccessException e1) {
				e1.printStackTrace();
			}
			
			if(obj == null) continue;
			
			obj.setName(name);
			obj.setID(oe.getAttribute("id"));
			obj.set(props);
			
			this.loadChildren(lvl, oe, obj);
			
			o.addChildren(obj);
		}
	}
	
	private void loadScenes(Level lvl, NodeList scenes) {
		for(int i = 0, l = scenes.getLength(); i < l; i++) {
			Element e = (Element) scenes.item(i);
			Element skyelem = this.getElementByTagName(e, "sky");
			Element fogelem = this.getElementByTagName(e, "fog");
			
			String id = e.getAttribute("id");
			
			PrimitiveProperties props = getProperties(e);
			
			Scene sce = new Scene();
			sce.set(props);
			sce.setID(id);
			
			if(e.hasAttribute("overrideMaterial")) {
				LevelObject overrideMat = lvl.getObjectById(e.getAttribute("overrideMaterial"));
				
				if(overrideMat instanceof Material) sce.setOverrideMaterial((Material) overrideMat);
			}
			
			this.loadChildren(lvl, e, sce);
			
			if(skyelem != null) {
				Element skyboxelem = this.getElementByTagName(skyelem, "skybox");
				
				NodeList sunelems = skyelem.getElementsByTagName("sun");
				Sky sky = new Sky(this.getProperties(skyelem));
				
				if(skyelem.hasAttribute("type")) sky.setType(Fury.valueOf("SKY_TYPE_" + skyelem.getAttribute("type").toUpperCase()));
				
				if(skyboxelem != null) {
					LevelObject lobj = lvl.getObjectById(skyboxelem.getAttribute("target"));
					
					if(lobj instanceof CubeTexture) sky.setCubeTexture((CubeTexture) lobj);
				}
				
				for(int j = 0, m = sunelems.getLength(); i < m; i++) {
					Element sunelem = (Element) sunelems.item(j);
					
					LevelObject lobj = sce.getChildById(sunelem.getAttribute("target"));
					
					if(lobj instanceof Light) sky.addSun((Light) lobj);
				}
				
				sce.setSky(sky);
			}
			
			if(fogelem != null) {
				Fog fog = new Fog(this.getProperties(fogelem));
				
				if(fogelem.hasAttribute("type")) fog.setType(Fury.valueOf("FOG_TYPE_" + fogelem.getAttribute("type").toUpperCase()));
				
				sce.setFog(fog);
			}
			
			lvl.scenes().add(sce);
		}
	}
	
	private void loadTargets(Level lvl, NodeList targetnodes) {
		// TODO targets
	}
	
	private void loadPasses(Level lvl, NodeList passnodes) {
		for(int i = 0, l = passnodes.getLength(); i < l; i++) {
			Element e = (Element) passnodes.item(i);
			NodeList postfxnodes = e.getElementsByTagName("postfx");
			
			String scetarget = e.getAttribute("scene");
			String camtarget = e.getAttribute("camera");
			String tartarget = e.getAttribute("target");
			
			LevelObject sceao = lvl.getObjectById(scetarget);
			LevelObject tarao = lvl.getObjectById(tartarget);
			
			if(!(sceao instanceof Scene)) continue;
			
			Scene sce = (Scene) sceao;
			RenderTarget tar = null;
			
			if(tarao instanceof RenderTarget) tar = (RenderTarget) tarao;
			
			Object3D camo = sce.getChildById(camtarget);
			if(!(camo instanceof Camera)) continue;
			
			Camera cam = (Camera) camo;
			
			RenderPass pass = new RenderPass(sce, cam, tar);
			
			for(int j = 0, h = postfxnodes.getLength(); j < h; j++) {
				Element pfxe = (Element) postfxnodes.item(j);
				
				String[] type = pfxe.getAttribute("type").split("/");
				
				try {
					Class<?> fxcls = Class.forName("io.github.nasso.fury.graphics.fx." + type[1]);
					
					if(PostEffect.class.isAssignableFrom(fxcls)) {
						PostEffect fx = (PostEffect) fxcls.newInstance();
						fx.set(this.getProperties(pfxe));
						fx.setEnabled(!pfxe.hasAttribute("enabled") || pfxe.getAttribute("enabled").equalsIgnoreCase("true"));
						pass.addPostEffects(fx);
					}
				} catch(ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch(InstantiationException e1) {
					e1.printStackTrace();
				} catch(IllegalAccessException e1) {
					e1.printStackTrace();
				}
			}
			
			lvl.renderPasses().add(pass);
		}
	}
	
	private void loadScripts(Level lvl, NodeList scriptnodes) {
		for(int i = 0, l = scriptnodes.getLength(); i < l; i++) {
			Element e = (Element) scriptnodes.item(i);
			
			boolean hasSrc = e.hasAttribute("src");
			
			String[] type = e.getAttribute("type").split("/");
			String src = hasSrc ? e.getAttribute("src") : e.getTextContent();
			
			if(type[0].equals("text")) {
				ScriptingLanguage lang = ScriptingLanguage.valueOf(type[1].toUpperCase());
				if(lang == null) continue;
				
				if(hasSrc) try {
					lvl.runScript(new File(src), lang);
				} catch(FileNotFoundException e1) {
					e1.printStackTrace();
				}
				else lvl.runScript(src, lang);
			}
		}
	}
	
	public Level loadLevel(File f) {
		Document doc = null;
		try {
			doc = this.builder.parse(f);
		} catch(SAXException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		if(doc == null) return null;
		
		Element lvlnode = (Element) doc.getElementsByTagName("level").item(0);
		
		Element scriptsnode = this.getElementByTagName(lvlnode, "scripts");
		NodeList scriptnodes = scriptsnode.getElementsByTagName("script");
		
		Element datanode = this.getElementByTagName(lvlnode, "data");
		NodeList geometrynodes = datanode.getElementsByTagName("geometry");
		NodeList texturenodes = datanode.getElementsByTagName("texture");
		NodeList materialnodes = datanode.getElementsByTagName("material");
		
		Element scenesnode = this.getElementByTagName(lvlnode, "scenes");
		NodeList scenenodes = scenesnode.getElementsByTagName("scene");
		
		Element rendernode = this.getElementByTagName(lvlnode, "render");
		NodeList targetnodes = rendernode.getElementsByTagName("target");
		NodeList passnodes = rendernode.getElementsByTagName("pass");
		
		Level lvl = new Level();
		
		this.loadScripts(lvl, scriptnodes);
		this.loadGeometries(lvl, geometrynodes);
		this.loadTextures(lvl, texturenodes);
		this.loadMaterials(lvl, materialnodes);
		this.loadScenes(lvl, scenenodes);
		this.loadTargets(lvl, targetnodes);
		this.loadPasses(lvl, passnodes);
		
		return lvl;
	}
}
