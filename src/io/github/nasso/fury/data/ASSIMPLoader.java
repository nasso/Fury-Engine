package io.github.nasso.fury.data;

import static org.lwjgl.assimp.Assimp.*;
import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.graphics.BufferAttribute;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Object3D;

import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;
import java.nio.file.Paths;

import org.joml.Matrix4f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.AIColor4D;
import org.lwjgl.assimp.AIFace;
import org.lwjgl.assimp.AIMaterial;
import org.lwjgl.assimp.AIMatrix4x4;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AINode;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.AIString;
import org.lwjgl.assimp.AIVector3D;

public class ASSIMPLoader {
	private ASSIMPLoader() {
	}
	
	/*
	static {
		for(long i = 0, l = aiGetImportFormatCount(); i < l; i++) {
			AIImporterDesc desc = aiGetImportFormatDescription(i);
			
			System.out.println("[ASSIMPLoader] Import format found: " + desc.mNameString() + " (" + desc.mFileExtensionsString() + ")");
		}
	}
	*/
	
	private static float[] flatten(AIVector3D.Buffer b) {
		float[] data = new float[b.remaining() * 3];
		int i = 0;
		
		while(b.hasRemaining()) {
			AIVector3D v = b.get();
			
			data[i * 3] = v.x();
			data[i * 3 + 1] = v.y();
			data[i * 3 + 2] = v.z();
			i++;
		}
		
		return data;
	}
	
	private static float[] flatten2(AIVector3D.Buffer b) {
		float[] data = new float[b.remaining() * 2];
		int i = 0;
		
		while(b.hasRemaining()) {
			AIVector3D v = b.get();
			
			data[i * 2] = v.x();
			data[i * 2 + 1] = v.y();
			i++;
		}
		
		return data;
	}
	
	private static float[] floatPointer = new float[1];
	private static int[] intPointer = new int[1];
	private static AIString strPointer = AIString.create();
	
	private static Material createMaterial(String resFolder, AIMaterial aimat) throws IOException {
		AIColor4D d = AIColor4D.create();
		aiGetMaterialColor(aimat, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, d);
		
		Material mat = new Material();
		mat.setDiffuseColor(d.r(), d.g(), d.b());
		
		aiGetMaterialFloatArray(aimat, AI_MATKEY_SHININESS, aiTextureType_NONE, 0, floatPointer, new int[] { 1 });
		mat.setShininess(floatPointer[0]);
		
		aiGetMaterialFloatArray(aimat, AI_MATKEY_SHININESS_STRENGTH, aiTextureType_NONE, 0, floatPointer, new int[] { 1 });
		mat.setSpecularIntensity(floatPointer[0]);
		
		aiGetMaterialIntegerArray(aimat, AI_MATKEY_ENABLE_WIREFRAME, aiTextureType_NONE, 0, intPointer, new int[] { 1 });
		mat.setPolygonMode(intPointer[0] == 0 ? Fury.FILL : Fury.LINES);
		
		aiGetMaterialString(aimat, AI_MATKEY_NAME, aiTextureType_NONE, 0, strPointer);
		mat.setName(strPointer.dataString());
		
		if(aiGetMaterialTextureCount(aimat, aiTextureType_DIFFUSE) > 0) {
			aiGetMaterialTexture(aimat, aiTextureType_DIFFUSE, 0, strPointer, null, null, null, null, null, intPointer);
			mat.setDiffuseTexture(TextureIO.loadTexture2D(resFolder + strPointer.dataString(), true));
		}
		
		if(aiGetMaterialTextureCount(aimat, aiTextureType_SHININESS) > 0) {
			aiGetMaterialTexture(aimat, aiTextureType_SHININESS, 0, strPointer, null, null, null, null, null, intPointer);
			mat.setSpecularTexture(TextureIO.loadTexture2D(resFolder + strPointer.dataString(), false));
		}
		
		if(aiGetMaterialTextureCount(aimat, aiTextureType_NORMALS) > 0) {
			aiGetMaterialTexture(aimat, aiTextureType_NORMALS, 0, strPointer, null, null, null, null, null, intPointer);
			mat.setNormalTexture(TextureIO.loadTexture2D(resFolder + strPointer.dataString(), false));
		}
		
		return mat;
	}
	
	private static int[] extractIndices(AIFace.Buffer faces, int count) {
		int[] indices = new int[count];
		
		for(int i = 0; faces.hasRemaining(); i += 3) {
			IntBuffer face = faces.get().mIndices();
			
			indices[i] = face.get();
			indices[i + 1] = face.get();
			indices[i + 2] = face.get();
		}
		
		return indices;
	}
	
	private static Mesh createMesh(String resFolder, AIMesh mesh, AIScene aiSce) throws IOException {
		PointerBuffer materials = aiSce.mMaterials();
		
		// Geometry
		BufferGeometry bGeom = new BufferGeometry();
		bGeom.setPositions(BufferAttribute.fromFloatList(flatten(mesh.mVertices())));
		bGeom.setNormals(BufferAttribute.fromFloatList(flatten(mesh.mNormals())));
		if(mesh.mNumUVComponents(0) == 2) bGeom.setUVs(BufferAttribute.fromFloatList(flatten2(mesh.mTextureCoords(0))));
		bGeom.setIndices(BufferAttribute.fromIntegerList(extractIndices(mesh.mFaces(), mesh.mNumFaces() * 3)));
		
		// Material
		Material mat = createMaterial(resFolder, AIMaterial.create(materials.get(mesh.mMaterialIndex())));
		
		Mesh m = new Mesh(bGeom, mat);
		m.setName(mesh.mName().dataString());
		
		return m;
	}
	
	private static Object3D objectify(AIScene sce, Mesh[] meshes, AINode node) {
		Object3D o = new Object3D();
		o.setName(node.mName().dataString());
		
		AIMatrix4x4 transform = node.mTransformation();
		o.setMatrix(new Matrix4f(transform.a1(), transform.b1(), transform.c1(), transform.d1(), transform.a2(), transform.b2(), transform.c2(), transform.d2(), transform.a3(), transform.b3(), transform.c3(), transform.d3(), transform.a4(), transform.b4(), transform.c4(), transform.d4()));
		
		if(node.mNumMeshes() > 0) {
			IntBuffer meshesIndex = node.mMeshes();
			if(meshesIndex != null) while(meshesIndex.hasRemaining())
				o.addChildren(meshes[meshesIndex.get()]);
		}
		
		if(node.mNumChildren() > 0) {
			PointerBuffer children = node.mChildren();
			if(children != null) while(children.hasRemaining())
				o.addChildren(objectify(sce, meshes, AINode.create(children.get())));
		}
		
		return o;
	}
	
	public static Object3D load(String fileName) throws IOException {
		AIScene aiSce = aiImportFile(fileName, aiProcess_Triangulate | aiProcess_FlipUVs);
		if(aiSce == null) {
			System.err.println("[ASSIMPLoader] Couldn't load the scene for: '" + fileName + "'\n\t> " + aiGetErrorString());
			
			return null;
		}
		
		String resFolder = Paths.get(fileName).getParent().toAbsolutePath().toString() + File.separator;
		
		PointerBuffer meshPointers = aiSce.mMeshes();
		
		Mesh[] meshes = new Mesh[aiSce.mNumMeshes()];
		for(int i = 0; i < meshes.length; i++)
			meshes[i] = createMesh(resFolder, AIMesh.create(meshPointers.get(i)), aiSce);
		
		return objectify(aiSce, meshes, aiSce.mRootNode());
	}
	
	public static Mesh loadMesh(String fileName, int meshIndex) throws IOException {
		AIScene aiSce = aiImportFile(fileName, aiProcess_Triangulate);
		int meshCount = aiSce.mNumMeshes();
		if(meshIndex > meshCount) return null;
		
		String resFolder = Paths.get(fileName).getParent().toAbsolutePath().toString() + File.separator;
		
		return createMesh(resFolder, AIMesh.create(aiSce.mMeshes().get(meshIndex)), aiSce);
	}
}
