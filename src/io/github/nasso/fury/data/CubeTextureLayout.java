package io.github.nasso.fury.data;

import io.github.nasso.fury.core.PrimitiveProperties;

public class CubeTextureLayout {
	/**
	 * <pre>
	 *   U
	 * L F R B
	 *   D
	 * </pre>
	 */
	public static final CubeTextureLayout PRESET_B = new CubeTextureLayout();
	
	private int xdivs = 4;
	private int ydivs = 3;
	
	private int posXCaseX = 1;
	private int posXCaseY = 1;
	private int negXCaseX = 3;
	private int negXCaseY = 1;
	
	private int posYCaseX = 0;
	private int posYCaseY = 1;
	private int negYCaseX = 2;
	private int negYCaseY = 1;
	
	private int posZCaseX = 1;
	private int posZCaseY = 0;
	private int negZCaseX = 1;
	private int negZCaseY = 2;
	
	private int posXRot = 0;
	private int negXRot = 2;
	private int posYRot = 1;
	private int negYRot = -1;
	private int posZRot = 0;
	private int negZRot = 0;
	
	public CubeTextureLayout() {
		
	}
	
	public CubeTextureLayout(PrimitiveProperties props) {
		this.xdivs = props.getInt("xdivs", 4);
		this.ydivs = props.getInt("ydivs", 3);
		
		this.posXCaseX = props.getInt("posXCaseX", 1);
		this.posXCaseY = props.getInt("posXCaseY", 1);
		this.setNegXCaseX(props.getInt("negXCaseX", 3));
		this.negXCaseY = props.getInt("negXCaseY", 1);
		
		this.posYCaseX = props.getInt("posYCaseX", 0);
		this.posYCaseY = props.getInt("posYCaseY", 1);
		this.negYCaseX = props.getInt("negYCaseX", 2);
		this.negYCaseY = props.getInt("negYCaseY", 1);
		
		this.posZCaseX = props.getInt("posZCaseX", 1);
		this.posZCaseY = props.getInt("posZCaseY", 0);
		this.negZCaseX = props.getInt("negZCaseX", 1);
		this.negZCaseY = props.getInt("negZCaseY", 2);
		
		this.posXRot = props.getInt("posXRot", 0);
		this.negXRot = props.getInt("negXRot", 2);
		this.posYRot = props.getInt("posYRot", 1);
		this.negYRot = props.getInt("negYRot", -1);
		this.posZRot = props.getInt("posZRot", 0);
		this.negZRot = props.getInt("negZRot", 0);
	}
	
	public int getXDivs() {
		return this.xdivs;
	}
	
	public void setXDivs(int xdivs) {
		this.xdivs = Math.max(1, xdivs);
	}
	
	public int getYDivs() {
		return this.ydivs;
	}
	
	public void setYDivs(int ydivs) {
		this.ydivs = Math.max(1, ydivs);
	}
	
	public int getPosXCaseX() {
		return this.posXCaseX;
	}
	
	public void setPosXCaseX(int posXCaseX) {
		this.posXCaseX = posXCaseX;
	}
	
	public int getPosXCaseY() {
		return this.posXCaseY;
	}
	
	public void setPosXCaseY(int posXCaseY) {
		this.posXCaseY = posXCaseY;
	}
	
	public int getNegXCaseY() {
		return this.negXCaseY;
	}
	
	public void setNegXCaseY(int negXCaseY) {
		this.negXCaseY = negXCaseY;
	}
	
	public int getPosYCaseX() {
		return this.posYCaseX;
	}
	
	public void setPosYCaseX(int posYCaseX) {
		this.posYCaseX = posYCaseX;
	}
	
	public int getPosYCaseY() {
		return this.posYCaseY;
	}
	
	public void setPosYCaseY(int posYCaseY) {
		this.posYCaseY = posYCaseY;
	}
	
	public int getNegYCaseX() {
		return this.negYCaseX;
	}
	
	public void setNegYCaseX(int negYCaseX) {
		this.negYCaseX = negYCaseX;
	}
	
	public int getNegYCaseY() {
		return this.negYCaseY;
	}
	
	public void setNegYCaseY(int negYCaseY) {
		this.negYCaseY = negYCaseY;
	}
	
	public int getPosZCaseX() {
		return this.posZCaseX;
	}
	
	public void setPosZCaseX(int posZCaseX) {
		this.posZCaseX = posZCaseX;
	}
	
	public int getPosZCaseY() {
		return this.posZCaseY;
	}
	
	public void setPosZCaseY(int posZCaseY) {
		this.posZCaseY = posZCaseY;
	}
	
	public int getNegZCaseX() {
		return this.negZCaseX;
	}
	
	public void setNegZCaseX(int negZCaseX) {
		this.negZCaseX = negZCaseX;
	}
	
	public int getNegZCaseY() {
		return this.negZCaseY;
	}
	
	public void setNegZCaseY(int negZCaseY) {
		this.negZCaseY = negZCaseY;
	}
	
	public int getPosXRot() {
		return this.posXRot;
	}
	
	public void setPosXRot(int posXRot) {
		this.posXRot = posXRot;
	}
	
	public int getNegXRot() {
		return this.negXRot;
	}
	
	public void setNegXRot(int negXRot) {
		this.negXRot = negXRot;
	}
	
	public int getPosYRot() {
		return this.posYRot;
	}
	
	public void setPosYRot(int posYRot) {
		this.posYRot = posYRot;
	}
	
	public int getNegYRot() {
		return this.negYRot;
	}
	
	public void setNegYRot(int negYRot) {
		this.negYRot = negYRot;
	}
	
	public int getPosZRot() {
		return this.posZRot;
	}
	
	public void setPosZRot(int posZRot) {
		this.posZRot = posZRot;
	}
	
	public int getNegZRot() {
		return this.negZRot;
	}
	
	public void setNegZRot(int negZRot) {
		this.negZRot = negZRot;
	}
	
	public int getNegXCaseX() {
		return this.negXCaseX;
	}
	
	public void setNegXCaseX(int negXCaseX) {
		this.negXCaseX = negXCaseX;
	}
}
