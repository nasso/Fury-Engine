package io.github.nasso.fury.data;

import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Object3D;

import java.io.IOException;

/**
 * @author Nasso
 */
public class ModelLoader {
	public static Object3D load(String fileName) throws IOException {
		return ASSIMPLoader.load(fileName);
	}
	
	public static Mesh loadMesh(String fileName, int meshIndex) throws IOException {
		return ASSIMPLoader.loadMesh(fileName, meshIndex);
	}
}
