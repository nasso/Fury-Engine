package io.github.nasso.fury.data;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.lwjgl.stb.STBImage;
import org.lwjgl.stb.STBImageWrite;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.CubeTexture;
import io.github.nasso.fury.graphics.Texture2D;

public class TextureIO {
	private static int[] int_buffer_a = new int[1];
	private static int[] int_buffer_b = new int[1];
	private static int[] int_buffer_c = new int[1];
	
	public static TextureData loadTextureData(String filePath) throws IOException {
		ByteBuffer data = STBImage.stbi_load(filePath, int_buffer_a, int_buffer_b, int_buffer_c, 4);
		
		TextureData texData = new TextureData();
		texData.setWidth(int_buffer_a[0]);
		texData.setHeight(int_buffer_b[0]);
		texData.setData(data);
		texData.setBytesPerPixel(int_buffer_c[0]);
		
		return texData;
	}
	
	public static Texture2D loadTexture2D(String filePath) throws IOException {
		return loadTexture2D(filePath, false);
	}
	
	public static Texture2D loadTexture2D(String filePath, boolean gammaCorrect) throws IOException {
		TextureData data = loadTextureData(filePath);
		if(data == null) return null;
		
		return new Texture2D.Builder().width(data.getWidth()).height(data.getHeight()).internalFormat(gammaCorrect ? Fury.SRGB_ALPHA : Fury.RGBA).data(data.getData()).build();
	}
	
	public static CubeTexture loadCubeTexture(String filePath, CubeTextureLayout layout) throws IOException {
		try {
			return loadCubeTexture(filePath, layout, null);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static CubeTexture loadCubeTexture(String filePath, CubeTextureLayout layout, PrimitiveProperties cubeProps) throws IOException {
		TextureData allData = loadTextureData(filePath);
		
		int width = allData.getWidth();
		int height = allData.getHeight();
		
		int cellWidth = width / layout.getXDivs();
		int gridSizeY = height / layout.getYDivs();
		
		TextureData posX = allData.subData(layout.getPosXCaseX() * cellWidth, layout.getPosXCaseY() * gridSizeY, cellWidth, gridSizeY);
		TextureData negX = allData.subData(layout.getNegXCaseX() * cellWidth, layout.getNegXCaseY() * gridSizeY, cellWidth, gridSizeY);
		TextureData posY = allData.subData(layout.getPosYCaseX() * cellWidth, layout.getPosYCaseY() * gridSizeY, cellWidth, gridSizeY);
		TextureData negY = allData.subData(layout.getNegYCaseX() * cellWidth, layout.getNegYCaseY() * gridSizeY, cellWidth, gridSizeY);
		TextureData posZ = allData.subData(layout.getPosZCaseX() * cellWidth, layout.getPosZCaseY() * gridSizeY, cellWidth, gridSizeY);
		TextureData negZ = allData.subData(layout.getNegZCaseX() * cellWidth, layout.getNegZCaseY() * gridSizeY, cellWidth, gridSizeY);
		
		posX.rotate(layout.getPosXRot());
		negX.rotate(layout.getNegXRot());
		posY.rotate(layout.getPosYRot());
		negY.rotate(layout.getNegYRot());
		posZ.rotate(layout.getPosZRot());
		negZ.rotate(layout.getNegZRot());
		
		CubeTexture.Builder builder = cubeProps == null ? new CubeTexture.Builder() : new CubeTexture.Builder(cubeProps);
		
		builder.width(cellWidth);
		builder.height(gridSizeY);
		
		builder.posXData(posX.getData());
		builder.posYData(posY.getData());
		builder.posZData(posZ.getData());
		
		builder.negXData(negX.getData());
		builder.negYData(negY.getData());
		builder.negZData(negZ.getData());
		
		return builder.build();
	}
	
	public static void writeTexture(String path, String format, TextureData texData) {
		int width = texData.getWidth();
		int height = texData.getHeight();
		int bpp = texData.getBytesPerPixel();
		ByteBuffer data = texData.getData();
		format = format.toLowerCase();
		if(!format.matches("png|tga|bmp")) { return; }
		
		File dest = new File(path);
		File parent = dest.getParentFile();
		
		if(!parent.exists() && !parent.mkdirs()) {
			System.err.println("Error while creating the directories " + dest);
			return;
		}
		
		if(format.matches("png")) {
			STBImageWrite.stbi_write_png(path, width, height, bpp, data, 0);
		} else if(format.matches("tga")) {
			STBImageWrite.stbi_write_tga(path, width, height, bpp, data);
		} else if(format.matches("bmp")) {
			STBImageWrite.stbi_write_bmp(path, width, height, bpp, data);
		}
	}
}
