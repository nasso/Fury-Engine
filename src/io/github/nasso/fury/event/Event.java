package io.github.nasso.fury.event;

public class Event {
	private Observable source = null;
	private Object data = null;
	
	public Event(Observable source) {
		this(source, null);
	}
	
	public Event(Observable source, Object data) {
		this.setSource(source);
		this.setData(data);
	}
	
	public Observable getSource() {
		return this.source;
	}
	
	public void setSource(Observable source) {
		this.source = source;
	}
	
	public Object getData() {
		return this.data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
}
