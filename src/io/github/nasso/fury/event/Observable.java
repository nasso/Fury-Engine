package io.github.nasso.fury.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Observable {
	private Map<String, List<Consumer<Event>>> listeners = new HashMap<String, List<Consumer<Event>>>();
	
	public void addEventListener(String event, Consumer<Event> consumer) {
		if(consumer == null) return;
		
		List<Consumer<Event>> listlist = this.listeners.get(event);
		
		if(listlist == null) {
			listlist = new ArrayList<Consumer<Event>>();
			this.listeners.put(event, listlist);
		}
		
		listlist.add(consumer);
	}
	
	public void removeEventListener(String event, Consumer<Event> consumer) {
		List<Consumer<Event>> listlist = this.listeners.get(event);
		
		if(listlist == null) return;
		
		listlist.remove(consumer);
	}
	
	public void triggerEvent(String event) {
		this.triggerEvent(event, null);
	}
	
	public void triggerEvent(String event, Object o) {
		List<Consumer<Event>> listlist = this.listeners.get(event);
		
		if(listlist == null) return;
		
		for(int i = 0, l = listlist.size(); i < l; i++)
			listlist.get(i).accept(new Event(this, o));
	}
}
