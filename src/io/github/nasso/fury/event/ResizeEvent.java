package io.github.nasso.fury.event;

public class ResizeEvent {
	private int width, height;
	
	public ResizeEvent(int w, int h) {
		this.setWidth(w);
		this.setHeight(h);
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
}
