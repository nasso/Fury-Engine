package io.github.nasso.fury.event;

import io.github.nasso.fury.core.Fury;

public class MouseButtonEvent {
	private int type = Fury.PRESS;
	private int button = 0;
	
	public MouseButtonEvent(int type, int button) {
		this.setType(type);
		this.setButton(button);
	}
	
	public int getButton() {
		return this.button;
	}
	
	public void setButton(int button) {
		this.button = button;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
}
