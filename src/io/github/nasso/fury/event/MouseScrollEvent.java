package io.github.nasso.fury.event;

public class MouseScrollEvent {
	private float scrollX = 0;
	private float scrollY = 0;
	
	public MouseScrollEvent(float x, float y) {
		this.setScrollX(x);
		this.setScrollY(y);
	}
	
	public float getScrollX() {
		return this.scrollX;
	}
	
	public void setScrollX(float scrollX) {
		this.scrollX = scrollX;
	}
	
	public float getScrollY() {
		return this.scrollY;
	}
	
	public void setScrollY(float scrollY) {
		this.scrollY = scrollY;
	}
}
