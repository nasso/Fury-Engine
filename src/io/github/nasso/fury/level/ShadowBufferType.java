package io.github.nasso.fury.level;

import io.github.nasso.fury.core.Fury;

import java.lang.reflect.Field;

public class ShadowBufferType {
	private ShadowBufferType() {
	}
	
	public static int valueOf(String what) {
		try {
			Field f = Fury.class.getField("SHADOW_BUFFER_TYPE_" + what.toUpperCase());
			if(f != null) return f.getInt(null);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch(NoSuchFieldException e) {
			e.printStackTrace();
		} catch(SecurityException e) {
			e.printStackTrace();
		}
		
		return Fury.LIGHT_TYPE_POINT;
	}
}
