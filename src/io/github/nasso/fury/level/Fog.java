package io.github.nasso.fury.level;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.maths.Color;

public class Fog implements Cloneable {
	private int type = Fury.FOG_TYPE_LINEAR;
	private String name = "";
	private Color color = new Color(1f);
	private float near = 50;
	private float far = 100;
	private float density = 0.0025f;
	
	public Fog(PrimitiveProperties props) {
		this.type = Fury.valueOf("FOG_TYPE_" + props.getString("type", "LINEAR").toUpperCase());
		
		this.name = props.getString("name", this.name);
		this.color.setVec3(props.getVector3f("color", this.color.toVec3()));
		
		this.near = props.getFloat("near", this.near);
		this.far = props.getFloat("far", this.far);
		this.density = props.getFloat("density", this.density);
	}
	
	public Fog(Color color, float near, float far) {
		this.color = color;
		this.near = near;
		this.far = far;
	}
	
	public Fog(Color color) {
		this(color, 1, 1000);
	}
	
	public Fog(int type, float density) {
		this.setType(type);
		this.setDensity(density);
	}
	
	public Fog(int type) {
		this(type, 0.0025f);
	}
	
	public Fog() {
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public float getNear() {
		return this.near;
	}
	
	public void setNear(float near) {
		this.near = near;
	}
	
	public float getFar() {
		return this.far;
	}
	
	public void setFar(float far) {
		this.far = far;
	}
	
	public void setNearFar(float near, float far) {
		this.setNear(near);
		this.setFar(far);
	}
	
	public float getDensity() {
		return this.density;
	}
	
	public void setDensity(float density) {
		this.density = density;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		if(FuryUtils.check4int(type, Fury.FOG_TYPE_LINEAR, Fury.FOG_TYPE_EXP2)) this.type = type;
	}
	
	public Fog clone() {
		Fog clone = new Fog(new Color(this.color));
		
		clone.type = this.type;
		clone.name = this.name;
		clone.near = this.near;
		clone.far = this.far;
		clone.density = this.density;
		
		return clone;
	}
}
