package io.github.nasso.fury.level;

import io.github.nasso.fury.core.PrimitiveProperties;

public abstract class NFClippedCamera extends Camera {
	protected float near = 0.1f;
	protected float far = 1000f;
	
	protected boolean logarithmicPerspectiveDepthCorrection = true;
	
	public void set(PrimitiveProperties props) {
		super.set(props);
		
		this.near = props.getFloat("near", this.near);
		this.far = props.getFloat("far", this.far);
	}
	
	public float getNear() {
		return this.near;
	}
	
	public void setNear(float near) {
		this.near = near;
	}
	
	public float getFar() {
		return this.far;
	}
	
	public void setFar(float far) {
		this.far = far;
	}
}
