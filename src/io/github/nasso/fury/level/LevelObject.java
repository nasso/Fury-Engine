package io.github.nasso.fury.level;

public interface LevelObject {
	public String getID();
	
	public void setID(String id);
	
	public String getName();
	
	public void setName(String name);
}
