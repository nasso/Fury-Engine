package io.github.nasso.fury.level;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

public abstract class Camera extends Object3D {
	protected Matrix4f viewMatrix = new Matrix4f();
	protected Matrix4f projectionMatrix = new Matrix4f();
	protected Matrix4f projViewMatrix = new Matrix4f();
	
	public Matrix4f getViewMatrix() {
		return this.viewMatrix;
	}
	
	public void updateMatrixWorld() {
		super.updateMatrixWorld();
		
		this.matrixWorld.invert(this.viewMatrix);
		this.updateProjViewMatrix();
	}
	
	public void updateProjViewMatrix() {
		this.projectionMatrix.mul(this.viewMatrix, this.projViewMatrix);
	}
	
	public Matrix4f getProjectionMatrix() {
		return this.projectionMatrix;
	}
	
	private Matrix4f _mat = new Matrix4f();
	
	public void lookAt(float x, float y, float z) {
		this._mat.setLookAt(this.position.x, this.position.y, this.position.z, x, y, z, this.up.x, this.up.y, this.up.z);
		
		Vector3f radAngles = new Vector3f();
		this._mat.getUnnormalizedRotation(new Quaternionf()).getEulerAnglesXYZ(radAngles);
		
		this.rotation.x = -(float) Math.toDegrees(radAngles.x);
		this.rotation.y = -(float) Math.toDegrees(radAngles.y);
		this.rotation.z = -(float) Math.toDegrees(radAngles.z);
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public abstract Camera clone();
	
	@Override
	public Camera copy(Object3D obj) {
		if(!(obj instanceof Camera)) throw new IllegalArgumentException(obj + " isn't an instance of Camera");
		
		Camera cam = (Camera) obj;
		
		super.copy(cam);
		this.viewMatrix.set(cam.viewMatrix);
		this.projectionMatrix.set(cam.projectionMatrix);
		
		return this;
	}
	
	public Matrix4f getProjViewMatrix() {
		return this.projViewMatrix;
	}
}
