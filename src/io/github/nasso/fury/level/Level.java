package io.github.nasso.fury.level;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.graphics.CubeTexture;
import io.github.nasso.fury.graphics.Geometry;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.graphics.RenderPass;
import io.github.nasso.fury.graphics.RenderTarget;
import io.github.nasso.fury.graphics.Texture2D;
import io.github.nasso.fury.scripting.ScriptManager;
import io.github.nasso.fury.scripting.ScriptingLanguage;
import io.github.nasso.fury.scripting.nashorn.NashornScriptManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Level extends Observable {
	private Map<ScriptingLanguage, ScriptManager> scriptManagers = new HashMap<ScriptingLanguage, ScriptManager>();
	
	private List<AbstractGeometry> geometries = new ArrayList<AbstractGeometry>();
	private List<Texture2D> textures2d = new ArrayList<Texture2D>();
	private List<CubeTexture> cubeTextures = new ArrayList<CubeTexture>();
	private List<Material> materials = new ArrayList<Material>();
	private List<Scene> scenes = new ArrayList<Scene>();
	private List<RenderTarget> renderTargets = new ArrayList<RenderTarget>();
	private List<RenderPass> renderPasses = new ArrayList<RenderPass>();
	
	private float delta = 0f;
	
	public float getDelta() {
		return this.delta;
	}
	
	public void setDelta(float delta) {
		this.delta = delta;
	}
	
	public Level() {
		try {
			this.setScriptManager(ScriptingLanguage.JAVASCRIPT, NashornScriptManager.class);
		} catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public void setScriptManager(ScriptingLanguage lang, Class<? extends ScriptManager> m) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Constructor<? extends ScriptManager> c = m.getConstructor(Level.class);
		
		if(c == null) return;
		
		this.scriptManagers.put(lang, c.newInstance(this));
	}
	
	public ScriptManager getScriptManager(ScriptingLanguage lang) {
		return this.scriptManagers.get(lang);
	}
	
	public void runScript(String script, ScriptingLanguage lang) {
		ScriptManager manager = this.getScriptManager(lang);
		
		if(manager == null) return;
		
		manager.runScript(script);
	}
	
	public void runScript(File script, ScriptingLanguage lang) throws FileNotFoundException {
		ScriptManager manager = this.getScriptManager(lang);
		
		if(manager == null) return;
		
		manager.runScript(script);
	}
	
	private LevelObject searchForId(String id, List<? extends LevelObject> objects) {
		for(int i = 0, l = objects.size(); i < l; i++) {
			LevelObject o = objects.get(i);
			if(id.equals(o.getID())) return o;
		}
		
		return null;
	}
	
	public LevelObject getObjectById(String id) {
		LevelObject o = this.searchForId(id, this.geometries);
		if(o != null) return o;
		o = this.searchForId(id, this.textures2d);
		if(o != null) return o;
		o = this.searchForId(id, this.cubeTextures);
		if(o != null) return o;
		o = this.searchForId(id, this.materials);
		if(o != null) return o;
		o = this.searchForId(id, this.scenes);
		if(o != null) return o;
		o = this.searchForId(id, this.renderTargets);
		if(o != null) return o;
		o = this.searchForId(id, this.renderPasses);
		if(o != null) return o;
		
		return null;
	}
	
	public void update() {
		this.triggerEvent("update");
		
		for(int i = 0, l = this.scenes.size(); i < l; i++)
			this.scenes.get(i).triggerTick(this.delta);
	}
	
	public Geometry createGeometry() {
		Geometry g = new Geometry();
		this.geometries.add(g);
		return g;
	}
	
	public BufferGeometry createBufferGeometry() {
		BufferGeometry g = new BufferGeometry();
		this.geometries.add(g);
		return g;
	}
	
	public Geometry create(Geometry source) {
		this.geometries.add(source);
		return source;
	}
	
	public BufferGeometry create(BufferGeometry source) {
		this.geometries.add(source);
		return source;
	}
	
	public Scene createScene() {
		Scene sce = new Scene();
		this.scenes.add(sce);
		return sce;
	}
	
	public Material createMaterial() {
		Material m = new Material();
		this.materials.add(m);
		return m;
	}
	
	public Texture2D create(Texture2D t) {
		this.textures2d.add(t);
		return t;
	}
	
	public CubeTexture create(CubeTexture t) {
		this.cubeTextures.add(t);
		return t;
	}
	
	public List<AbstractGeometry> geometries() {
		return this.geometries;
	}
	
	public List<Texture2D> textures2d() {
		return this.textures2d;
	}
	
	public List<CubeTexture> cubeTextures() {
		return this.cubeTextures;
	}
	
	public List<Material> materials() {
		return this.materials;
	}
	
	public List<Scene> scenes() {
		return this.scenes;
	}
	
	public List<RenderTarget> renderTargets() {
		return this.renderTargets;
	}
	
	public List<RenderPass> renderPasses() {
		return this.renderPasses;
	}
	
	private void disposeAndClear(List<? extends Disposable> disposables) {
		for(int i = 0, l = disposables.size(); i < l; i++)
			disposables.get(i).dispose();
		
		disposables.clear();
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
		
		this.disposeAndClear(this.geometries);
		this.disposeAndClear(this.textures2d);
		this.disposeAndClear(this.cubeTextures);
		this.disposeAndClear(this.materials);
		this.disposeAndClear(this.scenes);
		this.disposeAndClear(this.renderTargets);
		
		this.renderPasses.clear();
	}
}
