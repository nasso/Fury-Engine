package io.github.nasso.fury.level;

import io.github.nasso.fury.graphics.AbstractGeometry;
import io.github.nasso.fury.graphics.Material;

public class Mesh extends Object3D {
	private AbstractGeometry geom;
	private Material mat;
	
	public Mesh() {
		
	}
	
	public Mesh(Material mat) {
		this(null, mat);
	}
	
	public Mesh(AbstractGeometry geom, Material mat) {
		this.setGeometry(geom);
		this.setMaterial(mat);
	}
	
	public Mesh(AbstractGeometry geom) {
		this(geom, null);
	}
	
	public AbstractGeometry getGeometry() {
		return this.geom;
	}
	
	public void setGeometry(AbstractGeometry geom) {
		this.geom = geom;
	}
	
	public Material getMaterial() {
		return this.mat;
	}
	
	public void setMaterial(Material mat) {
		this.mat = mat;
	}
	
	public Mesh copy(Object3D other) {
		super.copy(other);
		
		if(other instanceof Mesh) {
			Mesh m = (Mesh) other;
			
			this.geom = m.geom;
			this.mat = m.mat;
		}
		
		return this;
	}
	
	public Mesh clone() {
		return new Mesh().copy(this);
	}
}
