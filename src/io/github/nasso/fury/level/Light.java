package io.github.nasso.fury.level;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.ShadowsProperties;
import io.github.nasso.fury.maths.Color;

import org.joml.Vector3f;

public class Light extends Object3D {
	private Vector3f color = new Vector3f(1.0f);
	private Vector3f groundColor = new Vector3f(1.0f);
	private ShadowsProperties shadowsProps = new ShadowsProperties();
	
	private int type = Fury.LIGHT_TYPE_POINT;
	
	private float energy = 1.0f;
	private float distance = 30.0f;
	private float coneSize = 30f;
	private float coneBlend = 0f;
	private float shadowZNearSun = -50;
	private float shadowZFarSun = 50;
	private float shadowZNear = 0.1f;
	private float shadowZFar = 100f;
	private float sunInnerEdge = 0.53623f;
	private float sunOuterEdge = 3.2f;
	private float sunBrightness = 4.0f;
	
	private boolean blinnPhong = true;
	private boolean specular = true;
	private boolean diffuse = true;
	private boolean enabled = true;
	
	public Light() {
		this(Fury.LIGHT_TYPE_POINT);
	}
	
	public Light(ShadowsProperties shadowsProps) {
		this();
		
		this.shadowsProps = shadowsProps;
	}
	
	public void set(PrimitiveProperties props) {
		super.set(props);
		
		this.color.set(props.getVector3f("color", this.color));
		this.groundColor.set(props.getVector3f("groundColor", this.groundColor));
		
		this.type = Fury.valueOf("LIGHT_TYPE_" + props.getString("lightType", "POINT").toUpperCase());
		
		this.energy = props.getFloat("energy", this.energy);
		this.distance = props.getFloat("distance", this.distance);
		this.coneSize = props.getFloat("coneSize", this.coneSize);
		this.coneBlend = props.getFloat("coneBlend", this.coneBlend);
		this.shadowZNearSun = props.getFloat("shadowZNearSun", this.shadowZNearSun);
		this.shadowZFarSun = props.getFloat("shadowZFarSun", this.shadowZFarSun);
		this.shadowZNear = props.getFloat("shadowZNear", this.shadowZNear);
		this.shadowZFar = props.getFloat("shadowZFar", this.shadowZFar);
		this.sunInnerEdge = props.getFloat("sunInnerEdge", this.sunInnerEdge);
		this.sunOuterEdge = props.getFloat("sunOuterEdge", this.sunOuterEdge);
		this.sunBrightness = props.getFloat("sunBrightness", this.sunBrightness);
		
		this.blinnPhong = props.getBool("blinnPhong", this.blinnPhong);
		this.specular = props.getBool("specular", this.specular);
		this.diffuse = props.getBool("diffuse", this.diffuse);
		this.enabled = props.getBool("enabled", this.enabled);
	}
	
	public Light(int lightType) {
		this(lightType, 1.0f);
	}
	
	public Light(float energy) {
		this(Fury.LIGHT_TYPE_POINT, energy);
	}
	
	public Light(int lightType, float energy) {
		this.setEnergy(energy);
		this.setType(lightType);
		this.setCastShadow(false);
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int lightType) {
		if(FuryUtils.check4int(lightType, Fury.LIGHT_TYPE_POINT, Fury.LIGHT_TYPE_SPOT, Fury.LIGHT_TYPE_SUN, Fury.LIGHT_TYPE_HEMISPHERE, Fury.LIGHT_TYPE_AMBIENT)) this.type = lightType;
	}
	
	public float getEnergy() {
		return this.energy;
	}
	
	public void setEnergy(float energy) {
		this.energy = energy;
	}
	
	public float getDistance() {
		return this.distance;
	}
	
	public void setDistance(float distance) {
		this.distance = Math.abs(distance);
	}
	
	public float getConeSize() {
		return this.coneSize;
	}
	
	public void setConeSize(float coneSize) {
		this.coneSize = Math.min(89, Math.abs(coneSize));
		
		this.coneBlend = Math.min(this.coneBlend, this.coneSize);
	}
	
	public float getConeBlend() {
		return this.coneBlend;
	}
	
	public void setConeBlend(float coneBlend) {
		this.coneBlend = Math.min(coneBlend, this.coneSize);
	}
	
	public boolean isSpecular() {
		return this.specular;
	}
	
	public void setSpecular(boolean specular) {
		this.specular = specular;
	}
	
	public boolean isDiffuse() {
		return this.diffuse;
	}
	
	public void setDiffuse(boolean diffuse) {
		this.diffuse = diffuse;
	}
	
	public Vector3f getColor() {
		return this.color;
	}
	
	public void setColor(Vector3f color) {
		this.color = color;
	}
	
	public void setColor(Color color) {
		this.setColor(color.r, color.g, color.b);
	}
	
	public void setColor(float r, float g, float b) {
		this.color.set(r, g, b);
	}
	
	public void setColor(int color) {
		this.color.set(((color >> 16) & 0xFF) / 255.0f, ((color >> 8) & 0xFF) / 255.0f, ((color >> 0) & 0xFF) / 255.0f);
	}
	
	public boolean isBlinnPhong() {
		return this.blinnPhong;
	}
	
	public void setBlinnPhong(boolean blinnPhong) {
		this.blinnPhong = blinnPhong;
	}
	
	public Light copy(Object3D other) {
		super.copy(other);
		
		if(other instanceof Light) {
			Light lo = (Light) other;
			
			this.color.set(lo.color);
			this.type = lo.type;
			this.energy = lo.energy;
			this.distance = lo.distance;
			this.coneSize = lo.coneSize;
			this.coneBlend = lo.coneBlend;
			
			this.blinnPhong = lo.blinnPhong;
			this.specular = lo.specular;
			this.diffuse = lo.diffuse;
		}
		
		return this;
	}
	
	public Light clone() {
		return new Light().copy(this);
	}
	
	public ShadowsProperties getShadowsProps() {
		return this.shadowsProps;
	}
	
	public void setShadowsProps(ShadowsProperties shadowsProps) {
		this.shadowsProps = shadowsProps;
	}
	
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Vector3f getGroundColor() {
		return this.groundColor;
	}
	
	public void setGroundColor(Vector3f groundColor) {
		this.groundColor.set(groundColor);
	}
	
	public void setGroundColor(int color) {
		this.groundColor.set(((color >> 16) & 0xFF) / 255.0f, ((color >> 8) & 0xFF) / 255.0f, ((color >> 0) & 0xFF) / 255.0f);
	}
	
	public void setGroundColor(Color color) {
		this.setGroundColor(color.r, color.g, color.b);
	}
	
	public void setGroundColor(float r, float g, float b) {
		this.groundColor.set(r, g, b);
	}
	
	public float getShadowZNear() {
		return this.shadowZNear;
	}
	
	public void setShadowZNear(float shadowZNear) {
		this.shadowZNear = shadowZNear;
	}
	
	public float getShadowZFar() {
		return this.shadowZFar;
	}
	
	public void setShadowZFar(float shadowZFar) {
		this.shadowZFar = shadowZFar;
	}
	
	public float getSunInnerEdge() {
		return this.sunInnerEdge;
	}
	
	public void setSunInnerEdge(float sunInnerEdge) {
		this.sunInnerEdge = sunInnerEdge;
	}
	
	public float getSunOuterEdge() {
		return this.sunOuterEdge;
	}
	
	public void setSunOuterEdge(float sunOuterEdge) {
		this.sunOuterEdge = sunOuterEdge;
	}
	
	public float getShadowZNearSun() {
		return this.shadowZNearSun;
	}
	
	public void setShadowZNearSun(float shadowZNearSun) {
		this.shadowZNearSun = shadowZNearSun;
	}
	
	public float getShadowZFarSun() {
		return this.shadowZFarSun;
	}
	
	public void setShadowZFarSun(float shadowZFarSun) {
		this.shadowZFarSun = shadowZFarSun;
	}
	
	public float getSunBrightness() {
		return this.sunBrightness;
	}
	
	public void setSunBrightness(float sunBrightness) {
		this.sunBrightness = sunBrightness;
	}
}
