package io.github.nasso.fury.level;

import io.github.nasso.fury.core.Disposable;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.Material;

public class Scene extends Object3D implements Disposable {
	private Fog fog = null;
	private Sky sky = new Sky();
	private Material overrideMaterial = null;
	
	private float skyboxDistance = 10f;
	private boolean autoUpdate = true;
	private boolean lighting = false;
	
	public Scene() {
		
	}
	
	public void set(PrimitiveProperties props) {
		super.set(props);
		
		this.setSkyboxDistance(props.getFloat("skyboxDistance", this.skyboxDistance));
		this.setAutoUpdate(props.getBool("autoUpdate", this.autoUpdate));
		this.setLighting(props.getBool("lighting", this.lighting));
	}
	
	public void dispose() {
		this.triggerEvent("dispose");
	}
	
	public Fog getFog() {
		return this.fog;
	}
	
	public void setFog(Fog fog) {
		this.fog = fog;
	}
	
	public Material getOverrideMaterial() {
		return this.overrideMaterial;
	}
	
	public void setOverrideMaterial(Material overrideMaterial) {
		this.overrideMaterial = overrideMaterial;
	}
	
	public boolean isAutoUpdate() {
		return this.autoUpdate;
	}
	
	public void setAutoUpdate(boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}
	
	@Override
	public Scene clone() {
		return new Scene().copy(this);
	}
	
	@Override
	public Scene copy(Object3D obj) {
		if(!(obj instanceof Scene)) throw new IllegalArgumentException(obj + " isn't an instance of Scene");
		
		Scene source = (Scene) obj;
		
		super.copy(source);
		
		if(source.fog != null) this.fog = source.fog.clone();
		
		if(source.overrideMaterial != null) this.overrideMaterial = source.overrideMaterial.clone();
		
		this.autoUpdate = source.autoUpdate;
		
		return this;
	}
	
	public Sky getSky() {
		return this.sky;
	}
	
	public void setSky(Sky sky) {
		this.sky = sky;
	}
	
	public float getSkyboxDistance() {
		return this.skyboxDistance;
	}
	
	public void setSkyboxDistance(float skyboxDistance) {
		this.skyboxDistance = skyboxDistance;
	}
	
	public boolean isLighting() {
		return lighting;
	}
	
	public void setLighting(boolean lighting) {
		this.lighting = lighting;
	}
}
