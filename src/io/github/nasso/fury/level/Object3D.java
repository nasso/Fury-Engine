package io.github.nasso.fury.level;

import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.event.Observable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class Object3D extends Observable implements Cloneable, LevelObject {
	private static Map<String, Integer> auto_names = new HashMap<String, Integer>();
	
	protected String name = "";
	protected Object3D parent;
	protected List<Object3D> children = new ArrayList<Object3D>();
	
	protected Vector3f position = new Vector3f();
	protected Vector3f scale = new Vector3f(1, 1, 1);
	protected Vector3f rotation = new Vector3f();
	protected Vector3f up = new Vector3f(0, 0, 1);
	
	protected Matrix4f matrix = new Matrix4f();
	protected Matrix4f matrixWorld = new Matrix4f();
	
	protected boolean visible = true;
	protected boolean castShadow = true;
	protected boolean receiveShadow = true;
	protected boolean frustumCulled = true;
	protected boolean matrixAutoUpdate = true;
	protected boolean matrixWorldNeedUpdate = false;
	protected boolean blockTick = false;
	
	protected Object userData;
	
	private String lvl_id = "";
	
	public void setID(String id) {
		this.lvl_id = id;
	}
	
	public String getID() {
		return this.lvl_id;
	}
	
	public Object3D(String name) {
		this.name = name;
	}
	
	public Object3D() {
		// Auto-name
		String name = this.getClass().getSimpleName();
		
		int uses = auto_names.getOrDefault(name, -1);
		uses++;
		
		auto_names.put(name, uses);
		
		name += "." + uses;
		
		this.setName(name);
	}
	
	public void set(PrimitiveProperties props) {
		this.name = props.getString("name", this.name);
		
		this.position.set(props.getVector3f("position", this.position));
		this.scale.set(props.getVector3f("scale", this.scale));
		this.rotation.set(props.getVector3f("rotation", this.rotation));
		this.up.set(props.getVector3f("up", this.up));
		
		this.visible = props.getBool("visible", this.visible);
		this.castShadow = props.getBool("castShadow", this.castShadow);
		this.receiveShadow = props.getBool("receiveShadow", this.receiveShadow);
		this.frustumCulled = props.getBool("frustumCulled", this.frustumCulled);
		this.matrixAutoUpdate = props.getBool("matrixAutoUpdate", this.matrixAutoUpdate);
		this.matrixWorldNeedUpdate = props.getBool("matrixWorldNeedUpdate", this.matrixWorldNeedUpdate);
		this.blockTick = props.getBool("blockTick", this.blockTick);
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public void setMatrix(Matrix4f mat) {
		this.matrix.set(mat);
		
		this.updatePosRotScale();
	}
	
	public void applyMatrix(Matrix4f mat) {
		this.matrix.mul(mat);
		
		this.updatePosRotScale();
	}
	
	public void translate(Vector3f v) {
		this.translate(v.x, v.y, v.z);
	}
	
	public void translate(float x, float y, float z) {
		this.position.add(x, y, z);
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public void rotate(Vector3f v) {
		this.rotate(v.x, v.y, v.z);
	}
	
	public void rotate(float x, float y, float z) {
		this.rotation.add(x, y, z);
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public void scale(Vector3f v) {
		this.scale(v.x, v.y, v.z);
	}
	
	public void scale(float x, float y, float z) {
		this.scale.mul(x, y, z);
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public void scale(float xyz) {
		this.scale.mul(xyz);
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public void setRotation(Quaternionf quat) {
		Matrix4f mat = new Matrix4f().set(quat);
		
		// assumes the upper 3x3 of mat is a pure rotation matrix (i.e,
		// unscaled)
		this.rotation.y = (float) Math.asin(FuryUtils.clamp(mat.m13(), -1, 1));
		
		if(Math.abs(mat.m13()) < 0.99999) {
			this.rotation.x = (float) Math.atan2(-mat.m23(), mat.m33());
			this.rotation.z = (float) Math.atan2(-mat.m12(), mat.m11());
		} else {
			this.rotation.x = (float) Math.atan2(mat.m32(), mat.m22());
			this.rotation.z = 0;
		}
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	private Quaternionf _quat = new Quaternionf();
	
	public void updatePosRotScale() {
		this.matrix.getTranslation(this.position);
		this.matrix.getUnnormalizedRotation(this._quat).getEulerAnglesXYZ(this.rotation);
		this.matrix.getScale(this.scale);
		
		// Converts to degrees
		this.rotation.x = (float) Math.toDegrees(this.rotation.x);
		this.rotation.y = (float) Math.toDegrees(this.rotation.y);
		this.rotation.z = (float) Math.toDegrees(this.rotation.z);
	}
	
	private Matrix4f _mat = new Matrix4f();
	
	public Vector3f localToWorldPos(Vector3f v) {
		return this.matrixWorld.transformPosition(v);
	}
	
	public Vector3f worldToLocalPos(Vector3f v) {
		return this.matrixWorld.invert(this._mat).transformPosition(v);
	}
	
	public Vector3f localToWorldDir(Vector3f v) {
		return this.matrixWorld.transformDirection(v);
	}
	
	public Vector3f worldToLocalDir(Vector3f v) {
		return this.matrixWorld.invert(this._mat).transformDirection(v);
	}
	
	public Vector4f localToWorld(Vector4f v) {
		return this.matrixWorld.transform(v);
	}
	
	public Vector4f worldToLocal(Vector4f v) {
		return this.matrixWorld.invert(this._mat).transform(v);
	}
	
	public void lookAt(Vector3f v) {
		this.lookAt(v.x, v.y, v.z);
	}
	
	public void lookAt(float x, float y, float z) {
		this.matrix.rotateTowards(x - this.position.x, y - this.position.y, z - this.position.z, this.up.x, this.up.y, this.up.z);
		
		this.updatePosRotScale();
		
		this.setMatrixWorldNeedUpdate(true);
	}
	
	public void updateMatrix() {
		this.matrix.identity().translation(this.position).rotateZYX((float) Math.toRadians(this.rotation.z), (float) Math.toRadians(this.rotation.y), (float) Math.toRadians(this.rotation.x)).scale(this.scale);
	}
	
	public void updateMatrixWorld() {
		this.updateMatrixWorld(false);
	}
	
	public void updateMatrixWorld(boolean force) {
		if(this.matrixAutoUpdate) this.updateMatrix();
		
		if(this.matrixWorldNeedUpdate || force) {
			if(this.parent == null) // this.matrixWorld = this.matrix
				this.matrixWorld.set(this.matrix);
			else // this.matrixWorld = this.parent.matrixWorld * this.matrix
				this.parent.matrixWorld.mul(this.matrix, this.matrixWorld);
			
			this.matrixWorldNeedUpdate = false;
		}
		
		for(int i = 0, l = this.children.size(); i < l; i++)
			this.children.get(i).updateMatrixWorld(force);
	}
	
	public Object3D clone() {
		return new Object3D().copy(this);
	}
	
	public Object3D copy(Object3D source) {
		this.children.clear();
		
		this.name = source.name;
		this.parent = source.parent;
		this.children.addAll(source.children);
		
		this.position.set(source.position);
		this.scale.set(source.scale);
		this.rotation.set(source.rotation);
		this.up.set(source.up);
		
		this.matrix.set(source.matrix);
		this.matrixWorld.set(source.matrixWorld);
		
		this.visible = source.visible;
		this.castShadow = source.castShadow;
		this.receiveShadow = source.receiveShadow;
		this.frustumCulled = source.frustumCulled;
		this.matrixAutoUpdate = source.matrixAutoUpdate;
		this.matrixWorldNeedUpdate = source.matrixWorldNeedUpdate;
		
		this.userData = source.userData;
		
		return this;
	}
	
	private void generateDescription(StringBuilder builder, int tabs) {
		builder.append("+ " + this.getClass().getSimpleName() + ": #" + this.lvl_id + " \"" + this.name + "\"\n");
		
		String prefix = new String(new char[tabs + 1]).replace("\0", "|\t"); // prefix
																				// =
																				// rep("|\t",
																				// tabs
																				// +
																				// 1)
		for(int i = 0, l = this.children.size(); i < l; i++) {
			builder.append(prefix + "|\n" + prefix);
			this.children.get(i).generateDescription(builder, tabs + 1);
		}
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		this.generateDescription(builder, 0);
		builder.append("o"); // Final "o" (makes it prettier :D)
		
		return builder.toString();
	}
	
	// GETTERS
	public Object3D getChildByName(String name) {
		for(int i = 0, l = this.children.size(); i < l; i++) {
			Object3D o = this.children.get(i);
			
			if(o.name.equals(name)) return o;
		}
		
		return null;
	}
	
	public Object3D getChild(int index) {
		if(index >= this.children.size()) return null;
		
		return this.children.get(index);
	}
	
	public String getName() {
		return this.name;
	}
	
	public Object3D getParent() {
		return this.parent;
	}
	
	public List<Object3D> getChildren() {
		return new ArrayList<Object3D>(this.children);
	}
	
	public void addChildren(List<? extends Object3D> obj) {
		for(int i = 0; i < obj.size(); i++) {
			Object3D o = obj.get(i);
			
			if(o.parent != null) o.parent.children.remove(o);
			
			this.children.add(o);
			o.parent = this;
		}
	}
	
	public void addChildren(Object3D... obj) {
		for(int i = 0; i < obj.length; i++) {
			Object3D o = obj[i];
			
			if(o.parent != null) o.parent.children.remove(o);
			
			this.children.add(o);
			o.parent = this;
		}
	}
	
	public void removeChildren(Object3D... obj) {
		for(int i = 0; i < obj.length; i++) {
			Object3D o = obj[i];
			
			this.children.remove(o);
			o.parent = null;
		}
	}
	
	public boolean hasChild(Object3D obj) {
		// Check if it's a direct child
		if(this.children.contains(obj)) return true;
		
		// If it's not, then call hasChild(obj) on all children
		for(int i = 0, l = this.children.size(); i < l; i++)
			if(this.children.get(i).hasChild(obj)) return true;
		
		// Return false if there's nothing
		return false;
	}
	
	public Object3D getChildById(String id) {
		for(int i = 0, l = this.children.size(); i < l; i++) {
			Object3D c = this.children.get(i);
			if(c.getID().equals(id)) return c;
		}
		
		for(int i = 0, l = this.children.size(); i < l; i++) {
			Object3D c = this.children.get(i).getChildById(id);
			if(c != null) return c;
		}
		
		return null;
	}
	
	public void giveAllChildren(Object3D dest) {
		for(int i = this.children.size() - 1; i >= 0; i--)
			dest.addChildren(this.children.get(i));
	}
	
	public Vector3f getPosition() {
		return this.position;
	}
	
	public Vector3f getScale() {
		return this.scale;
	}
	
	public Vector3f getRotation() {
		return this.rotation;
	}
	
	public Vector3f getUp() {
		return this.up;
	}
	
	public Matrix4f getMatrix() {
		return this.matrix;
	}
	
	public Matrix4f getMatrixWorld() {
		return this.matrixWorld;
	}
	
	public boolean isVisible() {
		return this.visible;
	}
	
	public boolean isCastShadow() {
		return this.castShadow;
	}
	
	public boolean isReceiveShadow() {
		return this.receiveShadow;
	}
	
	public boolean isFrustumCulled() {
		return this.frustumCulled;
	}
	
	public boolean isMatrixAutoUpdate() {
		return this.matrixAutoUpdate;
	}
	
	public boolean isMatrixWorldNeedUpdate() {
		return this.matrixWorldNeedUpdate;
	}
	
	public Object getUserData() {
		return this.userData;
	}
	
	// SETTERS
	public void setName(String name) {
		this.name = name;
	}
	
	public void setParent(Object3D parent) {
		parent.addChildren(this);
	}
	
	public void setPosition(Vector3f position) {
		this.setPosition(position.x, position.y, position.z);
	}
	
	public void setScale(Vector3f scale) {
		this.setScale(scale.x, scale.y, scale.z);
	}
	
	public void setRotation(Vector3f v) {
		this.setRotation(v.x, v.y, v.z);
	}
	
	public void setUp(Vector3f up) {
		this.setUp(up.x, up.y, up.z);
	}
	
	public void setPosition(float x, float y, float z) {
		if(this.position.x != x || this.position.y != y || this.position.z != z) {
			this.position.set(x, y, z);
			
			this.setMatrixWorldNeedUpdate(true);
		}
	}
	
	public void setScale(float x, float y, float z) {
		if(this.scale.x != x || this.scale.y != y || this.scale.z != z) {
			this.scale.set(x, y, z);
			
			this.setMatrixWorldNeedUpdate(true);
		}
	}
	
	public void setScale(float xyz) {
		this.setScale(xyz, xyz, xyz);
	}
	
	public void setRotation(float x, float y, float z) {
		if(this.rotation.x != x || this.rotation.y != y || this.rotation.z != z) {
			this.rotation.set(x, y, z);
			
			this.setMatrixWorldNeedUpdate(true);
		}
	}
	
	public void setUp(float x, float y, float z) {
		if(this.up.x != x || this.up.y != y || this.up.z != z) {
			this.up.set(x, y, z);
			
			this.setMatrixWorldNeedUpdate(true);
		}
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public void setCastShadow(boolean castShadow) {
		this.castShadow = castShadow;
	}
	
	public void setReceiveShadow(boolean receiveShadow) {
		this.receiveShadow = receiveShadow;
	}
	
	public void setFrustumCulled(boolean frustumCulled) {
		this.frustumCulled = frustumCulled;
	}
	
	public void setMatrixAutoUpdate(boolean matrixAutoUpdate) {
		this.matrixAutoUpdate = matrixAutoUpdate;
	}
	
	public void setMatrixWorldNeedUpdate(boolean matrixWorldNeedUpdate) {
		this.matrixWorldNeedUpdate = matrixWorldNeedUpdate;
		
		if(matrixWorldNeedUpdate) for(int i = 0, l = this.children.size(); i < l; i++)
			this.children.get(i).setMatrixWorldNeedUpdate(true);
	}
	
	public void setUserData(Object userData) {
		this.userData = userData;
	}
	
	public boolean isBlockTick() {
		return this.blockTick;
	}
	
	public void setBlockTick(boolean blockTick) {
		this.blockTick = blockTick;
	}
	
	public void triggerTick(float delta, boolean spread) {
		if(this.blockTick) return;
		
		this.triggerEvent("tick", delta);
		
		if(spread) for(int i = 0, l = this.children.size(); i < l; i++)
			this.children.get(i).triggerTick(delta);
	}
	
	public void triggerTick(float delta) {
		this.triggerTick(delta, true);
	}
}
