package io.github.nasso.fury.level;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.PrimitiveProperties;
import io.github.nasso.fury.graphics.CubeTexture;
import io.github.nasso.fury.maths.Color;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

public class Sky {
	private int type = Fury.SKY_TYPE_PROCEDURAL;
	private CubeTexture cubeTex = null;
	private List<Light> suns = new ArrayList<Light>();
	
	private Vector3f up = new Vector3f(0, 0, 1);
	private Color bottomColor = new Color(1.0f);
	private Color topColor = new Color(0.55f, 0.75f, 1.0f);
	
	private float cosPower = 1.5f;
	
	public Sky() {
		this.type = Fury.SKY_TYPE_PROCEDURAL;
	}
	
	public Sky(PrimitiveProperties props) {
		this.up.set(props.getVector3f("up", this.up));
		this.bottomColor.setVec3(props.getVector3f("bottomColor"));
		this.topColor.setVec3(props.getVector3f("topColor"));
		this.cosPower = props.getFloat("cosPower", this.cosPower);
	}
	
	public Sky(CubeTexture tex) {
		this.type = Fury.SKY_TYPE_CUBE_TEXTURE;
		this.cubeTex = tex;
	}
	
	public CubeTexture getCubeTexture() {
		return this.cubeTex;
	}
	
	public void setCubeTexture(CubeTexture cubeTex) {
		this.cubeTex = cubeTex;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		if(FuryUtils.check4int(type, Fury.SKY_TYPE_CUBE_TEXTURE, Fury.SKY_TYPE_PROCEDURAL)) this.type = type;
	}
	
	public List<Light> getSuns() {
		return this.suns;
	}
	
	public void addSun(Light sun) {
		this.suns.add(sun);
	}
	
	public Vector3f getUp() {
		return this.up;
	}
	
	public void setUp(Vector3f up) {
		this.up = up;
	}
	
	public Color getBottomColor() {
		return this.bottomColor;
	}
	
	public void setBottomColor(Color bottomColor) {
		this.bottomColor = bottomColor;
	}
	
	public Color getTopColor() {
		return this.topColor;
	}
	
	public void setTopColor(Color topColor) {
		this.topColor = topColor;
	}
	
	public float getCosPower() {
		return this.cosPower;
	}
	
	public void setCosPower(float cosPower) {
		this.cosPower = cosPower;
	}
}
