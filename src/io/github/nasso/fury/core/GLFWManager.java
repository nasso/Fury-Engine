package io.github.nasso.fury.core;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWErrorCallback;

public class GLFWManager {
	private static boolean initialized = false;
	private static PointerBuffer monitorsBuffer = null;
	
	public static int getMonitorCount() {
		return monitorsBuffer.limit();
	}
	
	public static long getMonitor(int index) {
		return monitorsBuffer.get(index);
	}
	
	public static boolean isInitialized() {
		return initialized;
	}
	
	/**
	 * Returns the time since the GLFW intialization.
	 * 
	 * @return
	 */
	public static double getTimeSec() {
		return glfwGetTime();
	}
	
	public static void init() {
		initialized = glfwInit();
		
		if(initialized) {
			GLFWErrorCallback.createPrint(System.err).set();
			
			monitorsBuffer = glfwGetMonitors();
		} else System.err.println("Couldn't init GLFW");
	}
	
	public static void release() {
		glfwTerminate();
		
		initialized = false;
	}
}
