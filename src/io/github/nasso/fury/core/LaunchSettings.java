package io.github.nasso.fury.core;

import io.github.nasso.fury.graphics.RenderMode;
import io.github.nasso.fury.graphics.Texture2D;

public class LaunchSettings {
	public enum GlobalRenderer {
		OPENGL
	}
	
	public enum VideoMode {
		FULLSCREEN, WINDOWED, MAXIMIZED
	}
	
	private GlobalRenderer renderer = GlobalRenderer.OPENGL;
	private RenderMode renderMode = RenderMode.DEFERRED_RENDERING;
	private VideoMode videoMode = VideoMode.FULLSCREEN;
	private Texture2D icon = null;
	private int videoWidth = 1280, videoHeight = 720;
	private int windowMonitor = 0;
	private String windowTitle = "FuryEngine";
	private boolean resizable = true;
	private boolean vsync = false;
	private boolean uiantialias = true;
	private boolean useInterleavedVBOs = true;
	
	public LaunchSettings() {
	}
	
	public LaunchSettings renderMode(RenderMode renderMode) {
		this.renderMode = renderMode;
		return this;
	}
	
	public LaunchSettings icon(Texture2D icon) {
		this.icon = icon;
		return this;
	}
	
	public LaunchSettings renderer(GlobalRenderer v) {
		this.renderer = v;
		return this;
	}
	
	public LaunchSettings videoMode(VideoMode v) {
		this.videoMode = v;
		return this;
	}
	
	public LaunchSettings videoWidth(int v) {
		this.videoWidth = v;
		return this;
	}
	
	public LaunchSettings videoHeight(int v) {
		this.videoHeight = v;
		return this;
	}
	
	public LaunchSettings videoSize(int x, int y) {
		return this.videoWidth(x).videoHeight(y);
	}
	
	public LaunchSettings vsync(boolean value) {
		this.vsync = value;
		return this;
	}
	
	public LaunchSettings windowTitle(String title) {
		this.windowTitle = title;
		return this;
	}
	
	public LaunchSettings windowMonitor(int mon) {
		this.windowMonitor = mon;
		return this;
	}
	
	public LaunchSettings resizable(boolean value) {
		this.resizable = value;
		return this;
	}
	
	public LaunchSettings uiAntialiasing(boolean value) {
		this.uiantialias = value;
		return this;
	}
	
	public LaunchSettings useInterleavedVBOs(boolean value) {
		this.useInterleavedVBOs = value;
		return this;
	}
	
	public GlobalRenderer getRenderer() {
		return this.renderer;
	}
	
	public VideoMode getVideoMode() {
		return this.videoMode;
	}
	
	public int getVideoWidth() {
		return this.videoWidth;
	}
	
	public int getVideoHeight() {
		return this.videoHeight;
	}
	
	public boolean isVSYNC() {
		return this.vsync;
	}
	
	public String getWindowTitle() {
		return this.windowTitle;
	}
	
	public int getWindowMonitor() {
		return this.windowMonitor;
	}
	
	public boolean isResizable() {
		return this.resizable;
	}
	
	public boolean isUIAntialiasing() {
		return this.uiantialias;
	}
	
	public RenderMode getRenderMode() {
		return this.renderMode;
	}
	
	public Texture2D getIcon() {
		return this.icon;
	}
	
	public boolean isUseInterleavedVBOs() {
		return this.useInterleavedVBOs;
	}
}
