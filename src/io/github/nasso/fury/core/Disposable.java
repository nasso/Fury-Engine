package io.github.nasso.fury.core;

public interface Disposable {
	public void dispose();
}
