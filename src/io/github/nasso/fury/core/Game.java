package io.github.nasso.fury.core;

import java.lang.reflect.InvocationTargetException;

import org.lwjgl.system.Configuration;

import io.github.nasso.fury.core.LaunchSettings.GlobalRenderer;
import io.github.nasso.fury.data.TextureData;
import io.github.nasso.fury.event.Observable;
import io.github.nasso.fury.graphics.RenderMode;
import io.github.nasso.fury.graphics.Renderer;
import io.github.nasso.fury.level.Level;
import io.github.nasso.fury.opengl.OGLDeferredRenderer;
import io.github.nasso.fury.opengl.OGLManager;

public abstract class Game extends Observable implements Disposable {
	private static Game launchedGame = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void launch(LaunchSettings settings) {
		StackTraceElement[] cause = Thread.currentThread().getStackTrace();
		
		boolean foundThisMethod = false;
		String callingClassName = null;
		for(StackTraceElement trace : cause) {
			String className = trace.getClassName();
			String methodName = trace.getMethodName();
			if(foundThisMethod) {
				callingClassName = className;
				break;
			} else if(Game.class.getName().equals(className) && methodName.equals("launch")) foundThisMethod = true;
		}
		
		if(callingClassName == null) throw new RuntimeException("Can't find the Game class to launch.");
		
		try {
			Class theClass = Class.forName(callingClassName, false, Thread.currentThread().getContextClassLoader());
			if(Game.class.isAssignableFrom(theClass)) {
				Class<? extends Game> gameClass = theClass;
				launch(gameClass, settings);
			} else throw new RuntimeException(theClass.getCanonicalName() + " doesn't extends " + Game.class.getCanonicalName());
		} catch(RuntimeException ex) {
			throw ex;
		} catch(Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public static void launch(Class<? extends Game> gameClass, LaunchSettings settings) {
		if(launchedGame != null) {
			System.err.println("Can't start 2 games.");
			return;
		}
		
		try {
			Game g = gameClass.getConstructor().newInstance();
			launchedGame = g;
			g.intern_start(settings);
		} catch(InstantiationException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		} catch(InvocationTargetException e) {
			e.printStackTrace();
		} catch(NoSuchMethodException e) {
			e.printStackTrace();
		} catch(SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public static Game getLaunchedGame() {
		return launchedGame;
	}
	
	// Non static
	private GameWindow window;
	private Renderer mainRenderer;
	private int fps = 0;
	private float maxFPS = 0;
	private boolean logFPS = false;
	
	private Level currentLevel = null;
	
	public Game() {
	}
	
	/**
	 * Called on the game initialisation.
	 */
	public abstract void init();
	
	private final void intern_start(LaunchSettings settings) {
		Configuration.DISABLE_CHECKS.set(true);
		
		// WARM UP
		GLFWManager.init();
		
		// Creates the windows and everything blablabla
		this.window = new GameWindow(settings);
		this.window.makeContextCurrent();
		this.window.setVSYNC(settings.isVSYNC());
		
		if(settings.getIcon() != null) this.window.setIcon(settings.getIcon());
		
		GlobalRenderer renderer = settings.getRenderer();
		if(renderer == GlobalRenderer.OPENGL) {
			this.window.contextVersion(Fury.OPENGL_API, OGLManager.VERSION_MAJOR, OGLManager.VERSION_MINOR); // OpenGL
																												// 3.3
			
			// Init OpenGL
			OGLManager.init(settings);
			
			// Creates the renderer
			RenderMode mode = settings.getRenderMode();
			if(mode == RenderMode.FORWARD_RENDERING) {
				// TODO: forward rendering
			} else if(mode == RenderMode.DEFERRED_RENDERING) this.mainRenderer = new OGLDeferredRenderer(settings.getVideoWidth(), settings.getVideoHeight());
			else if(mode == RenderMode.DEFERRED_LIGHTING) {
				// TODO: deferred lighting
			}
		}
		
		// Creates the UI Renderer (NanoVG)
		// NVGContext.create(settings.isUIAntialiasing());
		
		// Init and start the game
		this.init();
		this.window.show();
		this.triggerEvent("load");
		this.intern_loop();
		
		// COOL DOWN
		this.intern_dispose();
		this.window.dispose();
		this.mainRenderer.dispose();
		
		if(renderer == GlobalRenderer.OPENGL) OGLManager.dispose();
		
		GLFWManager.release();
	}
	
	/**
	 * Called on each game loop
	 * 
	 * @param delta
	 *            The delta time (in seconds)
	 */
	public abstract void update(float delta);
	
	private final void intern_loop() {
		double startTime = GLFWManager.getTimeSec();
		double lastDeltaUpdateTime = startTime;
		double lastFPSUpdateTime = startTime;
		double endTime = 0.0f;
		double elapsedTime = 0.0f;
		double delta = 0.0f;
		int frameAcc = 0;
		
		while(!this.window.shouldClose()) {
			startTime = GLFWManager.getTimeSec();
			delta = startTime - lastDeltaUpdateTime;
			lastDeltaUpdateTime = startTime;
			
			this.window.pollEvents();
			this.triggerEvent("update");
			this.update((float) delta);
			
			this.mainRenderer.setSize(this.window.getFrameWidth(), this.window.getFrameHeight());
			
			// render...
			if(this.currentLevel != null) {
				this.currentLevel.setDelta((float) delta);
				this.currentLevel.update();
				this.mainRenderer.render(this.currentLevel.renderPasses());
			}
			
			// update display
			this.window.swapBuffers();
			
			endTime = GLFWManager.getTimeSec();
			
			// FPS
			if(endTime - lastFPSUpdateTime >= 1.0) {
				lastFPSUpdateTime = endTime;
				this.fps = frameAcc;
				
				if(this.logFPS) System.out.println("FPS: " + this.fps + " | Frame time: " + elapsedTime + "s");
				
				frameAcc = -1; // -1 because we add +1 later in the loop
			}
			
			// FPS cap
			elapsedTime = endTime - startTime;
			if(this.maxFPS > 0.0f) if((1.0f / this.maxFPS) > elapsedTime) try {
				Thread.sleep((long) ((1.0f / this.maxFPS - endTime + startTime) * 1000d));
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			
			frameAcc++; // +1 frame
		}
	}
	
	/**
	 * Called on the game close
	 */
	public abstract void dispose();
	
	private final void intern_dispose() {
		this.triggerEvent("dispose");
		this.dispose();
		
		if(this.currentLevel != null) this.currentLevel.dispose();
		this.mainRenderer.dispose();
	}
	
	/**
	 * Requests to close the main window and thus close the game.
	 */
	public void quit() {
		this.window.setShouldClose(true);
	}
	
	public GameWindow getWindow() {
		return this.window;
	}
	
	public int getFPS() {
		return this.fps;
	}
	
	public boolean isLogFPS() {
		return this.logFPS;
	}
	
	public void setLogFPS(boolean logFPS) {
		this.logFPS = logFPS;
	}
	
	public float getMaxFPS() {
		return this.maxFPS;
	}
	
	public void setMaxFPS(float maxFPS) {
		this.maxFPS = maxFPS;
	}
	
	public TextureData takeScreenshot() {
		return this.mainRenderer.takeScreenshot();
	}
	
	public double getTime() {
		return GLFWManager.getTimeSec();
	}
	
	public Level getCurrentLevel() {
		return this.currentLevel;
	}
	
	public void loadLevel(Level currentLevel) {
		if(this.currentLevel != null) this.currentLevel.dispose();
		
		this.currentLevel = currentLevel;
		
		if(this.currentLevel != null) this.currentLevel.triggerEvent("load");
	}
}
