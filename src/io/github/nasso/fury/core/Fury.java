package io.github.nasso.fury.core;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL21.*;

import java.lang.reflect.Field;

public class Fury {
	// ------------ GLFW ------------
	
	public static final int OPENGL_API = GLFW_OPENGL_API;
	public static final int OPENGL_ES_API = GLFW_OPENGL_ES_API;
	
	/** The key or button was released. */
	public static final int RELEASE = GLFW_RELEASE;
	
	/** The key or button was pressed. */
	public static final int PRESS = GLFW_PRESS;
	
	/** The key was held down until it repeated. */
	public static final int REPEAT = GLFW_REPEAT;
	
	// Unknown
	public static final int KEY_UNKNOWN = GLFW_KEY_UNKNOWN;
	
	// Printable keys
	public static final int KEY_SPACE = GLFW_KEY_SPACE;
	public static final int KEY_APOSTROPHE = GLFW_KEY_APOSTROPHE;
	public static final int KEY_COMMA = GLFW_KEY_COMMA;
	public static final int KEY_MINUS = GLFW_KEY_MINUS;
	public static final int KEY_PERIOD = GLFW_KEY_PERIOD;
	public static final int KEY_SLASH = GLFW_KEY_SLASH;
	public static final int KEY_0 = GLFW_KEY_0;
	public static final int KEY_1 = GLFW_KEY_1;
	public static final int KEY_2 = GLFW_KEY_2;
	public static final int KEY_3 = GLFW_KEY_3;
	public static final int KEY_4 = GLFW_KEY_4;
	public static final int KEY_5 = GLFW_KEY_5;
	public static final int KEY_6 = GLFW_KEY_6;
	public static final int KEY_7 = GLFW_KEY_7;
	public static final int KEY_8 = GLFW_KEY_8;
	public static final int KEY_9 = GLFW_KEY_9;
	public static final int KEY_SEMICOLON = GLFW_KEY_SEMICOLON;
	public static final int KEY_EQUAL = GLFW_KEY_EQUAL;
	public static final int KEY_A = GLFW_KEY_A;
	public static final int KEY_B = GLFW_KEY_B;
	public static final int KEY_C = GLFW_KEY_C;
	public static final int KEY_D = GLFW_KEY_D;
	public static final int KEY_E = GLFW_KEY_E;
	public static final int KEY_F = GLFW_KEY_F;
	public static final int KEY_G = GLFW_KEY_G;
	public static final int KEY_H = GLFW_KEY_H;
	public static final int KEY_I = GLFW_KEY_I;
	public static final int KEY_J = GLFW_KEY_J;
	public static final int KEY_K = GLFW_KEY_K;
	public static final int KEY_L = GLFW_KEY_L;
	public static final int KEY_M = GLFW_KEY_M;
	public static final int KEY_N = GLFW_KEY_N;
	public static final int KEY_O = GLFW_KEY_O;
	public static final int KEY_P = GLFW_KEY_P;
	public static final int KEY_Q = GLFW_KEY_Q;
	public static final int KEY_R = GLFW_KEY_R;
	public static final int KEY_S = GLFW_KEY_S;
	public static final int KEY_T = GLFW_KEY_T;
	public static final int KEY_U = GLFW_KEY_U;
	public static final int KEY_V = GLFW_KEY_V;
	public static final int KEY_W = GLFW_KEY_W;
	public static final int KEY_X = GLFW_KEY_X;
	public static final int KEY_Y = GLFW_KEY_Y;
	public static final int KEY_Z = GLFW_KEY_Z;
	public static final int KEY_LEFT_BRACKET = GLFW_KEY_LEFT_BRACKET;
	public static final int KEY_BACKSLASH = GLFW_KEY_BACKSLASH;
	public static final int KEY_RIGHT_BRACKET = GLFW_KEY_RIGHT_BRACKET;
	public static final int KEY_GRAVE_ACCENT = GLFW_KEY_GRAVE_ACCENT;
	public static final int KEY_WORLD_1 = GLFW_KEY_WORLD_1;
	public static final int KEY_WORLD_2 = GLFW_KEY_WORLD_2;
	
	// Functions keys
	public static final int KEY_ESCAPE = GLFW_KEY_ESCAPE;
	public static final int KEY_ENTER = GLFW_KEY_ENTER;
	public static final int KEY_TAB = GLFW_KEY_TAB;
	public static final int KEY_BACKSPACE = GLFW_KEY_BACKSPACE;
	public static final int KEY_INSERT = GLFW_KEY_INSERT;
	public static final int KEY_DELETE = GLFW_KEY_DELETE;
	public static final int KEY_RIGHT = GLFW_KEY_RIGHT;
	public static final int KEY_LEFT = GLFW_KEY_LEFT;
	public static final int KEY_DOWN = GLFW_KEY_DOWN;
	public static final int KEY_UP = GLFW_KEY_UP;
	public static final int KEY_PAGE_UP = GLFW_KEY_PAGE_UP;
	public static final int KEY_PAGE_DOWN = GLFW_KEY_PAGE_DOWN;
	public static final int KEY_HOME = GLFW_KEY_HOME;
	public static final int KEY_END = GLFW_KEY_END;
	public static final int KEY_CAPS_LOCK = GLFW_KEY_CAPS_LOCK;
	public static final int KEY_SCROLL_LOCK = GLFW_KEY_SCROLL_LOCK;
	public static final int KEY_NUM_LOCK = GLFW_KEY_NUM_LOCK;
	public static final int KEY_PRINT_SCREEN = GLFW_KEY_PRINT_SCREEN;
	public static final int KEY_PAUSE = GLFW_KEY_PAUSE;
	public static final int KEY_F1 = GLFW_KEY_F1;
	public static final int KEY_F2 = GLFW_KEY_F2;
	public static final int KEY_F3 = GLFW_KEY_F3;
	public static final int KEY_F4 = GLFW_KEY_F4;
	public static final int KEY_F5 = GLFW_KEY_F5;
	public static final int KEY_F6 = GLFW_KEY_F6;
	public static final int KEY_F7 = GLFW_KEY_F7;
	public static final int KEY_F8 = GLFW_KEY_F8;
	public static final int KEY_F9 = GLFW_KEY_F9;
	public static final int KEY_F10 = GLFW_KEY_F10;
	public static final int KEY_F11 = GLFW_KEY_F11;
	public static final int KEY_F12 = GLFW_KEY_F12;
	public static final int KEY_F13 = GLFW_KEY_F13;
	public static final int KEY_F14 = GLFW_KEY_F14;
	public static final int KEY_F15 = GLFW_KEY_F15;
	public static final int KEY_F16 = GLFW_KEY_F16;
	public static final int KEY_F17 = GLFW_KEY_F17;
	public static final int KEY_F18 = GLFW_KEY_F18;
	public static final int KEY_F19 = GLFW_KEY_F19;
	public static final int KEY_F20 = GLFW_KEY_F20;
	public static final int KEY_F21 = GLFW_KEY_F21;
	public static final int KEY_F22 = GLFW_KEY_F22;
	public static final int KEY_F23 = GLFW_KEY_F23;
	public static final int KEY_F24 = GLFW_KEY_F24;
	public static final int KEY_F25 = GLFW_KEY_F25;
	public static final int KEY_KP_0 = GLFW_KEY_KP_0;
	public static final int KEY_KP_1 = GLFW_KEY_KP_1;
	public static final int KEY_KP_2 = GLFW_KEY_KP_2;
	public static final int KEY_KP_3 = GLFW_KEY_KP_3;
	public static final int KEY_KP_4 = GLFW_KEY_KP_4;
	public static final int KEY_KP_5 = GLFW_KEY_KP_5;
	public static final int KEY_KP_6 = GLFW_KEY_KP_6;
	public static final int KEY_KP_7 = GLFW_KEY_KP_7;
	public static final int KEY_KP_8 = GLFW_KEY_KP_8;
	public static final int KEY_KP_9 = GLFW_KEY_KP_9;
	public static final int KEY_KP_DECIMAL = GLFW_KEY_KP_DECIMAL;
	public static final int KEY_KP_DIVIDE = GLFW_KEY_KP_DIVIDE;
	public static final int KEY_KP_MULTIPLY = GLFW_KEY_KP_MULTIPLY;
	public static final int KEY_KP_SUBTRACT = GLFW_KEY_KP_SUBTRACT;
	public static final int KEY_KP_ADD = GLFW_KEY_KP_ADD;
	public static final int KEY_KP_ENTER = GLFW_KEY_KP_ENTER;
	public static final int KEY_KP_EQUAL = GLFW_KEY_KP_EQUAL;
	public static final int KEY_LEFT_SHIFT = GLFW_KEY_LEFT_SHIFT;
	public static final int KEY_LEFT_CONTROL = GLFW_KEY_LEFT_CONTROL;
	public static final int KEY_LEFT_ALT = GLFW_KEY_LEFT_ALT;
	public static final int KEY_LEFT_SUPER = GLFW_KEY_LEFT_SUPER;
	public static final int KEY_RIGHT_SHIFT = GLFW_KEY_RIGHT_SHIFT;
	public static final int KEY_RIGHT_CONTROL = GLFW_KEY_RIGHT_CONTROL;
	public static final int KEY_RIGHT_ALT = GLFW_KEY_RIGHT_ALT;
	public static final int KEY_RIGHT_SUPER = GLFW_KEY_RIGHT_SUPER;
	public static final int KEY_MENU = GLFW_KEY_MENU;
	
	// Mouse
	public static final int MOUSE_BUTTON_1 = GLFW_MOUSE_BUTTON_1;
	public static final int MOUSE_BUTTON_2 = GLFW_MOUSE_BUTTON_2;
	public static final int MOUSE_BUTTON_3 = GLFW_MOUSE_BUTTON_2;
	public static final int MOUSE_BUTTON_4 = GLFW_MOUSE_BUTTON_3;
	public static final int MOUSE_BUTTON_5 = GLFW_MOUSE_BUTTON_4;
	public static final int MOUSE_BUTTON_6 = GLFW_MOUSE_BUTTON_5;
	public static final int MOUSE_BUTTON_7 = GLFW_MOUSE_BUTTON_6;
	public static final int MOUSE_BUTTON_8 = GLFW_MOUSE_BUTTON_7;
	public static final int MOUSE_BUTTON_LAST = GLFW_MOUSE_BUTTON_LAST;
	public static final int MOUSE_BUTTON_LEFT = GLFW_MOUSE_BUTTON_LEFT;
	public static final int MOUSE_BUTTON_RIGHT = GLFW_MOUSE_BUTTON_RIGHT;
	public static final int MOUSE_BUTTON_MIDDLE = GLFW_MOUSE_BUTTON_MIDDLE;
	
	// Joystick
	public static final int JOYSTICK_1 = GLFW_JOYSTICK_1;
	public static final int JOYSTICK_2 = GLFW_JOYSTICK_2;
	public static final int JOYSTICK_3 = GLFW_JOYSTICK_3;
	public static final int JOYSTICK_4 = GLFW_JOYSTICK_4;
	public static final int JOYSTICK_5 = GLFW_JOYSTICK_5;
	public static final int JOYSTICK_6 = GLFW_JOYSTICK_6;
	public static final int JOYSTICK_7 = GLFW_JOYSTICK_7;
	public static final int JOYSTICK_8 = GLFW_JOYSTICK_8;
	public static final int JOYSTICK_9 = GLFW_JOYSTICK_9;
	public static final int JOYSTICK_10 = GLFW_JOYSTICK_10;
	public static final int JOYSTICK_11 = GLFW_JOYSTICK_11;
	public static final int JOYSTICK_12 = GLFW_JOYSTICK_12;
	public static final int JOYSTICK_13 = GLFW_JOYSTICK_13;
	public static final int JOYSTICK_14 = GLFW_JOYSTICK_14;
	public static final int JOYSTICK_15 = GLFW_JOYSTICK_15;
	public static final int JOYSTICK_16 = GLFW_JOYSTICK_16;
	
	// Cursor
	public static final int CURSOR_DISABLED = GLFW_CURSOR_DISABLED;
	public static final int CURSOR_HIDDEN = GLFW_CURSOR_HIDDEN;
	public static final int CURSOR_NORMAL = GLFW_CURSOR_NORMAL;
	
	// ------------ RENDERING ------------
	public static final int getOpenGLConst(int fury) {
		int opengl = 0;
		
		if(fury == Fury.REPEAT) opengl = GL_REPEAT;
		else opengl = fury; // Fury constants are the OpenGL ones
		
		return opengl;
	}
	
	public static final int NEARSET = GL_NEAREST;
	public static final int LINEAR = GL_LINEAR;
	
	public static final int NEARSET_MIPMAP_NEARSET = GL_NEAREST_MIPMAP_NEAREST;
	public static final int LINEAR_MIPMAP_NEARSET = GL_LINEAR_MIPMAP_NEAREST;
	public static final int NEARSET_MIPMAP_LINEAR = GL_NEAREST_MIPMAP_LINEAR;
	public static final int LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR;
	
	public static final int CLAMP = GL_CLAMP;
	// public static final int REPEAT = GL_REPEAT; Already defined for GLFW
	public static final int CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE;
	
	public static final int COLOR_INDEX = GL_COLOR_INDEX;
	public static final int STENCIL_INDEX = GL_STENCIL_INDEX;
	public static final int DEPTH_COMPONENT = GL_DEPTH_COMPONENT;
	public static final int RED = GL_RED;
	public static final int GREEN = GL_GREEN;
	public static final int BLUE = GL_BLUE;
	public static final int ALPHA = GL_ALPHA;
	public static final int RGB = GL_RGB;
	public static final int RGBA = GL_RGBA;
	public static final int LUMINANCE = GL_LUMINANCE;
	public static final int LUMINANCE_ALPHA = GL_LUMINANCE_ALPHA;
	public static final int SRGB = GL_SRGB;
	public static final int SRGB_ALPHA = GL_SRGB_ALPHA;
	
	public static final int POINT = GL_POINT;
	public static final int LINE = GL_LINE;
	public static final int FILL = GL_FILL;
	
	public static final int POINTS = GL_POINTS;
	public static final int LINES = GL_LINES;
	public static final int LINE_LOOP = GL_LINE_LOOP;
	public static final int TRIANGLES = GL_TRIANGLES;
	public static final int TRIANGLES_STRIP = GL_TRIANGLE_STRIP;
	public static final int TRIANGLES_FAN = GL_TRIANGLE_FAN;
	
	public static final int BYTE = GL_BYTE;
	public static final int UNSIGNED_BYTE = GL_UNSIGNED_BYTE;
	public static final int SHORT = GL_SHORT;
	public static final int UNSIGNED_SHORT = GL_UNSIGNED_SHORT;
	public static final int INT = GL_INT;
	public static final int UNSIGNED_INT = GL_UNSIGNED_INT;
	public static final int FLOAT = GL_FLOAT;
	public static final int BYTES_2 = GL_2_BYTES;
	public static final int BYTES_3 = GL_3_BYTES;
	public static final int BYTES_4 = GL_4_BYTES;
	public static final int DOUBLE = GL_DOUBLE;
	
	public static final int NONE = GL_NONE;
	public static final int FRONT = GL_FRONT;
	public static final int BACK = GL_BACK;
	public static final int FRONT_AND_BACK = GL_FRONT_AND_BACK;
	
	public static final int LIGHT_TYPE_POINT = 0;
	public static final int LIGHT_TYPE_SUN = 1;
	public static final int LIGHT_TYPE_SPOT = 2;
	public static final int LIGHT_TYPE_AMBIENT = 3;
	public static final int LIGHT_TYPE_HEMISPHERE = 4;
	
	public static final int SHADOW_BUFFER_TYPE_SIMPLE = 0;
	public static final int SHADOW_BUFFER_TYPE_POISSON_SAMPLED = 1;
	public static final int SHADOW_BUFFER_TYPE_STRATIFIED_POISSON_SAMPLED = 2;
	public static final int SHADOW_BUFFER_TYPE_ROTATED_POISSON_SAMPLED = 3;
	
	public static final int FOG_TYPE_LINEAR = 0;
	public static final int FOG_TYPE_EXP2 = 1;
	
	public static final int SKY_TYPE_CUBE_TEXTURE = 0;
	public static final int SKY_TYPE_PROCEDURAL = 1;
	
	public static int valueOf(String what) {
		try {
			Field f = Fury.class.getField(what);
			if(f != null) return f.getInt(null);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch(NoSuchFieldException e) {
			e.printStackTrace();
		} catch(SecurityException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
}
