package io.github.nasso.fury.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FuryUtils {
	private FuryUtils() {
	}
	
	public static byte clamp(byte x, byte min, byte max) {
		if(min == max) return min;
		else if(min > max) {
			byte temp = min;
			min = max;
			max = temp;
		}
		
		return (byte) Math.max(Math.min(x, max), min);
	}
	
	public static short clamp(short x, short min, short max) {
		if(min == max) return min;
		else if(min > max) {
			short temp = min;
			min = max;
			max = temp;
		}
		
		return (short) Math.max(Math.min(x, max), min);
	}
	
	public static int clamp(int x, int min, int max) {
		if(min == max) return min;
		else if(min > max) {
			int temp = min;
			min = max;
			max = temp;
		}
		
		return Math.max(Math.min(x, max), min);
	}
	
	public static float clamp(float x, float min, float max) {
		if(min == max) return min;
		else if(min > max) {
			float temp = min;
			min = max;
			max = temp;
		}
		
		return Math.max(Math.min(x, max), min);
	}
	
	public static double clamp(double x, double min, double max) {
		if(min == max) return min;
		else if(min > max) {
			double temp = min;
			min = max;
			max = temp;
		}
		
		return Math.max(Math.min(x, max), min);
	}
	
	public static long clamp(long x, long min, long max) {
		if(min == max) return min;
		else if(min > max) {
			long temp = min;
			min = max;
			max = temp;
		}
		
		return Math.max(Math.min(x, max), min);
	}
	
	public static byte lerp(byte a, byte b, byte x) {
		return (byte) (a + x * (b - a));
	}
	
	public static short lerp(short a, short b, float x) {
		return (short) (a + x * (b - a));
	}
	
	public static int lerp(int a, int b, float x) {
		return (int) (a + x * (b - a));
	}
	
	public static float lerp(float a, float b, float x) {
		return a + x * (b - a);
	}
	
	public static double lerp(double a, double b, float x) {
		return a + x * (b - a);
	}
	
	public static long lerp(long a, long b, float x) {
		return (long) (a + x * (b - a));
	}
	
	public static String readFile(String filePath, boolean inJar) {
		if(inJar) {
			InputStream ressource = FuryUtils.class.getClassLoader().getResourceAsStream(filePath);
			if(ressource == null) {
				System.err.println("Can't find ressource: " + filePath);
				
				return null;
			}
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(ressource));
			
			String line;
			String lines = "";
			
			try {
				while((line = reader.readLine()) != null)
					lines += line + "\n";
			} catch(IOException e) {
				e.printStackTrace();
			}
			
			return lines;
		} else {
			Path path = Paths.get(filePath);
			
			if(path == null || !Files.exists(path) || Files.isDirectory(path)) return null;
			
			String str = "";
			
			try {
				List<String> lines = Files.readAllLines(path);
				
				for(String l : lines)
					str += l + "\n";
			} catch(IOException e) {
				e.printStackTrace();
			}
			
			return str;
		}
	}
	
	public static String readFile(File file, boolean inJar) {
		return readFile(file.getAbsolutePath(), inJar);
	}
	
	public static String join(Object[] o, String c) {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < o.length - 1; i++) {
			builder.append(o[i]);
			builder.append(c);
		}
		
		builder.append(o[o.length - 1]);
		
		return builder.toString();
	}
	
	public static boolean check4int(int a, int... xs) {
		for(int i = 0, l = xs.length; i < l; i++)
			if(xs[i] == a) return true;
		
		return false;
	}
}
