package io.github.nasso.fury.core;

import java.util.HashMap;
import java.util.Map;

public class Timer {
	private static Map<String, Double> timers = new HashMap<String, Double>();
	
	public static void startTimer(String name) {
		timers.put(name, GLFWManager.getTimeSec());
	}
	
	public static double getTimeSec(String timer) {
		if(!timers.containsKey(timer)) return -1;
		
		return GLFWManager.getTimeSec() - timers.get(timer);
	}
	
	public static double getTimeMillisec(String timer) {
		if(!timers.containsKey(timer)) return -1;
		
		return (GLFWManager.getTimeSec() - timers.get(timer)) * 1000.0;
	}
	
	public static double printTimeSec(String timer) {
		double timeSec = getTimeSec(timer);
		
		System.out.println("Timer \"" + timer + "\" value: " + timeSec + "s");
		
		return timeSec;
	}
	
	public static double printTimeMillis(String timer) {
		double timeSec = getTimeMillisec(timer);
		
		System.out.println("Timer \"" + timer + "\" value: " + timeSec + "ms");
		
		return timeSec;
	}
}
