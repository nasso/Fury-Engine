package io.github.nasso.fury.core;

import java.util.HashMap;
import java.util.Map;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class PrimitiveProperties {
	private Map<String, Boolean> booleans = new HashMap<String, Boolean>();
	private Map<String, Integer> integers = new HashMap<String, Integer>();
	private Map<String, Float> floats = new HashMap<String, Float>();
	
	private Map<String, String> strings = new HashMap<String, String>();
	private Map<String, Vector2f> vec2fs = new HashMap<String, Vector2f>();
	private Map<String, Vector3f> vec3fs = new HashMap<String, Vector3f>();
	private Map<String, Vector4f> vec4fs = new HashMap<String, Vector4f>();
	private Map<String, Matrix3f> mat3fs = new HashMap<String, Matrix3f>();
	private Map<String, Matrix4f> mat4fs = new HashMap<String, Matrix4f>();
	
	public void setBool(String name, boolean value) {
		this.booleans.put(name, value);
	}
	
	public void setInt(String name, int value) {
		this.integers.put(name, value);
	}
	
	public void setFloat(String name, float value) {
		this.floats.put(name, value);
	}
	
	public void setString(String name, String value) {
		this.strings.put(name, value);
	}
	
	public void setVector2f(String name, Vector2f value) {
		this.vec2fs.put(name, value);
	}
	
	public void setVector3f(String name, Vector3f value) {
		this.vec3fs.put(name, value);
	}
	
	public void setVector4f(String name, Vector4f value) {
		this.vec4fs.put(name, value);
	}
	
	public void setMatrix3f(String name, Matrix3f value) {
		this.mat3fs.put(name, value);
	}
	
	public void setMatrix4f(String name, Matrix4f value) {
		this.mat4fs.put(name, value);
	}
	
	public boolean getBool(String name) {
		return this.booleans.get(name);
	}
	
	public int getInt(String name) {
		return this.integers.get(name);
	}
	
	public float getFloat(String name) {
		return this.floats.get(name);
	}
	
	public String getString(String name) {
		return this.strings.get(name);
	}
	
	public Vector2f getVector2f(String name) {
		return this.vec2fs.get(name);
	}
	
	public Vector3f getVector3f(String name) {
		return this.vec3fs.get(name);
	}
	
	public Vector4f getVector4f(String name) {
		return this.vec4fs.get(name);
	}
	
	public Matrix3f getMatrix3f(String name) {
		return this.mat3fs.get(name);
	}
	
	public Matrix4f getMatrix4f(String name) {
		return this.mat4fs.get(name);
	}
	
	public boolean getBool(String name, boolean defaultValue) {
		return this.booleans.getOrDefault(name, defaultValue);
	}
	
	public int getInt(String name, int defaultValue) {
		return this.integers.getOrDefault(name, defaultValue);
	}
	
	public float getFloat(String name, float defaultValue) {
		return this.floats.getOrDefault(name, defaultValue);
	}
	
	public String getString(String name, String defaultValue) {
		return this.strings.getOrDefault(name, defaultValue);
	}
	
	public Vector2f getVector2f(String name, Vector2f defaultValue) {
		return this.vec2fs.getOrDefault(name, defaultValue);
	}
	
	public Vector3f getVector3f(String name, Vector3f defaultValue) {
		return this.vec3fs.getOrDefault(name, defaultValue);
	}
	
	public Vector4f getVector4f(String name, Vector4f defaultValue) {
		return this.vec4fs.getOrDefault(name, defaultValue);
	}
	
	public Matrix3f getMatrix3f(String name, Matrix3f defaultValue) {
		return this.mat3fs.getOrDefault(name, defaultValue);
	}
	
	public Matrix4f getMatrix4f(String name, Matrix4f defaultValue) {
		return this.mat4fs.getOrDefault(name, defaultValue);
	}
}
