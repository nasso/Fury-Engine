package io.github.nasso.test;

import io.github.nasso.fury.core.Game;
import io.github.nasso.fury.core.LaunchSettings;
import io.github.nasso.fury.core.LaunchSettings.VideoMode;
import io.github.nasso.fury.data.FuryXMLLevelLoader;

import java.io.File;

public class FuryDynamicTester extends Game {
	private FuryXMLLevelLoader xmlLoader = new FuryXMLLevelLoader();
	
	public void init() {
		this.loadLevel(this.xmlLoader.loadLevel(new File("res/levels/test.lvl")));
	}
	
	public void update(float delta) {
		
	}
	
	public void dispose() {
		
	}
	
	public static void main(String[] argv) {
		Game.launch(new LaunchSettings().videoMode(VideoMode.WINDOWED).videoSize(1280, 720));
	}
}
