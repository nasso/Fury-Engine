package io.github.nasso.test;

import static org.lwjgl.assimp.Assimp.*;

import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.AINode;
import org.lwjgl.assimp.AIScene;

public class BugReportingTest {
	public static void main(String[] argv) {
		AIScene aiSce = aiImportFile("D:\\Programmation\\Java\\Workspace\\FuryEngine\\res\\models\\AK47u\\AK74u.fbx", aiProcess_Triangulate);
		
		if(aiSce == null) System.err.println("Fail! " + aiGetErrorString());
		
		AINode root = aiSce.mRootNode();
		PointerBuffer children = root.mChildren();
		while(children.hasRemaining()) {
			AINode node = AINode.create(children.get());
			
			System.out.print("Found node: ");
			System.out.println(node.mName().dataString());
		}
	}
}
