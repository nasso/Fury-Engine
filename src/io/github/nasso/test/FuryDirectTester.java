package io.github.nasso.test;

import io.github.nasso.fury.core.Fury;
import io.github.nasso.fury.core.FuryUtils;
import io.github.nasso.fury.core.Game;
import io.github.nasso.fury.core.GameWindow;
import io.github.nasso.fury.core.LaunchSettings;
import io.github.nasso.fury.core.LaunchSettings.VideoMode;
import io.github.nasso.fury.data.ModelLoader;
import io.github.nasso.fury.data.TextureData;
import io.github.nasso.fury.data.TextureIO;
import io.github.nasso.fury.extra.BoxBufferGeometry;
import io.github.nasso.fury.extra.FPSCamera;
import io.github.nasso.fury.graphics.BufferGeometry;
import io.github.nasso.fury.graphics.Material;
import io.github.nasso.fury.graphics.RenderPass;
import io.github.nasso.fury.graphics.Texture2D;
import io.github.nasso.fury.graphics.fx.FXAA;
import io.github.nasso.fury.graphics.fx.Gamma;
import io.github.nasso.fury.graphics.fx.Vignette;
import io.github.nasso.fury.level.Fog;
import io.github.nasso.fury.level.Level;
import io.github.nasso.fury.level.Light;
import io.github.nasso.fury.level.Mesh;
import io.github.nasso.fury.level.Scene;
import io.github.nasso.fury.level.Sky;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

public class FuryDirectTester extends Game {
	private Level lvl = new Level();
	
	private BufferGeometry terrainGeom = this.lvl.create(new BoxBufferGeometry(100, 100, 1));
	private Material terrainMat = this.lvl.createMaterial();
	private Mesh terrain = new Mesh(this.terrainGeom, this.terrainMat);
	
	private Mesh suzanne;
	
	private BufferGeometry cubeGeom = this.lvl.create(new BoxBufferGeometry(1, 1, 1));
	private Material cubeMat = this.lvl.createMaterial();
	private Mesh cube = new Mesh(this.cubeGeom, this.cubeMat);
	
	private Texture2D crateTextureD;
	private Texture2D crateTextureS;
	private Texture2D crateTextureN;
	private Texture2D basicTextureD;
	private Texture2D basicTextureN;
	
	private Scene sce = this.lvl.createScene();
	private Light hemiLight = new Light(Fury.LIGHT_TYPE_HEMISPHERE, 0.25f);
	private Light sun = new Light(Fury.LIGHT_TYPE_SUN, 1.0f);
	
	private FPSCamera cam = new FPSCamera(70, 1, 0.1f, 10000.0f);
	
	// TODO: All Javadoc
	public void init() {
		// Log FPS (Debug)
		this.setLogFPS(true);
		
		try {
			this.suzanne = ModelLoader.loadMesh("res/models/suzanne.obj", 0);
			
			this.basicTextureD = this.lvl.create(TextureIO.loadTexture2D("res/textures/diffuse3.jpg", true));
			this.basicTextureN = this.lvl.create(TextureIO.loadTexture2D("res/textures/normal3.jpg"));
			this.crateTextureD = this.lvl.create(TextureIO.loadTexture2D("res/textures/crateD.jpg", true));
			this.crateTextureS = this.lvl.create(TextureIO.loadTexture2D("res/textures/crateS.jpg"));
			this.crateTextureN = this.lvl.create(TextureIO.loadTexture2D("res/textures/crateN.png"));
			
			this.crateTextureD.setAnisotropy(16);
			this.crateTextureS.setAnisotropy(16);
			this.crateTextureN.setAnisotropy(16);
			this.basicTextureD.setAnisotropy(16);
			this.basicTextureN.setAnisotropy(16);
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		this.terrainMat.setDiffuseTexture(this.basicTextureD);
		this.terrainMat.setNormalTexture(this.basicTextureN);
		this.terrainMat.setUVScaling(100);
		this.terrainMat.setSpecularIntensity(0.4f);
		this.terrainMat.setShininess(128f);
		this.terrain.setPosition(0f, 0f, -0.5f);
		this.sce.addChildren(this.terrain);
		
		this.cubeMat.setDiffuseTexture(this.crateTextureD);
		this.cubeMat.setNormalTexture(this.crateTextureN);
		this.cubeMat.setSpecularTexture(this.crateTextureS);
		this.cube.setPosition(-5f, -4f, 0.5f);
		this.sce.addChildren(this.cube);
		
		this.suzanne.getMaterial().setSpecularIntensity(0.5f);
		this.suzanne.getMaterial().setShininess(256f);
		this.suzanne.setPosition(5, 5, 1.2f);
		this.sce.addChildren(this.suzanne);
		
		/*
		 * List<Mesh> suzanneList = new ArrayList<Mesh>(); for(int i = -20; i <
		 * 5; i++){ for(int j = -5; j < 5; j++){ Mesh suzanneDup =
		 * suzanne.clone();
		 * 
		 * suzanneDup.translate((i + 6) * 2, (j + 6) * 3, 0f);
		 * 
		 * suzanneList.add(suzanneDup); } } sce.addChildren(suzanneList);
		 */
		
		// Lights
		Sky sky = this.sce.getSky(); // Just get the default sky. No need to
										// instanciate
		sky.addSun(this.sun);
		
		this.sce.setFog(new Fog());
		
		this.sun.setCastShadow(true);
		this.sun.setRotation(-40f, 0f, 30f);
		this.sun.setColor(1f, 1f, 0.75f);
		this.sce.addChildren(this.sun);
		
		this.hemiLight.setColor(sky.getTopColor());
		this.hemiLight.setGroundColor(this.sun.getColor());
		this.sce.addChildren(this.hemiLight);
		
		this.cam.setFreeLookfly(true);
		this.sce.addChildren(this.cam);
		
		// TODO: Modulable mode: Graphics Module, Audio Module, Physics Module,
		// etc... module
		// Then render the scene usually
		RenderPass renderPass = new RenderPass(this.sce, this.cam);
		renderPass.addPostEffects(new Gamma(), new Vignette(), new FXAA());
		this.lvl.renderPasses().add(renderPass);
		
		this.loadLevel(this.lvl);
	}
	
	public void update(float delta) {
		GameWindow window = this.getWindow();
		
		// When ESC is released, quit the game
		if(window.isReleased(Fury.KEY_ESCAPE)) {
			this.quit();
			return;
		}
		
		// Get focus on cursor or not (toggle)
		if(window.isPressed(Fury.MOUSE_BUTTON_LEFT)) window.setCursorMode(window.getCursorMode() == Fury.CURSOR_NORMAL ? Fury.CURSOR_DISABLED : Fury.CURSOR_NORMAL);
		
		if(window.isPressed(Fury.KEY_F11)) window.toggleFullscreen();
		
		if(window.isPressed(Fury.KEY_F12)) {
			String when = "";
			Calendar cal = Calendar.getInstance();
			when += cal.get(Calendar.DAY_OF_MONTH) + "-";
			when += (cal.get(Calendar.MONTH) + 1) + "-";
			when += cal.get(Calendar.YEAR) + " ";
			when += cal.get(Calendar.HOUR_OF_DAY) + "-";
			when += cal.get(Calendar.MINUTE) + "-";
			when += cal.get(Calendar.SECOND);
			
			TextureData data = this.takeScreenshot();
			TextureIO.writeTexture("screenshots/screenshot" + when + ".png", "png", data);
		}
		
		// Update the camera (controls) if the cursor is disabled
		this.cam.setBlockTick(window.getCursorMode() != Fury.CURSOR_DISABLED);
		this.cam.setSpeed(FuryUtils.clamp(this.cam.getSpeed() + window.getScrollRelY(), 0.1f, 400f));
		this.cam.setRatio(window.getFrameWidth(), window.getFrameHeight());
		
		this.suzanne.rotate(delta * 8f, delta * 16f, 0);
	}
	
	public void dispose() {
	}
	
	public static void main(String[] args) {
		// TODO: Let the user choose the resolution (kind of launcher)
		Game.launch(new LaunchSettings().videoSize(1920, 1080).videoMode(VideoMode.MAXIMIZED));
	}
}
