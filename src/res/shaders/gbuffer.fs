#version 330 core

#define FREE 0.0

struct Material {
	sampler2D diffuseTexture;
	sampler2D normalTexture;
	sampler2D specularTexture;
	
	vec3 diffuseColor;
	
	float uvScaling;
	float normalMapIntensity;
	float shininess;
	// float reflectivity;
	float specularIntensity;
	float alphaThreshold;
	
	bool hasDiffuseTexture;
	bool hasNormalTexture;
	bool hasSpecularTexture;
	bool faceSideNormalCorrection;
	bool opaque;
};

uniform Material material;
uniform bool receiveShadows;

in vec3 pass_position_view;
in vec3 pass_normal_view;
in vec2 pass_uv;
in vec2 pass_uv2;

in float flogz;

layout(location = 0) out vec4 out_diffuse;
layout(location = 1) out vec4 out_position;
layout(location = 2) out vec4 out_normal;

void main(){
	// Diffuse
	out_diffuse = vec4(material.diffuseColor, material.shininess);
	if(material.hasDiffuseTexture){
		vec4 difTexValue = texture(material.diffuseTexture, pass_uv * material.uvScaling);
		
		if(!material.opaque && difTexValue.a < material.alphaThreshold){
			discard;
		}
		
		out_diffuse.rgb *= difTexValue.rgb;
	}
	
	// Specular and normal
	out_normal = vec4(pass_normal_view, material.specularIntensity);
	if(material.hasSpecularTexture){
		out_normal.a *= texture(material.specularTexture, pass_uv * material.uvScaling).x;
	}
	
	if(material.faceSideNormalCorrection && !gl_FrontFacing){
		out_normal.xyz = -out_normal.xyz;
	}
	
	if(material.hasNormalTexture){
		// Per-Pixel Tangent Space Normal Mapping
		// http://hacksoflife.blogspot.ch/2009/11/per-pixel-tangent-space-normal-mapping.html
		vec3 q0 = dFdx(pass_position_view.xyz);
		vec3 q1 = dFdy(pass_position_view.xyz);
		vec2 st0 = dFdx(pass_uv.st);
		vec2 st1 = dFdy(pass_uv.st);
		
		if(st0.t / st0.s != st1.t / st1.s){ // Degenerate (Some UVs are the same)
			vec3 mapNorm = texture(material.normalTexture, pass_uv * material.uvScaling).xyz * 2.0 - 1.0;
			mapNorm.xy = material.normalMapIntensity * mapNorm.xy;
			
			out_normal.xyz = normalize(mat3(normalize(q0 * st1.t - q1 * st0.t), normalize(-q0 * st1.s + q1 * st0.s), out_normal.xyz) * mapNorm);
		}
	}
	
	out_position = vec4(FREE, FREE, FREE, 1.0);
	if(!receiveShadows){
		out_position.a = -1.0;
	}
}
