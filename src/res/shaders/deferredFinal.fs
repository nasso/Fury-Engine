#version 330 core

#define FOG_TYPE_LINEAR 0
#define FOG_TYPE_EXP2 1

#define LOG2 1.442695

struct Fog {
	bool enabled;
	
	int type;
	
	float near;
	float far;
	float density;
	
	vec3 color;
};

uniform sampler2D color;
uniform sampler2D depth;
uniform mat4 projViewInv;
uniform vec3 camPos;
uniform Fog fog;

in vec2 pass_quad_uv;

out vec4 out_color;

void main(){
	if(fog.enabled) {
		float depth = texture(depth, pass_quad_uv).x * 2.0 - 1.0;
		vec2 ndcCoord = pass_quad_uv * 2.0 - 1.0;
		vec4 worldSpacePos = projViewInv * vec4(ndcCoord, depth, 1.0);
		worldSpacePos /= worldSpacePos.w;
		
		float dist = distance(camPos, worldSpacePos.xyz);
		
		float fogAmount = 0.0;
		if(fog.type == FOG_TYPE_LINEAR) {
			fogAmount = clamp(smoothstep(fog.near, fog.far, dist), 0.0, 1.0);
		} else if(fog.type == FOG_TYPE_EXP2) {
			fogAmount = 1.0 - clamp(exp2(-fog.density * fog.density * dist * dist * LOG2), 0.0, 1.0);
		}
		
		vec4 sampled_color = texture(color, pass_quad_uv);
		// out_color = vec4(mix(sampled_color.rgb, fog.color, fogAmount), 1.0);
		out_color = vec4(mix(sampled_color.rgb, fog.color, fogAmount), sampled_color.a);
		return;
	}
	
	out_color = texture(color, pass_quad_uv);
}
