#version 330 core

uniform mat4 projViewModel;

in vec3 position_model;

void main(){
	gl_Position = projViewModel * vec4(position_model, 1.0);
}
