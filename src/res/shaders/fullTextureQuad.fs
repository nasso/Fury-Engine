#version 330 core

uniform sampler2D color;
uniform sampler2D depth;
uniform bool copyDepth;

in vec2 pass_quad_uv;

out vec4 out_color;

void main(){
	if(copyDepth){
		gl_FragDepth = texture(depth, pass_quad_uv).x;
	}

	out_color = texture(color, pass_quad_uv);
}
