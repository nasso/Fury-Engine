#version 330 core

uniform mat4 projViewModel;

in vec3 position_model;
in vec2 uv;

out vec2 pass_uv;

void main(){
	pass_uv = uv;
	
	gl_Position = projViewModel * vec4(position_model, 1.0);
}
