#version 330 core

#define TYPE_CUBE 0
#define TYPE_PROC 1
#define MAX_SUN_COUNT 8

struct Light {
	vec3 color;
	vec3 direction;
	
	float energy;
	float innerEdge;
	float outerEdge;
	float brightness;
};

struct Sky {
	Light sun[MAX_SUN_COUNT];
	int sunCount;
	
	samplerCube cubeTexture;
	
	int type;
	
	vec3 up;
	vec3 bottomColor;
	vec3 topColor;
	
	float cosPower;
};

uniform Sky sky;

in vec3 pass_position_model;

out vec4 out_color;

// For compatibility on sh*tty drivers
vec3 getSkyColorCube(){
	return texture(sky.cubeTexture, pass_position_model).rgb;
}

vec3 getSkyColorProc(){
	vec3 frag = normalize(pass_position_model);
	float cosine = dot(frag, sky.up) * 0.5 + 0.5;
	
	vec3 skyColor = mix(sky.bottomColor, sky.topColor, pow(cosine, sky.cosPower));
	
	for(int i = 0, l = min(sky.sunCount, MAX_SUN_COUNT); i < l; i++){
		Light sun = sky.sun[i];
		
		float angleToSun = degrees(acos(dot(frag, -sun.direction)));
		
		float sunness = (1.0 - smoothstep(
			sun.innerEdge,
			sun.outerEdge,
			angleToSun)) * sun.energy * sun.brightness;
		
		skyColor = mix(skyColor, sun.color, sunness);
	}
	
	return skyColor;
}

void main(){
	vec3 skyColor = vec3(1.0);
	
	if(sky.type == TYPE_CUBE){
		skyColor = getSkyColorCube();
	} else if(sky.type == TYPE_PROC) {
		skyColor = getSkyColorProc();
	}
	
	out_color = vec4(skyColor, 1.0);
}
