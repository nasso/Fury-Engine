#version 330 core

uniform mat4 projView;
uniform float scale;

in vec3 position_model;

out vec3 pass_position_model;

void main(){
	pass_position_model = position_model;
	
	gl_Position = projView * vec4(position_model * scale, 1.0);
}
