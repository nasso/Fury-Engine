#version 330 core

#define BLURRED_TEXTURE_COUNT 3

uniform sampler2D blurredTexture[BLURRED_TEXTURE_COUNT];
uniform sampler2D color;
uniform float strength;

in vec2 pass_quad_uv;

out vec4 out_color;

void main(){
	vec4 total = vec4(0.0);
	
	for(int i = 0; i < BLURRED_TEXTURE_COUNT; i++){
		total += texture(blurredTexture[i], pass_quad_uv);
	}
	
	out_color = texture(color, pass_quad_uv).rgba + total.rgba * strength;
}
