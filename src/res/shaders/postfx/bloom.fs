#version 330 core

uniform sampler2D color;
uniform int textureLength;
uniform float threshold;
uniform float smoothness;
uniform float size;
uniform float kernel[11] = float[] (0.022657, 0.046108, 0.080127, 0.118904, 0.150677, 0.163053, 0.150677, 0.118904, 0.080127, 0.046108, 0.022657);

in vec2 pass_quad_uv;

out vec4 out_color;

void main(){
	const vec3 color_weights = vec3(0.2126, 0.7152, 0.0722);
	
	float minThresh = (threshold - smoothness * 0.5);
	float maxThresh = (threshold + smoothness * 0.5);
	
	vec2 pixelSize = vec2((1.0 / textureLength) * size);
	vec4 blurred = vec4(0.0);
	
	// The first bloom pass is an horizontal gaussian blur
	pixelSize.y = 0;
	
	for(int i = -5; i <= 5; i++){
		vec4 color = vec4(texture(color, pass_quad_uv + pixelSize * i).xyz, 1.0);
		float intensity = dot(color.rgb, color_weights);
		
		if(intensity < minThresh){
			color = vec4(0.0);
		}else if(intensity < maxThresh){
			color = color * smoothstep(minThresh, maxThresh, intensity);
		}
		
		blurred += color * kernel[i+5];
	}
	
	out_color = blurred.xyzw;
}
