#version 330 core

#define TYPE_REINHARD 0
#define TYPE_EXPOSURE 1
#define TYPE_AUTO_EXPOSURE 2

uniform sampler2D color;

uniform int type;
uniform float exposure;
uniform float adjustSpeed;

in vec2 pass_quad_uv;

out vec4 out_color;

void main(){
	if(type == TYPE_REINHARD){
		vec3 hdrColor = texture(color, pass_quad_uv).xyz;
		hdrColor /= hdrColor + 1.0;
		
		out_color = vec4(hdrColor.xyz, 1.0).xyzw;
		
		return;
	}else if(type == TYPE_EXPOSURE){
		out_color = vec4((vec3(1.0) - exp(-(texture(color, pass_quad_uv).xyz) * exposure)).xyz, 1.0).xyzw;
		
		return;
	}else if(type == TYPE_AUTO_EXPOSURE){
		
	}
	
	out_color = vec4(texture(color, pass_quad_uv).xyz, 1.0).xyzw;
}
