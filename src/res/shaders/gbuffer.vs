#version 330 core

uniform mat4 projViewModel;
uniform mat4 viewModel;
uniform mat3 normalView;

in vec3 normal_model;
in vec3 position_model;
in vec2 uv;
in vec2 uv2;

out vec3 pass_position_view;
out vec3 pass_normal_view;
out vec2 pass_uv;
out vec2 pass_uv2;

out float flogz;

void main(){
	pass_position_view = (viewModel * vec4(position_model, 1.0)).xyz;
	pass_normal_view = normalView * normal_model;
	pass_uv = uv;
	pass_uv2 = uv2;
	
	gl_Position = projViewModel * vec4(position_model, 1.0);
}
