#version 330 core

struct Material {
	sampler2D diffuseTexture;
	
	float uvScaling;
	float alphaThreshold;
	
	bool hasDiffuseTexture;
	bool opaque;
};

uniform Material material;

in vec2 pass_uv;

layout(location = 0) out float out_depth;

void main(){
	if(material.hasDiffuseTexture && !material.opaque){
		float alpha = texture(material.diffuseTexture, pass_uv * material.uvScaling).a;
		
		if(alpha < material.alphaThreshold){
			discard;
		}
	}
	
	out_depth = gl_FragCoord.z;
}
