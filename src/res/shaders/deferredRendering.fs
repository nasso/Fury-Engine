#version 330 core

#define PI 3.14159265358979

#define SHADOW_BUFFER_SIMPLE 0
#define SHADOW_BUFFER_POISSON_SAMPLED 1
#define SHADOW_BUFFER_STRATIFIED_POISSON_SAMPLED 2
#define SHADOW_BUFFER_ROTATED_POISSON_SAMPLED 3

#define MAX_CASCADED_SHADOWS 6

#define LIGHT_NONE -1
#define LIGHT_POINT 0
#define LIGHT_SUN 1
#define LIGHT_SPOT 2
#define LIGHT_AMBIENT 3
#define LIGHT_HEMISPHERE 4

#define MAX_POISSON_SAMPLES 16

const vec2 poissonDiskTen[10] = vec2[](
	vec2(-0.2283337, 0.09127928),
	vec2(0.2786196, 0.0529025),
	vec2(-0.4395757, 0.5550615),
	vec2(-0.4702352, -0.3828329),
	vec2(0.5072337, -0.4498647),
	vec2(0.5466216, 0.55743),
	vec2(-0.9210131, 0.09994493),
	vec2(0.8807642, 0.07761531),
	vec2(0.02402369, -0.9250473),
	vec2(-0.06769172, 0.9591652)
);

const vec2 poissonDiskSixteen[16] = vec2[](
	vec2(0.06523339, 0.6297334),
	vec2(-0.5013002, 0.1069309),
	vec2(0.7131211, 0.5130429),
	vec2(0.2735687, -0.09346303),
	vec2(0.02398307, 0.2313415),
	vec2(-0.47057, 0.66932),
	vec2(0.4848332, 0.843534),
	vec2(0.7611852, 0.003426897),
	vec2(-0.1162362, -0.44236),
	vec2(-0.8766336, 0.3523771),
	vec2(0.54353, -0.6489286),
	vec2(-0.5519209, -0.4512444),
	vec2(-0.09840571, -0.9288315),
	vec2(0.8834155, -0.4335088),
	vec2(-0.172233, 0.9711213),
	vec2(-0.9382731, -0.189225)
);

uniform sampler2DShadow depthMap[MAX_CASCADED_SHADOWS];
	
struct ShadowCascade {
	mat4 shadowMat;
	
	float poissonSpread;
	float bias;
	float near;
	float far;
	
	int poissonSamples;
	int bufferType;
	
	bool normalCull;
};

struct ShadowMap {
	ShadowCascade cascades[MAX_CASCADED_SHADOWS];
	
	float cascadeDistanceFade;
	float interCascadeFade;
	float cascadeMaxDistance;
	float cascadeDistribExp;
	
	int cascadeCount;
};

struct Light {
	ShadowMap shadowMap;

	vec3 color;
	vec3 groundColor;
	vec3 position;
	vec3 direction;
	
	int type;
	
	float energy;
	float dist;
	float coneSize;
	float coneBlend;
	
	bool blinnPhong;
	bool specular;
	bool diffuse;
	bool shadows;
};

uniform mat4 viewInv;
uniform mat4 projInv;
uniform sampler2D gbuffer_diffuse;
uniform sampler2D gbuffer_position;
uniform sampler2D gbuffer_normal;
uniform sampler2D gbuffer_depth;
uniform Light light;
uniform bool lightingEnabled;

in vec2 pass_quad_uv;
out vec4 out_final_color;

// --
struct GBuffer {
	vec3 position_world;
	
	vec3 diffuse;
	vec3 position;
	vec3 normal;
	
	float shininess;
	float alpha;
	float specularIntensity;
	
	bool receiveShadows;
} gbuffer;

float random(vec3 seed, int i){
	return fract(sin(dot(vec4(seed,i), vec4(12.9898,78.233,45.164,94.673))) * 43758.5453);
}

float randomAngle(vec3 seed, int i){
	return random(seed, i) * PI * 2;
}

float sampleShadowMapPCF(ShadowCascade casc, sampler2DShadow depthMap, vec3 coord){
	return 1.0 - texture(depthMap, vec3(coord.xy, coord.z - casc.bias));
}

float calcShadowness(ShadowCascade cascade, sampler2DShadow cascadeDepthMap){
	if(!light.shadows || !gbuffer.receiveShadows){
		return 0.0;
	}
	
	float poissonSpread = cascade.poissonSpread;
	
	int poissonSamples = cascade.poissonSamples;
	int type = cascade.bufferType;
	
	bool normalCull = cascade.normalCull;
	
	vec4 mapCoord = cascade.shadowMat * vec4(gbuffer.position_world, 1.0);
	mapCoord /= mapCoord.w;
	mapCoord.xyz = mapCoord.xyz * 0.5 + 0.5;
	
	if(mapCoord.x < 0.0 || mapCoord.x > 1.0 ||
		mapCoord.y < 0.0 || mapCoord.y > 1.0 ||
		mapCoord.z < 0.0 || mapCoord.z > 1.0){
		return 0.0;
	}
	
	if(normalCull && dot(gbuffer.normal, -light.direction) < 0){
		return 1.0;
	}
	
	if(type == SHADOW_BUFFER_SIMPLE){
		return sampleShadowMapPCF(cascade, cascadeDepthMap, mapCoord.xyz);
	}else if(type == SHADOW_BUFFER_POISSON_SAMPLED){
		int psamples = min(poissonSamples, MAX_POISSON_SAMPLES);
		
		if(psamples <= 0 || poissonSpread == 0){
			return 0.0;
		}
		
		float shadownessAccumulator = 0;
		if(psamples > 10){
			// Ten version
			for(int i = 0; i < psamples; i++){
				shadownessAccumulator += sampleShadowMapPCF(
					cascade,
					cascadeDepthMap,
					vec3(mapCoord.xy + poissonDiskTen[i] / poissonSpread, mapCoord.z));
			}
		}else{
			// Sixteen version
			for(int i = 0; i < psamples; i++){
				shadownessAccumulator += sampleShadowMapPCF(
					cascade,
					cascadeDepthMap,
					vec3(mapCoord.xy + poissonDiskSixteen[i] / poissonSpread, mapCoord.z));
			}
		}
		
		return shadownessAccumulator / psamples;
	}else if(type == SHADOW_BUFFER_STRATIFIED_POISSON_SAMPLED){
		int psamples = min(poissonSamples, MAX_POISSON_SAMPLES);
		
		if(psamples <= 0 || poissonSpread == 0){
			return 0.0;
		}
		
		float shadownessAccumulator = 0;
		if(psamples > 10){
			// Ten version
			for(int i = 0; i < psamples; i++){
				// The gbuffer.position_world is multiplied by 5000.0 to get a better precision
				int index = int(psamples * random(floor(gbuffer.position_world * 5000.0), i)) % psamples;
				
				shadownessAccumulator += sampleShadowMapPCF(
					cascade,
					cascadeDepthMap,
					vec3(mapCoord.xy + poissonDiskTen[index] / poissonSpread, mapCoord.z));
			}
		}else{
			// Sixteen version
			for(int i = 0; i < psamples; i++){
				// The gbuffer.position_world is multiplied by 5000.0 to get a better precision
				int index = int(psamples * random(floor(gbuffer.position_world * 5000.0), i)) % psamples;
				
				shadownessAccumulator += sampleShadowMapPCF(
					cascade,
					cascadeDepthMap,
					vec3(mapCoord.xy + poissonDiskSixteen[index] / poissonSpread, mapCoord.z));
			}
		}
		
		return shadownessAccumulator / psamples;
	}else if(type == SHADOW_BUFFER_ROTATED_POISSON_SAMPLED){
		int psamples = min(poissonSamples, MAX_POISSON_SAMPLES);
		
		if(psamples <= 0 || poissonSpread == 0){
			return 0.0;
		}
		
		// The gbuffer.position_world is multiplied by 5000.0 to get a better precision
		float angle = randomAngle(floor(gbuffer.position_world * 5000.0), psamples);
		float c = cos(angle);
		float s = sin(angle);
		
		float shadownessAccumulator = 0;
		
		if(psamples > 10){
			// Ten version
			for(int i = 0; i < psamples; i++){
				vec2 rotatedOffset = vec2(poissonDiskTen[i].x * c + poissonDiskTen[i].y * s, poissonDiskTen[i].x * -s + poissonDiskTen[i].y * c);
				
				shadownessAccumulator += sampleShadowMapPCF(
					cascade,
					cascadeDepthMap,
					vec3(mapCoord.xy + rotatedOffset / poissonSpread, mapCoord.z));
			}
		}else{
			// Sixteen version
			for(int i = 0; i < psamples; i++){
				vec2 rotatedOffset = vec2(poissonDiskSixteen[i].x * c + poissonDiskSixteen[i].y * s, poissonDiskSixteen[i].x * -s + poissonDiskSixteen[i].y * c);
				
				shadownessAccumulator += sampleShadowMapPCF(
					cascade,
					cascadeDepthMap,
					vec3(mapCoord.xy + rotatedOffset / poissonSpread, mapCoord.z));
			}
		}
		
		return shadownessAccumulator / psamples;
	}
	
	return 0.0;
}

vec3 applyPointLight(){
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);
	float falloff = 0.0;
	
	vec3 surfaceToLight = normalize(light.position - gbuffer.position);
	falloff = 1.0 - pow(clamp(length(light.position - gbuffer.position) / light.dist, 0.0, 1.0), 2);
	
	float cosTheta = clamp(dot(surfaceToLight, gbuffer.normal), 0.0, 1.0);
	
	if(light.diffuse){
		diffuse = gbuffer.diffuse * light.color * cosTheta;
	}
	
	if(light.specular){
		vec3 surfaceToCamera = normalize(-gbuffer.position); // since we are in cam space
		float specDot = 0.0;
		
		if(light.blinnPhong){
			vec3 halfwayVector = normalize(surfaceToLight + surfaceToCamera);
			specDot = dot(gbuffer.normal, halfwayVector);
		}else{
			vec3 reflectedVector = reflect(-surfaceToLight, gbuffer.normal);
			specDot = dot(surfaceToCamera, reflectedVector);
		}
		
		float specularCoef = pow(max(specDot, 0.0), gbuffer.shininess) * cosTheta;
		specular = light.color * specularCoef * gbuffer.specularIntensity;
	}
	
	return (diffuse + specular) * falloff * light.energy;
}

vec3 applySunLight(){
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);
	
	vec3 surfaceToLight = normalize(-light.direction);
	float cosTheta = clamp(dot(surfaceToLight, gbuffer.normal), 0.0, 1.0);
	
	if(light.diffuse){
		diffuse = gbuffer.diffuse * light.color * cosTheta;
	}
	
	if(light.specular){
		vec3 surfaceToCamera = normalize(-gbuffer.position); // since we are in cam space
		float specDot = 0.0;
		
		if(light.blinnPhong){
			vec3 halfwayVector = normalize(surfaceToLight + surfaceToCamera);
			specDot = dot(gbuffer.normal, halfwayVector);
		}else{
			vec3 reflectedVector = reflect(-surfaceToLight, gbuffer.normal);
			specDot = dot(surfaceToCamera, reflectedVector);
		}
		
		float specularCoef = pow(max(specDot, 0.0), gbuffer.shininess) * cosTheta;
		specular = light.color * specularCoef * gbuffer.specularIntensity;
	}
	
	float shadowness = 0.0;
	
	
	if(light.shadows){
		float currentDepth = -gbuffer.position.z;
		
		ShadowMap sm = light.shadowMap;
		if(currentDepth > 0.0 && currentDepth < sm.cascadeMaxDistance){
			int i = int(pow(clamp(currentDepth / sm.cascadeMaxDistance, 0.0, 1.0), 1.0 / sm.cascadeDistribExp) * sm.cascadeCount);
			
			ShadowCascade casc = sm.cascades[i];
			if(casc.far > currentDepth){
				shadowness = calcShadowness(casc, depthMap[i]) * (1.0 - smoothstep(sm.cascadeMaxDistance - sm.cascadeDistanceFade, sm.cascadeMaxDistance, currentDepth));
			}
			
			if(i < sm.cascadeCount - 1){
				shadowness = mix(shadowness, calcShadowness(light.shadowMap.cascades[i + 1], depthMap[i + 1]), smoothstep(casc.far - sm.interCascadeFade, casc.far, currentDepth));
			}	
		}
	}
	
	return (diffuse + specular) * light.energy * (1.0 - shadowness);
}

vec3 applySpotLight(){
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);
	float falloff = 0.0;
	
	vec3 surfaceToLight = normalize(light.position - gbuffer.position);
	falloff = 1.0 - pow(clamp(length(light.position - gbuffer.position) / light.dist, 0.0, 1.0), 2);
	
	// Apply the cone shape to falloff
	float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(light.direction))));
	if(lightToSurfaceAngle < light.coneSize){ // In the cone shape
		if(lightToSurfaceAngle > light.coneBlend){ // The only thing we need here is blending
			falloff *= 1.0 - (lightToSurfaceAngle - light.coneBlend) / (light.coneSize - light.coneBlend);
		}
	}else{ // Outside of the cone shape -> WILL BE IN THE DARK
		return diffuse;
	}
	
	float cosTheta = clamp(dot(surfaceToLight, gbuffer.normal), 0.0, 1.0);
	
	if(light.diffuse){
		diffuse = gbuffer.diffuse * light.color * cosTheta;
	}
	
	if(light.specular){
		vec3 surfaceToCamera = normalize(-gbuffer.position); // since we are in cam space
		float specDot = 0.0;
		
		if(light.blinnPhong){
			vec3 halfwayVector = normalize(surfaceToLight + surfaceToCamera);
			specDot = dot(gbuffer.normal, halfwayVector);
		}else{
			vec3 reflectedVector = reflect(-surfaceToLight, gbuffer.normal);
			specDot = dot(surfaceToCamera, reflectedVector);
		}
		
		float specularCoef = pow(max(specDot, 0.0), gbuffer.shininess) * cosTheta;
		specular = light.color * specularCoef * gbuffer.specularIntensity;
	}
	
	float shadowness = 0.0;
	if(light.shadows){
		shadowness = calcShadowness(light.shadowMap.cascades[0], depthMap[0]);
	}
	
	return (diffuse + specular) * falloff * light.energy * (1.0 - shadowness);
}

vec3 applyAmbientLight(){
	return (light.color * gbuffer.diffuse) * light.energy;
}

vec3 applyHemisphereLight(){
	return (mix(light.groundColor, light.color, dot(gbuffer.normal, -light.direction) * 0.5 + 0.5) * gbuffer.diffuse) * light.energy;
}

// TODO: Lights...
void main(){
	if(light.type == LIGHT_NONE) {
		if(lightingEnabled) {
			out_final_color = vec4(0.0, 0.0, 0.0, abs(texture(gbuffer_position, pass_quad_uv).w));
		} else {
			out_final_color = vec4(texture(gbuffer_diffuse, pass_quad_uv).rgb, abs(texture(gbuffer_position, pass_quad_uv).w));
		}
		
		return;
	}
	
	vec4 gbuffer_diffuse_value = texture(gbuffer_diffuse, pass_quad_uv);
	vec4 gbuffer_position_value = texture(gbuffer_position, pass_quad_uv);
	vec4 gbuffer_normal_value = texture(gbuffer_normal, pass_quad_uv);
	
	// Computes the view space position
	float depth = texture(gbuffer_depth, pass_quad_uv).x * 2.0 - 1.0;
	vec2 ndcCoord = pass_quad_uv * 2.0 - 1.0;
	vec4 viewSpacePos = projInv * vec4(ndcCoord, depth, 1.0);
	viewSpacePos /= viewSpacePos.w;
	
	gbuffer.diffuse = abs(gbuffer_diffuse_value.rgb);
	gbuffer.position = viewSpacePos.xyz;
	gbuffer.normal = normalize(gbuffer_normal_value.xyz);
	
	gbuffer.shininess = gbuffer_diffuse_value.w;
	gbuffer.alpha = abs(gbuffer_position_value.w);
	gbuffer.specularIntensity = gbuffer_normal_value.w;
	gbuffer.receiveShadows = gbuffer_position_value.w > 0.0;
	
	gbuffer.position_world = (viewInv * vec4(gbuffer.position, 1.0)).xyz;
	
	switch(light.type) {
		case LIGHT_POINT:
			out_final_color = vec4(applyPointLight(), gbuffer.alpha);
			break;
		case LIGHT_SUN:
			out_final_color = vec4(applySunLight(), gbuffer.alpha);
			break;
		case LIGHT_SPOT:
			out_final_color = vec4(applySpotLight(), gbuffer.alpha);
			break;
		case LIGHT_AMBIENT:
			out_final_color = vec4(applyAmbientLight(), gbuffer.alpha);
			break;
		case LIGHT_HEMISPHERE:
			out_final_color = vec4(applyHemisphereLight(), gbuffer.alpha);
			break;
		default:
			out_final_color = vec4(0.0, 0.0, 0.0, 0.0);
	}
}
