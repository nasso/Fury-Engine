var NH_ = {
	createClass: function(params) {
		var newClass = params.construct;
		
		if(params.parent) {
			newClass.prototype = Object.create(params.parent.prototype);
		}
		
		if(params.body) {
			for(var x in params.body) {
				newClass.prototype[x] = params.body[x];
			}
		}
		
		if(params.staticBody) {
			for(var x in params.staticBody) {
				if(x === "prototype") continue;
				
				newClass[x] = params.staticBody[x];
			}
		}
		
		newClass.prototype.constructor = params.construct;
		
		return newClass;
	},
	
	JH: {
		System: Java.type('java.lang.System'),
		Files: Java.type('io.github.nasso.fury.scripting.nashorn.FilesModule'),
		TextureIO: Java.type('io.github.nasso.fury.data.TextureIO')
	}
};

var console = {
	log: function(o) {
		NH_.JH.System.out.println("[i] " + o);
	},
	
	warn: function(o) {
		NH_.JH.System.out.println("[!] " + o);
	},
	
	err: function(o) {
		NH_.JH.System.err.println("[X] " + o);
	}
};

var Color = Java.type('io.github.nasso.fury.maths.Color');

var Vector2 = Java.type('org.joml.Vector2f');
var Vector3 = Java.type('org.joml.Vector3f');
var Vector4 = Java.type('org.joml.Vector4f');

var Matrxi3 = Java.type('org.joml.Matrix3f');
var Matrix4 = Java.type('org.joml.Matrix4f');

var Quaternion = Java.type('org.joml.Quaternionf');

var FuryMaths = {
	clamp: function(x, min, max) {
		if(min === max) {
			return min;
		} else if(min > max) {
			min = min ^ max;
			max = min ^ max;
			min = min ^ max;
		}
		
		return Math.max(Math.min(x, max), min);
	},
	
	lerp: function(a, b, x) {
		return a + x * (b - a);
	}
};

var Files = {
	exists: function(path) {
		return NH_.JH.Files.exists(path);
	}
};

var TextureData = NH_.createClass({
	construct: function(no) {
		no = no || new TextureData.NativeClass();
		
		this.no = no;
		
		Object.defineProperty(this, 'width', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWidth();
			},
			
			set: function(v) {
				no.setWidth(v);
			}
		});
		
		Object.defineProperty(this, 'height', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getHeight();
			},
			
			set: function(v) {
				no.setHeight(v);
			}
		});
		
		Object.defineProperty(this, 'data', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getData();
			},
			
			set: function(v) {
				no.setData(v);
			}
		});
		
		Object.defineProperty(this, 'bpp', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getBytesPerPixel();
			},
			
			set: function(v) {
				no.setBytesPerPixel(v);
			}
		});
	},
	
	body: {
		rotate: function(r) {
			this.no.rotate(r);
		},
		
		subData: function(sx, sy, w, h) {
			return new TextureData(this.no.subData(sx, sy, w, h));
		},
		
		clone: function() {
			return new TextureData(this.no.clone());
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.data.TextureData')
	}
});

var CubeTextureLayout = NH_.createClass({
	construct: function(no) {
		no = no || new CubeTextureLayout.NativeClass();
		
		this.no = no;
		
		Object.defineProperty(this, 'xdivs', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getXDivs();
			},
			
			set: function(v) {
				no.setXDivs(v);
			}
		});
		
		Object.defineProperty(this, 'ydivs', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getYDivs();
			},
			
			set: function(v) {
				no.setYDivs(v);
			}
		});
		
		var coords = ['X', 'Y', 'Z'];
		for(var i = 0; i < 3; i++) {
			for(var j = 0; j < 2; j++) {
				Object.defineProperty(this, 'pos'+coords[i]+'Case'+coords[j], {
					enumerable: false,
					configurable: false,
					
					get: function() {
						return no['getPos'+coords[i]+'Case'+coords[j]]();
					},
					
					set: function(v) {
						no['setPos'+coords[i]+'Case'+coords[j]](v);
					}
				});
				
				Object.defineProperty(this, 'neg'+coords[i]+'Case'+coords[j], {
					enumerable: false,
					configurable: false,
					
					get: function() {
						return no['getNeg'+coords[i]+'Case'+coords[j]]();
					},
					
					set: function(v) {
						no['setNeg'+coords[i]+'Case'+coords[j]](v);
					}
				});
			}
			
			Object.defineProperty(this, 'pos'+coords[i]+'Case'+coords[j], {
				enumerable: false,
				configurable: false,
				
				get: function() {
					return no['getPos'+coords[i]+'Rot'+coords[j]]();
				},
				
				set: function(v) {
					no['setPos'+coords[i]+'Rot'+coords[j]](v);
				}
			});
			
			Object.defineProperty(this, 'neg'+coords[i]+'Case'+coords[j], {
				enumerable: false,
				configurable: false,
				
				get: function() {
					return no['getNeg'+coords[i]+'Rot'+coords[j]]();
				},
				
				set: function(v) {
					no['setNeg'+coords[i]+'Rot'+coords[j]](v);
				}
			});
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.data.CubeTextureLayout')
	}
});

CubeTextureLayout.PRESET_B = new CubeTextureLayout(CubeTextureLayout.NativeClass.PRESET_B);

var Texture2D = NH_.createClass({ parent: Observable,
	construct: function(no) {
		Observable.call(this, no);
		
		Object.defineProperty(this, 'version', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getVersion();
			}
		});
		
		Object.defineProperty(this, 'id', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getID();
			},
			
			set: function(v) {
				this.no.setID(v);
			}
		});
		
		Object.defineProperty(this, 'name', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getName();
			},
			
			set: function(v) {
				this.no.setName(v);
			}
		});
		
		Object.defineProperty(this, 'width', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWidth();
			},
			
			set: function(v) {
				no.setWidth(v);
			}
		});
		
		Object.defineProperty(this, 'height', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getHeight();
			},
			
			set: function(v) {
				no.setHeight(v);
			}
		});
		
		Object.defineProperty(this, 'magFilter', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMagFilter();
			},
			
			set: function(v) {
				no.setMagFilter(v);
			}
		});
		
		Object.defineProperty(this, 'minFilter', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMinFilter();
			},
			
			set: function(v) {
				no.setMinFilter(v);
			}
		});
		
		Object.defineProperty(this, 'wrapS', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWrapS();
			},
			
			set: function(v) {
				no.setWrapS(v);
			}
		});
		
		Object.defineProperty(this, 'wrapT', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWrapT();
			},
			
			set: function(v) {
				no.setWrapT(v);
			}
		});
		
		Object.defineProperty(this, 'internalFormat', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getInternalFormat();
			},
			
			set: function(v) {
				no.setInternalFormat(v);
			}
		});
		
		Object.defineProperty(this, 'format', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFormat();
			},
			
			set: function(v) {
				no.setFormat(v);
			}
		});
		
		Object.defineProperty(this, 'type', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getType();
			},
			
			set: function(v) {
				no.setType(v);
			}
		});
		
		Object.defineProperty(this, 'anisotropy', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getAnisotropy();
			},
			
			set: function(v) {
				no.setAnisotropy(v);
			}
		});
		
		Object.defineProperty(this, 'data', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getData();
			},
			
			set: function(v) {
				no.setData(v);
			}
		});
	},
	
	body: {
		needUpdate: function() {
			this.no.needUpdate();
		},
		
		dispose: function() {
			this.no.dispose();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.graphics.Texture2D')
	}
});

var CubeTexture = NH_.createClass({ parent: Observable,
	construct: function(no) {
		Observable.call(this, no);
		
		Object.defineProperty(this, 'version', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getVersion();
			}
		});
		
		Object.defineProperty(this, 'id', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getID();
			},
			
			set: function(v) {
				this.no.setID(v);
			}
		});
		
		Object.defineProperty(this, 'name', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getName();
			},
			
			set: function(v) {
				this.no.setName(v);
			}
		});
		
		Object.defineProperty(this, 'width', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWidth();
			},
			
			set: function(v) {
				no.setWidth(v);
			}
		});
		
		Object.defineProperty(this, 'height', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getHeight();
			},
			
			set: function(v) {
				no.setHeight(v);
			}
		});
		
		Object.defineProperty(this, 'magFilter', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMagFilter();
			},
			
			set: function(v) {
				no.setMagFilter(v);
			}
		});
		
		Object.defineProperty(this, 'minFilter', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMinFilter();
			},
			
			set: function(v) {
				no.setMinFilter(v);
			}
		});
		
		Object.defineProperty(this, 'wrapR', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWrapR();
			},
			
			set: function(v) {
				no.setWrapR(v);
			}
		});
		
		Object.defineProperty(this, 'wrapS', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWrapS();
			},
			
			set: function(v) {
				no.setWrapS(v);
			}
		});
		
		Object.defineProperty(this, 'wrapT', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWrapT();
			},
			
			set: function(v) {
				no.setWrapT(v);
			}
		});
		
		Object.defineProperty(this, 'internalFormat', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getInternalFormat();
			},
			
			set: function(v) {
				no.setInternalFormat(v);
			}
		});
		
		Object.defineProperty(this, 'format', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFormat();
			},
			
			set: function(v) {
				no.setFormat(v);
			}
		});
		
		Object.defineProperty(this, 'type', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getType();
			},
			
			set: function(v) {
				no.setType(v);
			}
		});
		
		Object.defineProperty(this, 'anisotropy', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getAnisotropy();
			},
			
			set: function(v) {
				no.setAnisotropy(v);
			}
		});
		
		Object.defineProperty(this, 'posXData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPosXData();
			},
			
			set: function(v) {
				no.setPosXData(v);
			}
		});
		
		Object.defineProperty(this, 'posYData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPosYData();
			},
			
			set: function(v) {
				no.setPosYData(v);
			}
		});
		
		Object.defineProperty(this, 'posZData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPosZData();
			},
			
			set: function(v) {
				no.setPosZData(v);
			}
		});
		
		Object.defineProperty(this, 'negXData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getNegXData();
			},
			
			set: function(v) {
				no.setNegXData(v);
			}
		});
		
		Object.defineProperty(this, 'negYData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getNegYData();
			},
			
			set: function(v) {
				no.setNegYData(v);
			}
		});
		
		Object.defineProperty(this, 'negZData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getNegZData();
			},
			
			set: function(v) {
				no.setNegZData(v);
			}
		});
	},
	
	body: {
		needUpdate: function() {
			this.no.needUpdate();
		},
		
		dispose: function() {
			this.no.dispose();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.graphics.CubeTexture')
	}
});

var TextureIO = {
	loadTextureData: function(path) {
		return new TextureData(NH_.JH.TextureIO.loadTextureData(path));
	},
	
	loadTexture2D: function(path, gammaCorrect) {
		if(gammaCorrect === undefined) {
			return new Texture2D(NH_.JH.TextureIO.loadTexture2D(path));
		}
		
		return new Texture2D(NH_.JH.TextureIO.loadTexture2D(path, gammaCorrect));
	},
	
	loadCubeTexture: function(path, layout) {
		return new CubeTexture(NH_.JH.TextureIO.loadCubeTexture(path, layout));
	},
	
	writeTexture: function(path, format, data) {
		NH_.JH.TextureIO.writeTexture(path, format, data.no);
	}
};

var Observable = NH_.createClass({
	construct: function(no) {
		no = no || new Observable.NativeClass();
		
		Object.defineProperty(this, 'no', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no;
			}
		});
	},
	
	body: {
		addEventListener: function(e, listener) {
			this.no.addEventListener(e, listener);
		},
		
		triggerEvent: function(e) {
			this.no.triggerEvent(e);
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.event.Observable')
	}
});

var GameWindow = NH_.createClass({ parent: Observable,
	construct: function(no) {
		no = no || new GameWindow.NativeClass();
		
		Observable.call(this, no);
		
		Object.defineProperty(this, 'cursorMode', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getCursorMode();
			},
			
			set: function(v) {
				no.setCursorMode(v);
			}
		});
		
		Object.defineProperty(this, 'shouldClose', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.shouldClose();
			},
			
			set: function(v) {
				no.setShouldClose(v);
			}
		});
		
		Object.defineProperty(this, 'visible', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isVisible();
			},
			
			set: function(v) {
				if(v) no.show();
				else no.hide();
			}
		});
		
		Object.defineProperty(this, 'title', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWindowTitle();
			},
			
			set: function(v) {
				no.setWindowTitle(v);
			}
		});
		
		Object.defineProperty(this, 'width', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getWidth();
			},
			
			set: function(v) {
				no.setWidth(v);
			}
		});
		
		Object.defineProperty(this, 'height', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getHeight();
			},
			
			set: function(v) {
				no.setHeight(v);
			}
		});
		
		Object.defineProperty(this, 'frameWidth', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFrameWidth();
			}
		});
		
		Object.defineProperty(this, 'frameHeight', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFrameHeight();
			}
		});
		
		Object.defineProperty(this, 'devicePixelRatio', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getDevicePixelRatio();
			}
		});
		
		Object.defineProperty(this, 'fullscreen', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isFullscreen();
			},
			
			set: function(v) {
				no.setFullscreen(v);
			}
		});
		
		Object.defineProperty(this, 'mouseX', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMouseX();
			}
		});
		
		Object.defineProperty(this, 'mouseY', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMouseY();
			}
		});
		
		Object.defineProperty(this, 'mouseRelX', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMouseRelX();
			}
		});
		
		Object.defineProperty(this, 'mouseRelY', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMouseRelY();
			}
		});
		
		Object.defineProperty(this, 'scrollRelX', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getScrollRelX();
			}
		});
		
		Object.defineProperty(this, 'scrollRelY', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getScrollRelY();
			}
		});
	},
	
	body: {
		isReleased: function(k) {
			return this.no.isReleased(k);
		},
		isPressed: function(k) {
			return this.no.isPressed(k);
		},
		isDown: function(k) {
			return this.no.isDown(k);
		},
		isButtonDown: function(k) {
			return this.no.isButtonDown(k);
		},
		
		vsync: function(v) {
			this.no.setVSYNC(v);
		},
		
		setWindowed: function() {
			this.no.setWindowed();
		},
		
		toggleFullscreen: function() {
			this.no.toggleFullscreen();
		},
		
		maximize: function() {
			this.no.maximize();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.core.GameWindow')
	}
});

var Game = NH_.createClass({ parent: Observable,
	construct: function(no) {
		Observable.call(this, no);

		Object.defineProperty(this, 'window', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getWindow());
			}
		});
		
		Object.defineProperty(this, 'fps', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFPS();
			}
		});
		
		Object.defineProperty(this, 'logFPS', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isLogFPS();
			},
			
			set: function(v) {
				no.setLogFPS(v);
			}
		});
		
		Object.defineProperty(this, 'maxFPS', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMaxFPS();
			},
			
			set: function(v) {
				no.setMaxFPS(v);
			}
		});
		
		Object.defineProperty(this, 'time', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getTime();
			}
		});
		
		Object.defineProperty(this, 'currentLevel', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getCurrentLevel();
			}
		});
	},
	
	body: {
		quit: function() {
			this.no.quit();
		},
		
		loadLevel: function(lvl) {
			this.no.loadLevel(lvl.no);
		},
		
		takeScreenshot: function() {
			return new TextureData(this.no.takeScreenshot());
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.core.Game')
	}
});

var Object3D = NH_.createClass({ parent: Observable,
	construct: function(no) {
		no = no || new Object3D.NativeClass();
		
		Observable.call(this, no);
		
		Object.defineProperty(this, 'id', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getID();
			},
			
			set: function(v) {
				this.no.setID(v);
			}
		});
		
		Object.defineProperty(this, 'name', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getName();
			},
			
			set: function(v) {
				this.no.setName(v);
			}
		});
		
		Object.defineProperty(this, 'parent', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getParent();
			},
			
			set: function(v) {
				this.no.setParent(v.no);
			}
		});
		
		Object.defineProperty(this, 'children', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				var a = [];
				
				var li = no.getChildren();
				for(var i = 0, l = li.size(); i < l; i++) {
					a.push(getObjInst(li.get(i)));
				}
				
				return a;
			}
		});
		
		Object.defineProperty(this, 'position', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPosition();
			},
			
			set: function(v) {
				this.no.setPosition(v);
			}
		});
		
		Object.defineProperty(this, 'scale', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getScale();
			},
			
			set: function(v) {
				this.no.setScale(v);
			}
		});
		
		Object.defineProperty(this, 'rotation', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getRotation();
			},
			
			set: function(v) {
				this.no.setRotation(v);
			}
		});
		
		Object.defineProperty(this, 'up', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getUp();
			},
			
			set: function(v) {
				this.no.setUp(v);
			}
		});
		
		Object.defineProperty(this, 'matrix', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMatrix();
			},
			
			set: function(v) {
				this.no.setMatrix(v);
			}
		});
		
		Object.defineProperty(this, 'matrixWorld', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMatrixWorld();
			}
		});
		
		Object.defineProperty(this, 'visible', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isVisible();
			},
			
			set: function(v) {
				this.no.setVisible(v);
			}
		});
		
		Object.defineProperty(this, 'receiveShadow', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isReceiveShadow();
			},
			
			set: function(v) {
				this.no.setReceiveShadow(v);
			}
		});
		
		Object.defineProperty(this, 'frustumCulled', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isFrustumCulled();
			},
			
			set: function(v) {
				this.no.setFrustumCulled(v);
			}
		});
		
		Object.defineProperty(this, 'matrixAutoUpdate', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isMatrixAutoUpdate();
			},
			
			set: function(v) {
				this.no.setMatrixAutoUpdate(v);
			}
		});
		
		Object.defineProperty(this, 'matrixWorldNeedUpdate', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isMatrixWorldNeedUpdate();
			},
			
			set: function(v) {
				this.no.setMatrixWorldNeedUpdate(v);
			}
		});
		
		Object.defineProperty(this, 'blockTick', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isBlockTick();
			},
			
			set: function(v) {
				this.no.setBlockTick(v);
			}
		});
		
		Object.defineProperty(this, 'userData', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getUserData();
			},
			
			set: function(v) {
				this.no.setUserData(v);
			}
		});
	},
	
	body: {
		applyMatrix: function(m) {
			this.no.applyMatrix(m);
		},
		
		translate: function(x, y, z) {
			this.no.translate(x, y, z);
		},
		
		rotate: function(x, y, z) {
			this.no.rotate(x, y, z);
		},
		
		scale: function(x, y, z) {
			this.no.scale(x, y, z);
		},
		
		updatePosRotScale: function() {
			this.no.updatePosRotScale();
		},
		
		localToWorldPos: function(v) {
			return this.no.localToWorldPos(v);
		},
		
		worldToLocalPos: function(v) {
			return this.no.worldToLocalPos(v);
		},
		
		localToWorldDir: function(v) {
			return this.no.localToWorldDir(v);
		},
		
		worldToLocalDir: function(v) {
			return this.no.worldToLocalDir(v);
		},
		
		localToWorld: function(v) {
			return this.no.localToWorld(v);
		},
		
		worldToLocal: function(v) {
			return this.no.worldToLocal(v);
		},
		
		lookAt: function(x, y, z) {
			this.no.lookAt(x, y, z);
		},
		
		updateMatrix: function() {
			this.no.updateMatrix();
		},
		
		updateMatrixWorld: function(b) {
			this.no.updateMatrixWorld(b);
		},
		
		clone: function() {
			return getObjInst(this.no.clone());
		},
		
		copy: function(o) {
			this.no.copy(o.no);
			return this;
		},
		
		toString: function() {
			return this.no.toString();
		},
		
		getChildByName: function(v) {
			return getObjInst(this.no.getChildByName(v));
		},
		
		getChild: function(v) {
			return getObjInst(this.no.getChild(v));
		},
		
		addChildren: function(v) {
			if(Array.isArray(v)) {
				for(var i = 0, l = v.length; i < l; i++) {
					this.no.addChildren(v[i].no);
				}
			} else {
				for(var i = 0, l = arguments.length; i < l; i++) {
					this.no.addChildren(arguments[i].no);
				}
			}
		},
		
		removeChildren: function(v) {
			if(Array.isArray(v)) {
				for(var i = 0, l = v.length; i < l; i++) {
					this.no.removeChildren(v[i].no);
				}
			} else {
				for(var i = 0, l = arguments.length; i < l; i++) {
					this.no.removeChildren(arguments[i].no);
				}
			}
		},
		
		hasChild: function(v) {
			return this.no.hasChild(v.no);
		},
		
		getChildById: function(v) {
			return getObjInst(this.no.getChildById(v));
		},
		
		giveAllChildren: function(v) {
			this.no.giveAllChildren(v.no);
		},
		
		triggerTick: function(v) {
			this.no.triggerTick(v);
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Object3D')
	}
});

var Camera = NH_.createClass({ parent: Object3D,
	construct: function(no) {
		no = no || new Camera.NativeClass();
		
		Object3D.call(this, no);
		
		Object.defineProperty(this, 'viewMatrix', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getViewMatrix();
			}
		});
		
		Object.defineProperty(this, 'projectionMatrix', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getProjectionMatrix();
			}
		});
		
		Object.defineProperty(this, 'projViewMatrix', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getProjViewMatrix();
			}
		});
	},
	
	body: {
		updateProjViewMatrix: function() {
			this.no.updateProjViewMatrix();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Camera')
	}
});

var NFClippedCamera = NH_.createClass({ parent: Camera,
	construct: function(no) {
		no = no || new NFClippedCamera.NativeClass();
		
		Camera.call(this, no);
		
		Object.defineProperty(this, 'near', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getNear();
			},
			
			set: function(v) {
				no.setNear(v);
			}
		});
		
		Object.defineProperty(this, 'far', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFar();
			},
			
			set: function(v) {
				no.setFar(v);
			}
		});
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.NFClippedCamera')
	}
});

var PerspectiveCamera = NH_.createClass({ parent: NFClippedCamera,
	construct: function(no) {
		no = no || new PerspectiveCamera.NativeClass();
		
		NFClippedCamera.call(this, no);
		
		Object.defineProperty(this, 'fov', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFOV();
			},
			
			set: function(v) {
				no.setFOV(v);
			}
		});
		
		Object.defineProperty(this, 'ratio', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getRatio();
			},
			
			set: function(v) {
				no.setRatio(v);
			}
		});
	},
	
	body: {
		updateProjectionMatrix: function() {
			this.no.updateProjectionMatrix();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.extra.PerspectiveCamera')
	}
});

var OrthographicCamera = NH_.createClass({ parent: NFClippedCamera,
	construct: function(no) {
		no = no || new OrthographicCamera.NativeClass();
		
		NFClippedCamera.call(this, no);
		
		Object.defineProperty(this, 'left', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getLeft();
			},
			
			set: function(v) {
				no.setLeft(v);
			}
		});
		
		Object.defineProperty(this, 'right', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getRight();
			},
			
			set: function(v) {
				no.setRight(v);
			}
		});
		
		Object.defineProperty(this, 'top', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getTop();
			},
			
			set: function(v) {
				no.setTop(v);
			}
		});
		
		Object.defineProperty(this, 'bottom', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getBottom();
			},
			
			set: function(v) {
				no.setBottom(v);
			}
		});
		
		Object.defineProperty(this, 'zoom', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getZoom();
			},
			
			set: function(v) {
				no.setZoom(v);
			}
		});
	},
	
	body: {
		updateProjectionMatrix: function() {
			this.no.updateProjectionMatrix();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.extra.OrthographicCamera')
	}
});

var FPSCamera = NH_.createClass({ parent: PerspectiveCamera,
	construct: function(no) {
		no = no || new FPSCamera.NativeClass();
		
		PerspectiveCamera.call(this, no);
		
		Object.defineProperty(this, 'orientation', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getOrientation();
			}
		});
		
		Object.defineProperty(this, 'freeLookFly', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isFreeLookfly();
			},
			
			set: function(v) {
				no.setFreeLookfly(v);
			}
		});
		
		Object.defineProperty(this, 'allowVerticalFly', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isAllowVerticalFly();
			},
			
			set: function(v) {
				no.setAllowVerticalFly(v);
			}
		});
		
		Object.defineProperty(this, 'phi', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPhi();
			},
			
			set: function(v) {
				no.setPhi(v);
			}
		});
		
		Object.defineProperty(this, 'theta', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getTheta();
			},
			
			set: function(v) {
				no.setTheta(v);
			}
		});
		
		Object.defineProperty(this, 'mouseSensitivity', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMouseSensitivity();
			},
			
			set: function(v) {
				no.setMouseSensitivity(v);
			}
		});
		
		Object.defineProperty(this, 'speed', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getSpeed();
			},
			
			set: function(v) {
				no.setSpeed(v);
			}
		});
		
		Object.defineProperty(this, 'runSpeedMultiplier', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getRunSpeedMultiplier();
			},
			
			set: function(v) {
				no.setRunSpeedMultiplier(v);
			}
		});
		
		Object.defineProperty(this, 'vibrationAmount', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getVibrationAmount();
			},
			
			set: function(v) {
				no.setVibrationAmount(v);
			}
		});
		
		Object.defineProperty(this, 'bodyHeight', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getBodyHeight();
			},
			
			set: function(v) {
				no.setBodyHeight(v);
			}
		});
		
		Object.defineProperty(this, 'actionForward', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionForward();
			},
			
			set: function(v) {
				no.setActionForward(v);
			}
		});
		
		Object.defineProperty(this, 'actionStrafeLeft', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionStrafeLeft();
			},
			
			set: function(v) {
				no.setActionStrafeLeft(v);
			}
		});
		
		Object.defineProperty(this, 'actionBackward', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionBackward();
			},
			
			set: function(v) {
				no.setActionBackward(v);
			}
		});
		
		Object.defineProperty(this, 'actionStrafeRight', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionStrafeRight();
			},
			
			set: function(v) {
				no.setActionStrafeRight(v);
			}
		});
		
		Object.defineProperty(this, 'actionRun', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionRun();
			},
			
			set: function(v) {
				no.setActionRun(v);
			}
		});
		
		Object.defineProperty(this, 'actionUp', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionUp();
			},
			
			set: function(v) {
				no.setActionUp(v);
			}
		});
		
		Object.defineProperty(this, 'actionDown', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getActionDown();
			},
			
			set: function(v) {
				no.setActionDown(v);
			}
		});
		
		Object.defineProperty(this, 'isRunning', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isRunning();
			}
		});
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.extra.FPSCamera')
	}
});

var Mesh = NH_.createClass({ parent: Object3D,
	construct: function(no) {
		no = no || new Mesh.NativeClass();
		
		Object3D.call(this, no);
		
		Object.defineProperty(this, 'geometry', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getGeometry();
			},
			
			set: function(v) {
				no.setGeometry(v);
			}
		});
		
		Object.defineProperty(this, 'material', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getMaterial();
			},
			
			set: function(v) {
				no.setMaterial(v);
			}
		});
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Mesh')
	}
});

var ArrayList = NH_.createClass({ parent: Array,
	construct: function(no) {
		no = no || new ArrayList.NativeClass();
		
		Array.call(this);
		
		Object.defineProperty(this, 'no', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no;
			}
		});
		
		this.pullFromSource();
	},
	
	body: {
		/**
		* Updates the native ArrayList with the values of this JS Array.
		*/
		pushToSource: function() {
			var nosize = this.no.size();
			var jlen = this.length;
			
			if(nosize > jlen) {
				for(var i = 0, l = (nosize - jlen); i < l; i++) {
					this.no.remove(nosize - l);
				}
				
				nosize = jlen;
			}
			
			for(var i = 0; i < jlen; i++) {
				if(i < nosize) this.no.set(i, this[i].no);
				else if(i >= nosize) this.no.add(this[i].no);
			}
		},
		
		/**
		* Updates this JS Array from the native ArrayList
		*/
		pullFromSource: function() {
			this.length = this.no.size();
			for(var i = 0, l = this.length; i < l; i++) {
				this[i] = getObjInst(this.no.get(i));
			}
		}
	},
	
	staticBody: {
		NativeClass: Java.type('java.util.ArrayList')
	}
});

var ShadowsProperties = NH_.createClass({
	construct: function(no) {
		no = no || new ShadowsProperties.NativeClass();
		
		Object.defineProperty(this, 'no', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no;
			}
		});
		
		Object.defineProperty(this, 'cascadeMaxDistance', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getCascadeMaxDistance();
			},
			
			set: function(v) {
				no.setCascadeMaxDistance(v);
			}
		});
		
		Object.defineProperty(this, 'interCascadeFade', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getInterCascadeFade();
			},
			
			set: function(v) {
				no.setInterCascadeFade(v);
			}
		});
		
		Object.defineProperty(this, 'cascadeDistanceFade', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getCascadeDistanceFade();
			},
			
			set: function(v) {
				no.setCascadeDistanceFade(v);
			}
		});
		
		Object.defineProperty(this, 'cascadeDistributionExp', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getInterDistributionExp();
			},
			
			set: function(v) {
				no.setInterDistributionExp(v);
			}
		});
		
		Object.defineProperty(this, 'cascadeProperties', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getCascadeProperties());
			},
			
			set: function(a) {
				var ps = getObjInst(no.getCascadeProperties());
				
				ps.length = 0;
				for(var i = 0, l = a.length; i < l; i++) {
					ps.push(a[i]);
				}
				ps.pushToSource();
			}
		});
	},
	
	staticBody: {
		MAX_CASCADE_COUNT: 6,
		NativeClass: Java.type('io.github.nasso.fury.graphics.ShadowsProperties')
	}
});

var ShadowProperties = NH_.createClass({
	construct: function(no) {
		no = no || new ShadowProperties.NativeClass();
		
		Object.defineProperty(this, 'no', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no;
			}
		});
		
		Object.defineProperty(this, 'type', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getType();
			},
			
			set: function(v) {
				no.setType(v);
			}
		});
		
		Object.defineProperty(this, 'shadowMapSize', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getShadowMapSize();
			},
			
			set: function(v) {
				no.setShadowMapSize(v);
			}
		});
		
		Object.defineProperty(this, 'poissonSamples', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPoissonSamples();
			},
			
			set: function(v) {
				no.setPoissonSamples(v);
			}
		});
		
		Object.defineProperty(this, 'poissonSpread', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getPoissonSpread();
			},
			
			set: function(v) {
				no.setPoissonSpread(v);
			}
		});
		
		Object.defineProperty(this, 'bias', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getBias();
			},
			
			set: function(v) {
				no.setBias(v);
			}
		});
		
		Object.defineProperty(this, 'normalCulling', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isNormalCulling();
			},
			
			set: function(v) {
				no.setNormalCulling(v);
			}
		});
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.graphics.ShadowProperties')
	}
});

var Light = NH_.createClass({ parent: Object3D,
	construct: function(no) {
		no = no || new Light.NativeClass();
		
		Object3D.call(this, no);
		
		Object.defineProperty(this, 'color', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getColor();
			},
			
			set: function(v) {
				no.setColor(v);
			}
		});
		
		Object.defineProperty(this, 'groundColor', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getGroundColor();
			},
			
			set: function(v) {
				no.setGroundColor(v);
			}
		});
		
		Object.defineProperty(this, 'shadowsProps', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getShadowsProps());
			},
			
			set: function(v) {
				no.setShadowsProps(v.no);
			}
		});
		
		Object.defineProperty(this, 'energy', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getEnergy();
			},
			
			set: function(v) {
				no.setEnergy(v);
			}
		});
		
		Object.defineProperty(this, 'distance', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getDistance();
			},
			
			set: function(v) {
				no.setDistance(v);
			}
		});
		
		Object.defineProperty(this, 'coneSize', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getConeSize();
			},
			
			set: function(v) {
				no.setConeSize(v);
			}
		});
		
		Object.defineProperty(this, 'coneBlend', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getConeBlend();
			},
			
			set: function(v) {
				no.setConeBlend(v);
			}
		});
		
		Object.defineProperty(this, 'shadowZNearSun', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getShadowZNearSun();
			},
			
			set: function(v) {
				no.setShadowZNearSun(v);
			}
		});
		
		Object.defineProperty(this, 'shadowZFarSun', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getShadowZFarSun();
			},
			
			set: function(v) {
				no.setShadowZFarSun(v);
			}
		});
		
		Object.defineProperty(this, 'shadowZNear', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getShadowZNear();
			},
			
			set: function(v) {
				no.setShadowZNear(v);
			}
		});
		
		Object.defineProperty(this, 'shadowZFar', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getShadowZFar();
			},
			
			set: function(v) {
				no.setShadowZFar(v);
			}
		});
		
		Object.defineProperty(this, 'sunInnerEdge', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getSunInnerEdge();
			},
			
			set: function(v) {
				no.setSunInnerEdge(v);
			}
		});
		
		Object.defineProperty(this, 'sunOuterEdge', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getSunOuterEdge();
			},
			
			set: function(v) {
				no.setSunOuterEdge(v);
			}
		});
		
		Object.defineProperty(this, 'sunBrightness', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getSunBrightness();
			},
			
			set: function(v) {
				no.setSunBrightness(v);
			}
		});
		
		Object.defineProperty(this, 'blinnPhong', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isBlinnPhong();
			},
			
			set: function(v) {
				no.setBlinnPhong(v);
			}
		});
		
		Object.defineProperty(this, 'specular', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isSpecular();
			},
			
			set: function(v) {
				no.setSpecular(v);
			}
		});
		
		Object.defineProperty(this, 'diffuse', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isDiffuse();
			},
			
			set: function(v) {
				no.setDiffuse(v);
			}
		});
		
		Object.defineProperty(this, 'enabled', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isEnabled();
			},
			
			set: function(v) {
				no.setEnabled(v);
			}
		});
		
		Object.defineProperty(this, 'type', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getType();
			},
			
			set: function(v) {
				no.setType(v);
			}
		});
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Light')
	}
});

var Fog = NH_.createClass({
	construct: function(no) {
		no = no || new Fog.NativeClass();
		
		Object.defineProperty(this, 'no', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no;
			}
		});
		
		Object.defineProperty(this, 'type', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getType();
			},
			
			set: function(v) {
				no.setType(v);
			}
		});
		
		Object.defineProperty(this, 'name', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getName();
			},
			
			set: function(v) {
				no.setName(v);
			}
		});
		
		Object.defineProperty(this, 'color', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getColor();
			},
			
			set: function(v) {
				no.setColor(v);
			}
		});
		
		Object.defineProperty(this, 'near', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getNear();
			},
			
			set: function(v) {
				no.setNear(v);
			}
		});
		
		Object.defineProperty(this, 'far', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getFar();
			},
			
			set: function(v) {
				no.setFar(v);
			}
		});
		
		Object.defineProperty(this, 'density', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getDensity();
			},
			
			set: function(v) {
				no.setDensity(v);
			}
		});
	},
	
	body: {
		clone: function() {
			return getObjInst(this.no.clone());
		},
		
		setNearFar: function(n, f) {
			this.near = n;
			this.far = f;
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Fog')
	}
});

var Sky = NH_.createClass({
	construct: function(no) {
		no = no || new Sky.NativeClass();
		
		Object.defineProperty(this, 'no', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no;
			}
		});
		
		Object.defineProperty(this, 'type', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getType();
			},
			
			set: function(v) {
				no.setType(v);
			}
		});
		
		Object.defineProperty(this, 'cubeTexture', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getCubeTexture());
			},
			
			set: function(v) {
				no.setCubeTexture(v.no);
			}
		});
		
		Object.defineProperty(this, 'suns', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getSuns());
			},
			
			set: function(a) {
				var ps = getObjInst(no.getSuns());
				
				ps.length = 0;
				for(var i = 0, l = a.length; i < l; i++) {
					ps.push(a[i]);
				}
				ps.pushToSource();
			}
		});
		
		Object.defineProperty(this, 'up', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getUp();
			},
			
			set: function(v) {
				no.setUp(v);
			}
		});
		
		Object.defineProperty(this, 'bottomColor', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getBottomColor();
			},
			
			set: function(v) {
				no.setBottomColor(v);
			}
		});
		
		Object.defineProperty(this, 'topColor', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getTopColor();
			},
			
			set: function(v) {
				no.setTopColor(v);
			}
		});
		
		Object.defineProperty(this, 'cosPower', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getCosPower();
			},
			
			set: function(v) {
				no.setCosPower(v);
			}
		});
	},
	
	body: {
		addSun: function(s) {
			this.no.addSun(s.no);
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Sky')
	}
});

var Scene = NH_.createClass({ parent: Object3D,
	construct: function(no) {
		no = no || new Scene.NativeClass();
		
		Object3D.call(this, no);
		
		Object.defineProperty(this, 'skyboxDistance', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getSkyboxDistance();
			},
			
			set: function(v) {
				no.setSkyboxDistance(v);
			}
		});
		
		Object.defineProperty(this, 'autoUpdate', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.getAutoUpdate();
			},
			
			set: function(v) {
				no.setAutoUpdate(v);
			}
		});
		
		Object.defineProperty(this, 'overrideMaterial', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getOverrideMaterial());
			},
			
			set: function(v) {
				no.setOverrideMaterial(v.no);
			}
		});
		
		Object.defineProperty(this, 'fog', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getFog());
			},
			
			set: function(v) {
				no.setFog(v.no);
			}
		});
		
		Object.defineProperty(this, 'sky', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getSky());
			},
			
			set: function(v) {
				no.setSky(v.no);
			}
		});
		
		Object.defineProperty(this, 'lighting', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return no.isLighting();
			},
			
			set: function(v) {
				no.setLighting(v);
			}
		});
	},
	
	body: {
		dispose: function() {
			this.no.dispose();
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Scene')
	}
});

var PerspectiveCameraHelper = NH_.createClass({ parent: Object3D,
	construct: function(no) {
		no = no || new PerspectiveCameraHelper.NativeClass();
		
		Object3D.call(this, no);
		
		Object.defineProperty(this, 'upTriangle', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getUpTriangle());
			}
		});
		
		Object.defineProperty(this, 'cameraBody', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return getObjInst(no.getCameraBody());
			}
		});
	},
	
	body: {
		update: function(v) {
			this.no.update(v.no);
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.extra.PerspectiveCameraHelper')
	}
});

// Instance cache
var nobjPairs = {
	pairs: {
		'io.github.nasso.fury.core.Game': Game,
		'io.github.nasso.fury.core.GameWindow': GameWindow,
		
		'io.github.nasso.fury.level.Camera': Camera,
		'io.github.nasso.fury.level.Light': Light,
		'io.github.nasso.fury.level.Mesh': Mesh,
		'io.github.nasso.fury.level.NFClippedCamera': NFClippedCamera,
		'io.github.nasso.fury.level.Object3D': Object3D,
		'io.github.nasso.fury.extra.OrthographicCamera': OrthographicCamera,
		'io.github.nasso.fury.extra.PerspectiveCamera': PerspectiveCamera,
		'io.github.nasso.fury.level.Scene': Scene,
		
		'io.github.nasso.fury.level.Fog': Fog,
		'io.github.nasso.fury.level.Sky': Sky,
		
		'io.github.nasso.fury.extra.FPSCamera': FPSCamera,
		'io.github.nasso.fury.extra.PerspectiveCameraHelper': PerspectiveCameraHelper,
		
		'io.github.nasso.fury.graphics.ShadowsProperties': ShadowsProperties,
		'io.github.nasso.fury.graphics.ShadowProperties': ShadowProperties,
		
		'java.util.ArrayList': ArrayList
	}
};

nobjPairs.getPair = function(n) {
	return nobjPairs.pairs[n.getClass().getCanonicalName()];
};

var instancedObjects = {};
function getObjInst(n) {
	var instanced = instancedObjects[n.hashCode()];
	
	if(instanced) {
		return instanced;
	}
	
	var pair = nobjPairs.getPair(n);
	
	if(!pair) {
		console.err('Cannot find a JavaScript pair class for: ' + n.getClass().getCanonicalName());
		return null;
	}
	
	return (instancedObjects[n.hashCode()] = new pair(n));
}
/////////////////

var Level = NH_.createClass({ parent: Observable,
	construct: function(no) {
		no = no || new Level.NativeClass();
		
		Observable.call(this, no);
		
		Object.defineProperty(this, 'delta', {
			enumerable: false,
			configurable: false,
			
			get: function() {
				return this.no.getDelta();
			},
			
			set: function(v) {
				this.no.setDelta(v);
			}
		});
	},
	
	body: {
		getObjectById: function(id) {
			return getObjInst(this.no.getObjectById(id));
		}
	},
	
	staticBody: {
		NativeClass: Java.type('io.github.nasso.fury.level.Level')
	}
});
