<?xml version="1.0" encoding="UTF-8"?>
<level>
	<scripts>
		<script  type="text/javascript" src="res/scripts/test.min.js"></script>
	</scripts>
	
	<data>
		<geometry type="extra/BoxBufferGeometry" id="terrainGeom">
			<vector name="size">100 100 1</vector>
		</geometry>
		
		<geometry type="extra/SphereBufferGeometry" id="sphereGeom">
			<float name="radius">0.5</float>
			
			<int name="segments">24</int>
		</geometry>
		
		<texture type="data/2d" src="res/textures/199.JPG" id="basicTextureD">
			<string name="internalFormat">SRGB_ALPHA</string>
			<float name="anisotropy">16</float>
		</texture>
		
		<texture type="data/2d" src="res/textures/199_norm.JPG" id="basicTextureN">
			<string name="internalFormat">RGBA</string>
			<float name="anisotropy">16</float>
		</texture>
		
		<material id="terrainMat">
			<diffuse texture="basicTextureD" />
			<normal intensity="1.0" texture="basicTextureN" />
			<specular intensity="1.5" shininess="64" />
			
			<float name="uvScaling">200</float>
		</material>
		
		<material id="testMatA">
			<diffuse color="#f00" />
			<specular intensity="1.0" shininess="1024" />
		</material>
	</data>
	
	<scenes>
		<scene id="sce">
			<sky type="PROCEDURAL">
				<sun target="sunLight" />
				
				<float name="cosPower">1.0</float>
				
				<vector name="up">0 0 1</vector>
				<vector name="bottomColor">1 1 1</vector>
				<vector name="topColor">0.35 0.55 1</vector>
			</sky>
			
			<children>
				<object type="mesh" id="terrainMesh">
					<geometry target="terrainGeom" />
					<material target="terrainMat" />
					
					<vector name="position">0 0 -0.5</vector>
				</object>
				
				<object type="mesh" id="sphereA">
					<geometry target="sphereGeom" />
					<material target="testMatA" />
					
					<vector name="position">5 0 1</vector>
				</object>
				
				<object type="light/hemisphere" id="hemiLight">
					<float name="energy">0.25</float>
					
					<vector name="color">0.55 0.75 1</vector>
					<vector name="groundColor">1 1 0.75</vector>
				</object>
				
				<object type="light/sun" id="sunLight">
					<vector name="color">1 1 0.75</vector>
					<vector name="rotation">-45 0 0</vector>
					
					<float name="sunInnerEdge">0.53623</float>
					<float name="sunOuterEdge">3</float>
					<float name="sunBrightness">4</float>
					
					<bool name="castShadow">true</bool>
					
					<shadows>
						<cascade type="ROTATED_POISSON_SAMPLED">
							<int name="shadowMapSize">2048</int>
							<int name="poissonSamples">16</int>
							
							<float name="poissonSpread">650</float>
							<float name="bias">0.0016</float>
							
							<bool name="normalCulling">true</bool>
						</cascade>
						
						<cascade type="POISSON_SAMPLED">
							<int name="shadowMapSize">2048</int>
							<int name="poissonSamples">16</int>
							
							<float name="poissonSpread">900</float>
							<float name="bias">0.0032</float>
							
							<bool name="normalCulling">true</bool>
						</cascade>
						
						<cascade type="SIMPLE">
							<int name="shadowMapSize">2048</int>
							<int name="poissonSamples">16</int>
							
							<float name="poissonSpread">1200</float>
							<float name="bias">0.0048</float>
							
							<bool name="normalCulling">true</bool>
						</cascade>
						
						<cascade type="SIMPLE">
							<int name="shadowMapSize">2048</int>
							<int name="poissonSamples">16</int>
							
							<float name="poissonSpread">1200</float>
							<float name="bias">0.0064</float>
							
							<bool name="normalCulling">true</bool>
						</cascade>
						
						<float name="cascadeMaxDistance">200</float>
						<float name="interCascadeFade">2</float>
						<float name="cascadeDistanceFade">30</float>
						<float name="cascadeDistributionExp">2.2</float>
						
					</shadows>
				</object>
				
				<object type="extra/FPSCamera" id="cam">
					<vector name="position">0 0 1.8</vector>
					
					<float name="fov">70</float>
					<float name="ratio">1.777777</float>
					<float name="near">0.01</float>
					<float name="far">10000</float>
					
					<bool name="freeLookFly">true</bool>
					
					<children>
						<object type="object/import" src="res/models/AK74u/AK74u.dae" id="weapon">
							<vector name="position">0.2 -0.4 -0.4</vector>
							<vector name="rotation">-90 0 0</vector>
							<vector name="scale">0.5 0.5 0.5</vector>
						</object>
					</children>
				</object>
			</children>
		</scene>
	</scenes>
	
	<render>
		<!--
		
		<target type="2d" id="foo" width="1920" height="1080" />
		
		-->
		
		<pass scene="sce" camera="cam">
			<postfx type="fx/Bloom">
				<float name="threshold">1.15</float>
				<float name="smoothness">0.25</float>
				<float name="strength">1.0</float>
				<float name="size">2.0</float>
				<int name="downsample">4</int>
			</postfx>
			
			<postfx type="fx/Gamma">
				<float name="gamma">2.2</float>
			</postfx>
			
			<postfx type="fx/Vignette">
				<vector name="color">0 0 0</vector>
				<float name="opacity">0.5</float>
				<float name="distance">1.4</float>
				<float name="smoothness">0.7</float>
			</postfx>
			
			<postfx type="fx/FXAA">
				<float name="reduceMin">0.0078125</float>
				<float name="reduceMul">0.125</float>
				<float name="spanMax">8</float>
			</postfx>
		</pass>
	</render>
</level>
