level.addEventListener('load', function() {
	var sce = level.getObjectById("sce");
	var cam = sce.getChildById("cam");
	var sun = sce.getChildById("sunLight");
	
	var weapon = sce.getChildById("weapon");
	var weaponParts = weapon.children;
	for(var i = 0; i < weaponParts.length; i++) {
		var m = weaponParts[i].children[0];
		m.material.shininess = 64;
		m.material.specularIntensity = 0.2;
	}
	
	var weaponNormalPos = new Vector3(0.1, -0.2, -0.2);
	var weaponAimingPos = new Vector3(0.0095, -0.14, -0.05);
	
	var aimingAmount = 0;
	
	weapon.position.set(weaponNormalPos);
	
	// game.window.maximize();
	
	level.addEventListener('update', function() {
		if(game.window.isPressed(Fury.KEY_ESCAPE)) {
			if(game.window.cursorMode === Fury.CURSOR_NORMAL) {
				game.quit();
			} else {
				game.window.cursorMode = Fury.CURSOR_NORMAL;
			}
		}
		
		if(game.window.isPressed(Fury.MOUSE_BUTTON_LEFT)) {
			if(game.window.cursorMode !== Fury.CURSOR_DISABLED) {
				game.window.cursorMode = Fury.CURSOR_DISABLED;
			} else {
				// TODO: Fire
			}
		}
		
		if(game.window.isPressed(Fury.KEY_F9)) {
			console.log((game.logFPS = !game.logFPS) ? "Enabled FPS logging" : "Disabled FPS logging");
		}
		
		if(game.window.isPressed(Fury.KEY_F10)) {
			console.log(sce);
		}
		
		if(game.window.isPressed(Fury.KEY_F11)) {
			game.window.toggleFullscreen();
			
			// OR
			// game.window.fullscreen = !game.window.fullscreen;
		}
		
		if(game.window.isPressed(Fury.KEY_F12)) {
			var nameTried = 0;
			
			do {
				name = "screenshots/screenshot-"+nameTried+".png";
				nameTried++;
			} while(Files.exists(name));
			
			TextureIO.writeTexture(name, "png", game.takeScreenshot());
		}
		
		if(game.window.isButtonDown(Fury.MOUSE_BUTTON_RIGHT)) {
			if(aimingAmount < 1.0) {
				aimingAmount = FuryMaths.clamp(aimingAmount + 10 * level.delta, 0.0, 1.0);
				
				weapon.position.x = FuryMaths.lerp(weaponNormalPos.x, weaponAimingPos.x, aimingAmount);
				weapon.position.y = FuryMaths.lerp(weaponNormalPos.y, weaponAimingPos.y, aimingAmount);
				weapon.position.z = FuryMaths.lerp(weaponNormalPos.z, weaponAimingPos.z, aimingAmount);
			}
		} else if(aimingAmount > 0.0) {
			aimingAmount = FuryMaths.clamp(aimingAmount - 10 * level.delta, 0.0, 1.0);
			
			weapon.position.x = FuryMaths.lerp(weaponNormalPos.x, weaponAimingPos.x, aimingAmount);
			weapon.position.y = FuryMaths.lerp(weaponNormalPos.y, weaponAimingPos.y, aimingAmount);
			weapon.position.z = FuryMaths.lerp(weaponNormalPos.z, weaponAimingPos.z, aimingAmount);
		}
		
		cam.blockTick = game.window.cursorMode !== Fury.CURSOR_DISABLED;
		cam.speed = FuryMaths.clamp(cam.speed + game.window.scrollRelY, 0.1, 400);
		cam.ratio = game.window.frameWidth / game.window.frameHeight;
		
		sun.rotate(0, 0, level.delta * 10);
	});
});
